<?php

namespace App\Http\Controllers;

use Omnipay\Omnipay;
use Illuminate\Http\Request;

class BazarController extends Controller
{

    public $gateway;
    public $payment_id;

    public function __construct()
    {
        $this->gateway = Omnipay::create('PayPal_Rest');
        $this->gateway->setClientId("AeJYkCDNdRweVkoVylKLemKbT61eOV6QOEukxhK6M0bqqeIuKJqPKiglCEigyagffDIJUK92mxYQrzZD");
        $this->gateway->setSecret('EJye1tVcaEr8c4mQAijmHRswxgErBZqCxZCanQOfgmlxtaxSaTghkHBLlLiQEnuJkPBlSAHv30hhABHh');
        // $this->gateway->setClientId("ASApP-OLqzD8ErSsPqhgLNKS5eKKi0dAL6K6NqfHXsavd6yNou_blwGQ5RJmBByXBxqeQrUb2X-A_2ea");
        // $this->gateway->setSecret('EAQGCvAjyU-Bov4q7gzOLivxku6FWZNd7c2TZtUFb2FCkNzKSl6Yu8GiL9RG2R1hQSV1hQsSlbjsMlmF');
        // $this->gateway->setClientId(env('PAYPAL_SANDBOX_CLIENT_ID'));
        // $this->gateway->setSecret(env('PAYPAL_SANDBOX_CLIENT_SECRET'));
        $this->gateway->setTestMode(false); //set it to 'false' when go live

        // $this->gateway = Omnipay::create('PayPal_Rest');
        // $this->gateway->setClientId('AVxP-jU0lumxBdqgWJGwc-GydC3vPpRx2X3SrYY_j_b7gsSRy0DCflw6a7w7YA-7YTgxJMpJqAx68H5R');
        // $this->gateway->setSecret('EP0YEH1RwnV1qIw8Vgn21K9vaSEIk7gh1Ybp54kayQwUygeJQoA4-SqWCGIIm3f4B9mrNHVr0ZH1Q28v');
        // $this->gateway->setTestMode(true); //set it to 'false' when go live

    }


    public function paypal(Request $request)
    {

        // dd($request->all());

        try {

            $response = $this->gateway->purchase(array(
                'amount' =>  $request->amount ,
                'currency' => 'EUR' ,
                'returnUrl' => url('/'). '/payment-success' ,
                // 'returnUrl' => ' /paymentsuccess?user_id=111&order_id=999',
                'cancelUrl' => url('/') . '/dashboard/my-wallet',
            ))->send();
            // $response = $this->gateway->purchase(array(
            //     'amount' =>  $request->amount ,
            //     'currency' => $request->currency ?? 'EUR' ,
            //     'returnUrl' => url('/'). '/payment-success' ,
            //     // 'returnUrl' => ' /paymentsuccess?user_id=111&order_id=999',
            //     'cancelUrl' => url('/') . '/dashboard/my-wallet',
            // ))->send();

            if ($response->isSuccessful()) {
            // if ($response->isRedirect()) {

                // dd($response , 'Rediirect');
                $response->redirect(); // this will automatically forward the customer
            } else {

                // not successful
                // dd($response);
                // dd('NoT SUCCESS');
                return redirect('/dashboard/my-wallet')->with('error' , $response->getMessage());
                // return $response->getMessage();
            }
        } catch(Exception $e) {

            // dd($e->getMessage());
            return $e->getMessage();
        }

        // dd( 'S' , $response);
    }

    public function molliePayment(Request $request){


        // $payment = Mollie::api()->payments->create([
        //     "amount" => [
        //         "currency" => "EUR",
        //         "value" => "10.00" // You must send the correct number of decimals, thus we enforce the use of strings
        //     ],
        //     "description" => "Order #12345",
        //     "redirectUrl" => url('/'). '/payment-success',
        //     "webhookUrl" => 'http://site.openbazar.nl//payment-success',
        // ]);

        // dd($payment);

        $mollie = new \Mollie\Api\MollieApiClient();
        // $mollie->setApiKey("test_WuftgGxMUCmyVKaJ2zpsGEW4PpgpzR");
        // $mollie->setApiKey("live_t8JRHyUSbw6dFW6m3Ubrf8VrEFjyzV"); //openbazar
        $mollie->setApiKey("test_9E4wsUvbCJD6AHjC37NyeKWjayqVVf"); //openbazar
        $method = $mollie->methods->get(\Mollie\Api\Types\PaymentMethod::IDEAL, ["include" => "issuers"]);

        // $payment = $mollie->orders->create([
        $payment = $mollie->payments->create([
            "amount" => [
                "currency" => $request->currency ?? 'EUR',
                "value" => $request->amount
            ],
            // "method" => "creditcard",
            // "method"      => \Mollie\Api\Types\PaymentMethod::IDEAL,
            "method" => "paypal",
            "description" => "My first API payment",
            "redirectUrl" => url('/'). '/payment-success',
            "webhookUrl"  => "https://webshop.example.org/mollie-webhook/",
        ]);

        $this->payment_id = $payment->id;
        session()->put('payment_id' , $payment->id );
        // dd(session('payment_id'));
        // dd($this->payment_id);
        // $payment = $mollie->payments->get($payment->id);

        return redirect($payment->getCheckoutUrl());
    }


    public function paymentSuccess(Request $req)
    {
        // dd($req->all());
        $response = $req->all();
        return redirect('/payment-method')->with('paymentResponse' , $response );
    }



    public function molliePaymentSuccess(Request $req)
    {
        // dd($this->payment_id);
        $mollie = new \Mollie\Api\MollieApiClient();
        $mollie->setApiKey("test_9E4wsUvbCJD6AHjC37NyeKWjayqVVf"); //openbazar
        $payment = $mollie->payments->get(session('payment_id'));


        $response = new \stdClass();
        $response->payment = $payment;
        $response->status = $payment->status;

        if ($payment->isPaid())
        {
            $response->isPaid = true;
            // echo "Payment received.";
        }

        // dd($payment , $payment->isPaid() , $payment->status  , $response);

        return redirect('/payment-method')->with('paymentResponse' , $response );
    }



    public function acceptInvitation(Request $request){
        // dd($request->all());

        $referred = $request->all();

        session()->put('referred' , $referred);

        return redirect('/')->with('success' , 'Please signup to claim coins');
    }
    public function vue()
    {
        return view('layouts.vue-master');
    }

    public function index()
    {
        return view('bazar.home');
    }

    public function products()
    {
        return view('bazar.products');
    }
    public function categoryProducts()
    {
        return view('bazar.category-products');
    }

    public function viewProduct()
    {
        return view('bazar.view-product');
    }

    public function postAd()
    {
        return view('bazar.post-ad');
    }

    public function editAd()
    {
        return view('bazar.edit-ad');
    }






    public function dashboard()
    {
        return view('bazar.user.dashboard');
    }

    public function followers()
    {
        return view('bazar.dashboard.followers');
    }

    public function followings()
    {
        return view('bazar.dashboard.followings');
    }


    public function profile()
    {
        return view('bazar.dashboard.profile');
    }





    public function myAccount()
    {
        return view('bazar.dashboard.my-account');
    }

    public function myCoins()
    {
        return view('bazar.dashboard.my-coins');
    }

    public function myWallet()
    {
        return view('bazar.dashboard.my-wallet');
    }
    public function myAds()
    {
        return view('bazar.dashboard.ads');
    }

    public function inviteFreinds()
    {
        return view('bazar.dashboard.invite-freinds');
    }


    public function otherAds()
    {
        return view('bazar.dashboard.other-ads');
    }


    public function blockedUsers()
    {
        return view('bazar.dashboard.blocked-users');
    }












    public function chat()
    {
        return view('bazar.chat');
    }

    public function help()
    {
        return view('bazar.help');
    }


    public function notification()
    {
        return view('bazar.notification');
    }

    public function walletFAQs()
    {
        return view('bazar.wallet-faqs');
    }

    public function coinsFAQs()
    {
        return view('bazar.coins-faqs');
    }


    public function getApp()
    {
        return view('bazar.get-app');
    }

    public function paymentMethod()
    {
        return view('bazar.payment');
    }











    public function userAds()
    {
        return view('bazar.user.ads');
    }

    public function userProfile()
    {
        return view('bazar.user.followers');
    }
    public function userFollowers()
    {
        return view('bazar.user.followers');
    }

    public function userFollowings()
    {

        return view('bazar.user.followings');
    }






    public function privacyPolicy()
    {
        return view('bazar.privacy-policy');
    }

    public function contactUs()
    {
        return view('bazar.contact-us');
    }

    public function querySuccess()
    {
        return view('bazar.query-success');
    }

    public function termsOfUse()
    {
        return view('bazar.terms-of-use');
    }












}
