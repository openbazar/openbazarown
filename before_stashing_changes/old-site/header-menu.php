

<nav class="navbar navbar-default navbar-fixed-top top-nav-collapse" role="navigation">
	<div class="container">
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle mobile-nav" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="color: #57748C;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
		
			<a class="navbar-brand page-scroll" href="index.php"><img src="imgs/logo.png" class="logo" alt=""/></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#home" class="page-scroll">Home</a>
				</li>
				<li>
					<a href="#about" class="page-scroll">About App</a>
				</li>
				<li>
					<a href="#features" class="page-scroll">Features</a>
				</li>
				<li>
					<a href="#download" class="page-scroll">Get The App</a>
				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>	