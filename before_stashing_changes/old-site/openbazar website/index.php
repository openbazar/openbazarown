<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Openbazar App</title>

    <?php include_once("header-top.php"); ?>
</head>


<body>
    <?php include_once("header-menu.php"); ?>
    
    <section class="hero-section tablediv" id="home">
		<div class="intablediv">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1 class="hero-text">Online classifieds platform in Europe that brings millions of users together </h1>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-5">
								<a href="#x" target="_blank"><img src="imgs/store_apple.png" class="img-responsive center-block" alt=""></a>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-5">
								<a href="#x" target="_blank"><img src="imgs/store_google.png" class="img-responsive center-block" alt=""></a>
							</div>
						</div>
					</div>
					<div class="col-md-6 center-block">
						<div class="flex-container center-block">
						  <div class="flexslider">
							<ul class="slides">
							  <li> <img src="imgs/slide1.png" alt="" /> </li>
							  <li> <img src="imgs/slide2.png" alt=""  /> </li>
							  <li> <img src="imgs/slide3.jpg" alt=""  /> </li>
							</ul>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<!--
	<section class="content-section text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="red-text">About OpenBazar</h2>
					<p>Open Bazar is an online classifieds and advertising service platform in Europe that brings millions of users together without central institutions or intermediaries, to exchange their goods, products or find anything in their community. The platform enables merchants -to-user and user-to-user benefit in combination of credits and Open Bazar’s Coins through open bazar wallet and coins system. Users and merchants can browse through a large variety of Goods, Services, Real Estate, Rentals, Cars, Electronics, Fashion, Jobs and more other items.</p>
					<div class="row">
						<div class="col-sm-6">
							<h2 class="red-text">Our Mission</h2>
							<p>Open Bazar aims to empower merchants and users who want to take part in shaping the future of their finance by using our platform to buy, sell, rent, lend, offer service, open shop or find anything in their community. A community where underused goods are redistributed to fill a new need, and become wanted again, where non- product assets such as space, skills, money and coins are exchanged and traded in new ways of our Wallet and credits system that don’t always require centralized institutions or ‘middlemen’.</p>
						</div>
						<div class="col-sm-6">
							<h2 class="red-text">Our Vision</h2>
							<p>Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
-->
	
	<section class="content-section text-center grey-background" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="card-box">
						<h2 class="red-text">OpenBazar</h2>
						<hr>
						<p>Open Bazar is an online classifieds and advertising service platform in Europe that brings millions of users together without central institutions or intermediaries, to exchange their goods, products or find anything in their community. The platform enables merchants -to-user and user-to-user benefit in combination of credits and Open Bazar’s Coins through open bazar wallet and coins system. Users and merchants can browse through a large variety of Goods, Services, Real Estate, Rentals, Cars, Electronics, Fashion, Jobs and more other items.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card-box">
						<h2 class="red-text">Mission</h2>
						<hr>
						<p>Open Bazar aims to empower merchants and users who want to take part in shaping the future of their finance by using our platform to buy, sell, rent, lend, offer service, open shop or find anything in their community. A community where underused goods are redistributed to fill a new need, and become wanted again, where non- product assets such as space, skills, money and coins are exchanged and traded in new ways of our well designed Wallet and credits system that don’t always require centralized institutions or ‘middlemen’.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card-box">
						<h2 class="red-text">Vision</h2>
						<hr>
						<p>Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section" id="features">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<h2 class="red-text">Features</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque a dolor nec massa dapibus porttitor quis nec diam.</p>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-sm-4 text-right-sp pad-top-100">
					<h3 class="red-text">Feature Name</h3>
					<p>Always wanted to Shop everything in one place, at one time? Good news, We have got just the right app for you! </p>
					<br>
					<h3 class="red-text">Feature Name</h3>
					<p>Publish your ads or browse our hundreds of categories, from vehicles and apartments, real estate, bicycles, electronics, appliances, furniture and more, all from your mobile phone or tablet!  </p>
					<br>
					<h3 class="red-text">Feature Name</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget felis laoreet augue pretium ornare vel ut nisl.</p>
				</div>
				<div class="col-sm-4 center-block text-center">
					<div class="flex-container center-block text-center">
					  <div class="flexslider center-block text-center">
						<ul class="slides">
						  <li> <img src="imgs/slide1.png" alt="" /> </li>
						  <li> <img src="imgs/slide2.png" alt=""  /> </li>
						  <li> <img src="imgs/slide3.jpg" alt=""  /> </li>
						</ul>
					  </div>
					</div>
				</div>
				<div class="col-sm-4 pad-top-100">
					<h3 class="red-text">Feature Name</h3>
					<p>With multiple variations of brands and products at one platform, OpenBazar is what you’d want to have on your mobile phone. </p>
					<br>
					<h3 class="red-text">Feature Name</h3>
					<p>Our app currently supports two different languages (English and Dutch</p>
					<br>
					<h3 class="red-text">Earn for Liking & Posting Ads</h3>
					<p>We have an exclusive earning opportunity for our users get get. paid for liking and posting ads on OpenBazar</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="credit-packages text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h2>Credit Packages</h2>
					<p>Easier was not before! Now you can make one-time payment with the wallet and buy credit  then use it to feature your Ad, repost and buy a package or even upgrade your membership</p>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_standard.png" class="center-block img-responsive" alt="">
					<p>Add credit to your wallet by making one-time payment to buy our various offerings or upgrade your package of membership. </p>
				</div>
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_advanced.png" class="center-block img-responsive" alt="">
					<p>The easiest and fastest way to repost Ad, Feature Ad or Rocket Ad to get more views, no need to use credit card or transfer money just use your credits from your Wallet and save your time.</p>
				</div>
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_premium.png" class="center-block img-responsive" alt="">
					<p>We Offer you the smartest and safest way to make a benefit from our large variety and help you to shape your financial future</p>
				</div>
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_supreme.png" class="center-block img-responsive" alt="">
					<p>Start your own business and sell your goods or open your shop and create online a solid network and get more buyers without central institutions or intermediaries</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section text-center more-features">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="red-text">Earn more and faster by using our various offerings now! Get more offers and views, and sell or rent multiple products at one time.</h2>
				</div>
			</div>
			<br><br><br>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 border-bottom border-right">
					<img src="imgs/ic_wallet.png" class="center-block img-responsive" alt="">
					<h3>Wallet</h3>
					<p>Open Bazar Wallet is a secure encryption online tool which ensure that your credits always safe and it helps you collect rewards, store credits and buy various offerings and redeem Coins multiple times as and when required</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-bottom border-right">
					<img src="imgs/ic_coins.png" class="center-block img-responsive" alt="">
					<h3>Coins</h3>
					<p>Open Bazar Coins is an Affiliate system which allows you to collect Coins by inviting friends, posting Ads or like Ads and it helps you collect rewards, store credits and get various offerings and redeem Coins multiple times as and when required</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-bottom">
					<img src="imgs/ic_membership.png" class="center-block img-responsive" alt="">
					<h3>Membership</h3>
					<p>We offer 4 various of membership, Bronze to get more active Ads, Silver to get more Views and likes, Gold to shape your financial future and Diamond to create a solid network</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-right">
					<img src="imgs/ic_repost.png" class="center-block img-responsive" alt="">
					<h3>Repost Ad</h3>
					<p>Want to renew your ad's date of publication? Upload your ad to update and restore it at the top of the list and get up to 10 more views </p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-right">
					<img src="imgs/ic_features.png" class="center-block img-responsive" alt="">
					<h3>Features Ad</h3>
					<p>Get your Ad in the top section of the list with special badge and get up to 10 times more views</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<img src="imgs/ic_rocket.png" class="center-block img-responsive" alt="">
					<h3>Rocket</h3>
					<p>Get your Ad an Urgent badge and get up to 30 times more views to sell easier and faster</p>
				</div>
			</div>
		</div>
	</section>
    
    <?php include_once("footer-credits.php"); ?>
    <?php include_once("footer-assets.php"); ?>

</body>
</html>
