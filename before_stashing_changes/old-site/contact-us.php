
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Openbazar App</title>

    <?php include_once("header-top.php");
        ?>
    <style>
        .open_text{
            text-align: left !important;
        }
        .hero-section{
            padding: 5px 0 !important;
            height: 0% !important;
        }
        .hero-text {
            padding-top: 80px !important;
        }
        .send_query{background-color:#516A7C; color: white;}
        .send_query:hover { color: white;}
        .footer-section{background-color: #f8f8f8 !important;}
    </style>
    <style>
			@media (max-width:576px) {
					.logo {
						height: 35px;
						margin-top: -7px;
						position: absolute;
						left: 35%;
					}
				}

			</style>
</head>


<body>
    
    <nav class="navbar navbar-default navbar-fixed-top top-nav-collapse" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle mobile-nav" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="color: #57748C;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            
                <a class="navbar-brand page-scroll" href="index.php"><img src="imgs/logo.png" class="logo" alt=""/></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php#home" class="page-scroll">Home</a>
                    </li>
                    <li>
                        <a href="index.php#about" class="page-scroll">About App</a>
                    </li>
                    <li>
                        <a href="index.php#features" class="page-scroll">Features</a>
                    </li>
                    <li>
                        <a href="index.php#download" class="page-scroll">Get The App</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>	

    <section class="hero-section tablediv" id="home">
		<div class="intablediv">
			<div class="container">
				<div class="row">
					<!-- <div class="col-md-12"> -->
						<h1 class="hero-text text-center">Contact us </h1>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section text-center grey-background" id="about">
		<div class="container">
            <div class="row">
                <div class="section-title text-center">
                    <h2>Contact us</h2>
                    <br>
                    <p> Our knowledgeable and welcoming customer service team will be happy to help with any questions or queries.</p>
                </div>
                <br><br>
                <div class="section-body ">
                    <div class="col-md-4 text-left">
                        <h5>Contact us</h5>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;Email: &nbsp;&nbsp;openbazarnl@gmail.com <b></b> </p>
                        <br>
                        <h5>Available Hours: </h5>
                        <p>Monday- Saturday: 8am-9pm <br> Sunday: 10am-4pm</p>
                    </div>

                    <div class="col-md-8">
                        <form method="POST" action="connection.php">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="first_name" placeholder="*First Name" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="last_name" placeholder="*Last Name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="*Email" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" placeholder="*Phone" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control" name="type" placeholder="Select.." required>
                                            <option value="" selected disabled>Select ...</option>
                                            <option value="Suggestion">Suggestion</option>
                                            <option value="Complaint">Complaint</option>
                                            <option value="Feedback">Feedback</option>
                                            <option value="Other">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="col-md-12 form-control" name="message" placeholder="Enter message" rows="4" cols="50"></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <br>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="submit" name="send_query"  class="btn btn-rounded text-uppercase send_query">Submit <i class="fa fa-angle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		</div>
	</section>
	
    
    <!-- <section class="footer-section" id="download">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="download-box">
                        <h2 class="red-text">Download the app</h2>
                        <p>Whatever you’ve got in mind, we’ve got inside. OpenBazar App is what you’d want to have on your mobile phone.</p>
                        <br>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-6">
                                <a href="#x" target="_blank"><img src="imgs/store_apple.png" class="img-responsive center-block" alt=""></a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-6">
                                <a href="https://play.google.com/store/apps/details?id=com.open_bazar" target="_blank"><img src="imgs/store_google.png" class="img-responsive center-block" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="row">
                <p class="white-text">Follow Us</p>
                <ul>
                    <li><a href="https://www.facebook.com/OpenBAZAR4u/"><i class="zmdi zmdi-facebook zmdi-hc-lg"></i></a></li>
                    <li><a href="https://mobile.twitter.com/BazarOpen"><i class="zmdi zmdi-twitter zmdi-hc-lg"></i></a></li>
                    <li><a href="#x"><i class="zmdi zmdi-youtube-play zmdi-hc-lg"></i></a></li>
                    <li><a href="https://www.instagram.com/openbazar4u/?hl=nl"><i class="zmdi zmdi-instagram zmdi-hc-lg"></i></a></li>
                </ul>
                <br>
                
                <ul>
                    <li><a href="/terms-of-use.php">Terms of Use</a></li>
                    <li><a href="/privacy-policy.php">Privacy Policy</a></li>
                    <li><a href="/contact-us">Contact Us</a></li>
                </ul>
                <br><br>
                <p class="white-text">© 2019 OpenBazar.nl</p>
            </div>
        </div>
        
    </section> -->
    <?php include_once("footer-credits.php"); ?>
    <?php include_once("footer-assets.php"); ?>

</body>
</html>
