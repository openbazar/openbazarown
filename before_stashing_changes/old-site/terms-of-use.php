<!DOCTYPE html>
<html lang="en" class="no-js">
<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Openbazar App</title>

    <?php include_once("header-top.php"); ?>
    <style>
        /* .navbar-default {
            background-color: #FFFFFF !important;
            box-shadow: 0 0 1px 1px rgba(34,45,57,.15);
            transition: background .5s ease-in-out,padding .5s ease-in-out;
        } */
        .open_text{
            text-align: left !important;
        }
        .hero-section{
            padding: 5px 0 !important;
            height: 0% !important;
        }
        .hero-text {
            padding-top: 80px !important;
        }
        .footer-section{background-color: #f8f8f8 !important;}
    </style>
    <style>
			@media (max-width:576px) {
					.logo {
						height: 35px;
						margin-top: -7px;
						position: absolute;
						left: 35%;
					}
				}

			</style>
</head>


<body>
    
    <nav class="navbar navbar-default navbar-fixed-top top-nav-collapse" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle mobile-nav" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="color: #57748C;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            
                <a class="navbar-brand page-scroll" href="index.php"><img src="imgs/logo.png" class="logo" alt=""/></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php#home" class="page-scroll">Home</a>
                    </li>
                    <li>
                        <a href="index.php#about" class="page-scroll">About App</a>
                    </li>
                    <li>
                        <a href="index.php#features" class="page-scroll">Features</a>
                    </li>
                    <li>
                        <a href="index.php#download" class="page-scroll">Get The App</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>	

    <section class="hero-section tablediv" id="home">
		<div class="intablediv">
			<div class="container">
				<div class="row">
					<!-- <div class="col-md-12"> -->
						<h1 class="hero-text text-center">Terms Of Use </h1>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section text-center grey-background" id="about">
		<div class="container">
			<div class="row">
                <!-- <h3 class="card-title text-center">Terms Of Use</h3> -->
                <div class="card-box">
                    <div class="open_text">
                        <h3 class="card-title text-center">Welcome to Open Bazar!</h3>
                        <p class="card-text text-center">Please read this important legal information that sustains your use of the OpenBazar.nl platform and services.</p>
                        <br>

                        <h5 class="text-center">Terms of Use – OpenBazar.nl</h5>
                        <p class="text-center">Last modified: 03 December 2018</p>
                        <br>

                        <p >These Terms of Service provide you with information about the terms upon which we agree to provide, permit and allow you to access and use the Website. Please print a copy of these Terms of Service and refer to it as you use our Systems and Services. You agree to be bound by these Terms of Service if you continue using the Website. If you do not agree to these Terms of Service, you must immediately refrain from using the Website and the Services.</p>
                        <br>

                        <p ><b>Your account:</b> Some services in the Open Bazar require you to create your own account with your email and password. This e-mail will be the authorized e-mail. You will be responsible for the confidentiality of the password for your account. Responsible for all activities issued by him. In order to do this you must protect your own password and choose a word that is hard to reach by others. In turn, we advise all users to put their first and last names in the "Username".</p>
                        <br>

                        <p >When you use the Open Bazar you agree to the collection, transfer, storage and use of your personal information on various other sites by the Site.</p>
                        <br>

                        <p >You may also log in and create an account on the Site through third parties services such as Facebook or Google, which authorizes us to access, store and use your personal information as it is an accessible service as described in our Privacy Policy. If your account is compromised or misused, please contact our customer service team immediately.</p>
                        <br>

                        <p ><b>Open Bazar Rules:</b> You can post to any of our sections while avoiding the following:</p>
                        <ul>
                            <li>Posting false or misleading ads.</li>
                            <li>Copy, edit, or post content to another person.</li>
                            <li>Any acts of sabotage such as attempts to block the service or impose an unreasonable burden on the infrastructure of the site.</li>
                            <li>Violation of any rights of any other party.</li>
                            <li>Use any prohibited means to access the site database and collect content for any purpose, including bot, spider web and similar methods.</li>
                            <li>Publish any abusive mail, serial messages, or pyramid marketing scheme.</li>
                            <li>Spreading viruses or any other technology that would harm the site or the interests and property of users or anyone.</li>
                            <li>Violate any of the laws or any point of our prohibited content policy.</li>
                            <li>Collect information about other users including your email or other personal information.</li>
                            <li>Override actions used to block or restrict access to the site.</li>
                            <li>Use personal information about other people without their explicit consent.</li>
                        </ul>
                        <br>

                        <p>Any POST containing one or more of the prohibited things below will be deleted. The site reserves the right to permanently delete the user's account, blacklist it, reporting the content and the user information to Law Enforcement or the appropriate Authority. </p>
                        <ul>
                            <li>Tobacco products, narcotic drugs, psychotropic substances, liqueurs, medicines and analgesics, or even the establishment of links, whether direct or indirect, of articles, products or services prohibited by law.</li>
                            <li>Religious materials, including books, antiques, etc., or any information or description of any of these materials, may affect the religious feelings of any person or group.</li>
                            <li>Counterfeit or stolen goods or illegal and authorized services.</li>
                            <li>Prostitution or any other services, including the violation of the provisions of the law immoral.</li>
                            <li>Information and material indicating libel, defamation, threat or even abuse.</li>
                            <li>Human or natural human organs including blood and body fluids as well.</li>
                            <li>Goods, materials and services that infringe on the rights and intellectual property of any third party, or even the privacy of any person.</li>
                            <li>Police and army equipment of official badges, uniforms, coats, weapons, and other materials that prevent circulation.</li>
                            <li>Weapons and related tools (eg, firearms, ammunition, tear gas, rifles and sharp objects).</li>
                            <li>Posting Ads to Open Bazar competition sites or companies.</li>
                            <li>Phantom projects (such as: get rich quick and work from home).</li>
                            <li>Hazardous chemicals and pesticides.</li>
                        </ul>
                        <br>

                        <p><b>Reporting Ad:</b>Please use the report system to inform us of any problems or inappropriate content to work together to keep track of the services provided by the site. In turn, we will terminate our services for those who misuse the site, remove their content, and take legal action to safeguard the rights of other users. If any such action is taken, the site is not responsible for the nature of any inappropriate content that has been published and its subsequent consequences.</p>
                        <br>

                        <p>Report any of the below practices:</p>
                        <ul>
                            <li>Create more than one account.</li>
                            <li>Duplicate Ad serving.</li>
                            <li>Posting Ads with wrong or inappropriate images.	</li>
                            <li>Upload photos with names and phone numbers.</li>
                            <li>Post Ads with unreasonable prices.</li>
                            <li>Spread fraud and fake Ads.</li>
                            <li>Deploy Ads with misleading titles, content, and images.</li>
                            <li>Post Ads with a prepay option or bank transfer.	</li>
                            <li>Post Ads with public content or public content.	</li>
                            <li>Posting items that can be worn without uploading images.</li>
                            <li>Publish Ads in wrong sections.</li>
                            <li>Post Ads with multiple items.</li>
                            <li>Advertising agencies.</li>
                            <li>Post Ads with redirect links to other sites.</li>
                        </ul>
                        <br>
                        
                        <p>Reporting Violations of Intellectual Property Rights Users are prohibited from posting any content that infringes the rights of third parties; this includes, but is not limited to, infringement of intellectual property rights and trademarks (eg, advertising of fictitious material for sale). We have the right to remove any content that violates the terms of our publishing policy and protect the rights of others. If you feel that one of our ads violates your trademark and trademark rights, all you have to do is notify our customer service department, and only those who can submit a notification against the terms and conditions you are likely to contact will be notified                                </p>
                        <br>

                        <p><b>Charges and Services:</b> In general, advertising on the Open Bazar is for free, but we charge a fee for some of the services we provide to users. If you use a service for which we charge a fee, you will be required to review and approve it. Payment will be made in local currency. We may change this mechanism from time to time and we will notify you of any change to our payment policy by posting it on the site. The payment method is temporary when you post new promotions or services. On the other hand, these fees are non-refundable and you must pay them. If you do not, the validity of the service you use will be cancelled.  </p>
                        <br>

                        <p><b>Disclaimers and Limitations of Liability:</b> Open Bazar Services are provided as they exist or are available, and as a User you agree not to hold the Site responsible for any content posted by other users including but not limited to: advertisements or direct messages between users. The site does not guarantee the accuracy of the advertisements or the existing means of communication or the extent of security or even compliance with the laws in them, because most of what is posted on the site by users, in addition to the lack of continuous access to services, Out of our control; including the postponement or delay due to your location or the Internet. TO THE MAXIMUM EXTENT PERMITTED BY LAW, WE DISCLAIM ALL WARRANTIES, CONDITIONS AND CONDITIONS EXPRESS OR IMPLIED, INCLUDING THE QUALITY, MERCHANTABILITY, MERCHANTABILITY, FITNESS AND RELIABILITY OF THE PURPOSE FOR THE PURPOSE. We are also not responsible for any loss, whether by money (including profit), good reputation or any special or indirect damages resulting from your use of the Site, even if we have been informed of or expect such a thing. Some jurisdictions do not allow waiver of warranties or the exclusion of damages, so such disclaimers and exceptions may not apply to you.                            </p>
                        <br>

                        <p><b>Open Bazar Wallet:</b> Is a pre-payment system that allows registered members on the Open Bazar to buy credit to be used to purchase services offered as Featured Ads, Bump Up Ads and upgrading membership by the Open Bazar. The balance of the Open Bazar portfolio cannot be transferred to   other users and it is not replaced by money and the user can not earn interest or financial gain, in addition portfolio balances purchased are final and irrevocable.
                        </p><br>

                        <p>Any dispute relating to the Open Bazar portfolio must be reported within (24) hours from the time of the user's discovery of a problem. Otherwise, the user has waived the right to file any claim against the Open Bazar site.
                        </p><br>

                        <p>Each Open Bazar Wallet credit purchased by a registered user has no expiration. However, once a user’s account becomes inactive, or is deactivated, or otherwise terminated, all Open Bazar Wallet credits, if any, shall automatically be forfeited.
                        </p><br>

                        <p>Open Bazar reserves the right to suspend the sale of portfolio balances or change the cost, as well as the way in which the registered user can buy, store or use the Open Bazar portfolio.
                        </p><br>

                        <p>Open Bazar reserves the right to confiscate or cancel any portfolio balances purchased directly or indirectly or obtained through fraudulent or illegal methods without further notice to the user.
                        </p><br>
                        
                        <p>In case Open Bazar Administration finds reasons or evidence to confirm that any User has violated one of the terms of use of the Wallet or other terms of use of the Site, you shall prevent the use of the Open Bazar Portfolio and delete your Ads whether paid or free.
                        </p>
                        <br>

                        <p><b>Open Bazar Coins:</b> is an Affiliate system which allows registered members on the Open Bazar to collect Coins by placing an Ad, inviting friends or like Ads and redeem them to credit in Open Bazar wallet to be used to purchase services offered as Featured Ads, Bump Up Ads and upgrading membership by the Open Bazar. The balance of the Open Bazar coins cannot be transferred to other users and it is not replaced by money and the user can not earn interest or financial gain.</p>
                        <br>

                        <p>Any dispute relating to the Open Bazar coins must be reported within (24) hours from the time of the user's discovery of a problem. Otherwise, the user has waived the right to file any claim against the Open Bazar site.
                        </p><br>

                        <p>Each Open Bazar Coins collected by a registered user has no expiration. However, once a user’s account becomes inactive, or is deactivated, or otherwise terminated, all Open Bazar Coins, if any, shall automatically be forfeited.
                        </p><br>

                        <p>Open Bazar reserves the right to suspend coins affiliate system or change the cost, as well as the way in which the registered user can collect, store or use the Open Bazar Coins.
                        </p><br>

                        <p>Open Bazar reserves the right to confiscate or cancel any Coins balances collected directly or indirectly or obtained through fraudulent or illegal methods without further notice to the user.
                        </p><br>
                        
                        <p>In case Open Bazar Administration finds reasons or evidence to confirm that any User has violated one of the terms of use of the Coins or other terms of use of the Site, you shall prevent the use of the Open Bazar Portfolio and delete your Ads whether paid or free.
                        </p>
                        <br>

                        <p><b>Rights and Knowledgeable:</b> Open Bazar market has all rights, ownership and interests of its applications, including patent rights, copyrights, secrets and trademarks, and all other proprietary rights that include additions, updates and modifications to its applications. The use of code, software engineering, symbols, or otherwise, may not be modified, translated, or modified from any other application, and the copyright, trademark and intellectual property notices contained within or upon access may not be removed, Any of the open market applications.</p>
                        <br>

                        <p>You acknowledge and agree that any questions, comments, suggestions, ideas, comments or other information about the Site and / or the Open Bazar service you provide to us are not confidential and may be used by us at our discretion.</p>
                        <br>

                        <p><b>These were our general conditions:</b> If we change the conditions in the future, you will find this on our platform. We make sure that changes are not detrimental to you.</p>
                        <br>

                        <p><b>Open Bazar</b></p>
                        <p>@All Rights Reserved 2017-2018 OpenBazar.nl</p>


                    </div>
                </div>
            </div>
		</div>
	</section>
	
    
    <!-- <section class="footer-section" id="download">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="download-box">
                        <h2 class="red-text">Download the app</h2>
                        <p>Whatever you’ve got in mind, we’ve got inside. OpenBazar App is what you’d want to have on your mobile phone.</p>
                        <br>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-6">
                                <a href="#x" target="_blank"><img src="imgs/store_apple.png" class="img-responsive center-block" alt=""></a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-6">
                                <a href="https://play.google.com/store/apps/details?id=com.open_bazar" target="_blank"><img src="imgs/store_google.png" class="img-responsive center-block" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="row">
                <p class="white-text">Follow Us</p>
                <ul>
                    <li><a href="https://www.facebook.com/OpenBAZAR4u/"><i class="zmdi zmdi-facebook zmdi-hc-lg"></i></a></li>
                    <li><a href="https://mobile.twitter.com/BazarOpen"><i class="zmdi zmdi-twitter zmdi-hc-lg"></i></a></li>
                    <li><a href="#x"><i class="zmdi zmdi-youtube-play zmdi-hc-lg"></i></a></li>
                    <li><a href="https://www.instagram.com/openbazar4u/?hl=nl"><i class="zmdi zmdi-instagram zmdi-hc-lg"></i></a></li>
                </ul>
                <br>
                
                <ul>
                    <li><a href="/terms-of-use.php">Terms of Use</a></li>
                    <li><a href="/privacy-policy.php">Privacy Policy</a></li>
                    <li><a href="/contact-us.php">Contact Us</a></li>
                </ul>
                <br><br>
                <p class="white-text">© 2019 OpenBazar.nl</p>
            </div>
        </div>
        
    </section> -->
    <?php include_once("footer-credits.php"); ?>
    <?php include_once("footer-assets.php"); ?>

</body>
</html>
