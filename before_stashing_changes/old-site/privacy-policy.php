<!DOCTYPE html>
<html lang="en" class="no-js">
<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Openbazar App</title>

    <?php include_once("header-top.php"); ?>
    <style>
        /* .navbar-default {
            background-color: #FFFFFF !important;
            box-shadow: 0 0 1px 1px rgba(34,45,57,.15);
            transition: background .5s ease-in-out,padding .5s ease-in-out;
        } */
        .open_text{
            text-align: left !important;
        }
        .hero-section{
            padding: 5px 0 !important;
            height: 0% !important;
        }
        .hero-text {
            padding-top: 80px !important;
        }
        .footer-section{background-color: #f8f8f8 !important;}
        
    </style>
    <style>
			@media (max-width:576px) {
					.logo {
						height: 35px;
						margin-top: -7px;
						position: absolute;
						left: 35%;
					}
				}

			</style>
</head>


<body>
    
    <nav class="navbar navbar-default navbar-fixed-top top-nav-collapse" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle mobile-nav" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="color: #57748C;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            
                <a class="navbar-brand page-scroll" href="index.php"><img src="imgs/logo.png" class="logo" alt=""/></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php#home" class="page-scroll">Home</a>
                    </li>
                    <li>
                        <a href="index.php#about" class="page-scroll">About App</a>
                    </li>
                    <li>
                        <a href="index.php#features" class="page-scroll">Features</a>
                    </li>
                    <li>
                        <a href="index.php#download" class="page-scroll">Get The App</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>	

    <section class="hero-section tablediv" id="home">
		<div class="intablediv">
			<div class="container">
				<div class="row">
					<!-- <div class="col-md-12"> -->
						<h1 class="hero-text text-center">Privacy Policy </h1>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section text-center grey-background" id="about">
		<div class="container">
			<div class="row">
                <div class="card-box">
                    <h3 class="card-title text-center">Privacy Policy</h3>
                    <br>
                    <div class="open_text">
                        <p>This policy describes Open Bazar Privacy Policy for the collection, use, storage, sharing and protection of your personal information. The same policy applies to the Website, and all sites, applications, services and tools related to the Open Bazar, regardless of how these services are accessed.</p>
                        <br>
                        <p>When you use the Open Bazar and any related services, you expressly grant the Site the right to collect, use and retain your personal information, as described in our Privacy Policy and Terms of Use for the Open Bazar.</p>
                        <br>

                        <p>You can visit the Open Bazar site without registering a personal account. When you decide to provide the Site with your personal information, you agree that such information will be sent and stored in its own servers.</p>
                        <br>
                        <p>When you visit the site or use the services provided by the site or to users in general, or respond to ads or other existing content, the site will collect information sent from your device automatically, whether a computer or mobile phone. This information includes:</p>
                        <ul>
                            <li>The information collected through your interaction on the site, which includes: device number, device type, location information, internet contact information, page view statistics, site visits, return links, ad data, protocol address and Internet access information.</li>
                            <li>The information collected by the site through cookies (trace logs), Internet players and similar technologies.</li>
                        </ul>
                        <br>
                        
                        <p>Open Bazar collects any information that you enter on the site or that you provide when you use any of its services. This information includes: </p>
                        <ul>
                            <li>The information you provide to the Open Bazar when registering or using the services.</li>
                            <li>Additional information you provide to the Open Bazar through social media platforms or third party services.</li>
                            <li>Information provided in the course of settlement of disputes, correspondence via the site and messages sent to him/her.</li>
                            <li>Information about your location and location of your device; and your information ID whether you have already done this service on your mobile phone.</li>
                        </ul>
                        <br>

                        <p>Open Bazar may receive or collect additional information about you through third parties and add it to your account information. This information includes: demographic data, navigation data, contact data and additional data about you from other sources such as public authorities to the extent permitted by law.</p>
                        <br>

                        <p>You agree that the Open Bazar will use your personal information for the following purposes:</p>
                        <ul>
                            <li>To enable you to access his/her various services and customer service by email or phone number.</li>
                            <li>To prevent, detect and investigate when there is a possibility of prohibited activities where fraud and violation of the law and non-compliance with the terms of use of the site.</li>
                            <li>To customize, measure and improve Open Bazar services, content and advertisements.</li>
                            <li>To communicate with you via email, notifications, messages or telephone to inquire about open Bazar services for marketing purposes, updates and promotions based on message priorities (if any) or for any purposes as set forth in this Privacy Policy.</li>
                            <li>To provide more of the other services you have requested, as described in the information gathering mechanism on the site.</li>
                            <li>To be considered a potential candidate when you upload your CV on the Jobs page.</li>
                        </ul>
                        <br>

                        <p>User Rights:</p>
                        <ul>
                            <div>
                                You may have certain legal rights regarding your personal data to be deleted depending on the country you are in or how we interact with you.to delete your data you need to contact us through this <a href="https://openbazar.nl/contact-us.php">link</a>. You will not have to pay a fee to access your personal data (or to exercise any of the other rights). However, we may charge a reasonable fee if your request is clearly unfounded, repetitive or excessive or for any other reason. Alternatively, we may refuse to comply with your request in these circumstances. We try to respond to all legitimate requests within three months. Occasionally it may take us longer if your request is particularly complex or you have made several requests. In this case, we will notify you and keep you updated.

                            </div>
                        </ul>


                        <p>Open Bazar may share your personal information:</p>
                        <ul>
                            <li>To comply with legal obligations or a court order.</li>
                            <li>If necessary to prevent, detect or protect against criminal offenses, such as fraud, fraud and lawsuits.</li>
                            <li>If necessary to maintain our policy or to protect the rights and freedoms of others.</li>
                        </ul>
                        <br>

                        <p><b>Open Bazar</b></p>
                        <p>@All Rights Reserved 2017-2018 OpenBazar.nl</p>

                    </div>
                </div>
            </div>
		</div>
	</section>
	
    
    <!-- <section class="footer-section" id="download">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="download-box">
                        <h2 class="red-text">Download the app</h2>
                        <p>Whatever you’ve got in mind, we’ve got inside. OpenBazar App is what you’d want to have on your mobile phone.</p>
                        <br>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-6">
                                <a href="#x" target="_blank"><img src="imgs/store_apple.png" class="img-responsive center-block" alt=""></a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-6">
                                <a href="https://play.google.com/store/apps/details?id=com.open_bazar" target="_blank"><img src="imgs/store_google.png" class="img-responsive center-block" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="row">
                <p class="white-text">Follow Us</p>
                <ul>
                    <li><a href="https://www.facebook.com/OpenBAZAR4u/"><i class="zmdi zmdi-facebook zmdi-hc-lg"></i></a></li>
                    <li><a href="https://mobile.twitter.com/BazarOpen"><i class="zmdi zmdi-twitter zmdi-hc-lg"></i></a></li>
                    <li><a href="#x"><i class="zmdi zmdi-youtube-play zmdi-hc-lg"></i></a></li>
                    <li><a href="https://www.instagram.com/openbazar4u/?hl=nl"><i class="zmdi zmdi-instagram zmdi-hc-lg"></i></a></li>
                </ul>
                <br>
                
                <ul>
                    <li><a href="/terms-of-use.php">Terms of Use</a></li>
                    <li><a href="/privacy-policy.php">Privacy Policy</a></li>
                    <li><a href="/contact-us.php">Contact Us</a></li>
                </ul>
                <br><br>
                <p class="white-text">© 2019 OpenBazar.nl</p>
            </div>
        </div>
        
    </section> -->
    <?php include_once("footer-credits.php"); ?>
    <?php include_once("footer-assets.php"); ?>

</body>
</html>
