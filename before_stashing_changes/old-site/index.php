<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<script data-ad-client="ca-pub-6933751435930860" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Openbazar App</title>

		<?php include_once("header-top.php"); ?>
		<style>
			@media (max-width:576px) {
					.logo {
						height: 35px;
						margin-top: -7px;
						position: absolute;
						left: 35%;
					}
				}

			</style>
		
</head>


<body>
    <?php include_once("header-menu.php"); ?>
    
    <section class="hero-section tablediv" id="home">
		<div class="intablediv">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1 class="hero-text">Online classifieds platform in Europe and Asia that brings millions of users together </h1>
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<a href="https://apps.apple.com/nl/app/open-bazar/id1486285030?l=en" target="_blank"><img src="imgs/store_apple.png" class="img-responsive center-block" alt=""></a>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6">
								<a href="https://play.google.com/store/apps/details?id=com.open_bazar" target="_blank"><img src="imgs/store_google.png" class="img-responsive center-block" alt=""></a>
							</div>
						</div>
					</div>
					<div class="col-md-6 center-block">
						<div class="flex-container center-block">
						  <div class="flexslider">
							<ul class="slides">
							  <li> <img src="imgs/slide1.png" alt="" /> </li>
							  <li> <img src="imgs/slide2.png" alt=""  /> </li>
							  <li> <img src="imgs/slide3.jpg" alt=""  /> </li>
							</ul>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<!--
	<section class="content-section text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="red-text">About OpenBazar</h2>
					<p>Open Bazar is an online classifieds and advertising service platform in Europe and Asia that brings millions of users together without central institutions or intermediaries, to exchange their goods, products or find anything in their community. The platform enables merchants -to-user and user-to-user benefit in combination of credits and Open Bazar’s Coins through open bazar wallet and coins system. Users and merchants can browse through a large variety of Goods, Services, Real Estate, Rentals, Electronics, Fashion, Jobs and more other items.</p>
					<div class="row">
						<div class="col-sm-6">
							<h2 class="red-text">Our Mission</h2>
							<p>Open Bazar aims to empower merchants and users who want to take part in shaping the future of their finance by using our platform to buy, sell, rent, lend, offer service, open shop or find anything in their community. A community where underused goods are redistributed to fill a new need, and become wanted again, where non- product assets such as space, skills, money and coins are exchanged and traded in new ways of our Wallet and credits system that don’t always require centralized institutions or ‘middlemen’.</p>
						</div>
						<div class="col-sm-6">
							<h2 class="red-text">Our Vision</h2>
							<p>Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe and Asia In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
-->
	
	<section class="content-section text-center grey-background" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="card-box">
						<h2 class="red-text">About</h2>
						<hr>
						<p>Open Bazar is an online classifieds and advertising service platform in Europe and Asia that brings millions of users together without central institutions or intermediaries, to exchange their goods, products or find anything in their community. The platform enables merchants -to-user and user-to-user benefit in combination of credits and Open Bazar’s Coins through open bazar wallet and coins system. Users and merchants can browse through a large variety of Goods, Services, Real Estate, Rentals, Electronics, Fashion, Jobs and more other items.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card-box">
						<h2 class="red-text">Mission</h2>
						<hr>
						<p>Open Bazar aims to empower merchants and users who want to take part in shaping the future of their finance by using our platform to buy, sell, rent, lend, offer service, open shop or find anything in their community. A community where underused goods are redistributed to fill a new need, and become wanted again, where non- product assets such as space, skills, money and coins are exchanged and traded in new ways of our well designed Wallet and credits system that don’t always require centralized institutions or ‘middlemen’.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card-box">
						<h2 class="red-text">Vision</h2>
						<hr>
						<p>Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe and Asia In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section" id="features">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<h2 class="red-text">Features</h2>
					<p>Distinguishing advertisements for faster selling, buying, renting, shops and offering services at one place. Find what you want quickly with our easy and smart App, get more views by adding up to 30 pictures to your Ad, and benefit from earning coins and follow favorite users.</p>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-sm-12 col-md-4">
					<h3 class="red-text">Friends network</h3>
					<p>Invite your friends through social networks and contacts and be aware of their latest ads </p>
					<br>
					<h3 class="red-text">Job Feature</h3>
					<p>Ability to communicate with the seller or buyer or advertisers about vacancies through the application.</p>
					<br>
					<h3 class="red-text">Like, post Ad and invite friend</h3>
					<p>We have an exclusive earning opportunity for our users to get paid for liking ads, inviting friends and posting ads on Open Bazar</p>
					<br>
					
					<h3 class="red-text">All in one Platform</h3>
					<p>Always wanted to Shop everything in one place with multiple variations of brands and products, at one time? Good news, We have got just the right app for you!</p>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="flex-container center-block text-center">
					  <div class="flexslider center-block text-center">
						<ul class="slides">
						  <li> <img src="imgs/slide1.png" alt="" /> </li>
						  <li> <img src="imgs/slide2.png" alt=""  /> </li>
						  <li> <img src="imgs/slide3.jpg" alt=""  /> </li>
						</ul>
					  </div>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<h3 class="red-text">Free Application </h3>
					<p>Fashionable and attractive design will appeal to you, it is fast, smart and easy to use!</p>
					<br>
					<h3 class="red-text">Notification system</h3>
					<p>An advanced and smart notification system to receive all updates about your Ads and your followers Ads.</p>
					<br>
					<h3 class="red-text">Follow users</h3>
					<p>Follow your favorite users and stay up to date with their latest ads and be as the first one receive notification if they post an Ad. </p>
					<br>
					<h3 class="red-text">Within 30 second post Ad</h3>
					<p>Publish your ad in 30 second or browse our hundreds of categories, from vehicles and apartments, real estate and more, all from your mobile phone or tablet!</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="credit-packages text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h2>Credit Packages</h2>
					<p>Easier was not before! Now you can make one-time payment with the wallet and buy credit  then use it to feature your Ad, repost and buy a package or even upgrade your membership</p>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_standard.png" class="center-block img-responsive" alt="">
					<p>Add credit to your wallet by making one-time payment to buy our various offerings or upgrade your package of membership. </p>
				</div>
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_advanced.png" class="center-block img-responsive" alt="">
					<p>The easiest and fastest way to repost Ad, Feature Ad or Rocket Ad to get more views, no need to use credit card or transfer money just use your credits from your Wallet and save your time.</p>
				</div>
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_premium.png" class="center-block img-responsive" alt="">
					<p>We Offer you the smartest and safest way to make a benefit from our large variety and help you to shape your financial future</p>
				</div>
				<div class="col-md-3 col-sm-6">
					<img src="imgs/package_supreme.png" class="center-block img-responsive" alt="">
					<p>Start your own business and sell your goods or open your shop and create online a solid network and get more buyers without central institutions or intermediaries</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="content-section text-center more-features">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2 class="red-text">Earn more and faster by using our various offerings now! Get more offers and views, and sell or rent multiple products at one time.</h2>
				</div>
			</div>
			<br><br><br>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 border-bottom border-right">
					<img src="imgs/ic_wallet.png" class="center-block img-responsive" alt="">
					<h3>Wallet</h3>
					<p>Open Bazar Wallet is a secure encryption online tool which ensure that your credits always safe and it helps you collect rewards, store credits and buy various offerings and redeem Coins multiple times as and when required</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-bottom border-right">
					<img src="imgs/ic_coins.png" class="center-block img-responsive" alt="">
					<h3>Coins</h3>
					<p>Open Bazar Coins is an Affiliate system which allows you to collect Coins by inviting friends, posting Ads or like Ads and it helps you collect rewards, store credits and get various offerings and redeem Coins multiple times as and when required</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-bottom">
					<img src="imgs/ic_membership.png" class="center-block img-responsive" alt="">
					<h3>Membership</h3>
					<p>We offer 4 various of membership, Bronze to get more active Ads, Silver to get more Views and likes, Gold to shape your financial future and Diamond to create a solid network</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-right">
					<img src="imgs/ic_repost.png" class="center-block img-responsive" alt="">
					<h3>Repost Ad</h3>
					<p>Want to renew your ad's date of publication? Upload your ad to update and restore it at the top of the list and get up to 10 more views </p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 border-right">
					<img src="imgs/ic_features.png" class="center-block img-responsive" alt="">
					<h3>Features Ad</h3>
					<p>Get your Ad in the top section of the list with special badge and get up to 10 times more views</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<img src="imgs/ic_rocket.png" class="center-block img-responsive" alt="">
					<h3>Rocket</h3>
					<p>Get your Ad an Urgent badge and get up to 30 times more views to sell easier and faster</p>
				</div>
			</div>
		</div>
	</section>
    
    <?php include_once("footer-credits.php"); ?>
    <?php include_once("footer-assets.php"); ?>

</body>
</html>
