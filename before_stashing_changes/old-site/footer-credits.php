<section class="footer-section" id="download">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="download-box">
					<h2 class="red-text">Download the app</h2>
					<p>Whatever you’ve got in mind, we’ve got inside. OpenBazar App is what you’d want to have on your mobile phone.</p>
					<br>
					<div class="row">
						<div class="col-xs-6 col-sm-6 col-md-6">
							<a href="https://apps.apple.com/nl/app/open-bazar/id1486285030?l=en" target="_blank"><img src="imgs/store_apple.png" class="img-responsive center-block" alt=""></a>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6">
							<a href="https://play.google.com/store/apps/details?id=com.open_bazar" target="_blank"><img src="imgs/store_google.png" class="img-responsive center-block" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="row">
			<p class="white-text">Follow Us</p>
			<ul>
				<li><a href="https://www.facebook.com/OpenBAZAR4u/"><i class="zmdi zmdi-facebook zmdi-hc-lg"></i></a></li>
				<li><a href="https://mobile.twitter.com/BazarOpen"><i class="zmdi zmdi-twitter zmdi-hc-lg"></i></a></li>
				<li><a href="https://www.youtube.com/c/OpenBazar"><i class="zmdi zmdi-youtube-play zmdi-hc-lg"></i></a></li>
				<li><a href="https://www.instagram.com/openbazar4u/?hl=nl"><i class="zmdi zmdi-instagram zmdi-hc-lg"></i></a></li>
			</ul>
			<br>
			
			<ul>
			<!-- http://134.209.86.177/app-admin/terms-of-use -->
				<li><a href="/terms-of-use.php">Terms of Use</a></li>
				<li><a href="/privacy-policy.php">Privacy Policy</a></li>
				<li><a href="/contact-us.php">Contact Us</a></li>
			</ul>
			<br><br>
			<p class="white-text">© 2019 OpenBazar.nl</p>
		</div>
	</div>
	
</section>
