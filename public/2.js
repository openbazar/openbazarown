(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/authModals.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/partials/authModals.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loadingText_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loadingText.vue */ "./resources/js/partials/loadingText.vue");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loadingText: _loadingText_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  data: function data() {
    return {
      step: 1,
      loading: false,
      fullPageLoading: false,
      google_loading: false,
      facebook_loading: false,
      country: '',
      fireUser: '',
      social_user: '',
      referred: '',
      login_user: {
        email: "",
        password: ""
      },
      signup_user: {
        email: "",
        password: "",
        confirm_password: "",
        name: "",
        phone_no: "",
        signup_type: "Email",
        app_type: "Website",
        reffer_id: ""
      },
      reset_password: {
        email: '',
        verification_code: '',
        password: "",
        confirm_password: "",
        otp: ""
      },
      countries: [{
        id: 1,
        name: 'Belgium',
        flag: '/bazar/belgium.png'
      }, {
        id: 2,
        name: 'France',
        flag: '/bazar/france.png'
      }, {
        id: 3,
        name: 'Germany',
        flag: '/bazar/germany.png'
      }, {
        id: 4,
        name: 'India',
        flag: '/bazar/india.png'
      }, {
        id: 5,
        name: 'Netherlands',
        flag: '/bazar/netherlands.png'
      }, {
        id: 6,
        name: 'Poland',
        flag: '/bazar/poland.png'
      }, {
        id: 7,
        name: 'Spain',
        flag: '/bazar/spain.png'
      }]
    };
  },
  mounted: function mounted() {
    // console.log(this.$store.getters.isAuthenticated);
    if (this.$root.$refs._wrapper && this.$root.$refs._wrapper.social_user) {
      this.social_user = this.$root.$refs._wrapper.social_user;
      this.loginWithSocialUser();
    } else {// console.log('No Social User')
    }

    if (this.$root.$refs._wrapper && this.$root.$refs._wrapper.referred) {
      this.referred = this.$root.$refs._wrapper.referred;
      this.signup_user.email = this.referred.email;
      this.signup_user.name = this.referred.name; // $('#registerModal').modal('show')
      // this.openRegisterModal();
      // console.log('openRegisterModal shoul open')
    } else {// console.log('No Social User')
      }

    if (this.loading) {
      $('.modal').modal({
        backdrop: 'static',
        keyboard: false
      });
    }
  },
  methods: {
    selectCountry: function selectCountry(country) {
      // this.$emit('country-selected' , this.countries[id] )
      this.$root.country = country;
      this.country = country;
      console.log(this.$root.cities[0]); // if(this.$store.getters.isAuthenticated){
      // axios.post('/v1/update_user_country' , {
      //     user_id : this.$store.getters.isAuthenticated.id,
      //     country: country.id
      // },{
      //     headers:{
      //         Authorization: "Bearer "+ this.$store.getters.isAuthenticated.token
      //     }
      // });

      this.$store.commit('setCountry', country); // if(this.$store.getters.getActualURL){
      //     this.$router.push(this.$store.getters.getActualURL)
      //     this.$store.commit('setCurrentUrl' , '' )
      // }else{

      this.$router.push('/products'); // }
      // }else{
      //     this.openLoginModal();
      // }
    },
    loginWithSocialUser: function loginWithSocialUser() {
      var _this = this;

      this.fullPageLoading = true;
      console.log("this.social_user", this.social_user);
      axios.post('/v1/sociallogin', {
        email: this.social_user.email,
        name: this.social_user.name,
        signup_type: this.social_user.source,
        social_details: JSON.stringify(this.social_user.user),
        social_token: this.social_user.token,
        profile_image: this.social_user.avatar,
        app_type: 'Website'
      }).then(function (res) {
        _this.fullPageLoading = false;
        console.log(res.data.data);

        if (res.data.status == 'success') {
          firebase.app(); // Get a reference to the database service

          var database = firebase.database(); // this.fireUser = ''
          // database.ref('Users/user'+res.data.data.id).once( 'value' , (snapshot) => {
          // let fireUser = snapshot.val();
          // this.fireUser = snapshot.val();
          //     console.log(fireUser , 'this.fireUser auth')
          //     var user = Object.assign( {} , fireUser);
          //         console.log(user , 'online status true')
          //         if(fireUser && fireUser.online != 'true'){
          //             user.online = 'true';
          //             database.ref('Users/user'+res.data.data.id).set({
          //                 ...user
          //             })
          //         }
          // });

          database.ref('Users/user' + res.data.data.id).once('value', function (snapshot) {
            console.log(snapshot.val(), 'snapshot.val()');
            var user = {};

            if (snapshot.val()) {
              user = snapshot.val();
              user.online = 'true';
            } else {
              user = {
                name: res.data.data.name,
                image: _this.social_user.avatar,
                msgcount: '0',
                online: 'true'
              };
            }

            database.ref('Users/user' + res.data.data.id).set(_objectSpread({}, user));
          });

          _this.$store.commit('login', res.data.data);

          _this.updateFireUser(res.data.data);

          if (_this.$store.getters.getActualURL) {
            _this.$router.push(_this.$store.getters.getActualURL);

            _this.$store.commit('setCurrentUrl', '');
          } else {
            _this.$router.push('/products');
          }
        } else {
          console.log(res);

          _this.$toastr.error(res.data.message);
        }

        _this.$root.$refs._wrapper.social_user = '';
      })["catch"](function (err) {
        console.log(err);
        _this.loading = false;
        _this.fullPageLoading = false; // $('#loginModal').modal('hide')

        _this.$toastr.error('An unexpected error occurred');

        _this.$root.$refs._wrapper.social_user = '';
      });
    },
    login: function login() {
      var _this2 = this;

      this.loading = true;
      this.fireUser = '';
      axios.post('/v1/login', {
        email: this.login_user.email,
        password: this.login_user.password,
        app_type: 'Website'
      }).then(function (res) {
        _this2.loading = false;
        _this2.fullPageLoading = false;

        if (res.data.status == 'success') {
          $('#loginModal').modal('hide'); // this.$toastr.success('Logged in successfully');
          // this.$store.commit('setCountry' , this.country );

          _this2.$store.commit('login', res.data.data);

          axios.post('/v1/update_user_country', {
            user_id: _this2.$store.getters.isAuthenticated.id,
            country: _this2.country.id
          }, {
            headers: {
              Authorization: "Bearer " + _this2.$store.getters.isAuthenticated.token
            }
          });

          if (_this2.$store.getters.getActualURL) {
            _this2.$router.push(_this2.$store.getters.getActualURL);

            _this2.$store.commit('setCurrentUrl', '');
          } else {
            _this2.$router.push('/products');
          }

          _this2.$root.$refs.navbar.getFireUser(); // Get a reference to the database service


          _this2.updateFireUser(res.data.data); // this.fireUser = ''
          // database.ref('Users/user'+res.data.data.id).once( 'value' , (snapshot) => {
          // let fireUser = snapshot.val();
          // this.fireUser = snapshot.val();
          //     console.log(fireUser , 'this.fireUser auth')
          //     var user = Object.assign( {} , fireUser);
          //         console.log(user , 'online status true')
          //         if(fireUser && fireUser.online != 'true'){
          //             user.online = 'true';
          //             database.ref('Users/user'+res.data.data.id).set({
          //                 ...user
          //             })
          //         }
          // });
          // this.$root.$refs.navbar.city = this.$root.cities[0];

        } else {
          console.log(res);

          _this2.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        console.log(err);
        _this2.loading = false;
        _this2.fullPageLoading = false; // $('#loginModal').modal('hide')

        _this2.$toastr.error('An unexpected error occurred');
      });
    },
    updateFireUser: function updateFireUser(user) {
      var _this3 = this;

      this.fireUser = '';
      firebase.app();
      var database = firebase.database();
      database.ref('Users/user' + user.id).once('value', function (snapshot) {
        var fireUser = snapshot.val();
        _this3.fireUser = snapshot.val();
        console.log(fireUser, 'this.fireUser auth');
        var user = Object.assign({}, fireUser);
        console.log(user, 'online status true');

        if (fireUser && fireUser.online != 'true') {
          user.online = 'true';
          database.ref('Users/user' + user.id).set(_objectSpread({}, user));
        }
      });
    },
    register: function register() {
      var _this4 = this;

      if (this.signup_user.password == this.signup_user.confirm_password) {
        this.loading = true; // console.log(this.signup_user)

        axios.post('/v1/signup', {
          email: this.signup_user.email,
          password: this.signup_user.password,
          name: this.signup_user.name,
          phone_no: this.signup_user.phone_no,
          app_type: this.signup_user.app_type,
          signup_type: this.signup_user.signup_type,
          reffer_id: this.referred.reffer_id
        }).then(function (res) {
          if (res.data.status == 'success') {
            _this4.loading = false;
            $('#registerModal').modal('hide');
            _this4.fullPageLoading = true;
            _this4.login_user.email = _this4.signup_user.email;
            _this4.login_user.password = _this4.signup_user.password;
            _this4.$root.current_user = res.data.data;
            firebase.app(); // Get a reference to the database service

            var database = firebase.database();
            var user = {};
            console.log(res.data.data, res.data.data.name);
            user = {
              name: res.data.data.name,
              image: '',
              msgcount: '0',
              online: 'true'
            };
            database.ref('Users/user' + res.data.data.id).set(_objectSpread({}, user));

            _this4.login(); // this.$toastr.success('Logged in successfully');
            // this.$router.push('/products')

          } else {
            console.log(res);
            _this4.loading = false; // $('#loginModal').modal('hide')

            _this4.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          console.log(err);
          _this4.loading = false;
          $('#registerModal').modal('hide');

          _this4.$toastr.error('An unexpected error occurred');
        });
      } else {
        this.$toastr.error('Password does not match');
      }
    },
    sendVerificationCode: function sendVerificationCode() {
      var _this5 = this;

      // this.step = 2
      this.loading = true;
      axios.post('/v1/emailotp', {
        email: this.reset_password.email,
        type: 'other'
      }, {
        headers: {
          'Accept': 'application/json, text/plain',
          'Content-Type': 'application/json;charset=UTF-8',
          'X-Requested-With': 'XMLHttpRequest'
        }
      }).then(function (res) {
        if (res.data.status == 'success') {
          _this5.loading = false;
          _this5.step = 2;
          _this5.reset_password.otp = res.data.otp;
        } else {
          console.log(res);
          _this5.loading = false;

          _this5.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        console.log(err);
        _this5.loading = false;

        _this5.$toastr.error('An unexpected error occurred'); // this.step = 2

      }); // console.log('sendVerificationCode')
    },
    verifyCode: function verifyCode() {
      // this.step = 2
      this.loading = true;

      if (this.reset_password.verification_code == this.reset_password.otp) {
        this.step = 3;
        this.loading = false;
        this.reset_password.email = '';
        this.reset_password.verification_code = '';
      } else {
        this.loading = false;
        this.$toastr.error('Code does not match');
      } // console.log('sendVerificationCode')

    },
    updatePassword: function updatePassword() {
      var _this6 = this;

      if (this.reset_password.password == this.reset_password.confirm_password) {
        this.loading = true;
        axios.post('/v1/reset_password', {
          email: this.reset_password.email,
          password: this.reset_password.password
        }).then(function (res) {
          if (res.data.status == 'success') {
            _this6.loading = false;

            _this6.closeModal();

            _this6.openLoginModal();

            _this6.step = 1;
            _this6.reset_password.password = '';
            _this6.reset_password.confirm_password = '';
          } else {
            console.log(res);
            _this6.loading = false;

            _this6.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          console.log(err);
          _this6.loading = false;

          _this6.$toastr.error('An unexpected error occurred');
        });
      } else {
        this.$toastr.error('Password does not match');
      }
    },
    openLoginModal: function openLoginModal() {
      this.closeModal();
      $('#loginModal').modal('show');
      this.checkIfModalOpen();
    },
    openRegisterModal: function openRegisterModal() {
      this.closeModal();
      $('#registerModal').modal('show');
      this.checkIfModalOpen();
    },
    openForgetModal: function openForgetModal() {
      this.step = 1;
      this.closeModal();
      $('#forgetModal').modal('show');
      this.checkIfModalOpen();
    },
    checkIfModalOpen: function checkIfModalOpen() {
      $(document).on('hidden.bs.modal', function (event) {
        if ($('.modal:visible').length) {
          $('body').addClass('modal-open');
        }
      });
    },
    closeModal: function closeModal() {
      $('.modal').modal('hide');
    },
    socialLogin: function socialLogin(type) {
      if (type == 'google') {
        this.$store.commit('setCountry', this.country);
        this.google_loading = true;
        window.location = '/login-google';
      }

      if (type == 'facebook') {
        this.facebook_loading = true;
        this.$store.commit('setCountry', this.country);
        window.location = '/login-facebook';
      } // console.log(type)

    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/loadingText.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/partials/loadingText.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    loading: {
      type: Boolean,
      "default": false
    },
    text: {
      type: Text,
      "default": ''
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/authModals.vue?vue&type=template&id=5f70b3bd&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/partials/authModals.vue?vue&type=template&id=5f70b3bd& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("loading", {
        attrs: {
          active: _vm.fullPageLoading,
          loader: "dots",
          height: 170,
          width: 170,
          color: "#d11d1d",
          "background-color": "rgb(0 0 0)",
          "lock-scroll": false,
          "is-full-page": true
        },
        on: {
          "update:active": function($event) {
            _vm.fullPageLoading = $event
          }
        }
      }),
      _vm._v(" "),
      _vm.$route.path == "/"
        ? _c(
            "div",
            {
              staticClass: "country-flags-section",
              class: _vm.$route.path == "/" ? "hide-from-navbar" : ""
            },
            [
              _c("div", { staticClass: "country-select-title" }, [
                _vm._v("Select Country")
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "country-flags" },
                _vm._l(_vm.countries, function(country, index) {
                  return _c(
                    "div",
                    {
                      key: index,
                      staticClass: "country-flag",
                      class: {
                        active:
                          _vm.$store.getters.currentCountry.id == country.id
                      }
                    },
                    [
                      _c(
                        "a",
                        {
                          attrs: { href: "javascript:;" },
                          on: {
                            click: function($event) {
                              return _vm.selectCountry(country)
                            }
                          }
                        },
                        [
                          _c("img", { attrs: { src: country.flag } }),
                          _vm._v(" "),
                          _c("div", { staticClass: "country-name" }, [
                            _vm._v(_vm._s(country.name))
                          ])
                        ]
                      )
                    ]
                  )
                }),
                0
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "authModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "loginModalTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _c("div", { staticClass: "modal-body" }, [
                  _c(
                    "div",
                    { staticClass: "auth-components" },
                    [_c("auth-components")],
                    1
                  )
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "loginModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "loginModalTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _c(
                  "div",
                  {
                    staticClass: "modal-remove-icon",
                    on: { click: _vm.closeModal }
                  },
                  [_c("i", { staticClass: "fa fa-times-circle" })]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "auth-components" }, [
                    _c("div", { staticClass: "login-form" }, [
                      _c("div", { staticClass: "form-heading" }, [
                        _vm._v("\n                Login\n              ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "form",
                        {
                          attrs: { autocomplete: "off" },
                          on: {
                            submit: function($event) {
                              $event.preventDefault()
                              return _vm.login($event)
                            }
                          }
                        },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.login_user.email,
                                expression: "login_user.email"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              required: "",
                              type: "email",
                              placeholder: "Email Address"
                            },
                            domProps: { value: _vm.login_user.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.login_user,
                                  "email",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.login_user.password,
                                expression: "login_user.password"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              type: "password",
                              placeholder: "Password"
                            },
                            domProps: { value: _vm.login_user.password },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.login_user,
                                  "password",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "submit-btn",
                              attrs: { disabled: _vm.loading, type: "submit" }
                            },
                            [
                              _vm.loading
                                ? _c("loader")
                                : _c("span", [_vm._v("Login")])
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _vm._m(0),
                      _vm._v(" "),
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "signup-with-facebook",
                            on: {
                              click: function($event) {
                                return _vm.socialLogin("facebook")
                              }
                            }
                          },
                          [
                            _vm.facebook_loading
                              ? _c("loader")
                              : _c("span", [
                                  _c("i", {
                                    staticClass: "fab fa-facebook-f mr-2"
                                  }),
                                  _vm._v(" Sign in with Facebook")
                                ])
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "signup-with-google",
                            on: {
                              click: function($event) {
                                return _vm.socialLogin("google")
                              }
                            }
                          },
                          [
                            _vm.google_loading
                              ? _c("loader")
                              : _c("span", [
                                  _c("i", {
                                    staticClass: "fab fa-google mr-2"
                                  }),
                                  _vm._v(" Sign in with Google")
                                ])
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "d-flex align-items-center" }, [
                        _c(
                          "div",
                          { staticClass: "already-have-account text-left" },
                          [
                            _vm._v("Do not have account ?"),
                            _c(
                              "a",
                              {
                                attrs: { href: "javascript:;" },
                                on: { click: _vm.openRegisterModal }
                              },
                              [_vm._v("Sign up")]
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "forgot-password" }, [
                          _c(
                            "a",
                            {
                              attrs: { href: "javascript:;" },
                              on: { click: _vm.openForgetModal }
                            },
                            [_vm._v("Forgot Password?")]
                          )
                        ])
                      ])
                    ])
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "registerModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "loginModalTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _c(
                  "div",
                  {
                    staticClass: "modal-remove-icon",
                    on: { click: _vm.closeModal }
                  },
                  [_c("i", { staticClass: "fa fa-times-circle" })]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "auth-components" }, [
                    _c("div", { staticClass: "signup-form" }, [
                      _c("div", { staticClass: "form-heading" }, [
                        _vm._v("\n                Sign Up\n              ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-heading-description" }, [
                        _vm._v(
                          "\n                To Post ads, like ads, chat with users follow users or earn free coins\n              "
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "signup-with-facebook",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                return _vm.socialLogin("facebook")
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fab fa-facebook-f mr-2" }),
                            _vm._v(" Sign up with Facebook")
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", [
                        _c(
                          "button",
                          {
                            staticClass: "signup-with-google",
                            attrs: { type: "button" },
                            on: {
                              click: function($event) {
                                return _vm.socialLogin("google")
                              }
                            }
                          },
                          [
                            _c("i", { staticClass: "fab fa-google mr-2" }),
                            _vm._v(" Sign up with Google")
                          ]
                        )
                      ]),
                      _vm._v(" "),
                      _vm._m(1),
                      _vm._v(" "),
                      _c(
                        "form",
                        {
                          on: {
                            submit: function($event) {
                              $event.preventDefault()
                              return _vm.register($event)
                            }
                          }
                        },
                        [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.signup_user.name,
                                expression: "signup_user.name"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              type: "text",
                              required: "",
                              placeholder: "Name"
                            },
                            domProps: { value: _vm.signup_user.name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.signup_user,
                                  "name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.signup_user.phone_no,
                                expression: "signup_user.phone_no"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              type: "text",
                              required: "",
                              placeholder: "Phone Number"
                            },
                            domProps: { value: _vm.signup_user.phone_no },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.signup_user,
                                  "phone_no",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.signup_user.email,
                                expression: "signup_user.email"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              type: "email",
                              required: "",
                              placeholder: "Email Address"
                            },
                            domProps: { value: _vm.signup_user.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.signup_user,
                                  "email",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.signup_user.password,
                                expression: "signup_user.password"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              type: "password",
                              required: "",
                              placeholder: "Password"
                            },
                            domProps: { value: _vm.signup_user.password },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.signup_user,
                                  "password",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.signup_user.confirm_password,
                                expression: "signup_user.confirm_password"
                              }
                            ],
                            staticClass: "form-input",
                            attrs: {
                              type: "password",
                              required: "",
                              placeholder: "Confirm Password"
                            },
                            domProps: {
                              value: _vm.signup_user.confirm_password
                            },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.signup_user,
                                  "confirm_password",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "privacy-text" }, [
                            _vm._v(
                              "\n                  By signing up you agree on our terms and conditions\n                "
                            )
                          ]),
                          _vm._v(" "),
                          _vm._m(2),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "submit-btn",
                              attrs: { disabled: _vm.loading, type: "submit" }
                            },
                            [
                              _vm.loading
                                ? _c("loader")
                                : _c("span", [_vm._v(" Create Account ")])
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "already-have-account" }, [
                        _vm._v("Already have an account ? "),
                        _c(
                          "a",
                          {
                            attrs: { href: "javascript:;" },
                            on: { click: _vm.openLoginModal }
                          },
                          [_vm._v("Login")]
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "forgetModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "loginModalTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm.step == 2
                  ? _c(
                      "div",
                      {
                        staticClass: "modal-remove-icon",
                        on: {
                          click: function($event) {
                            _vm.step = 1
                          }
                        }
                      },
                      [_c("i", { staticClass: "fas fa-arrow-circle-left" })]
                    )
                  : _c(
                      "div",
                      {
                        staticClass: "modal-remove-icon",
                        on: { click: _vm.closeModal }
                      },
                      [_c("i", { staticClass: "fa fa-times-circle" })]
                    ),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "auth-components" }, [
                    _c("div", { staticClass: "forget-form" }, [
                      _vm.step == 1
                        ? _c("div", [
                            _c("div", { staticClass: "form-heading" }, [
                              _vm._v(
                                "\n                  Reset Password\n                "
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "form",
                              {
                                on: {
                                  submit: function($event) {
                                    $event.preventDefault()
                                    return _vm.sendVerificationCode($event)
                                  }
                                }
                              },
                              [
                                _c("div", { staticClass: "small-text" }, [
                                  _vm._v("Please enter your registered email")
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.reset_password.email,
                                      expression: "reset_password.email"
                                    }
                                  ],
                                  staticClass: "form-input",
                                  attrs: {
                                    required: "",
                                    type: "email",
                                    placeholder: "Email"
                                  },
                                  domProps: { value: _vm.reset_password.email },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.reset_password,
                                        "email",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "submit-btn",
                                    attrs: {
                                      disabled: _vm.loading,
                                      type: "submit"
                                    }
                                  },
                                  [
                                    _vm.loading
                                      ? _c("loader")
                                      : _c("span", [
                                          _vm._v(" Send Verification Link ")
                                        ])
                                  ],
                                  1
                                )
                              ]
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.step == 2
                        ? _c("div", [
                            _c("div", { staticClass: "form-heading" }, [
                              _vm._v(
                                "\n                  Verification Code\n                "
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "form",
                              {
                                on: {
                                  submit: function($event) {
                                    $event.preventDefault()
                                    return _vm.verifyCode($event)
                                  }
                                }
                              },
                              [
                                _c("div", { staticClass: "small-text" }, [
                                  _vm._v(
                                    "Please enter your 5 digits verification code that has been sent to your email"
                                  )
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value:
                                        _vm.reset_password.verification_code,
                                      expression:
                                        "reset_password.verification_code"
                                    }
                                  ],
                                  staticClass: "form-input",
                                  attrs: {
                                    type: "text",
                                    required: "",
                                    placeholder: "Verification"
                                  },
                                  domProps: {
                                    value: _vm.reset_password.verification_code
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.reset_password,
                                        "verification_code",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "small-text",
                                    staticStyle: {
                                      "font-size": "14px",
                                      "margin-top": "10px"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "Please if you did'nt recieve email in 10 seconds, Check spam"
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "submit-btn",
                                    staticStyle: { "margin-top": "10px" },
                                    attrs: {
                                      disabled: _vm.loading,
                                      type: "submit"
                                    }
                                  },
                                  [
                                    _vm.loading
                                      ? _c("loader")
                                      : _c("span", [_vm._v(" Submit ")])
                                  ],
                                  1
                                )
                              ]
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.step == 3
                        ? _c("div", [
                            _c("div", { staticClass: "form-heading" }, [
                              _vm._v(
                                "\n                  Verification Code\n                "
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "form",
                              {
                                on: {
                                  submit: function($event) {
                                    $event.preventDefault()
                                    return _vm.updatePassword($event)
                                  }
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.reset_password.password,
                                      expression: "reset_password.password"
                                    }
                                  ],
                                  staticClass: "form-input",
                                  attrs: {
                                    type: "password",
                                    required: "",
                                    placeholder: "Password"
                                  },
                                  domProps: {
                                    value: _vm.reset_password.password
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.reset_password,
                                        "password",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value:
                                        _vm.reset_password.confirm_password,
                                      expression:
                                        "reset_password.confirm_password"
                                    }
                                  ],
                                  staticClass: "form-input",
                                  attrs: {
                                    type: "password",
                                    required: "",
                                    placeholder: "Confirm Password"
                                  },
                                  domProps: {
                                    value: _vm.reset_password.confirm_password
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.reset_password,
                                        "confirm_password",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "submit-btn",
                                    attrs: {
                                      disabled: _vm.loading,
                                      type: "submit"
                                    }
                                  },
                                  [
                                    _vm.loading
                                      ? _c("loader")
                                      : _c("span", [
                                          _vm._v(" Change Password ")
                                        ])
                                  ],
                                  1
                                )
                              ]
                            )
                          ])
                        : _vm._e()
                    ])
                  ])
                ])
              ])
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "seprator" }, [
      _c("div", { staticClass: "seprate-1" }),
      _vm._v(" "),
      _c("div", { staticClass: "or-text" }, [_vm._v("OR")]),
      _vm._v(" "),
      _c("div", { staticClass: "seprate-2" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "seprator" }, [
      _c("div", { staticClass: "seprate-1" }),
      _vm._v(" "),
      _c("div", { staticClass: "or-text" }, [_vm._v("OR")]),
      _vm._v(" "),
      _c("div", { staticClass: "seprate-2" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "privacy-links" }, [
      _c(
        "a",
        {
          staticClass: "mr-5",
          attrs: { href: "/terms-of-use", target: "_blank" }
        },
        [_vm._v("Terms and Conditions")]
      ),
      _vm._v(" "),
      _c("a", { attrs: { href: "/privacy-policy", target: "_blank" } }, [
        _vm._v("Privacy Policy")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/loadingText.vue?vue&type=template&id=9b21feec&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/partials/loadingText.vue?vue&type=template&id=9b21feec& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [_vm.loading ? _c("loader") : _c("span", [_vm._v(_vm._s(_vm.text))])],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/partials/authModals.vue":
/*!**********************************************!*\
  !*** ./resources/js/partials/authModals.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _authModals_vue_vue_type_template_id_5f70b3bd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./authModals.vue?vue&type=template&id=5f70b3bd& */ "./resources/js/partials/authModals.vue?vue&type=template&id=5f70b3bd&");
/* harmony import */ var _authModals_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authModals.vue?vue&type=script&lang=js& */ "./resources/js/partials/authModals.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _authModals_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _authModals_vue_vue_type_template_id_5f70b3bd___WEBPACK_IMPORTED_MODULE_0__["render"],
  _authModals_vue_vue_type_template_id_5f70b3bd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/partials/authModals.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/partials/authModals.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/partials/authModals.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_authModals_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./authModals.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/authModals.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_authModals_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/partials/authModals.vue?vue&type=template&id=5f70b3bd&":
/*!*****************************************************************************!*\
  !*** ./resources/js/partials/authModals.vue?vue&type=template&id=5f70b3bd& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_authModals_vue_vue_type_template_id_5f70b3bd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./authModals.vue?vue&type=template&id=5f70b3bd& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/authModals.vue?vue&type=template&id=5f70b3bd&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_authModals_vue_vue_type_template_id_5f70b3bd___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_authModals_vue_vue_type_template_id_5f70b3bd___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/partials/loadingText.vue":
/*!***********************************************!*\
  !*** ./resources/js/partials/loadingText.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loadingText_vue_vue_type_template_id_9b21feec___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loadingText.vue?vue&type=template&id=9b21feec& */ "./resources/js/partials/loadingText.vue?vue&type=template&id=9b21feec&");
/* harmony import */ var _loadingText_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loadingText.vue?vue&type=script&lang=js& */ "./resources/js/partials/loadingText.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _loadingText_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _loadingText_vue_vue_type_template_id_9b21feec___WEBPACK_IMPORTED_MODULE_0__["render"],
  _loadingText_vue_vue_type_template_id_9b21feec___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/partials/loadingText.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/partials/loadingText.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/partials/loadingText.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingText_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./loadingText.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/loadingText.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingText_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/partials/loadingText.vue?vue&type=template&id=9b21feec&":
/*!******************************************************************************!*\
  !*** ./resources/js/partials/loadingText.vue?vue&type=template&id=9b21feec& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingText_vue_vue_type_template_id_9b21feec___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./loadingText.vue?vue&type=template&id=9b21feec& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/loadingText.vue?vue&type=template&id=9b21feec&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingText_vue_vue_type_template_id_9b21feec___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingText_vue_vue_type_template_id_9b21feec___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);