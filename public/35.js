(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    main_product: {},
    product: {},
    select_attribute_step: {},
    current_attributes: {},
    attributes: {}
  },
  data: function data() {
    return {
      attribute_step: 0,
      selected_attributes: []
    };
  },
  watch: {
    select_attribute_step: function select_attribute_step() {
      // console.log(this.select_attribute_step , 'select_attribute_step update')
      this.attribute_step = this.select_attribute_step;
    },
    current_attributes: function current_attributes() {
      // console.log(this.selected_attributes , 'selected_attributes update')
      this.selected_attributes = this.current_attributes;
    }
  },
  mounted: function mounted() {
    this.attribute_step = this.select_attribute_step; // console.log(this.current_attributes)

    if (this.current_attributes) {
      this.selected_attributes = this.current_attributes;
    } else {
      this.selected_attributes = [];
    }
  },
  methods: {
    selectAttribute: function selectAttribute(attribute) {
      if (this.attribute_step <= this.attributes.length) {
        var attr = Object.assign({}, {
          id: "",
          tital: this.attributes[this.attribute_step],
          value: attribute,
          image: "",
          seletecvalue: attribute
        }); // console.log(attribute)

        if (this.selected_attributes[this.attribute_step]) {
          this.selected_attributes[this.attribute_step] = attr;
        } else {
          // this.selected_attributes.push(attr);
          this.$set(this.selected_attributes, this.selected_attributes.length, attr); // this.selected_attributes.push(attr);
        }

        this.attribute_step = this.attribute_step + 1;
      }

      if (this.attribute_step == this.attributes.length) {
        this.$emit('product-attribute-selected', this.selected_attributes, true); // console.log('emitting' , this.selected_attributes )
      }
    },
    prevAttribute: function prevAttribute() {
      if (this.attribute_step > 0) {
        this.attribute_step = this.attribute_step - 1;
      } else {}
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "attribute-top" }, [
        _c("div", { staticClass: "bz-text-red text-center" }, [
          _vm.attribute_step > 0
            ? _c("i", {
                staticClass: "fas fa-arrow-left float-left",
                on: { click: _vm.prevAttribute }
              })
            : _vm._e(),
          _vm._v(
            "\r\n            " +
              _vm._s(_vm.attributes[_vm.attribute_step]) +
              " \r\n        "
          )
        ])
      ]),
      _vm._v(" "),
      _vm._l(_vm.product[_vm.attributes[_vm.attribute_step]], function(
        attribute,
        index
      ) {
        return _c(
          "div",
          {
            key: "sub-attr" + index,
            staticClass: "single-item",
            class: {
              selected: _vm.selected_attributes[_vm.attribute_step]
                ? attribute ==
                  _vm.selected_attributes[_vm.attribute_step].seletecvalue
                : false
            },
            on: {
              click: function($event) {
                return _vm.selectAttribute(attribute)
              }
            }
          },
          [
            _c("div", { staticClass: "single-list-item-icon-container" }),
            _vm._v(" "),
            _c("div", { staticClass: "single-list-item-name" }, [
              _vm._v(" " + _vm._s(attribute) + " ")
            ]),
            _vm._v(" "),
            _c("i", { staticClass: "fas fa-angle-right" })
          ]
        )
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/post-ad/product-attributes.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/post-ad/product-attributes.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product-attributes.vue?vue&type=template&id=f7a9dd02& */ "./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&");
/* harmony import */ var _product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-attributes.vue?vue&type=script&lang=js& */ "./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/post-ad/product-attributes.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./product-attributes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./product-attributes.vue?vue&type=template&id=f7a9dd02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);