(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-offer-ads.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/my-offer-ads.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../partials/loader.vue */ "./resources/js/partials/loader.vue");
/* harmony import */ var _partials_loadingSingleAd_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./partials/loadingSingleAd.vue */ "./resources/js/user/dashboard/partials/loadingSingleAd.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    LoadingSingleAd: _partials_loadingSingleAd_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  mounted: function mounted() {
    this.loadMyAds(); // this.loadRecentViewedAds()
  },
  data: function data() {
    return {
      loading: false,
      no_more_ads: false,
      fav_loading: '',
      ads: [],
      index: 1
    };
  },
  methods: {
    loadMyAds: function loadMyAds() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/my_offerads', {
        // index: this.index,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        _this.loading = false;

        if (res.data.status == 'success') {
          var _this$ads;

          _this.index = _this.index + 1;

          (_this$ads = _this.ads).push.apply(_this$ads, _toConsumableArray(res.data.data));
        } else if (res.data.message == "No data") {
          _this.no_more_ads = true;
        } else {
          console.log(res);
          _this.loading = false;

          _this.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    loadRecentViewedAds: function loadRecentViewedAds() {
      var _this2 = this;

      this.loading = true;
      axios.post('/v1/recent_view', {
        index: this.ads.length,
        // index: this.index,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        _this2.loading = false;
        console.log(res.data.message);

        if (res.data.status == 'success') {
          var _this2$ads;

          (_this2$ads = _this2.ads).push.apply(_this2$ads, _toConsumableArray(res.data.data));

          _this2.index = _this2.index + 1;
        } else if (res.data.message == "No data") {
          _this2.no_more_ads = true;
        } else {
          console.log(res); // this.loading = false
          // this.$toastr.error( res.data.message );
        }
      })["catch"](function (err) {
        _this2.loading = false;

        _this2.$toastr.error('An unexpected error occurred');
      });
    },
    loadMoreAds: function loadMoreAds() {
      if (!this.loading) {
        this.loadMyAds();
      }
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    } // favoriteAd(ad){
    //     this.fav_loading = ad
    //     axios.post('/v1/ad_view', {
    //         post_id: ad.id,
    //         user_id: this.$store.getters.isAuthenticated.id,
    //         type: 'favorite'
    //     },{
    //         headers:{
    //             Authorization: "Bearer "+ this.$store.getters.isAuthenticated.token
    //         }
    //     }).then (res => {
    //     // console.log(res.data)
    //     this.fav_loading = ''
    //     if(res.data.data.favorite ==  '1'){
    //         ad.fav_stattus = "Yes"
    //     }else{
    //         ad.fav_stattus = "No"
    //     }
    //     }).catch( err => {
    //         this.fav_loading = ''
    //         this.$toastr.error('An unexpected error occurred');
    //     })
    // },

  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-offer-ads.vue?vue&type=template&id=4f4f08a2&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/my-offer-ads.vue?vue&type=template&id=4f4f08a2& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid user-dashboard" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-lg-3 col-md-3 col-sm-12 col-xs-12 desktop-only" },
        [_c("left-dashboard")],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-9 col-md-9 col-sm-12 col-xs-12 profile-form-container"
        },
        [
          _c("div", { staticClass: "dashboard" }, [
            _c("div", { staticClass: "bz-dashboard-page-header" }, [
              _c("div", { staticClass: "bz-page-header-title text-center" }, [
                _vm._v(
                  "\r\n                    My Offer Ads (" +
                    _vm._s(_vm.$store.getters.getUserStats.bidcount) +
                    ")\r\n                "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "small-text" })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "dashboard" }, [
              _c(
                "div",
                { staticClass: "ads-listing" },
                [
                  _vm._l(_vm.ads, function(ad) {
                    return _c("single-ad", { key: ad.id, attrs: { _ad: ad } })
                  }),
                  _vm._v(" "),
                  _vm.loading
                    ? _c(
                        "div",
                        _vm._l(4, function(index) {
                          return _c("loading-single-ad", { key: index })
                        }),
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  !_vm.loading && _vm.ads.length == 0
                    ? _c("no-data-box", {
                        attrs: {
                          message: "No Ads Found",
                          icon: "/bazar/dashboard-icons/advertising.png"
                        }
                      })
                    : _vm._e()
                ],
                2
              )
            ])
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=template&id=4d08e83b&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=template&id=4d08e83b& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "ad-list-item" }, [
    _c(
      "div",
      { staticClass: "ad-list-item-main-container d-block p-2" },
      [
        _c("b-skeleton", { attrs: { animation: "wave", width: "85%" } }),
        _vm._v(" "),
        _c("b-skeleton", { attrs: { animation: "wave", width: "55%" } }),
        _vm._v(" "),
        _c("b-skeleton", { attrs: { animation: "wave", width: "70%" } })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/my-offer-ads.vue":
/*!******************************************************!*\
  !*** ./resources/js/user/dashboard/my-offer-ads.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _my_offer_ads_vue_vue_type_template_id_4f4f08a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./my-offer-ads.vue?vue&type=template&id=4f4f08a2& */ "./resources/js/user/dashboard/my-offer-ads.vue?vue&type=template&id=4f4f08a2&");
/* harmony import */ var _my_offer_ads_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./my-offer-ads.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/my-offer-ads.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _my_offer_ads_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _my_offer_ads_vue_vue_type_template_id_4f4f08a2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _my_offer_ads_vue_vue_type_template_id_4f4f08a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/my-offer-ads.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/my-offer-ads.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/user/dashboard/my-offer-ads.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_my_offer_ads_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./my-offer-ads.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-offer-ads.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_my_offer_ads_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/my-offer-ads.vue?vue&type=template&id=4f4f08a2&":
/*!*************************************************************************************!*\
  !*** ./resources/js/user/dashboard/my-offer-ads.vue?vue&type=template&id=4f4f08a2& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_my_offer_ads_vue_vue_type_template_id_4f4f08a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./my-offer-ads.vue?vue&type=template&id=4f4f08a2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-offer-ads.vue?vue&type=template&id=4f4f08a2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_my_offer_ads_vue_vue_type_template_id_4f4f08a2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_my_offer_ads_vue_vue_type_template_id_4f4f08a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/user/dashboard/partials/loadingSingleAd.vue":
/*!******************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/loadingSingleAd.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loadingSingleAd_vue_vue_type_template_id_4d08e83b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loadingSingleAd.vue?vue&type=template&id=4d08e83b& */ "./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=template&id=4d08e83b&");
/* harmony import */ var _loadingSingleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loadingSingleAd.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _loadingSingleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _loadingSingleAd_vue_vue_type_template_id_4d08e83b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _loadingSingleAd_vue_vue_type_template_id_4d08e83b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/partials/loadingSingleAd.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./loadingSingleAd.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=template&id=4d08e83b&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=template&id=4d08e83b& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleAd_vue_vue_type_template_id_4d08e83b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./loadingSingleAd.vue?vue&type=template&id=4d08e83b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/loadingSingleAd.vue?vue&type=template&id=4d08e83b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleAd_vue_vue_type_template_id_4d08e83b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleAd_vue_vue_type_template_id_4d08e83b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);