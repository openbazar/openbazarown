(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/post-ad.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! quill/dist/quill.core.css */ "./node_modules/quill/dist/quill.core.css");
/* harmony import */ var quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_core_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! quill/dist/quill.snow.css */ "./node_modules/quill/dist/quill.snow.css");
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! quill/dist/quill.bubble.css */ "./node_modules/quill/dist/quill.bubble.css");
/* harmony import */ var quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_bubble_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-quill-editor */ "./node_modules/vue-quill-editor/dist/vue-quill-editor.js");
/* harmony import */ var vue_quill_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    quillEditor: vue_quill_editor__WEBPACK_IMPORTED_MODULE_3__["quillEditor"]
  },
  data: function data() {
    return {
      ad: '',
      country: "France",
      city: this.$root.cities[0],
      title: '',
      description: '',
      phone: '',
      price: '',
      price_type: 'Price',
      price_types: ['Price', 'Ask For Price', 'Exchange', 'Free'],
      images: [],
      uploaded_images: [],
      submitting: false,
      category: '',
      countries: [{
        id: 1,
        name: 'Belgium',
        flag: '/bazar/belgium.png'
      }, {
        id: 2,
        name: 'France',
        flag: '/bazar/france.png'
      }, {
        id: 3,
        name: 'Germany',
        flag: '/bazar/germany.png'
      }, {
        id: 4,
        name: 'India',
        flag: '/bazar/india.png'
      }, {
        id: 5,
        name: 'Netherlands',
        flag: '/bazar/netherlands.png'
      }, {
        id: 6,
        name: 'Poland',
        flag: '/bazar/poland.png'
      }, {
        id: 7,
        name: 'Spain',
        flag: '/bazar/spain.png'
      }],
      attributes: {
        'ref': 'openIndicator',
        'role': 'presentation',
        'class': 'vs__open-indicator'
      },
      main_categories: [{
        name: 'Sell',
        id: 1,
        image: 'sell1.png'
      }, {
        name: 'Rent',
        id: 2,
        image: 'rent1.png'
      }, {
        name: 'Services',
        id: 3,
        image: 'services1.png'
      }, {
        name: 'Shops',
        id: 4,
        image: 'shop1.png'
      }],
      options: {
        modules: {
          toolbarOptions: [['bold', 'italic', 'underline', 'strike'], // toggled buttons
          ['blockquote', 'code-block'], [{
            'list': 'ordered'
          }, {
            'list': 'bullet'
          }], [{
            'header': [1, 2, 3, 4, 5, 6, false]
          }], [{
            'align': []
          }]]
        }
      }
    };
  },
  watch: {
    $route: function $route(to, from) {
      console.log(to, from);
      this.loadAdDetails(); // react to route changes...
    },
    category: function category() {
      if (this.category && this.category.attributes && this.category.attributes.length) {
        this.category.attributes.forEach(function (category) {
          if (category.id) {
            category.id = category.id.toString();
          }
        });
        console.log("this.category.attributes", this.category.attributes);
      }
    }
  },
  computed: {// user_cities(){
    //     var all_cities = []
    //     this.cities.map( city => {
    //         console.log(city)
    //         all_cities.push( city)
    //     })
    //     return all_cities;
    // }
  },
  mounted: function mounted() {
    if (this.$route.params.id) {
      this.loadAdDetails();
    } else {
      this.phone = this.$store.getters.isAuthenticated.phone_no;
    }
  },
  methods: {
    loadAdDetails: function loadAdDetails() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/getpost_detail', {
        id: this.$route.params.id,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        // console.log(res.data)
        _this.loading = false;

        if (res.data.status == 'success') {
          console.log(res.data.data[0]);

          _this.fillForm(res.data.data[0]);
        } else {}
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    handleCategories: function handleCategories(value) {
      console.log('handleCategories', value);
      this.category = value; // this.category.main_category = $value.main_category
      // this.category.sub_category = $value.sub_category
    },
    createAd: function createAd() {
      var _this2 = this;

      if (this.isValid()) {
        console.log(this.category);
        this.submitting = true;
        axios.post('/v1/insert_ads', {
          user_id: this.$store.getters.isAuthenticated.id,
          ad_title: this.title,
          // ad_description: this.description.replace(/(<([^>]+)>)/ig,""),
          ad_description: this.stripTags(this.description),
          country_id: this.$store.getters.currentCountry.id,
          mobile: this.phone,
          price: this.price,
          price_type: this.price_type.toLowerCase(),
          category_id: this.category.category.id,
          mobile_verify: 'true',
          city: this.city.city_name,
          timestamp: new Date().getTime(),
          main_category: this.category.main_category.id,
          category: JSON.stringify(this.category.attributes),
          images: this.images.length > 0 ? JSON.stringify(this.images) : null
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          console.log(res.data);

          if (res.data.status == 'success') {
            _this2.$router.push('/product/' + res.data.data.id + '/view');

            _this2.submitting = false;
          } else {
            console.log(res);
            _this2.submitting = false;

            _this2.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this2.submitting = false;

          _this2.$toastr.error('An unexpected error occurred');
        });
      }
    },
    updateAd: function updateAd() {
      var _this3 = this;

      if (this.isValid()) {
        this.submitting = true;
        axios.post('/v1/update_ads', {
          id: this.ad.id,
          user_id: this.$store.getters.isAuthenticated.id,
          ad_title: this.title,
          ad_description: this.stripTags(this.description),
          country_id: this.$store.getters.currentCountry.id,
          mobile: this.phone,
          price: this.price,
          price_type: this.price_type.toLowerCase(),
          category_id: this.category.category.id,
          mobile_verify: 'true',
          city: this.city.city_name,
          timestamp: new Date().getTime(),
          main_category: this.category.main_category.id,
          category: JSON.stringify(this.category.attributes),
          images: this.images.length > 0 ? JSON.stringify(this.images) : null
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          console.log(res.data);

          if (res.data.status == 'success') {
            // this.$router.push('/product/'+res.data.data.id + '/view');
            _this3.submitting = false;

            _this3.$router.push('/product/' + _this3.ad.id + '/view');
          } else {
            console.log(res);
            _this3.submitting = false;

            _this3.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this3.submitting = false;

          _this3.$toastr.error('An unexpected error occurred');
        });
      }
    },
    stripTags: function stripTags(string) {
      var regex = /(&nbsp;|<([^>]+)>)/ig;
      var result = string.replace(regex, "");
      console.log("result", result);
      return result;
    },
    handleUploadedImages: function handleUploadedImages(image) {
      this.images.push({
        image: image.file
      });
      this.uploaded_images.push(image);
    },
    imageRemoved: function imageRemoved($index) {
      this.images.splice($index, 1);
      this.uploaded_images.splice($index, 1);
    },
    isValid: function isValid() {
      if (this.title && this.description && this.phone && this.price_type && this.city && this.category && this.category.attributes && this.category.main_category) {
        return true;
      } else {
        this.$toastr.error('Required fields are missing');
      }
    },
    fillForm: function fillForm(ad) {
      console.log(ad);
      this.ad = ad;
      this.title = ad.ad_title;
      this.description = ad.ad_description;
      this.price = ad.price;
      this.price_type = this.capitalizeWords(ad.price_type);
      this.phone = ad.mobile; // this.category = JSON.parse(ad.category)

      console.log(ad.main_category - 1, 'cat id', this.main_categories[0]);
      var ad_category = {
        main_category: this.main_categories[ad.main_category - 1],
        category: JSON.parse(ad.category),
        attributes: JSON.parse(ad.category)
      };
      this.category = ad_category; // console.log(this.category , JSON.parse(ad.category))

      if (ad.images) {
        this.images = JSON.parse(ad.images);
      }
    },
    capitalizeWords: function capitalizeWords(string) {
      return string.replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.city-select-container{\r\n    position: relative;\n}\n.city-select{\r\n    width: 100% !important;\r\n    border: 1px solid #ced4da;\r\n    background-color: white;\r\n    border-radius: 5px;\r\n    padding: 4px 0;\r\n    padding-left: 25px;\n}\n.city-select-post-ad img{\r\n    width: 15px;\n}\n.city-select-post-ad{\r\n    position: absolute;\r\n    z-index: 4;\r\n    top: 9px;\r\n    left: 10px;\n}\n.price-select{\r\n    width: 100% !important;\r\n    border: 1px solid #ced4da;\r\n    background-color: white;\r\n    border-radius: 5px;\r\n    padding: 4px 6px;\n}\n.ql-container.ql-snow{\r\n    border: 1px solid #ced4da;\n}\n.ql-toolbar.ql-snow{\r\n    border: 1px solid #ced4da;\n}\r\n\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./post-ad.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=template&id=7f0eccc3&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/post-ad.vue?vue&type=template&id=7f0eccc3& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container post-ad", attrs: { id: "app" } }, [
    _c("div", { staticClass: "bz-page-header" }, [
      _c("div", { staticClass: "bz-page-header-title" }, [
        !_vm.ad ? _c("span", [_vm._v("Post Your Free Ad")]) : _vm._e(),
        _vm._v(" "),
        _vm.ad ? _c("span", [_vm._v("Edit Your Ad")]) : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "small-text" }, [
        _vm._v(" Required fields are marked with an asterik (*) ")
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "post-ad-body" },
      [
        _vm._m(0),
        _vm._v(" "),
        _c("ad-post-gallery-section", {
          attrs: { photos: _vm.ad.images },
          on: {
            "image-uploaded": _vm.handleUploadedImages,
            "image-removed": _vm.imageRemoved,
            "uploading-status": function($event) {
              _vm.submitting = $event
            },
            "uploading-images-started": function($event) {
              _vm.submitting = true
            },
            "uploading-images-completed": function($event) {
              _vm.submitting = false
            }
          }
        }),
        _vm._v(" "),
        _c("categories-section", {
          attrs: {
            ad_category: _vm.ad.category,
            main_category: _vm.ad.main_category
          },
          on: { "meta-information-selected": _vm.handleCategories }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "ad-title-section ad-section" }, [
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-sm-12" }, [
              _c("div", { staticClass: "input-group ad-title-input " }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.title,
                      expression: "title"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: {
                    type: "text",
                    placeholder: "* Ad Title ...",
                    "aria-describedby": "max-length",
                    maxlength: "60"
                  },
                  domProps: { value: _vm.title },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.title = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _vm._m(2)
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "ad-title-section ad-section" }, [
          _vm._m(3),
          _vm._v(" "),
          _vm._m(4),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-md-6 col-sm-12" },
              [
                _c("quill-editor", {
                  staticStyle: { height: "170px", "margin-bottom": "45px" },
                  model: {
                    value: _vm.description,
                    callback: function($$v) {
                      _vm.description = $$v
                    },
                    expression: "description"
                  }
                })
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "ad-price-section ad-section" }, [
          _c("div", { staticClass: "section-heading mb-3" }, [
            _vm._v("Price Type ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row mt-2" }, [
            _c("div", { staticClass: "col-md-6 col-sm-12" }, [
              _c(
                "div",
                { staticClass: "input-group ad-price-input " },
                [
                  _c("v-select", {
                    staticClass: "price-select",
                    attrs: {
                      clearable: false,
                      options: _vm.price_types,
                      searchable: false,
                      label: "name"
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "open-indicator",
                        fn: function(ref) {
                          var attributes = ref.attributes
                          return [
                            _c(
                              "span",
                              _vm._b(
                                { staticClass: "select-open-indicator" },
                                "span",
                                attributes,
                                false
                              ),
                              [
                                _c("img", {
                                  attrs: { src: "/bazar/down-arrow (1).png" }
                                })
                              ]
                            )
                          ]
                        }
                      },
                      {
                        key: "selected-option",
                        fn: function(ref) {
                          var name = ref.name
                          return [
                            _c("div", { staticClass: "country-icon" }, [
                              _c("span", [
                                _vm._v(
                                  "\n                                    " +
                                    _vm._s(name) +
                                    "\n                                "
                                )
                              ])
                            ])
                          ]
                        }
                      }
                    ]),
                    model: {
                      value: _vm.price_type,
                      callback: function($$v) {
                        _vm.price_type = $$v
                      },
                      expression: "price_type"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.price_type == "Price"
                ? _c(
                    "div",
                    { staticClass: "input-group ad-price-input mt-3" },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.price,
                            expression: "price"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          placeholder: "* Enter Amount",
                          "aria-describedby": "max-length",
                          maxlength: "10"
                        },
                        domProps: { value: _vm.price },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.price = $event.target.value
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm._m(5)
                    ]
                  )
                : _vm._e()
            ]),
            _vm._v(" "),
            _vm._m(6)
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "ad-phone-number-section ad-section" }, [
          _c("div", { staticClass: "section-heading mb-3" }, [
            _vm._v("Phone Number ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-sm-12" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.phone,
                    expression: "phone"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  placeholder: "* Phone Number ...",
                  "aria-describedby": "max-length",
                  maxlength: "15"
                },
                domProps: { value: _vm.phone },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.phone = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _vm._m(7)
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "ad-city-section ad-section" }, [
          _c("div", { staticClass: "section-heading mb-3" }, [
            _vm._v("Select City ")
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6 col-sm-12" }, [
              _c(
                "div",
                { staticClass: "city-select-container" },
                [
                  _vm._m(8),
                  _vm._v(" "),
                  _c("v-select", {
                    staticClass: "city-select",
                    attrs: {
                      clearable: false,
                      options: _vm.$root.cities,
                      searchable: true,
                      label: "city_name"
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "open-indicator",
                        fn: function(ref) {
                          var attributes = ref.attributes
                          return [
                            _c(
                              "span",
                              _vm._b(
                                { staticClass: "select-open-indicator" },
                                "span",
                                attributes,
                                false
                              ),
                              [
                                _c("img", {
                                  attrs: { src: "/bazar/down-arrow (1).png" }
                                })
                              ]
                            )
                          ]
                        }
                      }
                    ]),
                    model: {
                      value: _vm.city,
                      callback: function($$v) {
                        _vm.city = $$v
                      },
                      expression: "city"
                    }
                  })
                ],
                1
              )
            ]),
            _vm._v(" "),
            _vm._m(9)
          ])
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 text-center" }, [
        !_vm.ad
          ? _c(
              "button",
              {
                staticClass: "submit-button",
                attrs: { href: "#", disabled: _vm.submitting },
                on: { click: _vm.createAd }
              },
              [
                _vm.submitting
                  ? _c(
                      "span",
                      [
                        _c("loader", {
                          attrs: { small: false, border_width: "3px" }
                        })
                      ],
                      1
                    )
                  : _c("span", [_vm._v("Save and submit your ad")])
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.ad
          ? _c(
              "button",
              {
                staticClass: "submit-button",
                attrs: { href: "#", disabled: _vm.submitting },
                on: { click: _vm.updateAd }
              },
              [
                _vm.submitting
                  ? _c("span", [_c("loader", { attrs: { small: false } })], 1)
                  : _c("span", [_vm._v("Save and update your ad")])
              ]
            )
          : _vm._e(),
        _vm._v(" "),
        !_vm.ad
          ? _c(
              "div",
              { staticClass: "agreement-text" },
              [
                _vm._v("By Submitting your Ad you agree to our "),
                _c("router-link", { attrs: { to: "/terms-of-use" } }, [
                  _vm._v(" Terms and conditions ")
                ])
              ],
              1
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "ad-section" }, [
      _c("div", { staticClass: "section-heading-main " }, [
        _vm._v("Ad Details")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "section-heading mb-3" }, [
      _vm._v("Ad Title "),
      _c("div", { staticClass: "small-text" }, [
        _vm._v("(Eg: Samsung S9 Black New For Sell)")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c(
        "span",
        { staticClass: "input-group-text", attrs: { id: "max-length" } },
        [_vm._v("60")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mandatory-field" }, [
      _c("i", { staticClass: "fa fa-asterisk" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "section-heading mb-3" }, [
      _vm._v("Ad Description "),
      _c("div", { staticClass: "small-text" }, [
        _vm._v("(Eg: Add More Details)")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("span", { staticClass: "left-seprate" }),
      _vm._v(" "),
      _c(
        "span",
        { staticClass: "input-group-text", attrs: { id: "max-length" } },
        [_vm._v("Euros")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-sm-12" }, [
      _c("div", { staticClass: "instruction-text-col mt-2" }, [
        _vm._v("Select one of the 4 price types")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-sm-12" }, [
      _c("div", { staticClass: "instruction-text-col" }, [
        _vm._v("Your call settings will be saved for the next time."),
        _c("br"),
        _vm._v("You can change the settings at any time")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "city-select-post-ad" }, [
      _c("img", { attrs: { src: "/bazar/location.png" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 col-sm-12" }, [
      _c("div", { staticClass: "instruction-text-col" }, [
        _vm._v(
          "Select your city to help other users to find your ads easier and faster"
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/products/post-ad.vue":
/*!*******************************************!*\
  !*** ./resources/js/products/post-ad.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _post_ad_vue_vue_type_template_id_7f0eccc3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./post-ad.vue?vue&type=template&id=7f0eccc3& */ "./resources/js/products/post-ad.vue?vue&type=template&id=7f0eccc3&");
/* harmony import */ var _post_ad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./post-ad.vue?vue&type=script&lang=js& */ "./resources/js/products/post-ad.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _post_ad_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./post-ad.vue?vue&type=style&index=0&lang=css& */ "./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _post_ad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _post_ad_vue_vue_type_template_id_7f0eccc3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _post_ad_vue_vue_type_template_id_7f0eccc3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/post-ad.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/post-ad.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/products/post-ad.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./post-ad.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************!*\
  !*** ./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./post-ad.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/products/post-ad.vue?vue&type=template&id=7f0eccc3&":
/*!**************************************************************************!*\
  !*** ./resources/js/products/post-ad.vue?vue&type=template&id=7f0eccc3& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_template_id_7f0eccc3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./post-ad.vue?vue&type=template&id=7f0eccc3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/post-ad.vue?vue&type=template&id=7f0eccc3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_template_id_7f0eccc3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_post_ad_vue_vue_type_template_id_7f0eccc3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);