(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/CategoriesSection.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/CategoriesSection.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  computed: {
    filteredCategories: function filteredCategories() {
      var self = this;

      if (this.categories.length) {
        return this.categories.filter(function (cat) {
          return cat.en_name.toLowerCase().indexOf(self.search.toLowerCase()) >= 0;
        });
      } else {
        return [];
      }
    },
    filteredSubCategories: function filteredSubCategories() {
      var self = this;

      if (this.sub_categories.length) {
        return this.sub_categories.filter(function (cat) {
          return cat.en_name.toLowerCase().indexOf(self.search.toLowerCase()) >= 0;
        });
      } else {
        return [];
      }
    },
    _selected_product: function _selected_product() {
      if (this.selected_product) {
        var main_product = JSON.parse(this.selected_product.product);
        var main_keys = Object.keys(JSON.parse(this.selected_product.product));
        var _product = {
          id: '',
          image: main_product[main_keys[1]],
          tital: main_product[main_keys[0]],
          value: main_keys[2],
          seletecvalue: main_keys[2]
        };
        return _product;
      }

      return false;
    }
  },
  props: {
    already_selected: {
      type: Boolean,
      "default": false
    },
    main_category: {},
    ad_category: {// type: String,
      // default: '',
    }
  },
  watch: {
    ad_category: function ad_category() {
      this.fillCurrentCategory(); // this.getCategories()
      // this.getSubCategories()
      // this.selectCurrentDelivery();
      // this.selectCurrentCondition();
    },
    main_category: function main_category() {
      this.selected_main_category = this.main_categories[this.main_category - 1];
      this.getCategories();
      this.selectCurrentData();
    }
  },
  mounted: function mounted() {// if(this.ad_category){
    // this.selected_main_category = this.main_categories[0];
    // this.selected_category = this.sub_categories[0];
    // this.selected_condition = 'Used';
    // this.selected_delivery = "Courier";
    // }
  },
  data: function data() {
    return {
      step: 1,
      search: '',
      main_categories: [{
        name: 'Sell',
        id: 1,
        image: 'sell1.png'
      }, {
        name: 'Rent',
        id: 2,
        image: 'rent1.png'
      }, {
        name: 'Services',
        id: 3,
        image: 'services1.png'
      }, {
        name: 'Shops',
        id: 4,
        image: 'shop1.png'
      }],
      selected_main_category: '',
      categories: [],
      selected_category: "",
      sub_categories: [// { name: 'Video Games and Console' , id: 15 , image: 'game-console.png'  },
      ],
      products: [],
      selected_sub_category: "",
      selected_product: "",
      selected_product_attributes: "",
      product_attributes: [],
      attribute_step: 0,
      selected_condition: "",
      selected_delivery: "",
      current_category: '',
      loading_categories: false,
      is_attributes_selected: false,
      loading: false,
      loading_base_item: false
    };
  },
  methods: {
    openCategoriesModal: function openCategoriesModal(category) {
      // this.selected_main_category = category;
      $('#categoriesModal').modal('show');
      this.selected_main_category = category;
      this.getCategories();
      this.step = 2;
    },
    getCategories: function getCategories() {
      var _this = this;

      this.loading_categories = true;
      this.loading = true; // console.log(this.selected_main_category.id)

      axios.post('/v1/get_category', {
        maincategory_id: this.selected_main_category.id
      }).then(function (res) {
        _this.removeCategory();

        _this.removeSubCategory();

        _this.removeProduct();

        _this.loading_categories = false;
        _this.loading = false;
        _this.categories = res.data.data;

        if (_this.ad_category) {
          _this.selectCurrentCategory();
        }
      })["catch"](function (err) {
        _this.loading_categories = false;
        _this.loading = false;
        console.log(err);
      });
    },
    getSubCategories: function getSubCategories() {
      var _this2 = this;

      this.loading_sub_categories = true;
      this.loading = true;
      axios.post('/v1/get_subcategory', {
        id: this.selected_category.id
      }).then(function (res) {
        _this2.removeSubCategory();

        _this2.removeProduct();

        _this2.loading_categories = false;
        _this2.loading = false;
        _this2.sub_categories = res.data.data;

        if (_this2.ad_category) {
          _this2.selectCurrentSubCategory();

          _this2.getSubCategoryProducts();
        }
      })["catch"](function (err) {
        _this2.loading_categories = false;
        _this2.loading = false;
        console.log(err);
      });
    },
    getSubCategoryProducts: function getSubCategoryProducts() {
      var _this3 = this;

      this.loading_sub_categories = true;
      this.loading = true;
      axios.post('/v1/get_productcat_new', {
        id: this.selected_sub_category.id
      }).then(function (res) {
        _this3.loading_categories = false;
        _this3.loading = false;
        _this3.loading_base_item = false;
        _this3.products = res.data.data;

        if (_this3.ad_category) {
          _this3.products.map(function (product, key) {
            var product_keys = Object.keys(JSON.parse(product.product)); // console.log(
            //     JSON.parse(product.product)[product_keys[2]] ,
            //     product_keys[2] ,
            //     this.current_category[2].value  ,
            //     product_keys[2]  == this.current_category[2].value
            //     )
            // console.log(product_keys[2] , this.current_category[2].value , product_keys[2]  == this.current_category[2].value)

            if (product_keys[2] == _this3.current_category[2].value) {
              _this3.selected_product = product;
              _this3.selected_product_attributes = Object.keys(JSON.parse(product.product)[product_keys[2]]);

              _this3.current_category.map(function (attr, index) {
                if (index > 2) {
                  _this3.product_attributes.push(attr);
                }
              });
            } // console.log(product , product_keys[2]  , 'Fetched Products')

          }); // this.selectCurrentProduct();

        } // console.log(JSON.parse(this.products[0].product) , JSON.parse(this.products[0].product)['Aston Martin'] , Object.keys(JSON.parse(this.products[0].product)) );

      })["catch"](function (err) {
        _this3.loading_categories = false;
        _this3.loading = false;
        console.log(err);
        _this3.loading_base_item = false;
      });
    },
    selectCurrentProduct: function selectCurrentProduct(product, key) {
      this.search = ''; // console.log(product , this.selected_product)

      if (product != this.selected_product) {
        console.log('Product Changed');
        this.removeProduct();
        this.selected_product_attributes = '';
        this.selected_product = product;
        this.product_attributes = [];
        this.selected_product_attributes = Object.keys(JSON.parse(product.product)[key]);
      } else {
        console.log('Same Product ');
        this.attribute_step = 0;
      } // console.log(product , key)

    },
    showInConsole: function showInConsole(product) {// this.selected_product = product;
      // this.selected_product_attributes = Object.keys(JSON.parse(product.product)[key]);
      // console.log(product )
    },
    selectMainCategory: function selectMainCategory(category) {
      this.step = 2;
    },
    selectCategory: function selectCategory(category) {
      this.selected_category = category;
      this.search = '';
      this.removeSubCategory();
      this.getSubCategories(); // let ad_category = {
      //     id: "",
      //     tital: "Category",
      //     value: category.en_name,
      //     image: "Category",
      //     seletecvalue: category.en_name,
      // }

      this.product_attributes[0] = category;
      this.step = 3;
    },
    selectSubCategory: function selectSubCategory(category) {
      this.selected_sub_category = category;
      this.search = '';
      this.getSubCategoryProducts();
      this.removeProduct(); // this.selected_product_attributes = ''
      // this.selected_product = ''
      // this.attribute_step = 0

      this.step = 4;
    },
    // selectCondition(condition){
    //     this.selected_condition = condition;
    //     this.step = 5
    // },
    // selectDelivery(delivery){
    //     this.selected_delivery = delivery;
    //     $('#categoriesModal').modal('hide');
    //     // this.step = 4
    //     this.updateParent();
    // },
    changeStep: function changeStep(step) {
      this.step = step;

      if (step == 3) {
        this.removeProduct();
      }
    },
    editStep: function editStep(step) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      $('#categoriesModal').modal('show');
      this.step = step;
      console.log('Edit Step', index);

      if (!index) {
        console.log('no index');
        this.attribute_step = 0;
      } else {
        console.log('index changed');
        this.attribute_step = index;
      }

      if (this.step == 4 && !index) {
        // this.removeProduct()
        this.attribute_step = -1;
      }
    },
    updateParent: function updateParent() {
      var attributes = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      // console.log(attributes)
      this.$emit('meta-information-selected', {
        main_category: this.selected_main_category,
        category: this.selected_category,
        attributes: [{
          id: this.selected_category.id,
          tital: "Category",
          value: this.selected_category.en_name,
          seletecvalue: this.selected_category.en_name,
          image: this.selected_category.image
        }, {
          id: this.selected_sub_category.id,
          tital: "Sub-Category",
          value: this.selected_sub_category.en_name,
          seletecvalue: this.selected_sub_category.en_name,
          image: this.selected_sub_category.image
        }, this._selected_product].concat(_toConsumableArray(attributes))
      });
    },
    fillCurrentCategory: function fillCurrentCategory() {
      this.current_category = JSON.parse(this.ad_category);

      if (this.current_category) {// this.selectMainCategory(this.main_categories[0])
        // this.selectCategory(this.categories[0])
        // this.selectSubCategory(this.sub_categories[0])
        // this.selectCondition('Used')
        // this.selectDelivery('Pick up or Send')
      }

      if (this.ad_category) {
        this.is_attributes_selected = true;
        this.loading_base_item = true;
      }
    },
    selectCurrentCategory: function selectCurrentCategory() {
      var _this4 = this;

      this.categories.map(function (cat) {
        if (parseInt(_this4.current_category[0].id) == cat.id) {
          _this4.selected_category = cat;
        }
      });

      if (this.selected_category) {
        this.getSubCategories();
      }
    },
    selectCurrentSubCategory: function selectCurrentSubCategory() {
      var _this5 = this;

      this.sub_categories.map(function (cat) {
        if (parseInt(_this5.current_category[1].id) == cat.id) {
          _this5.selected_sub_category = cat;
        }
      });
    },
    selectCurrentDelivery: function selectCurrentDelivery() {// this.selected_delivery =
    },
    selectCurrentCondition: function selectCurrentCondition() {},
    selectCurrentData: function selectCurrentData() {// this.current_category.map( item => {
      //     // console.log(item)
      //     if(item.tital == 'Delivery'){
      //         this.selected_delivery = item.seletecvalue
      //     }
      //     if(item.tital == 'Condition'){
      //         this.selected_condition = item.seletecvalue
      //     }
      // })
    },
    handleProductAttributes: function handleProductAttributes(attributes, status) {
      this.is_attributes_selected = status;
      this.product_attributes = attributes;
      this.$set.apply(this, [this.product_attributes, 0].concat(_toConsumableArray(attributes))); // this.product_attributes = attributes

      $('#categoriesModal').modal('hide'); // this.category.push(...attributes);
      // this.product_attributes.push(...attributes)

      this.updateParent(attributes);
    },
    removeCategory: function removeCategory() {
      this.selected_category = '';
      this.removeSubCategory();
    },
    removeSubCategory: function removeSubCategory() {
      console.log('Sub Category Remove');
      this.selected_sub_category = '';
      this.removeProduct();
    },
    removeProduct: function removeProduct() {
      console.log('Product Remove');

      if (this.selected_product) {
        this.selected_product = '';
        this.product_attributes = [];
        this.selected_product_attributes = '';
        this.attribute_step = 0;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/CategoriesSection.vue?vue&type=template&id=06fb9722&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/CategoriesSection.vue?vue&type=template&id=06fb9722& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "post-ad-category-area" },
    [
      _vm.loading_base_item
        ? _c("loader", { staticClass: "bz-text-red", attrs: { small: false } })
        : _vm._e(),
      _vm._v(" "),
      _vm.selected_main_category &&
      _vm.selected_category &&
      _vm.selected_product &&
      _vm.is_attributes_selected &&
      _vm.product_attributes
        ? _c("div", { staticClass: "ad-category-section ad-section" }, [
            _c(
              "div",
              { staticClass: "selected-cateogry" },
              [
                _vm.selected_main_category
                  ? _c(
                      "div",
                      {
                        staticClass: "selected-cateogry-item",
                        on: {
                          click: function($event) {
                            return _vm.editStep(1)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "sub-text" }, [
                          _vm._v("Main Category")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "category-name" }, [
                          _vm._v(
                            "  " + _vm._s(_vm.selected_main_category.name) + " "
                          ),
                          _c("i", { staticClass: "fas fa-angle-right" })
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.selected_category
                  ? _c(
                      "div",
                      {
                        staticClass: "selected-cateogry-item",
                        on: {
                          click: function($event) {
                            return _vm.editStep(2)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "sub-text" }, [
                          _vm._v("Category")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "category-name" }, [
                          _vm._v(
                            "  " + _vm._s(_vm.selected_category.en_name) + " "
                          ),
                          _c("i", { staticClass: "fas fa-angle-right" })
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.selected_sub_category
                  ? _c(
                      "div",
                      {
                        staticClass: "selected-cateogry-item",
                        on: {
                          click: function($event) {
                            return _vm.editStep(3)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "sub-text" }, [
                          _vm._v("Sub Category")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "category-name" }, [
                          _vm._v(
                            "  " +
                              _vm._s(_vm.selected_sub_category.en_name) +
                              " "
                          ),
                          _c("i", { staticClass: "fas fa-angle-right" })
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm._selected_product
                  ? _c(
                      "div",
                      {
                        staticClass: "selected-cateogry-item",
                        on: {
                          click: function($event) {
                            return _vm.editStep(4)
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "sub-text" }, [
                          _vm._v(_vm._s(_vm._selected_product.tital))
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "category-name" }, [
                          _vm._v(
                            "  " + _vm._s(_vm._selected_product.value) + " "
                          ),
                          _c("i", { staticClass: "fas fa-angle-right" })
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm._l(_vm.product_attributes, function(attr, index) {
                  return _c(
                    "div",
                    {
                      key: index,
                      staticClass: "selected-cateogry-item",
                      on: {
                        click: function($event) {
                          return _vm.editStep(4, index)
                        }
                      }
                    },
                    [
                      _c("span", { staticClass: "sub-text" }, [
                        _vm._v(_vm._s(attr.tital))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "category-name" }, [
                        _vm._v("  " + _vm._s(attr.value) + "  "),
                        _c("i", { staticClass: "fas fa-angle-right" })
                      ])
                    ]
                  )
                })
              ],
              2
            )
          ])
        : _c("div", { staticClass: "ad-category-section ad-section" }, [
            _vm._m(0),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "ad-category" }, [
              _c("div", { staticClass: "single-category " }, [
                _c(
                  "a",
                  {
                    attrs: { href: "javascript:;" },
                    on: {
                      click: function($event) {
                        return _vm.openCategoriesModal(_vm.main_categories[0])
                      }
                    }
                  },
                  [_vm._m(2)]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "single-category" }, [
                _c(
                  "a",
                  {
                    attrs: { href: "javascript:;" },
                    on: {
                      click: function($event) {
                        return _vm.openCategoriesModal(_vm.main_categories[1])
                      }
                    }
                  },
                  [_vm._m(3)]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "single-category" }, [
                _c(
                  "a",
                  {
                    attrs: { href: "javascript:;" },
                    on: {
                      click: function($event) {
                        return _vm.openCategoriesModal(_vm.main_categories[2])
                      }
                    }
                  },
                  [_vm._m(4)]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "single-category" }, [
                _c(
                  "a",
                  {
                    attrs: { href: "javascript:;" },
                    on: {
                      click: function($event) {
                        return _vm.openCategoriesModal(_vm.main_categories[3])
                      }
                    }
                  },
                  [_vm._m(5)]
                )
              ])
            ])
          ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "categoriesModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "categoriesModalTitle",
            "aria-hidden": "true",
            "data-backdrop": "static",
            "data-keyboard": "false"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(6),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "modal-body" },
                  [
                    _vm.loading
                      ? _c("loader", {
                          staticClass: "bz-text-red",
                          attrs: { small: false }
                        })
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "selected-category-breadcrumbs" },
                      [
                        _vm.selected_main_category
                          ? _c(
                              "a",
                              {
                                attrs: { href: "javascript:;" },
                                on: {
                                  click: function($event) {
                                    return _vm.changeStep(1)
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(_vm.selected_main_category.name) +
                                    " "
                                ),
                                _c("i", { staticClass: "fas fa-angle-right" })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.selected_category
                          ? _c(
                              "a",
                              {
                                attrs: { href: "javascript:;" },
                                on: {
                                  click: function($event) {
                                    return _vm.changeStep(2)
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(_vm.selected_category.en_name) +
                                    " "
                                ),
                                _c("i", { staticClass: "fas fa-angle-right" })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.selected_sub_category
                          ? _c(
                              "a",
                              {
                                attrs: { href: "javascript:;" },
                                on: {
                                  click: function($event) {
                                    return _vm.changeStep(3)
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(_vm.selected_sub_category.en_name) +
                                    " "
                                ),
                                _c("i", { staticClass: "fas fa-angle-right" })
                              ]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm._selected_product
                          ? _c(
                              "a",
                              {
                                attrs: { href: "javascript:;" },
                                on: {
                                  click: function($event) {
                                    _vm.attribute_step = -1
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  " " +
                                    _vm._s(_vm._selected_product.value) +
                                    " "
                                ),
                                _c("i", { staticClass: "fas fa-angle-right" })
                              ]
                            )
                          : _vm._e()
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.step == 1,
                            expression: "step == 1"
                          }
                        ],
                        staticClass: "main-categories"
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "listing-items" },
                          _vm._l(_vm.main_categories, function(category) {
                            return _c(
                              "div",
                              {
                                key: category.id,
                                staticClass: "single-item",
                                class: {
                                  selected:
                                    _vm.selected_main_category == category
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.selectMainCategory(category)
                                  }
                                }
                              },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "single-list-item-icon-container"
                                  },
                                  [
                                    _c("img", {
                                      staticClass:
                                        "single-list-item-icon main-category",
                                      attrs: { src: "/bazar/" + category.image }
                                    })
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "single-list-item-name" },
                                  [_vm._v(" " + _vm._s(category.name) + " ")]
                                ),
                                _vm._v(" "),
                                _c("i", { staticClass: "fas fa-angle-right" })
                              ]
                            )
                          }),
                          0
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.step == 2,
                            expression: "step == 2"
                          }
                        ],
                        staticClass: "sub-categories"
                      },
                      [
                        _c("div", { staticClass: "search-bar" }, [
                          _c("i", { staticClass: "fas fa-search search-icon" }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.search,
                                expression: "search"
                              }
                            ],
                            attrs: { placeholder: "Search" },
                            domProps: { value: _vm.search },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.search = $event.target.value
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "listing-items" },
                          [
                            _vm._l(_vm.filteredCategories, function(category) {
                              return _c(
                                "div",
                                {
                                  key: category.id,
                                  staticClass: "single-item",
                                  class: {
                                    selected: _vm.selected_category == category
                                  },
                                  on: {
                                    click: function($event) {
                                      return _vm.selectCategory(category)
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "single-list-item-icon-container"
                                    },
                                    [
                                      _c("img", {
                                        staticClass: "single-list-item-icon",
                                        attrs: {
                                          src:
                                            _vm.$root.virtualmin_server +
                                            "public/images/" +
                                            category.image
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "single-list-item-name" },
                                    [
                                      _vm._v(
                                        " " + _vm._s(category.en_name) + " "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("i", { staticClass: "fas fa-angle-right" })
                                ]
                              )
                            }),
                            _vm._v(" "),
                            _vm.filteredCategories.length == 0
                              ? _c(
                                  "div",
                                  { staticClass: "no-categories-message" },
                                  [
                                    _vm._v(
                                      "\r\n                    Not Found\r\n                "
                                    )
                                  ]
                                )
                              : _vm._e()
                          ],
                          2
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.step == 3,
                            expression: "step == 3"
                          }
                        ],
                        staticClass: "sub-categories"
                      },
                      [
                        _c("div", { staticClass: "search-bar" }, [
                          _c("i", { staticClass: "fas fa-search search-icon" }),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.search,
                                expression: "search"
                              }
                            ],
                            attrs: { placeholder: "Search" },
                            domProps: { value: _vm.search },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.search = $event.target.value
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "listing-items" },
                          [
                            _vm._l(_vm.filteredSubCategories, function(
                              category
                            ) {
                              return _c(
                                "div",
                                {
                                  key: category.id,
                                  staticClass: "single-item",
                                  class: {
                                    selected:
                                      _vm.selected_sub_category == category
                                  },
                                  on: {
                                    click: function($event) {
                                      return _vm.selectSubCategory(category)
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "single-list-item-icon-container"
                                    },
                                    [
                                      _c("img", {
                                        staticClass: "single-list-item-icon",
                                        attrs: {
                                          src:
                                            _vm.$root.virtualmin_server +
                                            "public/images/" +
                                            category.image
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "single-list-item-name" },
                                    [
                                      _vm._v(
                                        " " + _vm._s(category.en_name) + " "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("i", { staticClass: "fas fa-angle-right" })
                                ]
                              )
                            }),
                            _vm._v(" "),
                            _vm.filteredSubCategories.length == 0
                              ? _c(
                                  "div",
                                  { staticClass: "no-categories-message" },
                                  [
                                    _vm._v(
                                      "\r\n                    Not Found\r\n                "
                                    )
                                  ]
                                )
                              : _vm._e()
                          ],
                          2
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.step == 4,
                            expression: "step == 4"
                          }
                        ],
                        staticClass: "sub-categories"
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "listing-items" },
                          [
                            _vm._l(_vm.products, function(product) {
                              return _c(
                                "div",
                                {
                                  directives: [
                                    {
                                      name: "show",
                                      rawName: "v-show",
                                      value:
                                        !_vm.selected_product_attributes ||
                                        _vm.attribute_step == -1,
                                      expression:
                                        "!selected_product_attributes || attribute_step == -1"
                                    }
                                  ],
                                  key: product.id,
                                  staticClass: "single-item",
                                  class: {
                                    selected: _vm.selected_product == product
                                  },
                                  on: {
                                    click: function($event) {
                                      _vm.selectCurrentProduct(
                                        product,
                                        Object.keys(
                                          JSON.parse(product.product)
                                        )[2]
                                      )
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "single-list-item-icon-container"
                                    },
                                    [
                                      _c("img", {
                                        staticClass: "single-list-item-icon",
                                        attrs: {
                                          src:
                                            _vm.$root.virtualmin_server +
                                            "public/images/" +
                                            JSON.parse(product.product)[
                                              Object.keys(
                                                JSON.parse(product.product)
                                              )[1]
                                            ]
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "single-list-item-name" },
                                    [
                                      _vm._v(
                                        " " +
                                          _vm._s(
                                            Object.keys(
                                              JSON.parse(product.product)
                                            )[2]
                                          ) +
                                          " "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("i", { staticClass: "fas fa-angle-right" })
                                ]
                              )
                            }),
                            _vm._v(" "),
                            _vm.selected_product_attributes
                              ? _c("product-attributes", {
                                  attrs: {
                                    main_product: _vm.selected_product,
                                    current_attributes: _vm.product_attributes,
                                    select_attribute_step: _vm.attribute_step,
                                    product: JSON.parse(
                                      _vm.selected_product.product
                                    )[
                                      Object.keys(
                                        JSON.parse(_vm.selected_product.product)
                                      )[2]
                                    ],
                                    attributes: _vm.selected_product_attributes
                                  },
                                  on: {
                                    "product-attribute-selected":
                                      _vm.handleProductAttributes
                                  }
                                })
                              : _vm._e()
                          ],
                          2
                        )
                      ]
                    )
                  ],
                  1
                )
              ])
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "mandatory-field" }, [
      _c("i", { staticClass: "fa fa-asterisk" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-6 col-sm-12" }, [
        _c(
          "div",
          { staticClass: "ad-category-section-title section-heading" },
          [_vm._v("What are you advertising ?")]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-6 col-sm-12" }, [
        _c(
          "div",
          {
            staticClass: "instruction-text-col instruction-text-col-categories"
          },
          [_vm._v("Select one of the main categories below")]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/sell1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Sell")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/rent1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Rent")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/services1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Services")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/shop1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Shops")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title w-100" }, [
        _vm._v("What are you advertising ?\r\n            "),
        _c(
          "div",
          {
            staticClass: "float-right mr-3",
            attrs: { "data-dismiss": "modal", "aria-label": "Close" }
          },
          [_c("i", { staticClass: "fas fa-times" })]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/post-ad/CategoriesSection.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/post-ad/CategoriesSection.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CategoriesSection_vue_vue_type_template_id_06fb9722___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CategoriesSection.vue?vue&type=template&id=06fb9722& */ "./resources/js/components/post-ad/CategoriesSection.vue?vue&type=template&id=06fb9722&");
/* harmony import */ var _CategoriesSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CategoriesSection.vue?vue&type=script&lang=js& */ "./resources/js/components/post-ad/CategoriesSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CategoriesSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CategoriesSection_vue_vue_type_template_id_06fb9722___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CategoriesSection_vue_vue_type_template_id_06fb9722___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/post-ad/CategoriesSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/post-ad/CategoriesSection.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/post-ad/CategoriesSection.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CategoriesSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./CategoriesSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/CategoriesSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CategoriesSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/post-ad/CategoriesSection.vue?vue&type=template&id=06fb9722&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/post-ad/CategoriesSection.vue?vue&type=template&id=06fb9722& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CategoriesSection_vue_vue_type_template_id_06fb9722___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./CategoriesSection.vue?vue&type=template&id=06fb9722& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/CategoriesSection.vue?vue&type=template&id=06fb9722&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CategoriesSection_vue_vue_type_template_id_06fb9722___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CategoriesSection_vue_vue_type_template_id_06fb9722___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);