(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProduct.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/loadingSingleProduct.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/products.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _singleProduct_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleProduct.vue */ "./resources/js/products/singleProduct.vue");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _singleProductList_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./singleProductList.vue */ "./resources/js/products/singleProductList.vue");
/* harmony import */ var _loadingSingleProduct_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loadingSingleProduct.vue */ "./resources/js/products/loadingSingleProduct.vue");
/* harmony import */ var _sidebar_filters_new_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sidebar-filters-new.vue */ "./resources/js/products/sidebar-filters-new.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    'single-product': _singleProduct_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_1___default.a,
    SingleProductList: _singleProductList_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    LoadingSingleProduct: _loadingSingleProduct_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    SidebarFiltersNew: _sidebar_filters_new_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  props: {
    search_query: {
      type: Object
    }
  },
  data: function data() {
    return {
      ads: [],
      type: 'all',
      location: '',
      search: '',
      country: '',
      city: '',
      // city: this.$root.cities[0],
      // country: 'Canada',
      loading: false,
      searching: false,
      searching_now: false,
      fullPage: true,
      fullPageLoading: false,
      shouldHideSelected: false,
      index: 1,
      no_more_products: false,
      isLoading: this.loading,
      new_filters: {
        sub_categories: [],
        sub_category: '',
        price_range: ['', ''],
        price_type: [],
        sorted_by: [],
        posted_within: [],
        condition: []
      },
      countries: [{
        id: 1,
        name: 'Belgium',
        flag: '/bazar/belgium.png'
      }, {
        id: 2,
        name: 'France',
        flag: '/bazar/france.png'
      }, {
        id: 3,
        name: 'Germany',
        flag: '/bazar/germany.png'
      }, {
        id: 4,
        name: 'India',
        flag: '/bazar/india.png'
      }, {
        id: 5,
        name: 'Netherlands',
        flag: '/bazar/netherlands.png'
      }, {
        id: 6,
        name: 'Poland',
        flag: '/bazar/poland.png'
      }, {
        id: 7,
        name: 'Spain',
        flag: '/bazar/spain.png'
      }]
    };
  },
  destroyed: function destroyed() {
    this.$destroy();
  },
  created: function created() {
    var _this = this;

    // console.log(this.$route.params)
    this.country = this.$store.getters.currentCountry;
    console.log(this.city, 'City Products');
    this.fetchAds();
    this.$root.$refs.products = this;
    this.$root.$on('products-clicked', function (route) {
      // window.scrollTo(0, 0);
      if (route.path == '/products') {
        _this.search = "";
        _this.city = '';
        _this.searching = false;

        _this.updateNavbar();

        _this.fetchAds(true);
      }

      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    });
  },
  methods: {
    searchInitiated: function searchInitiated() {
      var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      console.log('Sea/rch Initiated', query);

      if (query.search) {
        this.search = query.search;
      }

      if (query.city) {
        this.city = query.city;
      }

      this.searchQuery();
    },
    updateNavbar: function updateNavbar() {
      var search_query = {
        search: this.search,
        city: this.city
      };
      this.$root.$refs.navbar.updateSearchBar(search_query);
    },
    searchQuery: function searchQuery() {
      this.updateNavbar();

      if (this.search || this.city.city_name != 'All Cities') {
        this.searching = true;
        this.searchAds();
      } else {
        this.searching = false;
        this.fetchAds(true);
      }
    },
    fetchAds: function fetchAds() {
      var _this2 = this;

      var reset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

      if (!this.loading) {
        if (reset) {
          console.log('reset');
          this.ads = [];
          this.fullPageLoading = true;
          this.no_more_products = false;
          this.index = 1;
        }

        this.loading = true;
        var customAxios = axios.create({
          headers: {
            'Content-Type': 'application/json'
          }
        });
        customAxios.post('/get_all_post', {
          type: 'all',
          country_id: this.$store.getters.currentCountry.id,
          index: this.index
        }).then(function (res) {
          _this2.loading = false;
          _this2.fullPageLoading = false;

          if (res.data.status == 'success') {
            var _this2$ads;

            (_this2$ads = _this2.ads).push.apply(_this2$ads, _toConsumableArray(res.data.data)); // this.loadAdDetails (res.data.data);


            _this2.index = _this2.index + 1;

            if (res.data.noty) {
              _this2.$store.commit('setNotificationCount', res.data.noty);
            }
          } else if (res.data.status == 'fail') {
            _this2.no_more_products = true;
          } else {
            console.log(res);

            _this2.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this2.loading = false;
          _this2.fullPageLoading = false;
          console.log("error in catch");

          _this2.$toastr.error('An unexpected error occurred');
        });
      }
    },
    loadAdDetails: function loadAdDetails(ads) {
      var _this3 = this;

      ads.map(function (ad) {
        axios.post('/v1/getpost_detail', {
          id: ad.id,
          user_id: _this3.$store.getters.isAuthenticated.id
        }, {
          headers: {
            Authorization: "Bearer " + _this3.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          // console.log(res.data)
          if (res.data.status == 'success') {
            ad.details = res.data.data[0];
          } else {
            console.log(res);

            _this3.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this3.$toastr.error('An unexpected error occurred');
        });
      });
    },
    loadMoreAds: function loadMoreAds() {
      // this.index += 1;
      console.log('loadMoreAds');

      if (!this.loading) {
        this.fetchAds();
      }
    },
    refreshProducts: function refreshProducts() {
      this.index = 1;
      this.ads = [];
      this.no_more_products = false;
      this.city = this.$root.cities[0];
      this.fetchAds();
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    },
    searchAds: function searchAds() {
      var _this4 = this;

      this.loading = true;
      this.ads = [];
      this.fullPageLoading = true;
      this.no_more_products = false;
      this.index = 0;
      var customAxios = axios.create({
        headers: {
          'Content-Type': 'application/json'
        }
      });
      customAxios.post('/search_ads', {
        type: 'all',
        // type: this.type,
        search: this.search,
        city: this.city.city_name,
        index: this.ads.length,
        country_id: this.$store.getters.currentCountry.id
      }).then(function (res) {
        // console.log(res.data)
        _this4.loading = false;
        _this4.fullPageLoading = false;

        if (res.data.status == 'true') {
          _this4.ads = res.data.data;
          window.scrollTo(0, 0);
        } else if (res.data.status == 'fail') {
          _this4.no_more_products = true;
        } else {
          console.log(res); // this.$toastr.error( res.data.message );
        }
      })["catch"](function (err) {
        _this4.loading = false;
        _this4.fullPageLoading = false;

        _this4.$toastr.error('An unexpected error occurred');
      });
    } // updateSelect2Value(country){
    //     this.location = country;
    //     $('.location-select').val(country); // Select the option with a value of '1'
    //     $('.location-select').trigger('change'); // Notify any JS components that the value changed
    // },

  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/singleProduct.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/singleProduct.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    ad: {
      type: Object,
      "default": ''
    },
    no_more_products: {
      type: Boolean,
      "default": false
    },
    searching: {
      type: Boolean,
      "default": false
    },
    index: {
      type: Number,
      "default": 0
    }
  },
  data: function data() {
    return {
      hasIntersected: false
    };
  },
  methods: {
    enter: function enter(entry) {
      // Since we can't use `.once` in render function,
      // we'll need to do that check ourselfs.
      console.log('Intesection Entered');

      if (!this.hasIntersected) {
        console.log('Intesection Entered and fired');
        this.$emit('load-more-ads');
        this.hasIntersected = true;
      }
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/products.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.homepage-ads-in-feed-banners{\n    /* background-color: red; */\n    max-height: 120px !important;\n    height: 120px !important;\n    width: 100%;\n    padding: 0 4px;\n    max-width: 100%;\n    text-align: center !important;\n}\n@media screen and (max-width:550px) {\n.homepage-ads-in-feed-banners{\n        max-height: 320px !important;\n        height: 320px !important;\n}\n}\n\n/* .v-main-products{\n    width: 45%;\n}\n.v-main-products .vs__dropdown-toggle{\n    padding: 0;\n    height: 42px;\n    background-color: #fafafa;\n    border: none;\n}\n@media screen and (max-width:786px) {\n    .v-main-products{\n        width: 100%;\n        margin-bottom: 12px;\n    }\n    .v-main-products .vs__dropdown-toggle{\n        border-radius: 10px;\n    }\n} */\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/products.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./products.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProduct.vue?vue&type=template&id=7f6dfa30&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/loadingSingleProduct.vue?vue&type=template&id=7f6dfa30& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "product-box" }, [
    _c("div", { staticClass: "product" }, [
      _c("a", { attrs: { href: "#" } }, [
        _c(
          "div",
          { staticClass: "feature-image" },
          [
            _c("b-skeleton-img", { attrs: { "no-aspect": "", height: "100%" } })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "loading-product-details" },
        [
          _c("b-skeleton", { attrs: { animation: "wave", width: "85%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "55%" } }),
          _vm._v(" "),
          _c("b-skeleton", { attrs: { animation: "wave", width: "70%" } })
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=template&id=67b320be&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/products.vue?vue&type=template&id=67b320be& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "homepage" },
    [
      _c("loading", {
        attrs: {
          active: _vm.fullPageLoading,
          loader: "dots",
          height: 170,
          width: 170,
          color: "#d11d1d",
          "background-color": "rgb(0 0 0)",
          "lock-scroll": true,
          "is-full-page": true
        },
        on: {
          "update:active": function($event) {
            _vm.fullPageLoading = $event
          }
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "search-bar" }, [
        _c("div", { staticClass: "best-way-text" }, [
          _vm._v("\n            The Best Way to Buy And Sell Locally\n        ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "search-filters" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.search,
                expression: "search"
              }
            ],
            staticClass: "search-input",
            attrs: { placeholder: "What are you looking for..." },
            domProps: { value: _vm.search },
            on: {
              keydown: function($event) {
                if (
                  !$event.type.indexOf("key") &&
                  _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                ) {
                  return null
                }
                return _vm.searchQuery($event)
              },
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.search = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "v-select-container" },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("v-select", {
                staticClass:
                  "v-select-for-location v-main-products v-select-location-only",
                attrs: {
                  "hide-selected": true,
                  filter: _vm.$root.citySearch,
                  clearable: false,
                  options: _vm.$root.cities,
                  label: "city_name",
                  placeholder: "Select City"
                },
                on: {
                  input: _vm.searchQuery,
                  keydown: function($event) {
                    if (
                      !$event.type.indexOf("key") &&
                      _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                    ) {
                      return null
                    }
                    return _vm.searchQuery($event)
                  }
                },
                scopedSlots: _vm._u([
                  {
                    key: "open-indicator",
                    fn: function(ref) {
                      var attributes = ref.attributes
                      return [
                        _c(
                          "span",
                          _vm._b(
                            { staticClass: "select-open-indicator" },
                            "span",
                            attributes,
                            false
                          ),
                          [
                            _c("img", {
                              attrs: { src: "/bazar/down-arrow (1).png" }
                            })
                          ]
                        )
                      ]
                    }
                  }
                ]),
                model: {
                  value: _vm.city,
                  callback: function($$v) {
                    _vm.city = $$v
                  },
                  expression: "city"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "search-button",
              attrs: { href: "javascript:;" },
              on: { click: _vm.searchQuery }
            },
            [
              _vm.searching_now
                ? _c("loader", { staticClass: "bz-text-red" })
                : _vm._e()
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "categories" }, [
          _c("div", { staticClass: "single-category" }, [
            _c(
              "a",
              {
                attrs: { href: "javascript:;" },
                on: {
                  click: function($event) {
                    return _vm.$router.push({
                      name: "Main Category Products",
                      params: { main_category_id: 1 }
                    })
                  }
                }
              },
              [_vm._m(1)]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "single-category" }, [
            _c(
              "a",
              {
                attrs: { href: "javascript:;" },
                on: {
                  click: function($event) {
                    return _vm.$router.push({
                      name: "Main Category Products",
                      params: { main_category_id: 2 }
                    })
                  }
                }
              },
              [_vm._m(2)]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "single-category" }, [
            _c(
              "a",
              {
                attrs: { href: "javascript:;" },
                on: {
                  click: function($event) {
                    return _vm.$router.push({
                      name: "Main Category Products",
                      params: { main_category_id: 3 }
                    })
                  }
                }
              },
              [_vm._m(3)]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "single-category" }, [
            _c(
              "a",
              {
                attrs: { href: "javascript:;" },
                on: {
                  click: function($event) {
                    return _vm.$router.push({
                      name: "Main Category Products",
                      params: { main_category_id: 4 }
                    })
                  }
                }
              },
              [_vm._m(4)]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _vm.$store.getters.getViewOption == "card"
        ? _c(
            "div",
            {
              staticClass: "product-section",
              attrs: { id: "product-section" }
            },
            [
              !_vm.loading && _vm.ads.length == 0
                ? _c("div", { staticClass: "no-ads-message" }, [
                    _vm._v("No Ads Found")
                  ])
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "product-list" },
                [
                  _vm._l(_vm.ads, function(ad, index) {
                    return [
                      index > 0 && index % _vm.$root.numberOfAdsForAdsense === 0
                        ? _c("Adsense", {
                            key: index * index + index,
                            staticStyle: {
                              border: "0pt none",
                              display: "inline-block",
                              width: "100%"
                            },
                            attrs: {
                              "data-ad-client": "ca-pub-2462277409343800",
                              "data-ad-slot": "6925040421",
                              "is-new-ads-code": "yes",
                              "ins-class": "homepage-ads-in-feed-banners"
                            }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("single-product", {
                        key: ad.id,
                        attrs: {
                          no_more_products: _vm.no_more_products,
                          searching: _vm.searching,
                          ad: ad,
                          index: index
                        },
                        on: { "load-more-ads": _vm.loadMoreAds }
                      })
                    ]
                  }),
                  _vm._v(" "),
                  _vm._l(8, function(index) {
                    return _c("loading-single-product", {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.loading,
                          expression: "loading"
                        }
                      ],
                      key: index
                    })
                  })
                ],
                2
              )
            ]
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "abs-location-icon-main-products" }, [
      _c("img", { attrs: { src: "/bazar/location.png" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/sell1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Sell")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/rent1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Rent")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/services1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Services")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-category-content" }, [
      _c("img", { attrs: { src: "/bazar/shop1.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "single-category-name" }, [_vm._v("Shops")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/singleProduct.vue?vue&type=template&id=6a63cfcc&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/singleProduct.vue?vue&type=template&id=6a63cfcc& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "product-box" },
    [
      _c(
        "div",
        { staticClass: "product" },
        [
          _c(
            "router-link",
            {
              attrs: {
                to: {
                  name: "view-product",
                  params: { id: _vm.ad.id, base_ad: _vm.ad }
                }
              }
            },
            [
              _c("div", { staticClass: "feature-image" }, [
                JSON.parse(_vm.ad.images) && JSON.parse(_vm.ad.images).length
                  ? _c("img", {
                      attrs: {
                        src:
                          _vm.$root.virtualmin_server +
                          "/public/images/" +
                          JSON.parse(_vm.ad.images)[0].image
                      },
                      on: {
                        error: function($event) {
                          $event.target.src = "/bazar/no-image-preview.png"
                        }
                      }
                    })
                  : _c("img", {
                      attrs: { src: "/bazar/no-image-preview.png" }
                    }),
                _vm._v(" "),
                _c("div", { staticClass: "add-upload-time" }, [
                  _vm._v(
                    _vm._s(_vm._f("formatDifferenceDate")(_vm.ad.created_at))
                  )
                ]),
                _vm._v(" "),
                JSON.parse(_vm.ad.images) && JSON.parse(_vm.ad.images).length
                  ? _c("div", {
                      staticClass: "blurred-image",
                      style:
                        "background-image: url(" +
                        _vm.$root.virtualmin_server +
                        "/public/images/" +
                        JSON.parse(_vm.ad.images)[0].image +
                        ")"
                    })
                  : _vm._e()
              ])
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "product-details" }, [
            _vm.ad.boost_type && _vm.ad.boost_type == "0"
              ? _c("img", {
                  staticClass: "boost-image-icon",
                  attrs: { src: "/bazar/icon-1.png" }
                })
              : _vm._e(),
            _vm._v(" "),
            _vm.ad.boost_type && _vm.ad.boost_type == "1"
              ? _c("img", {
                  staticClass: "boost-image-icon",
                  attrs: { src: "/bazar/icon-2.png" }
                })
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "product-meta-box " }, [
              _vm.ad.price_type == "price"
                ? _c("span", { staticClass: "price" }, [
                    _vm._v(
                      _vm._s(_vm.$store.getters.getCuerrency) +
                        " " +
                        _vm._s(_vm.ad.price)
                    )
                  ])
                : _c("span", { staticClass: "price" }, [
                    _vm._v(_vm._s(_vm.ad.price_type))
                  ]),
              _vm._v(" "),
              _c("span", { staticClass: "views" }, [
                _c("i", { staticClass: "fa fa-eye" }),
                _vm._v(" " + _vm._s(_vm.ad.views_count) + " ")
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "product-title " }, [
              _vm._v(_vm._s(_vm.ad.ad_title))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "product-location " }, [
              _c("i", {
                staticClass:
                  "fa fa-map-marker-alt text-primary-red address-icon"
              }),
              _vm._v(" "),
              _c("div", [
                _vm._v(
                  "\n                      " +
                    _vm._s(_vm.ad.city) +
                    "\n                      "
                )
              ])
            ])
          ])
        ],
        1
      ),
      _vm._v(" "),
      !_vm.no_more_products && !_vm.searching && _vm.index % 9 == 0
        ? _c("Intersect", { on: { enter: _vm.enter } }, [_c("div")])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/products/loadingSingleProduct.vue":
/*!********************************************************!*\
  !*** ./resources/js/products/loadingSingleProduct.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loadingSingleProduct_vue_vue_type_template_id_7f6dfa30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loadingSingleProduct.vue?vue&type=template&id=7f6dfa30& */ "./resources/js/products/loadingSingleProduct.vue?vue&type=template&id=7f6dfa30&");
/* harmony import */ var _loadingSingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loadingSingleProduct.vue?vue&type=script&lang=js& */ "./resources/js/products/loadingSingleProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _loadingSingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _loadingSingleProduct_vue_vue_type_template_id_7f6dfa30___WEBPACK_IMPORTED_MODULE_0__["render"],
  _loadingSingleProduct_vue_vue_type_template_id_7f6dfa30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/loadingSingleProduct.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/loadingSingleProduct.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/products/loadingSingleProduct.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./loadingSingleProduct.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/loadingSingleProduct.vue?vue&type=template&id=7f6dfa30&":
/*!***************************************************************************************!*\
  !*** ./resources/js/products/loadingSingleProduct.vue?vue&type=template&id=7f6dfa30& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProduct_vue_vue_type_template_id_7f6dfa30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./loadingSingleProduct.vue?vue&type=template&id=7f6dfa30& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/loadingSingleProduct.vue?vue&type=template&id=7f6dfa30&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProduct_vue_vue_type_template_id_7f6dfa30___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loadingSingleProduct_vue_vue_type_template_id_7f6dfa30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/products/products.vue":
/*!********************************************!*\
  !*** ./resources/js/products/products.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _products_vue_vue_type_template_id_67b320be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./products.vue?vue&type=template&id=67b320be& */ "./resources/js/products/products.vue?vue&type=template&id=67b320be&");
/* harmony import */ var _products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./products.vue?vue&type=script&lang=js& */ "./resources/js/products/products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./products.vue?vue&type=style&index=0&lang=css& */ "./resources/js/products/products.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _products_vue_vue_type_template_id_67b320be___WEBPACK_IMPORTED_MODULE_0__["render"],
  _products_vue_vue_type_template_id_67b320be___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/products.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/products.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/products/products.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./products.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/products.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************!*\
  !*** ./resources/js/products/products.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./products.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/products/products.vue?vue&type=template&id=67b320be&":
/*!***************************************************************************!*\
  !*** ./resources/js/products/products.vue?vue&type=template&id=67b320be& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_template_id_67b320be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./products.vue?vue&type=template&id=67b320be& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/products.vue?vue&type=template&id=67b320be&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_template_id_67b320be___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_products_vue_vue_type_template_id_67b320be___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/products/singleProduct.vue":
/*!*************************************************!*\
  !*** ./resources/js/products/singleProduct.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _singleProduct_vue_vue_type_template_id_6a63cfcc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleProduct.vue?vue&type=template&id=6a63cfcc& */ "./resources/js/products/singleProduct.vue?vue&type=template&id=6a63cfcc&");
/* harmony import */ var _singleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./singleProduct.vue?vue&type=script&lang=js& */ "./resources/js/products/singleProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _singleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _singleProduct_vue_vue_type_template_id_6a63cfcc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _singleProduct_vue_vue_type_template_id_6a63cfcc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/singleProduct.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/singleProduct.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/products/singleProduct.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_singleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./singleProduct.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/singleProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_singleProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/singleProduct.vue?vue&type=template&id=6a63cfcc&":
/*!********************************************************************************!*\
  !*** ./resources/js/products/singleProduct.vue?vue&type=template&id=6a63cfcc& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_singleProduct_vue_vue_type_template_id_6a63cfcc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./singleProduct.vue?vue&type=template&id=6a63cfcc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/singleProduct.vue?vue&type=template&id=6a63cfcc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_singleProduct_vue_vue_type_template_id_6a63cfcc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_singleProduct_vue_vue_type_template_id_6a63cfcc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/products/singleProductList.vue":
/*!*****************************************************!*\
  !*** ./resources/js/products/singleProductList.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var render, staticRenderFns
var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_0__["default"])(
  script,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

component.options.__file = "resources/js/products/singleProductList.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ })

}]);