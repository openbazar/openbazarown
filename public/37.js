(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[37],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/footer.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/partials/footer.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/footer.vue?vue&type=template&id=7dab91ac&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/partials/footer.vue?vue&type=template&id=7dab91ac& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "bazar-footer",
      class: { "grey-footer": _vm.$route.path == "/get-the-app" }
    },
    [
      _c("div", { staticClass: "bazar-footer-content" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "follow-us" }, [
          _vm._v("\n                  Follow Us\n              ")
        ]),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "footer-menu" },
          [
            _c("router-link", { attrs: { to: "/terms-of-use" } }, [
              _vm._v(" Terms of use ")
            ]),
            _vm._v(" "),
            _c("router-link", { attrs: { to: "/privacy-policy" } }, [
              _vm._v(" Privacy Policy ")
            ]),
            _vm._v(" "),
            _c("router-link", { attrs: { to: "/contact-us" } }, [
              _vm._v(" Contact us ")
            ]),
            _vm._v(" "),
            _c("router-link", { attrs: { to: "/help" } }, [_vm._v(" Help ")])
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "all-rights" }, [
          _vm._v(
            "\n                  Copyright © 2018 - 2021 OpenBazar.nl. All rights reserved\n              "
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "download-box" }, [
      _c("div", { staticClass: "app-download-text" }, [
        _vm._v("\n                      Download the app\n                  ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "qoute" }, [
        _vm._v(
          "\n                      Whatever you’ve got in mind, we’ve got inside. OpenBazar App is what you’d want to have on your mobile phone.\n                  "
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "download-button" }, [
        _c(
          "a",
          {
            attrs: {
              href:
                "https://play.google.com/store/apps/details?id=com.open_bazar",
              target: "_blank"
            }
          },
          [
            _c("img", {
              staticClass: "google-download",
              attrs: { src: "/bazar/google-download-icon.png" }
            })
          ]
        ),
        _vm._v(" "),
        _c(
          "a",
          {
            attrs: {
              href:
                "https://apps.apple.com/nl/app/open-bazar/id1486285030?l=en",
              target: "_blank"
            }
          },
          [
            _c("img", {
              staticClass: "apple-download",
              attrs: { src: "/bazar/apple-download-icon.png" }
            })
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "social-icons" }, [
      _c("div", { staticClass: "social-icon" }, [
        _c("a", { attrs: { href: "https://www.facebook.com/OpenBAZAR4u/" } }, [
          _c("i", { staticClass: "fab fa-facebook-f" })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "social-icon" }, [
        _c("a", { attrs: { href: "https://mobile.twitter.com/BazarOpen" } }, [
          _c("i", { staticClass: "fab fa-twitter" })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "social-icon" }, [
        _c("a", { attrs: { href: "https://www.youtube.com/c/OpenBazar" } }, [
          _c("i", { staticClass: "fab fa-youtube" })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "social-icon" }, [
        _c(
          "a",
          { attrs: { href: "https://www.instagram.com/openbazar4u/?hl=nl" } },
          [_c("i", { staticClass: "fab fa-instagram" })]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/partials/footer.vue":
/*!******************************************!*\
  !*** ./resources/js/partials/footer.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _footer_vue_vue_type_template_id_7dab91ac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer.vue?vue&type=template&id=7dab91ac& */ "./resources/js/partials/footer.vue?vue&type=template&id=7dab91ac&");
/* harmony import */ var _footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer.vue?vue&type=script&lang=js& */ "./resources/js/partials/footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _footer_vue_vue_type_template_id_7dab91ac___WEBPACK_IMPORTED_MODULE_0__["render"],
  _footer_vue_vue_type_template_id_7dab91ac___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/partials/footer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/partials/footer.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/partials/footer.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./footer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/footer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_footer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/partials/footer.vue?vue&type=template&id=7dab91ac&":
/*!*************************************************************************!*\
  !*** ./resources/js/partials/footer.vue?vue&type=template&id=7dab91ac& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_footer_vue_vue_type_template_id_7dab91ac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./footer.vue?vue&type=template&id=7dab91ac& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/partials/footer.vue?vue&type=template&id=7dab91ac&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_footer_vue_vue_type_template_id_7dab91ac___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_footer_vue_vue_type_template_id_7dab91ac___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);