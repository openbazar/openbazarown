(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[14],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/main-wrapper.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    return {
      search_query: '',
      location: '',
      category: '',
      isDeltaLoaded: false,
      loadingPage: false,
      logoutLoading: false,
      fireUser: '' // mene_open: false,

    };
  },
  props: {
    referred: {},
    payment_response: {},
    error_message: {},
    social_user: {}
  },
  created: function created() {
    var _this = this;

    // console.log(this.$router.currentRoute , 'currentRoute  wrapper')
    this.$root.$refs._wrapper = this;
    this.$root.$on('logout-initiated', function () {
      _this.logoutLoading = true;
    });
    var firebaseConfig = {
      // Test Firebase on my email
      // apiKey: "AIzaSyCWaM6cWBXsKQqrwmUCMFfOcNbjRS6hAso",
      // authDomain: "openbazar-366a2.firebaseapp.com",
      // projectId: "openbazar-366a2",
      // storageBucket: "openbazar-366a2.appspot.com",
      // messagingSenderId: "546979912823",
      // appId: "1:546979912823:web:59072c985542e93624fa72",
      // measurementId: "G-T44KD0VMS5"
      // Live Firebase on openbazarnl@gmail
      apiKey: "AIzaSyDgwGG8qXO_mA60W94j_SyFYA87ktrrF7Y",
      authDomain: "open-bazar.firebaseapp.com",
      databaseURL: "https://open-bazar.firebaseio.com",
      projectId: "open-bazar",
      storageBucket: "open-bazar.appspot.com",
      messagingSenderId: "292828447760",
      appId: "1:292828447760:web:73ffee939515cc4d4cb695"
    }; // Initialize Firebase

    firebase.initializeApp(firebaseConfig);
    var remoteConfig = firebase.remoteConfig();
    remoteConfig.settings.minimumFetchIntervalMillis = 1; // remoteConfig.defaultConfig = {
    //     "eurotoinr": "INR"
    // };

    var val = remoteConfig.getValue("eurotoinr"); //remoteConfig.fetch()

    remoteConfig.fetchAndActivate().then(function () {
      remoteConfig.activate();

      var _val = remoteConfig.getValue("eurotoinr");

      _this.$store.commit('setConversionRate', _val); // console.log(  _val , remoteConfig , remoteConfig._storageCache.activeConfig , 'Firebase')
      // console.log(  _val )
      // ...

    })["catch"](function (err) {
      console.log(err); // ...
    }); // let vm = this
    // window.OneSignal = window.OneSignal || [];
    // OneSignal.push(function() {
    //     OneSignal.init({
    //     appId: "0138436f-2e82-4890-9beb-bf3182aa8dab",
    //     notifyButton: {
    //         enable: false,
    //     },
    //     subdomainName: "openbazar",
    //     });
    //     OneSignal.on('notificationDisplay', function(event) {
    //         console.warn('OneSignal notification displayed:', event);
    //         if(vm.$root.$refs.chatComponent){
    //             vm.$root.$refs.chatComponent.newMessageRecieved(event);
    //         }
    //     });
    //     OneSignal.isPushNotificationsEnabled(function(isEnabled) {
    //         if (isEnabled){
    //             // console.log("Push notifications are enabled!");
    //         }
    //         else{
    //             // console.log("Push notifications are not enabled yet.");
    //             OneSignal.showSlidedownPrompt();
    //         }
    //     });
    //     OneSignal.getUserId(function(userId) {
    //         // console.log("OneSignal User ID:", userId);
    //         // (Output) OneSignal User ID: 270a35cd-4dda-4b3f-b04e-41d7463a2316
    //     });
    // });
  },
  mounted: function mounted() {
    var _this2 = this;

    this.$root.$on('fire-user-update', function (data) {
      console.log('fire-user-update', data);
      _this2.fireUser = data;
    });

    if (this.error_message) {
      this.$toastr.error(this.error_message);
    }

    if (this.$store.getters.isAuthenticated) {
      firebase.app(); // Get a reference to the database service

      var database = firebase.database();
      database.ref('Users/user' + this.$store.getters.isAuthenticated.id).once('value', function (snapshot) {
        _this2.fireUser = snapshot.val();
        var fireUser = snapshot.val();
        var user = Object.assign({}, fireUser);
        user.online = 'true'; // console.log(user , 'online status true')

        if (fireUser && fireUser.online != 'true') {
          database.ref('Users/user' + _this2.$store.getters.isAuthenticated.id).set(_objectSpread({}, user));
        }
      });
    }

    window.addEventListener("beforeunload", function (e) {
      var total_tabs = JSON.parse(localStorage.getItem('WINDOW_VALIDATION'));
      console.log('sdadas', _this2.fireUser, total_tabs, localStorage.getItem('WINDOW_VALIDATION'));

      if (total_tabs && total_tabs.length == 0) {
        var user_update = '';
        database.ref('Users/user' + _this2.$store.getters.isAuthenticated.id).once('value', function (snapshot) {
          user_update = snapshot.val();
        });

        if (user_update) {
          // var user_update = Object.assign( {} , this.fireUser);
          var timestamp = new Date().getTime();
          user_update.online = timestamp;
          console.log(user_update, 'online status timestamp');
          database.ref('Users/user' + _this2.$store.getters.isAuthenticated.id).set(_objectSpread({}, user_update));
          _this2.fireUser = ''; // console.log(user_update , 'user_update')
        }
      } // var confirmationMessage = 'It looks like you have been editing something. '
      //                         + 'If you leave before saving, your changes will be lost.';
      // (e || window.event).returnValue = confirmationMessage; //Gecko + IE
      // return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.

    }); // window.addEventListener('unload', function (e) {
    //     e.preventDefault();
    //     e.returnValue = '';
    // });
    // window.onbeforeunload =  () => {
    //     return false
    // }

    if (this.$root.$refs.paymentMethods && this.payment_response) {
      console.log("Has Payment"); // this.$router.push({ name: "payment method", params: {payment_response : this.payment_response}} );

      this.$root.$refs.paymentMethods.paypalPaymentComplete(this.payment_response);
    } // setTimeout(() => {
    //     $('#cookieModal').modal('show')
    // }, 2000);
    // console.log( 'cookie' , document.cookie );
    // this.$router.beforeEach((to, from, next) => {
    //     this.loadingPage = true
    //     next()
    // })
    // this.$router.afterEach((to, from, next) => {
    //     this.loadingPage = false
    // })

  },
  methods: {
    getCookie: function getCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }

      return null;
    },
    updateFirebaseBeforeClose: function updateFirebaseBeforeClose() {
      return true;
    },
    searchQueryUpdated: function searchQueryUpdated($value) {
      this.search_query = $value; // console.log('searchQueryUpdated' , $value)
    },
    searchLocationUpdated: function searchLocationUpdated($value) {
      this.location = $value;
      console.log('searchLocationUpdated', $value);
    },
    searchCategoryUpdated: function searchCategoryUpdated($value) {
      // console.log('searchCategoryUpdated' , $value)
      this.category = $value;
    },
    searchInitiated: function searchInitiated($value) {
      // console.log($value)
      this.$refs.routerView.searchQuery($value);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.router-loading{\r\n    position: fixed;\r\n    width: 100%;\r\n    height: 100%;\r\n    background-color: #ffffff9c;\r\n    z-index: 1000;\r\n    display: flex;\r\n    justify-content: flex-end;\r\n    padding: 15px;\r\n    color: black;\n}\r\n\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader??ref--6-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--6-2!../../node_modules/vue-loader/lib??vue-loader-options!./main-wrapper.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=template&id=a41cb706&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/main-wrapper.vue?vue&type=template&id=a41cb706& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("loading", {
        attrs: {
          active: _vm.logoutLoading,
          loader: "dots",
          height: 170,
          width: 170,
          color: "#d11d1d",
          "background-color": "rgb(0 0 0)",
          "lock-scroll": true,
          "is-full-page": true
        },
        on: {
          "update:active": function($event) {
            _vm.logoutLoading = $event
          }
        }
      }),
      _vm._v(" "),
      _vm.loadingPage
        ? _c("div", { staticClass: "router-loading" }, [_c("b-spinner")], 1)
        : _vm._e(),
      _vm._v(" "),
      _c("navbar", { on: { "search-initiated": _vm.searchInitiated } }),
      _vm._v(" "),
      _vm.$store.getters.isAuthenticated
        ? _c("router-link", { attrs: { to: "/post-ad" } }, [
            _c("div", { staticClass: "add-new-post-floating-button" }, [
              _c("img", { attrs: { src: "/bazar/camera.png" } })
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("router-view", { key: _vm.$route.path }),
      _vm._v(" "),
      _c("bz-footer"),
      _vm._v(" "),
      _vm._m(0)
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "cookieModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "passwordChangeModalTitle",
          "aria-hidden": "true",
          "data-backdrop": "static",
          "data-keyboard": "false"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _c("div", { staticClass: "modal-header" }, [
                _c(
                  "h5",
                  {
                    staticClass: "modal-title",
                    attrs: { id: "exampleModalCenterTitle" }
                  },
                  [_vm._v("Change Password")]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "modal-body" }, [
                _c("p", [_vm._v("Select Cookies")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "modal-footer" }, [
                _c(
                  "button",
                  {
                    staticClass:
                      "bz-sqaure-btn bz-btn-red full-mobile update-password-btn",
                    attrs: { type: "submit", "data-dismiss": "modal" }
                  },
                  [_c("span", [_vm._v("Accept")])]
                )
              ])
            ])
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/main-wrapper.vue":
/*!***************************************!*\
  !*** ./resources/js/main-wrapper.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _main_wrapper_vue_vue_type_template_id_a41cb706___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main-wrapper.vue?vue&type=template&id=a41cb706& */ "./resources/js/main-wrapper.vue?vue&type=template&id=a41cb706&");
/* harmony import */ var _main_wrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./main-wrapper.vue?vue&type=script&lang=js& */ "./resources/js/main-wrapper.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _main_wrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main-wrapper.vue?vue&type=style&index=0&lang=css& */ "./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _main_wrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _main_wrapper_vue_vue_type_template_id_a41cb706___WEBPACK_IMPORTED_MODULE_0__["render"],
  _main_wrapper_vue_vue_type_template_id_a41cb706___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/main-wrapper.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/main-wrapper.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./resources/js/main-wrapper.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib??ref--4-0!../../node_modules/vue-loader/lib??vue-loader-options!./main-wrapper.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************!*\
  !*** ./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/style-loader!../../node_modules/css-loader??ref--6-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--6-2!../../node_modules/vue-loader/lib??vue-loader-options!./main-wrapper.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/main-wrapper.vue?vue&type=template&id=a41cb706&":
/*!**********************************************************************!*\
  !*** ./resources/js/main-wrapper.vue?vue&type=template&id=a41cb706& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_template_id_a41cb706___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./main-wrapper.vue?vue&type=template&id=a41cb706& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/main-wrapper.vue?vue&type=template&id=a41cb706&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_template_id_a41cb706___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_main_wrapper_vue_vue_type_template_id_a41cb706___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);