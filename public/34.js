(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/gallery-section.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/gallery-section.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    photos: {
      type: String,
      "default": ''
    }
  },
  watch: {
    photos: function photos() {
      // console.log(this.photos)
      this.formatCurrentFiles();
    }
  },
  data: function data() {
    return {
      loading: false,
      isDragging: false,
      files: [],
      acceptedTypes: ['image/gif', 'image/jpg', 'image/jpeg', 'image/png']
    };
  },
  mounted: function mounted() {},
  methods: {
    formatCurrentFiles: function formatCurrentFiles() {
      var _this = this;

      if (this.photos) {
        JSON.parse(this.photos).forEach(function (image) {
          // console.log(image.image)
          _this.files.push({
            name: image.image,
            url: _this.$root.virtualmin_server + 'public/images/' + image.image,
            status: 'success'
          });
        });
      }
    },
    filesDropped: function filesDropped(event) {
      var droppedFile = event.dataTransfer.files; // extract dropped files

      if (!droppedFile) return;
      this.handleFiles(_toConsumableArray(droppedFile));
    },
    imageSelected: function imageSelected(event) {
      var files = event.target.files;
      if (!files) return; // console.log(files)

      this.handleFiles(_toConsumableArray(files));
    },
    handleFiles: function handleFiles(files) {
      var _this2 = this;

      this.$emit('uploading-status', true);
      this.$emit('uploading-images-started');
      console.log('uploading-images-started');
      files.forEach(function (file) {
        if (_this2.files.length < 30) {
          if (_this2.isValid(file)) _this2.processFile(file);else {
            _this2.$toastr.error('Please Select Only Images');

            console.log("invalid file"); // please handle this error
          }
        } else {
          _this2.$toastr.error('You can not upload more than 30 images');
        }
      }); // this.$emit('uploading-images-completed')
    },
    processFile: function processFile(file) {
      var index = this.addToFiles(file);
      this.postImage(file, index);
    },
    postImage: function postImage(file, index) {
      var _this3 = this;

      this.files[index].status = 'uploading'; // Update file status to success

      var fd = new FormData();
      fd.append('image', file);
      axios.post('/v1/uploadimage', fd, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token,
          'Content-Type': 'multipart/form-data'
        } // onUploadProgress: function(progressEvent) {
        //     this.files[index].progress = parseInt( Math.round((progressEvent.loaded / progressEvent.total) * 100))
        // }.bind(this),

      }).then(function (response) {
        if (_this3.files.length - 1 == index) {
          _this3.$emit('uploading-status', false); // this.$emit('uploading-images-completed')
          // console.log('uploading-images-completed')

        }

        if (response.data.status == 'success') {
          _this3.files[index].file = response.data.image; // Update file status to success

          _this3.files[index].status = 'success'; // Update file status to success

          _this3.$emit('image-uploaded', _this3.files[index]);
        } else {
          _this3.files.splice(index, 1);

          _this3.$toastr.error('Something went wrong while uploading');
        }
      })["catch"](function (error) {
        _this3.$toastr.error('Image Upload Failed');

        _this3.files[index].status = 'failed'; // Update file status to success

        console.log(error);

        if (_this3.files.length - 1 == index) {
          _this3.$emit('uploading-status', false);
        }

        _this3.files.splice(index, 1);

        _this3.$toastr.error('Something went wrong while uploading');

        _this3.uploading = false;
      });
    },
    deleteImg: function deleteImg(image) {
      var _this4 = this;

      image.status = 'deleting';
      axios.post('/delete-event-img', {
        media: image.file.id
      }).then(function (response) {
        _this4.files.splice(_this4.files.indexOf(image), 1);

        _this4.$toastr.info('Image Removed');
      })["catch"](function (error) {
        console.log(error);

        _this4.$toastr.error('Image Remove Failed');
      });
    },
    reUplaodImage: function reUplaodImage(index, image) {
      if (image.original_file) {
        this.postImage(image.original_file, index);
      } else {
        this.files.splice(this.files.indexOf(image), 1);
      }
    },
    removeImage: function removeImage(image) {
      this.$emit('image-removed', this.files.indexOf(image));
      this.files.splice(this.files.indexOf(image), 1); // this.$toastr.info('Image Removed');
    },
    addToFiles: function addToFiles(file) {
      var src = URL.createObjectURL(file); // console.log(src)

      var length = this.files.push({
        file: null,
        original_file: file,
        name: file.name,
        url: src,
        status: 'uploading',
        // uploading, success, error
        progress: 0 // Upload % 0-100

      });
      return length - 1; // index of the file in this.files[]
    },
    isValid: function isValid(file) {
      return this.acceptedTypes.includes(file.type);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/gallery-section.vue?vue&type=template&id=c0957460&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/gallery-section.vue?vue&type=template&id=c0957460& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "ad-gallery-section ad-section" }, [
    _c(
      "div",
      { staticClass: "ad-gallery-section-title section-heading mb-3" },
      [_vm._v("Add Photos")]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "gallery-container" }, [
      _c("div", { staticClass: "main-gallery" }, [
        _c("div", { staticClass: "small-photo-text" }, [
          _vm._v(_vm._s(_vm.files.length) + " of 30 Photos used")
        ]),
        _vm._v(" "),
        _c("input", {
          ref: "photos_input",
          staticClass: "d-none",
          attrs: { type: "file", multiple: "", accept: "images/*" },
          on: { change: _vm.imageSelected }
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "ad-gallery" },
          [
            _vm.files.length == 0
              ? _vm._l(5, function(index) {
                  return _c(
                    "div",
                    { key: index, staticClass: "ad-gallery-item dummy" },
                    [
                      index == 1
                        ? _c("img", {
                            staticClass: "add-image-item",
                            attrs: { src: "/bazar/add-image-icon.png" },
                            on: {
                              click: function($event) {
                                return _vm.$refs.photos_input.click()
                              }
                            }
                          })
                        : _vm._e()
                    ]
                  )
                })
              : _vm._e(),
            _vm._v(" "),
            _vm.files.length > 0
              ? [
                  _vm._l(_vm.files, function(file, index) {
                    return _c(
                      "div",
                      { key: index, staticClass: "ad-gallery-item" },
                      [
                        _c("img", {
                          class: {
                            "featured-image": index == 0,
                            "other-image": index > 0
                          },
                          attrs: { src: file.url }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "remove-btn",
                            on: {
                              click: function($event) {
                                return _vm.removeImage(file)
                              }
                            }
                          },
                          [_c("i", { staticClass: "fas fa-times" })]
                        )
                      ]
                    )
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "ad-gallery-item" }, [
                    _c("img", {
                      staticClass: "add-image-item",
                      attrs: { src: "/bazar/add-image-icon.png" },
                      on: {
                        click: function($event) {
                          return _vm.$refs.photos_input.click()
                        }
                      }
                    })
                  ])
                ]
              : _vm._e()
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "gallery-icon",
          class: {
            "w-45": _vm.files.length > 4,
            "hide-desktop": _vm.files.length > 6
          }
        },
        [_vm._m(0)]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "upload-image-container" }, [
      _c("img", { attrs: { src: "/bazar/ad-gallery-image.png" } }),
      _vm._v(" "),
      _c("div", { staticClass: "upload-image-text" }, [
        _vm._v(
          "You can upload upto 30 images. Prictures increases the chance to get more view and make deal faster"
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/post-ad/gallery-section.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/post-ad/gallery-section.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _gallery_section_vue_vue_type_template_id_c0957460___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gallery-section.vue?vue&type=template&id=c0957460& */ "./resources/js/components/post-ad/gallery-section.vue?vue&type=template&id=c0957460&");
/* harmony import */ var _gallery_section_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery-section.vue?vue&type=script&lang=js& */ "./resources/js/components/post-ad/gallery-section.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _gallery_section_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _gallery_section_vue_vue_type_template_id_c0957460___WEBPACK_IMPORTED_MODULE_0__["render"],
  _gallery_section_vue_vue_type_template_id_c0957460___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/post-ad/gallery-section.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/post-ad/gallery-section.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/post-ad/gallery-section.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_section_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./gallery-section.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/gallery-section.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_section_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/post-ad/gallery-section.vue?vue&type=template&id=c0957460&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/post-ad/gallery-section.vue?vue&type=template&id=c0957460& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_section_vue_vue_type_template_id_c0957460___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./gallery-section.vue?vue&type=template&id=c0957460& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/gallery-section.vue?vue&type=template&id=c0957460&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_section_vue_vue_type_template_id_c0957460___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_section_vue_vue_type_template_id_c0957460___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);