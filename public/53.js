(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[53],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/notifications.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/notifications.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loading: false,
      show: false,
      no_more_notifications: false,
      notifications: [],
      index: 0
    };
  },
  mounted: function mounted() {
    this.loadNotifications();
  },
  methods: {
    loadNotifications: function loadNotifications() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/fetch_notification', {
        index: this.index,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        console.log(res.data);
        _this.loading = false;

        if (res.data.status == 'success') {
          var _this$notifications;

          (_this$notifications = _this.notifications).push.apply(_this$notifications, _toConsumableArray(res.data.data));

          _this.index = _this.index + 1;

          _this.$store.commit('setNotificationCount', res.data.noty);
        } else if (res.data.status == 'fail') {
          _this.no_more_notifications = true;
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    markAllRead: function markAllRead() {
      var _this2 = this;

      axios.post('/v1/notification_status', {
        type: 'all',
        noty_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        if (res.data.status == 'true') {
          _this2.markAsReadNotification();
        } else if (res.data.status == 'fail') {}
      })["catch"](function (err) {
        _this2.loading = false;

        _this2.$toastr.error('An unexpected error occurred');
      });
    },
    loadMoreNotifications: function loadMoreNotifications() {
      this.loadNotifications();
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    },
    markAsReadAndNext: function markAsReadAndNext(notification) {
      this.$store.commit('decrementNotifcation');
      this.$router.push({
        name: 'view-product',
        params: {
          id: notification.refer_id,
          notification: notification
        }
      }); // axios.post('/v1/notification_status', {
      //     type: 'single' ,
      //     noty_id: notification.id,
      // },{
      //     headers:{
      //         Authorization: "Bearer "+ this.$store.getters.isAuthenticated.token
      //     }
      // }).then (res => {
      // if(res.data.status ==  'true'){
      //     // this.markAsReadNotification();
      // }else if(res.data.status ==  'fail'){
      // }
      // }).catch( err => {
      //     this.loading = false
      //     this.$toastr.error('An unexpected error occurred');
      // })
    },
    markAsReadNotification: function markAsReadNotification() {
      this.notifications.map(function (not) {
        not.status = 1;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/notifications.vue?vue&type=template&id=6388b2bc&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/notifications.vue?vue&type=template&id=6388b2bc& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "notification-page container" }, [
    _vm._m(0),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "section-heading cursor-pointer",
        on: { click: _vm.markAllRead }
      },
      [_vm._v("\n            Mark All as Read\n        ")]
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "list" },
      [
        _vm._l(_vm.notifications, function(notification, index) {
          return _c(
            "div",
            {
              key: index,
              staticClass: "list-item notification-item cursor-pointer",
              on: {
                click: function($event) {
                  return _vm.markAsReadAndNext(notification)
                }
              }
            },
            [
              _c("div", { staticClass: "notification-item-image" }, [
                notification.sender_image
                  ? _c("img", {
                      attrs: {
                        src:
                          _vm.$root.virtualmin_server +
                          "public/images/" +
                          notification.sender_image
                      },
                      on: {
                        error: function($event) {
                          $event.target.src = "/bazar/default-user-avatar.png"
                        }
                      }
                    })
                  : _c("img", {
                      attrs: { src: "/bazar/default-user-avatar.png" }
                    })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "notification-item-description" }, [
                _vm._v(
                  "\n                " +
                    _vm._s(notification.tital) +
                    "\n            "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "notification-item-time " }, [
                notification.status == 0
                  ? _c("i", { staticClass: "fas fa-circle _pending" })
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "time-ago" }, [
                  _vm._v(
                    "\n                    " +
                      _vm._s(
                        _vm._f("formatDifferenceDate")(notification.created_at)
                      ) +
                      "\n                "
                  )
                ])
              ]),
              _vm._v(" "),
              !_vm.no_more_notifications &&
              index == _vm.notifications.length - 1
                ? _c(
                    "Intersect",
                    {
                      on: {
                        enter: _vm.loadMoreNotifications,
                        leave: function($event) {
                          return _vm.pageEnd("Leave")
                        }
                      }
                    },
                    [_c("div")]
                  )
                : _vm._e()
            ],
            1
          )
        }),
        _vm._v(" "),
        _vm.loading
          ? _c("div", { staticClass: "loading-skeliton" }, [
              _c(
                "div",
                { staticClass: "ads-listing" },
                _vm._l(8, function(index) {
                  return _c(
                    "div",
                    { key: index, staticClass: "ad-list-item" },
                    [
                      _c(
                        "div",
                        {
                          staticClass: "ad-list-item-main-container d-block p-2"
                        },
                        [
                          _c("b-skeleton", {
                            attrs: { animation: "wave", width: "85%" }
                          }),
                          _vm._v(" "),
                          _c("b-skeleton", {
                            attrs: { animation: "wave", width: "55%" }
                          }),
                          _vm._v(" "),
                          _c("b-skeleton", {
                            attrs: { animation: "wave", width: "70%" }
                          })
                        ],
                        1
                      )
                    ]
                  )
                }),
                0
              )
            ])
          : _vm._e()
      ],
      2
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-dashboard-page-header" }, [
      _c("div", { staticClass: "bz-page-header-title text-center" }, [
        _vm._v("\n                Notifications\n            ")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/notifications.vue":
/*!*********************************************!*\
  !*** ./resources/js/user/notifications.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _notifications_vue_vue_type_template_id_6388b2bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notifications.vue?vue&type=template&id=6388b2bc& */ "./resources/js/user/notifications.vue?vue&type=template&id=6388b2bc&");
/* harmony import */ var _notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./notifications.vue?vue&type=script&lang=js& */ "./resources/js/user/notifications.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _notifications_vue_vue_type_template_id_6388b2bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _notifications_vue_vue_type_template_id_6388b2bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/notifications.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/notifications.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/user/notifications.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./notifications.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/notifications.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_notifications_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/notifications.vue?vue&type=template&id=6388b2bc&":
/*!****************************************************************************!*\
  !*** ./resources/js/user/notifications.vue?vue&type=template&id=6388b2bc& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_notifications_vue_vue_type_template_id_6388b2bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./notifications.vue?vue&type=template&id=6388b2bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/notifications.vue?vue&type=template&id=6388b2bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_notifications_vue_vue_type_template_id_6388b2bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_notifications_vue_vue_type_template_id_6388b2bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);