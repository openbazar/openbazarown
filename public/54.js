(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[54],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/followers.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/other_user/followers.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../partials/loader.vue */ "./resources/js/partials/loader.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.loadMyFollowers();
  },
  data: function data() {
    return {
      loading: false,
      no_more_products: false,
      following_process: "",
      index: 0,
      followers: []
    };
  },
  methods: {
    loadMyFollowers: function loadMyFollowers() {
      var _this = this;

      if (!this.loading) {
        this.loading = true;
        axios.post('/v1/other_followers', {
          index: this.index,
          other_id: this.$store.getters.isAuthenticated.id,
          user_id: this.$route.params.user_id
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          // console.log(res.data)
          _this.loading = false; // console.log(res.data.data.length , res.data.data.length == "0" , res.data.data.length == 0);

          if (res.data.status == 'success' && res.data.data.length > 0) {
            var _this$followers;

            (_this$followers = _this.followers).push.apply(_this$followers, _toConsumableArray(res.data.data));

            _this.index = _this.index + 1;
          } else if (res.data.data.length == 0) {
            _this.no_more_products = true;
          } else {
            // console.log(res)
            // this.loading = false
            _this.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this.loading = false;

          _this.$toastr.error('An unexpected error occurred');
        });
      }
    },
    loadMoreFollowers: function loadMoreFollowers() {
      // if(this.followers.length < this.$store.getters.getUserStats.followers){
      this.loadMyFollowers(); // }
    },
    follow: function follow(follower) {
      var _this2 = this;

      console.log(follower);
      this.following_process = follower;
      axios.post('/v1/follow', {
        following_id: follower.signupid,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        // console.log(res.data)
        if (res.data.status == 'success') {
          _this2.following_process = '';

          if (res.data.message == 'Unfollow Successfully') {
            follower.followstatus = 'NO';
          } else {
            follower.followstatus = 'YES';
          }
        } else {
          console.log(res);

          _this2.$toastr.error(res.data.message);

          _this2.following_process = '';
        }
      })["catch"](function (err) {
        _this2.$toastr.error('An unexpected error occurred');

        _this2.following_process = '';
      });
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    },
    gotoPage: function gotoPage(follower) {
      if (!this.following_process) {
        this.$router.push('/user/' + follower.signupid + '/ads');
      }
    } // followNow(follower){
    //     this.following_process = true;
    //     axios.post('/v1/my_followers', {
    //         user_id: 1,
    //         // index: 5,
    //         // user_id: this.$store.getters.isAuthenticated.id,
    //     },{
    //         headers:{
    //             Authorization: "Bearer "+ this.$store.getters.isAuthenticated.token
    //         }
    //     }).then (res => {
    //     // console.log(res.data)
    //     if(res.data.status ==  'success'){
    //         this.following_process = false
    //         // follower.followstatus = 'YES'
    //     }else{
    //         console.log(res)
    //         this.following_process = false
    //         this.$toastr.error( res.data.message );
    //     }
    //     }).catch( err => {
    //         this.following_process = false
    //         this.$toastr.error('An unexpected error occurred');
    //     })
    // }

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/followers.vue?vue&type=template&id=a05e75d4&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/other_user/followers.vue?vue&type=template&id=a05e75d4& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid user-dashboard" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-lg-3 col-md-3 col-sm-12 col-xs-12" },
        [_c("left-menu", { attrs: { user_id: _vm.$route.params.user_id } })],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-9 col-md-9 col-sm-12 col-xs-12 profile-form-container"
        },
        [
          _c("div", { staticClass: "dashboard" }, [
            _c("div", { staticClass: "bz-dashboard-page-header" }, [
              _c(
                "div",
                { staticClass: "bz-page-header-title text-center" },
                [
                  _vm._v("\r\n                    Followers ("),
                  _vm.$root.$refs.userLeftMenu &&
                  _vm.$root.$refs.userLeftMenu.user_info
                    ? [
                        _vm._v(
                          _vm._s(
                            _vm.$root.$refs.userLeftMenu.user_info.followers
                          )
                        )
                      ]
                    : _vm._e(),
                  _vm._v(")\r\n                ")
                ],
                2
              ),
              _vm._v(" "),
              _c("div", { staticClass: "small-text" })
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "follow-list" },
              [
                _vm._l(_vm.followers, function(follower) {
                  return _c(
                    "div",
                    {
                      key: follower.id,
                      staticClass: "follow-list-item cursor-pointer",
                      on: {
                        click: function($event) {
                          return _vm.gotoPage(follower)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "follow-list-item-image-box" }, [
                        follower && follower.profile_image
                          ? _c("img", {
                              attrs: {
                                src: _vm._f("rootServerPath")(
                                  _vm.$root.isValidURL(follower.profile_image)
                                    ? follower.profile_image
                                    : follower.profile_image
                                )
                              },
                              on: {
                                error: function($event) {
                                  $event.target.src =
                                    "/bazar/default-user-avatar.png"
                                }
                              }
                            })
                          : _c("img", {
                              attrs: { src: "/bazar/default-user-avatar.png" }
                            })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "follow-list-item-user-info" }, [
                        _c("div", { staticClass: "user-name" }, [
                          _vm._v(" " + _vm._s(follower.name) + " ")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "user-posts" }, [
                          _vm._v(_vm._s(follower.ads_count) + " Posts")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "follow-list-item-btns" }, [
                        follower.followstatus == "YES"
                          ? _c(
                              "button",
                              {
                                staticClass: "following",
                                attrs: {
                                  href: "#",
                                  disabled: _vm.following_process == follower
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.follow(follower)
                                  }
                                }
                              },
                              [
                                _vm.following_process == follower
                                  ? _c("loader")
                                  : _c("span", [_vm._v("Following")])
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        follower.followstatus == "NO"
                          ? _c(
                              "button",
                              {
                                staticClass: "follow",
                                attrs: {
                                  href: "#",
                                  disabled: _vm.following_process == follower
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.follow(follower)
                                  }
                                }
                              },
                              [
                                _vm.following_process == follower
                                  ? _c("loader")
                                  : _c("span", [_vm._v("Follow")])
                              ],
                              1
                            )
                          : _vm._e()
                      ])
                    ]
                  )
                }),
                _vm._v(" "),
                _vm.loading
                  ? _c(
                      "div",
                      _vm._l(4, function(index) {
                        return _c("loading-single-ad", { key: index })
                      }),
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                !_vm.loading && _vm.followers.length == 0
                  ? _c("no-data-box", {
                      attrs: {
                        message: "No Followers Found",
                        icon: "/bazar/dashboard-icons/followers.png"
                      }
                    })
                  : _vm._e(),
                _vm._v(" "),
                !_vm.no_more_products
                  ? _c(
                      "Intersect",
                      {
                        on: {
                          enter: _vm.loadMoreFollowers,
                          leave: function($event) {
                            return _vm.pageEnd("Leave")
                          }
                        }
                      },
                      [_c("div")]
                    )
                  : _vm._e()
              ],
              2
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/other_user/followers.vue":
/*!****************************************************!*\
  !*** ./resources/js/user/other_user/followers.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _followers_vue_vue_type_template_id_a05e75d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./followers.vue?vue&type=template&id=a05e75d4& */ "./resources/js/user/other_user/followers.vue?vue&type=template&id=a05e75d4&");
/* harmony import */ var _followers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./followers.vue?vue&type=script&lang=js& */ "./resources/js/user/other_user/followers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _followers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _followers_vue_vue_type_template_id_a05e75d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _followers_vue_vue_type_template_id_a05e75d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/other_user/followers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/other_user/followers.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/user/other_user/followers.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_followers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./followers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/followers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_followers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/other_user/followers.vue?vue&type=template&id=a05e75d4&":
/*!***********************************************************************************!*\
  !*** ./resources/js/user/other_user/followers.vue?vue&type=template&id=a05e75d4& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_followers_vue_vue_type_template_id_a05e75d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./followers.vue?vue&type=template&id=a05e75d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/followers.vue?vue&type=template&id=a05e75d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_followers_vue_vue_type_template_id_a05e75d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_followers_vue_vue_type_template_id_a05e75d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);