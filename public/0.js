(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product/social-share.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  props: {
    ad: {}
  },
  watch: {
    ad: function ad() {
      this.message = window.location.origin + "/view-ad-details/".concat(this.ad.id); // this.message = window.location.origin + `/product/${this.ad.id}/view`;

      this.fullUrl = window.location.origin + "/view-ad-details/".concat(this.ad.id); // this.fullUrl = window.location.origin + `/product/${this.ad.id}/view`;
    }
  },
  methods: {
    openAdShareModal: function openAdShareModal() {
      $('#openAdShareModal').modal('show');
    },
    onCopy: function onCopy(e) {
      // this.$copyText(window.location.origin + this.$route.fullPath)
      this.$toastr.success('Link copied to clipboard'); // alert('You just copied: ' + e.text)
    },
    onError: function onError(e) {
      this.$toastr.error('Failed to copy texts');
    }
  },
  mounted: function mounted() {
    console.log(this.$route.fullPath);
  },
  data: function data() {
    return {
      message: window.location.origin + "/product/".concat(this.ad.id, "/view"),
      fullUrl: window.location.origin + "/view-ad-details/".concat(this.ad.id) // fullUrl : window.location.origin + `/product/${this.ad.id}/view`,

    };
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.social-share-component{\r\n    position: relative;\n}\n.social-share-component .social-media-share-link {\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    margin-top: 15px;\r\n    margin-bottom: 5px;\r\n    padding: 0 20px;\n}\n.social-share-component .social-media-share-link .link-input,.link-copy-button{\r\n    border: none;\r\n    outline: none;\r\n    font-size: 16px;\r\n    padding: 5px 10px;\r\n    font-weight: 100;\r\n    min-width: 100px;\n}\n.social-share-component .social-media-share-link .link-input{\r\n    background-color: #f5f5f5;\r\n    color: #4e6a80;\r\n    flex-grow: 1;\r\n    border-radius: 2px 0 0 2px;\n}\n.social-share-component .social-media-share-link .link-copy-button{\r\n    background-color: #d11d1d;\r\n    border-radius: 0 2px 2px 0;\r\n    color: #ffffff;\r\n    text-decoration: none !important;\n}\n.social-share-component .social-media-share-container-top{\r\n    border-bottom: 1px solid #00000012;\r\n    font-size: 16px;\r\n    font-weight: 100;\r\n    padding-bottom: 5px;\r\n    text-align: center;\r\n    margin: -8px -15px 15px;\r\n    color: #4e6a80;\n}\n.social-share-component .social-media-share-container{\r\n    text-align: center;\n}\n.social-share-component .social-media-share-container img{\r\n    width: 40px;\r\n    height: auto;\r\n    margin-right: 15px;\n}\n@media screen and (max-width:768px) {\n.social-share-component .social-media-share-link {\r\n        padding: 0 5px;\n}\n.social-share-component .social-media-share-container img{\r\n        margin-right: 5px;\n}\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./social-share.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=template&id=4267b64c&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product/social-share.vue?vue&type=template&id=4267b64c& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "social-share-component" }, [
    _c(
      "div",
      {
        staticClass: "modal fade",
        attrs: {
          id: "openAdShareModal",
          tabindex: "-1",
          role: "dialog",
          "aria-labelledby": "exampleModalCenterTitle",
          "aria-hidden": "true"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "modal-dialog modal-dialog-centered",
            attrs: { role: "document" }
          },
          [
            _c("div", { staticClass: "modal-content" }, [
              _c("div", { staticClass: "modal-body" }, [
                _c("div", { staticClass: "social-media-share-container-top" }, [
                  _vm._v(
                    "\r\n              Share this ad with others\r\n          "
                  )
                ]),
                _vm._v(" "),
                _vm.ad
                  ? _c(
                      "div",
                      { staticClass: "social-media-share-container" },
                      [
                        _c(
                          "ShareNetwork",
                          {
                            attrs: {
                              network: "twitter",
                              url: _vm.fullUrl,
                              title: _vm.ad.ad_title,
                              description: _vm.ad.ad_description.substr(0, 100),
                              media:
                                this.ad.images &&
                                JSON.parse(this.ad.images).length
                                  ? JSON.parse(this.ad.images)[0].image
                                  : "",
                              hashtags:
                                "openbazar," +
                                JSON.parse(_vm.ad.category)[0].value
                            }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/share-icons/Twitter.png" }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "ShareNetwork",
                          {
                            attrs: {
                              network: "facebook",
                              url: _vm.fullUrl,
                              title: _vm.ad.ad_title,
                              description: _vm.ad.ad_description.substr(0, 100),
                              hashtags:
                                "openbazar," +
                                JSON.parse(_vm.ad.category)[0].value
                            }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/share-icons/facebook.png" }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "ShareNetwork",
                          {
                            attrs: {
                              network: "whatsapp",
                              url: _vm.fullUrl,
                              title: _vm.ad.ad_title,
                              description: _vm.ad.ad_description.substr(0, 100)
                            }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/share-icons/whatsapp.png" }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "ShareNetwork",
                          {
                            attrs: {
                              network: "skype",
                              url: _vm.fullUrl,
                              title: _vm.ad.ad_title,
                              description: _vm.ad.ad_description.substr(0, 100)
                            }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/share-icons/Skype.png" }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "ShareNetwork",
                          {
                            attrs: {
                              network: "email",
                              url: _vm.fullUrl,
                              title: _vm.ad.ad_title,
                              description: _vm.ad.ad_description.substr(0, 100)
                            }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/share-icons/email.png" }
                            })
                          ]
                        )
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "social-media-share-link" }, [
                  _c("input", {
                    staticClass: "link-input",
                    attrs: { placeholder: "link", disabled: "" },
                    domProps: { value: _vm.fullUrl }
                  }),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      directives: [
                        {
                          name: "clipboard",
                          rawName: "v-clipboard:copy",
                          value: _vm.message,
                          expression: "message",
                          arg: "copy"
                        },
                        {
                          name: "clipboard",
                          rawName: "v-clipboard:success",
                          value: _vm.onCopy,
                          expression: "onCopy",
                          arg: "success"
                        },
                        {
                          name: "clipboard",
                          rawName: "v-clipboard:error",
                          value: _vm.onError,
                          expression: "onError",
                          arg: "error"
                        }
                      ],
                      staticClass: "link-copy-button",
                      attrs: {
                        href: "javascript:;",
                        title: "Copy Link",
                        type: "button"
                      }
                    },
                    [_vm._v("Copy Link")]
                  )
                ])
              ])
            ])
          ]
        )
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/products/view-product/social-share.vue":
/*!*************************************************************!*\
  !*** ./resources/js/products/view-product/social-share.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _social_share_vue_vue_type_template_id_4267b64c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./social-share.vue?vue&type=template&id=4267b64c& */ "./resources/js/products/view-product/social-share.vue?vue&type=template&id=4267b64c&");
/* harmony import */ var _social_share_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./social-share.vue?vue&type=script&lang=js& */ "./resources/js/products/view-product/social-share.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _social_share_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./social-share.vue?vue&type=style&index=0&lang=css& */ "./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _social_share_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _social_share_vue_vue_type_template_id_4267b64c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _social_share_vue_vue_type_template_id_4267b64c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/view-product/social-share.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/view-product/social-share.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/products/view-product/social-share.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./social-share.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./social-share.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/products/view-product/social-share.vue?vue&type=template&id=4267b64c&":
/*!********************************************************************************************!*\
  !*** ./resources/js/products/view-product/social-share.vue?vue&type=template&id=4267b64c& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_template_id_4267b64c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./social-share.vue?vue&type=template&id=4267b64c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/social-share.vue?vue&type=template&id=4267b64c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_template_id_4267b64c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_social_share_vue_vue_type_template_id_4267b64c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);