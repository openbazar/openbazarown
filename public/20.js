(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[20],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/public/home.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_authModals_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../partials/authModals.vue */ "./resources/js/partials/authModals.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    authModals: _partials_authModals_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  created: function created() {
    $(document).ready(function () {
      $('.flexslider').flexslider({
        //animation: 'fade',
        //slideshow: false,
        controlsContainer: '.flexslider'
      });
    });
  },
  destroyed: function destroyed() {
    this.$destroy();
  },
  mounted: function mounted() {
    this.$root.$on('products-clicked', function (route) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    });
    var player = new YT.Player('youtube_iframe', {
      events: {
        'onReady': function onReady(event) {
          // console.log(`onReadyEvent`, event)
          player.playVideo(); // player.unMute()
        }
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/public/home.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.bg-openbazar-red{\n        background-color: #d11d1d !important;\n}\n.bg-openbazar-red-dark{\n        background-color: #a01111 !important;\n}\n.bg-openbazar-white{\n        background-color: #F2F2F2 !important;\n}\n.bg-openbazar-blue{\n        background-color: #617a93  !important;\n}\n.home-banner-top{\n        max-height: 250px !important;\n        height: 220px !important;\n        width: 100%;\n        max-width: 100%;\n        text-align: center !important;\n}\n.bazar-body .intro-page{\n\t\tbackground-color: white !important;\n}\n.youtube-container iframe{\n\t\tposition: absolute;\n\t\ttop: 0;\n\t\tleft: 0;\n\t\twidth: 100%;\n\t\theight: 100%;\n}\n.youtube-container{\n\t\ttext-align: center;\n\t\tmargin-top: -1px;\n\t\tbackground-color: #f2f2f2;\n\t\tdisplay: flex;\n\t\talign-items: center;\n\t\theight: 465px;\n\t\tjustify-content: center;\n}\n.youtube-container .left-side {\n\t\twidth: 40%;\n\t\theight: 100%;\n\t\ttext-align: right;\n}\n.youtube-container .left-side img {\n        width: 95%;\n\t\t/* height: 100%; */\n\t\tmax-width: 340px;\n        position: absolute;\n        right: 0;\n        bottom: 0;\n}\n.youtube-container .right-side img {\n\t\twidth: 95%;\n\t\t/* height: 100%; */\n\t\tmax-width: 340px;\n        position: absolute;\n        left: 10px;\n        bottom: 0;\n}\n.youtube-container .right-side {\n\t\twidth: 40%;\n\t\theight: 100%;\n\t\ttext-align: left;\n\t\tpadding-left: 25px;\n}\n.youtube-container .center {\n\t\twidth: 75%;\n\t\tposition: relative;\n\t\theight: 100%;\n\t\tmax-width: 900px;\n}\n.homepage-banner-container{\n        /* height: 165px; */\n        padding-top: 8px;\n}\n.homepage-banner{\n        max-height: 150px !important;\n        height: 150px !important;\n        width: 100% !important;\n        max-width: 100%;\n        text-align: center !important;\n        /* background-color: red; */\n}\n@media screen and (max-width:768px){\n.homepage-banner{\n            max-height: 320px !important;\n            height: 320px !important;\n            width: 100% !important;\n            max-width: 100%;\n            text-align: center !important;\n}\n}\n@media screen and (max-width:1152px){\n.youtube-container{\n\t\t\theight: 360px;\n}\n}\n@media screen and (max-width:1049px){\n.youtube-container{\n\t\t\theight: 300px;\n}\n}\n@media screen and (max-width:950px){\n.youtube-container{\n\t\t\theight: 275px;\n}\n}\n@media screen and (max-width:841px){\n.youtube-container{\n\t\t\theight: 245px;\n}\n}\n@media screen and (max-width:768px){\n.youtube-container{\n\t\t\theight: 400px;\n\t\t\tpadding: 10px;\n}\n.youtube-container .right-side {\n\t\t\tdisplay: none;\n}\n.youtube-container .left-side {\n\t\t\tdisplay: none;\n}\n.youtube-container .center {\n\t\t\twidth: 100%;\n\t\t\tmin-width: 100%;\n\t\t\tmax-width: 100%;\n}\n}\n@media screen and (max-width:577px){\n.youtube-container{\n\t\t\theight: 300px;\n}\n}\n@media screen and (max-width:500px){\n.youtube-container{\n\t\t\theight: 250px;\n}\n}\n@media screen and (max-width:438px){\n.youtube-container{\n\t\t\theight: 220px;\n}\n}\n\n\n\n\n\n\n\t", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/public/home.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./home.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=template&id=46a49b37&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/public/home.vue?vue&type=template&id=46a49b37& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "intro-page" }, [
    _c(
      "div",
      { staticClass: "homepage-banner-container bg-openbazar-white mb-3" },
      [
        _c("Adsense", {
          staticStyle: {
            border: "0pt none",
            display: "inline-block",
            width: "100%",
            height: "200px"
          },
          attrs: {
            "data-ad-client": "ca-pub-2462277409343800",
            "data-ad-slot": "1509062212",
            "ins-class": "home-banner-top",
            "is-new-ads-code": "yes"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "section",
      { staticClass: "intro-section" },
      [_vm._m(0), _vm._v(" "), _c("auth-modals")],
      1
    ),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "homepage-banner-container bg-openbazar-blue" },
      [
        _c("Adsense", {
          staticStyle: {
            border: "0pt none",
            display: "inline-block",
            width: "100%"
          },
          attrs: {
            "data-ad-client": "ca-pub-2462277409343800",
            "data-ad-slot": "2000016808",
            "ins-class": "homepage-banner",
            "is-new-ads-code": "yes"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "homepage-banner-container bg-openbazar-blue" },
      [
        _c("Adsense", {
          staticStyle: {
            border: "0pt none",
            display: "inline-block",
            width: "100%"
          },
          attrs: {
            "data-ad-client": "ca-pub-2462277409343800",
            "data-ad-slot": "2000016808",
            "ins-class": "homepage-banner",
            "is-new-ads-code": "yes"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(3),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "homepage-banner-container" },
      [
        _c("Adsense", {
          staticStyle: {
            border: "0pt none",
            display: "inline-block",
            width: "100%"
          },
          attrs: {
            "data-ad-client": "ca-pub-2462277409343800",
            "data-ad-slot": "2000016808",
            "ins-class": "homepage-banner",
            "is-new-ads-code": "yes"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(4),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "homepage-banner-container" },
      [
        _c("Adsense", {
          staticStyle: {
            border: "0pt none",
            display: "inline-block",
            width: "100%"
          },
          attrs: {
            "data-ad-client": "ca-pub-2462277409343800",
            "data-ad-slot": "2000016808",
            "ins-class": "homepage-banner",
            "is-new-ads-code": "yes"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(5),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "homepage-banner-container" },
      [
        _c("Adsense", {
          staticStyle: {
            border: "0pt none",
            display: "inline-block",
            width: "100%"
          },
          attrs: {
            "data-ad-client": "ca-pub-2462277409343800",
            "data-ad-slot": "2000016808",
            "ins-class": "homepage-banner",
            "is-new-ads-code": "yes"
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _vm._m(6)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "intro-text-area" }, [
      _c("div", { staticClass: "intro-heading-container" }, [
        _c("div", { staticClass: "open-baraz-logo" }, [
          _c("img", { attrs: { src: "/bazar/logo.png" } })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "welcome-title" }, [
          _vm._v("Welcome "),
          _c("span", { staticClass: "welcome-to" }, [_vm._v("to")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "open-bazar-title" }, [_vm._v("Open BAZAR")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "intro-text" }, [
        _vm._v(
          "\r\n                Open Bazar is an online classifieds and advertising services platform in Europe and Asia that brings together millions of users without central institutions or intermediaries, to exchange their goods, products or find anything in their community. The platform allows merchants to gain an advantage between users and users by combining Open Bazar credits and coins through an open wallet and coin system. Users and traders can browse a wide variety of goods, services, real estate, Cars, rentals, electronics, fashion, jobs, and more.\r\n            "
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "seprate" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "youtube-container" }, [
      _c("div", { staticClass: "left-side position-relative" }, [
        _c("img", { attrs: { src: "/bazar/youtube-left-image-1.png" } })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "center" }, [
        _c(
          "div",
          { staticClass: "videoWrapper", attrs: { id: "videoWithJs" } },
          [
            _c("iframe", {
              attrs: {
                id: "youtube_iframe",
                src:
                  "https://www.youtube.com/embed/zS1MNMBlpoE?enablejsapi=1&autoplay=1&mute=1",
                frameborder: "0",
                allow: "autoplay",
                allowfullscreen: ""
              }
            })
          ]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "right-side position-relative" }, [
        _c("img", { attrs: { src: "/bazar/youtube-right-image-1.png" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      { staticClass: "hero-section tablediv", attrs: { id: "home" } },
      [
        _c("div", { staticClass: "intablediv" }, [
          _c("div", { staticClass: "container" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-12 col-lg-6" }, [
                _c("h1", { staticClass: "hero-text" }, [
                  _vm._v(
                    "Online classifieds platform in Europe and Asia that brings millions of users together "
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row download-app-buttons" }, [
                  _c(
                    "div",
                    { staticClass: "col-xs-6 col-6 col-sm-6 col-md-6" },
                    [
                      _c(
                        "a",
                        {
                          attrs: {
                            href:
                              "https://apps.apple.com/nl/app/open-bazar/id1486285030?l=en",
                            target: "_blank"
                          }
                        },
                        [
                          _c("img", {
                            staticClass: "img-responsive center-block",
                            attrs: { src: "/imgs/store_apple.png", alt: "" }
                          })
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-xs-6 col-6 col-sm-6 col-md-6" },
                    [
                      _c(
                        "a",
                        {
                          attrs: {
                            href:
                              "https://play.google.com/store/apps/details?id=com.open_bazar",
                            target: "_blank"
                          }
                        },
                        [
                          _c("img", {
                            staticClass: "img-responsive center-block",
                            attrs: { src: "/imgs/store_google.png", alt: "" }
                          })
                        ]
                      )
                    ]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-12 col-lg-6 center-block" }, [
                _c("div", { staticClass: "flex-container center-block" }, [
                  _c("div", { staticClass: "flexslider flexslider-main" }, [
                    _c("div", { staticClass: "iphone-notch" }),
                    _vm._v(" "),
                    _c("ul", { staticClass: "slides" }, [
                      _c("li", [
                        _c("img", {
                          attrs: { src: "/bazar/mobile-slide-11.png", alt: "" }
                        })
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("img", {
                          attrs: { src: "/bazar/mobile-slide-2.png", alt: "" }
                        })
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("img", {
                          attrs: { src: "/bazar/mobile-slide-3.png", alt: "" }
                        })
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("img", {
                          attrs: { src: "/bazar/mobile-slide-4.png", alt: "" }
                        })
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("img", {
                          attrs: { src: "/bazar/mobile-slide-5.png", alt: "" }
                        })
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("img", {
                          attrs: { src: "/bazar/mobile-slide-6.png", alt: "" }
                        })
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      {
        staticClass: "content-section text-center grey-background",
        attrs: { id: "about" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-4 mb-30" }, [
              _c("div", { staticClass: "card-box" }, [
                _c("h2", { staticClass: "red-text" }, [_vm._v("About")]),
                _vm._v(" "),
                _c("hr"),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Open Bazar is an online classifieds and advertising service platform in Europe and Asia that brings millions of users together without central institutions or intermediaries, to exchange their goods, products or find anything in their community. The platform enables merchants -to-user and user-to-user benefit in combination of credits and Open Bazar’s Coins through open bazar wallet and coins system. Users and merchants can browse through a large variety of Goods, Services, Real Estate, Rentals, Electronics, Fashion, Jobs and more other items."
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 mb-30" }, [
              _c("div", { staticClass: "card-box" }, [
                _c("h2", { staticClass: "red-text" }, [_vm._v("Mission")]),
                _vm._v(" "),
                _c("hr"),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Open Bazar aims to empower merchants and users who want to take part in shaping the future of their finance by using our platform to buy, sell, rent, lend, offer service, open shop or find anything in their community. A community where underused goods are redistributed to fill a new need, and become wanted again, where non- produc/such as space, skills, money and coins are exchanged and traded in new ways of our well designed Wallet and credits system that don’t always require centralized institutions or ‘middlemen’."
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 mb-30" }, [
              _c("div", { staticClass: "card-box" }, [
                _c("h2", { staticClass: "red-text" }, [_vm._v("Vision")]),
                _vm._v(" "),
                _c("hr"),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Open Bazar leads its strong passion for the sustainable development of an innovative platform, enabling one of the fastest growing network communities in Europe and Asia In a rapidly globalizing world, our goal is to become a meeting place for shopping lovers, wandering adventurers and all Internet users who are looking for something new to do or try, no matter where they are. One platform accommodates the buyers, the sellers and the merchants and meets their needs, where the merchant can be buyer and every user can sell."
                  )
                ])
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      { staticClass: "content-section", attrs: { id: "features" } },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-md-12 col-lg-6 offset-lg-3 text-center" },
              [
                _c("h2", { staticClass: "red-text" }, [_vm._v("Features")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Distinguishing advertisements for faster selling, buying, renting, shops and offering services at one place. Find what you want quickly with our easy and smart App, get more views by adding up to 30 pictures to your Ad, and benefit from earning coins and follow favorite users."
                  )
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("br"),
          _c("br"),
          _vm._v(" "),
          _c("div", { staticClass: "row features-list-row" }, [
            _c("div", { staticClass: "col-sm-12 col-md-12 col-lg-4" }, [
              _c("h3", { staticClass: "red-text" }, [
                _vm._v("Friends network")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Invite your friends through social networks and contacts and be aware of their latest ads "
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h3", { staticClass: "red-text" }, [_vm._v("Job Feature")]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Ability to communicate with the seller or buyer or advertisers about vacancies through the application."
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h3", { staticClass: "red-text" }, [
                _vm._v("Like, post Ad and invite friend")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "We have an exclusive earning opportunity for our users to get paid for liking ads, inviting friends and posting ads on Open Bazar"
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h3", { staticClass: "red-text" }, [
                _vm._v("All in one Platform")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Always wanted to Shop everything in one place with multiple variations of brands and products, at one time? Good news, We have got just the right app for you!"
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-sm-12 col-md-12 col-lg-4" }, [
              _c(
                "div",
                { staticClass: "flex-container center-block text-center" },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "flexslider flexslider-features center-block text-center"
                    },
                    [
                      _c("div", { staticClass: "iphone-notch" }),
                      _vm._v(" "),
                      _c("ul", { staticClass: "slides" }, [
                        _c("li", [
                          _c("img", {
                            attrs: {
                              src: "/bazar/mobile-slide-11.png",
                              alt: ""
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("img", {
                            attrs: { src: "/bazar/mobile-slide-2.png", alt: "" }
                          })
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("img", {
                            attrs: { src: "/bazar/mobile-slide-3.png", alt: "" }
                          })
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("img", {
                            attrs: { src: "/bazar/mobile-slide-4.png", alt: "" }
                          })
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("img", {
                            attrs: { src: "/bazar/mobile-slide-5.png", alt: "" }
                          })
                        ]),
                        _vm._v(" "),
                        _c("li", [
                          _c("img", {
                            attrs: { src: "/bazar/mobile-slide-6.png", alt: "" }
                          })
                        ])
                      ])
                    ]
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-sm-12 col-md-12 col-lg-4" }, [
              _c("h3", { staticClass: "red-text" }, [
                _vm._v("Free Application ")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Fashionable and attractive design will appeal to you, it is fast, smart and easy to us"
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h3", { staticClass: "red-text" }, [
                _vm._v("Notification system")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "An advanced and smart notification system to receive all updates about your Ads and your followers Ads."
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h3", { staticClass: "red-text" }, [_vm._v("Follow users")]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Follow your favorite users and stay up to date with their latest ads and be as the first one receive notification if they post an Ad. "
                )
              ]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("h3", { staticClass: "red-text" }, [
                _vm._v("Within 30 second post Ad")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Publish your ad in 30 second or browse our hundreds of categories, from vehicles and apartments, real estate and more, all from your mobile phone or tablet!"
                )
              ])
            ])
          ])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "home-credit-packages text-center" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-8 offset-lg-2" }, [
            _c("h2", [_vm._v("Credit Packages")]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Easier was not before! Now you can make one-time payment with the wallet and buy credit  then use it to feature your Ad, repost and buy a package or even upgrade your membership"
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("br"),
        _c("br"),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-3 col-sm-6" }, [
            _c("img", {
              staticClass: "center-block img-responsive",
              attrs: { src: "/imgs/package_standard.png", alt: "" }
            }),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Add credit to your wallet by making one-time payment to buy our various offerings or upgrade your package of membership. "
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-3 col-sm-6" }, [
            _c("img", {
              staticClass: "center-block img-responsive",
              attrs: { src: "/imgs/package_advanced.png", alt: "" }
            }),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "The easiest and fastest way to repost Ad, Feature Ad or Rocket Ad to get more views, no need to use credit card or transfer money just use your credits from your Wallet and save your time."
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-3 col-sm-6" }, [
            _c("img", {
              staticClass: "center-block img-responsive",
              attrs: { src: "/imgs/package_premium.png", alt: "" }
            }),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "We Offer you the smartest and safest way to make a benefit from our large variety and help you to shape your financial future"
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-3 col-sm-6" }, [
            _c("img", {
              staticClass: "center-block img-responsive",
              attrs: { src: "/imgs/package_supreme.png", alt: "" }
            }),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Start your own business and sell your goods or open your shop and create online a solid network and get more buyers without central institutions or intermediaries"
              )
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      { staticClass: "content-section text-center more-features" },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-10 offset-1" }, [
              _c("h2", { staticClass: "red-text" }, [
                _vm._v(
                  "Earn more and faster by using our various offerings now! Get more offers and views, and sell or rent multiple products at one time."
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _c("br"),
          _c("br"),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              {
                staticClass:
                  "col-md-4 col-sm-6 col-xs-12 border-bottom border-right"
              },
              [
                _c("img", {
                  staticClass: "center-block img-responsive",
                  attrs: { src: "/imgs/ic_wallet.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h3", { staticClass: "mt-4 mb-3" }, [_vm._v("Wallet")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Open Bazar Wallet is a secure encryption online tool which ensure that your credits always safe and it helps you collect rewards, store credits and buy various offerings and redeem Coins multiple times as and when required"
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "col-md-4 col-sm-6 col-xs-12 border-bottom border-right"
              },
              [
                _c("img", {
                  staticClass: "center-block img-responsive",
                  attrs: { src: "/imgs/ic_coins.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h3", { staticClass: "mt-4 mb-3" }, [_vm._v("Coins")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Open Bazar Coins is an Affiliate system which allows you to collect Coins by inviting friends, posting Ads or like Ads and it helps you collect rewards, store credits and get various offerings and redeem Coins multiple times as and when required"
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-md-4 col-sm-6 col-xs-12 border-bottom" },
              [
                _c("img", {
                  staticClass: "center-block img-responsive",
                  attrs: { src: "/imgs/ic_membership.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h3", { staticClass: "mt-4 mb-3" }, [_vm._v("Membership")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "We offer 4 various of membership, Bronze to get more active Ads, Silver to get more Views and likes, Gold to shape your financial future and Diamond to create a solid network"
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-md-4 col-sm-6 col-xs-12 border-right" },
              [
                _c("img", {
                  staticClass: "center-block img-responsive",
                  attrs: { src: "/imgs/ic_repost.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h3", { staticClass: "mt-4 mb-3" }, [_vm._v("Repost Ad")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Want to renew your ad's date of publication? Upload your ad to update and restore it at the top of the list and get up to 10 more views "
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-md-4 col-sm-6 col-xs-12 border-right" },
              [
                _c("img", {
                  staticClass: "center-block img-responsive",
                  attrs: { src: "/imgs/ic_features.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h3", { staticClass: "mt-4 mb-3" }, [_vm._v("Features Ad")]),
                _vm._v(" "),
                _c("p", { staticClass: "text-green" }, [
                  _vm._v(
                    "Get your Ad in the top section of the list with special badge and get up to 10 times more views"
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 col-sm-6 col-xs-12" }, [
              _c("img", {
                staticClass: "center-block img-responsive",
                attrs: { src: "/imgs/ic_rocket.png", alt: "" }
              }),
              _vm._v(" "),
              _c("h3", { staticClass: "mt-4 mb-3" }, [_vm._v("Rocket")]),
              _vm._v(" "),
              _c("p", { staticClass: "text-green" }, [
                _vm._v(
                  "Get your Ad an Urgent badge and get up to 30 times more views to sell easier and faster"
                )
              ])
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/public/home.vue":
/*!**************************************!*\
  !*** ./resources/js/public/home.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _home_vue_vue_type_template_id_46a49b37___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.vue?vue&type=template&id=46a49b37& */ "./resources/js/public/home.vue?vue&type=template&id=46a49b37&");
/* harmony import */ var _home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.vue?vue&type=script&lang=js& */ "./resources/js/public/home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _home_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.vue?vue&type=style&index=0&lang=css& */ "./resources/js/public/home.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _home_vue_vue_type_template_id_46a49b37___WEBPACK_IMPORTED_MODULE_0__["render"],
  _home_vue_vue_type_template_id_46a49b37___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/public/home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/public/home.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/public/home.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/public/home.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************!*\
  !*** ./resources/js/public/home.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./home.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/public/home.vue?vue&type=template&id=46a49b37&":
/*!*********************************************************************!*\
  !*** ./resources/js/public/home.vue?vue&type=template&id=46a49b37& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_template_id_46a49b37___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./home.vue?vue&type=template&id=46a49b37& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/public/home.vue?vue&type=template&id=46a49b37&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_template_id_46a49b37___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_home_vue_vue_type_template_id_46a49b37___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);