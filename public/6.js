(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    main_product: {},
    product: {},
    select_attribute_step: {},
    current_attributes: {},
    attributes: {}
  },
  data: function data() {
    return {
      attribute_step: 0,
      selected_attributes: []
    };
  },
  watch: {
    select_attribute_step: function select_attribute_step() {
      // console.log(this.select_attribute_step , 'select_attribute_step update')
      this.attribute_step = this.select_attribute_step;
    },
    current_attributes: function current_attributes() {
      // console.log(this.selected_attributes , 'selected_attributes update')
      this.selected_attributes = this.current_attributes;
    }
  },
  mounted: function mounted() {
    this.attribute_step = this.select_attribute_step; // console.log(this.current_attributes)

    if (this.current_attributes) {
      this.selected_attributes = this.current_attributes;
    } else {
      this.selected_attributes = [];
    }
  },
  methods: {
    selectAttribute: function selectAttribute(attribute) {
      if (this.attribute_step <= this.attributes.length) {
        var attr = Object.assign({}, {
          id: "",
          tital: this.attributes[this.attribute_step],
          value: attribute,
          image: "",
          seletecvalue: attribute
        }); // console.log(attribute)

        if (this.selected_attributes[this.attribute_step]) {
          this.selected_attributes[this.attribute_step] = attr;
        } else {
          // this.selected_attributes.push(attr);
          this.$set(this.selected_attributes, this.selected_attributes.length, attr); // this.selected_attributes.push(attr);
        }

        this.attribute_step = this.attribute_step + 1;
      }

      if (this.attribute_step == this.attributes.length) {
        this.$emit('product-attribute-selected', this.selected_attributes, true); // console.log('emitting' , this.selected_attributes )
      }
    },
    prevAttribute: function prevAttribute() {
      if (this.attribute_step > 0) {
        this.attribute_step = this.attribute_step - 1;
      } else {}
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-loading-overlay */ "./node_modules/vue-loading-overlay/dist/vue-loading.min.js");
/* harmony import */ var vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_post_ad_product_attributes_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/post-ad/product-attributes.vue */ "./resources/js/components/post-ad/product-attributes.vue");
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../partials/loader.vue */ "./resources/js/partials/loader.vue");
/* harmony import */ var _view_product_showMarker_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view-product/showMarker.vue */ "./resources/js/products/view-product/showMarker.vue");
/* harmony import */ var _view_product_social_share_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./view-product/social-share.vue */ "./resources/js/products/view-product/social-share.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Loading: vue_loading_overlay__WEBPACK_IMPORTED_MODULE_0___default.a,
    Loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    ProductAttributes: _components_post_ad_product_attributes_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    ShowMarker: _view_product_showMarker_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    SocialShare: _view_product_social_share_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  props: {
    base_ad: {},
    notification: {}
  },
  watch: {
    $route: function $route(to, from) {
      this.loadAdDetails(); // react to route changes...

      var showLoginRoutes = ['post-ad', 'chat', 'notifications'];
      console.log(showLoginRoutes.includes(to.name), to);

      if (showLoginRoutes.includes(to.name) && (!store.getters.isAuthenticated || !store.getters.isAuthenticated.token)) {
        $('#loginModal').modal('show');
      }
    }
  },
  computed: {
    ad_attributes: function ad_attributes() {
      if (this.ad.category) {
        return JSON.parse(this.ad.category);
      } else {
        return [];
      }
    },
    ad_images: function ad_images() {
      if (this.ad.images && JSON.parse(this.ad.images).length) {
        // console.log(JSON.parse(this.ad.category));
        return JSON.parse(this.ad.images);
      } else {
        return false;
      }
    }
  },
  mounted: function mounted() {
    // channel_condition = object.booleanMethod();
    // if (channel_condition) {
    //   my_google_ad_channel = '123457789';
    // }
    // else {
    //   my_google_ad_channel = '263477489';
    // }
    // (adsbygoogle = window.adsbygoogle || []).push({
    //   params: { google_ad_channel: my_google_ad_channel}
    // });
    window.addEventListener('scroll', this.currentScroll);
    this.loadAdDetails();

    if (this.notification) {
      this.markNotification();
    }

    $(".post-report:checkbox").click(function () {
      $(".post-report").not(this).prop('checked', false);
      $(this).attr("checked", true);
    });
    firebase.app(); // Get a reference to the database service

    var database = firebase.database(); // Provide custom logger which prefixes log statements with "[FIREBASE]"

    firebase.database.enableLogging(function (message) {// console.log("[FIREBASE]", message);
    });
    firebase.database().ref('Users/user10174').once('value', function (snapshot) {
      console.log('Users/user10174', snapshot.val());
    });
    firebase.database().ref('Users/user10173').once('value', function (snapshot) {
      console.log('Users/user10173', snapshot.val());
    });
  },
  methods: {
    reportAdModalFunc: function reportAdModalFunc() {
      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        $('#reportAdModal').modal('show');
      } else {
        $('#loginModal').modal('show');
      }
    },
    currentScroll: function currentScroll() {
      if (document.documentElement.scrollTop > 0) {
        this.topSpace = '80px !important;';
      } else {
        this.topSpace = 'auto !important;';
      } // console.log(`document.documentElement.scrollTop`, document.documentElement.scrollTop)
      // console.log(`window.pageXOffset`, window.pageXOffset)

    },
    loadAdDetails: function loadAdDetails() {
      var _this = this;

      this.loading = true; // axios.post('/v1/getpost_detail', {

      axios.post('/getpost_detail', {
        id: this.$route.params.id,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        console.log(res, 'res');
        _this.adsenseKey += 1;

        if (res.data.status == 'success') {
          _this.loading = false;
          _this.ad = res.data.data[0];

          if (_this.ad.price_type == 'price') {
            _this.comment_type = 0;
          } else {
            _this.comment_type = 1;
          }

          _this.loadSimilarAds();

          _this.loadUserInfo();

          _this.loadAdOffers();

          _this.loadAdComments();

          _this.defineAdQuality();

          _this.initializeSliders();

          _this.updateViewAd();

          _this.getFirebaseChat();

          _this.getFirebaseChatInstance(); // if(this.ad_images){
          // }

        } else {
          _this.loading = false;

          _this.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    getFirebaseChatInstance: function getFirebaseChatInstance() {
      var _this2 = this;

      firebase.database().ref('Chat/user' + this.ad.user_id + '/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id).once('value', function (snapshot) {
        console.log("user_ads", 'Chat/user' + _this2.ad.user_id + '/ads' + _this2.ad.id + '_user' + _this2.$store.getters.isAuthenticated.id);
        _this2.current_chat_instance = snapshot.val();
        console.log("this.current_chat_instance", _this2.current_chat_instance ? 'Found' : "Not Found");
        console.log("this.current_chat_instance", _this2.current_chat_instance);
      });
      firebase.database().ref('Chat/user' + this.$store.getters.isAuthenticated.id + '/ads' + this.ad.id + '_user' + this.ad.user_id).once('value', function (snapshot) {
        console.log("user_ads", 'Chat/user' + _this2.$store.getters.isAuthenticated.id + '/ads' + _this2.ad.id + '_user' + _this2.ad.user_id);
        _this2.other_chat_instance = snapshot.val();
        console.log("this.other_chat_instance", _this2.other_chat_instance ? 'Found' : "Not Found");
        console.log("this.other_chat_instance", _this2.other_chat_instance);
      });
    },
    likeAd: function likeAd() {
      var _this3 = this;

      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        if (this.ad.like_stattus != 'Yes' && !this.making_like) {
          this.making_like = true;
          axios.post('/v1/ad_view', {
            post_id: this.ad.id,
            user_id: this.$store.getters.isAuthenticated.id,
            type: 'likes'
          }, {
            headers: {
              Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
            }
          }).then(function (res) {
            _this3.making_like = false;

            if (res.data.data.likes == '1') {
              _this3.ad.like_stattus = "Yes";
              _this3.ad.like_count = _this3.ad.like_count + 1;
            }
          })["catch"](function (err) {
            // this.loading = false
            _this3.making_like = false;

            _this3.$toastr.error('An unexpected error occurred');
          });
        }
      } else {
        $('#loginModal').modal('show');
      }
    },
    favoriteAd: function favoriteAd() {
      var _this4 = this;

      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        if (!this.making_favorite) {
          this.making_favorite = true;
          axios.post('/v1/ad_view', {
            post_id: this.ad.id,
            user_id: this.$store.getters.isAuthenticated.id,
            type: 'favorite'
          }, {
            headers: {
              Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
            }
          }).then(function (res) {
            _this4.making_favorite = false;

            if (res.data.data.favorite == '1') {
              _this4.ad.fav_stattus = "Yes";
              _this4.ad.favorite_count = _this4.ad.favorite_count + 1;
            } else {
              _this4.ad.fav_stattus = "No";
              _this4.ad.favorite_count = _this4.ad.favorite_count - 1;
            }
          })["catch"](function (err) {
            _this4.making_favorite = false;

            _this4.$toastr.error('An unexpected error occurred');
          });
        }
      } else {
        $('#loginModal').modal('show');
      }
    },
    updateViewAd: function updateViewAd() {
      var _this5 = this;

      axios.post('/v1/ad_view', {
        post_id: this.ad.id,
        user_id: this.$store.getters.isAuthenticated.id,
        type: 'view'
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        if (res.data.status == 'fail') {
          _this5.ad.views_count = _this5.ad.views_count + 1;
        }
      })["catch"](function (err) {// this.$toastr.error('An unexpected error occurred');
      });
    },
    deleteAd: function deleteAd() {
      var _this6 = this;

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#4e6a80',
        cancelButtonColor: '#d11d1d' // confirmButtonText: 'Delete'

      }).then(function (result) {
        if (result.isConfirmed) {
          _this6.loading = true;
          axios.post('/v1/delete_ads', {
            user_id: _this6.$store.getters.isAuthenticated.id,
            id: _this6.ad.id
          }, {
            headers: {
              Authorization: "Bearer " + _this6.$store.getters.isAuthenticated.token
            }
          }).then(function (res) {
            if (res.data.status == 'success') {
              _this6.loading = false;

              _this6.$router.back();
            } else {
              _this6.loading = false;

              _this6.$toastr.error(res.data.message);
            }
          })["catch"](function (err) {
            _this6.loading = false;

            _this6.$toastr.error('An unexpected error occurred');
          }); // Swal.fire(
          // 'Deleted!',
          // 'Your file has been deleted.',
          // 'success'
          // )
        }
      });
    },
    loadSimilarAds: function loadSimilarAds() {
      var _this7 = this;

      this.loading = true;
      var customAxios = axios.create({
        headers: {
          'Content-Type': 'application/json'
        }
      });
      customAxios.post('/v1/similar_ads', {
        category_id: JSON.parse(this.ad.category)[0].id
      }).then(function (res) {
        if (res.data.status == 'success') {
          _this7.loading = false;
          _this7.similar_ads = res.data.data;
        } else {
          _this7.loading = false;

          _this7.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this7.loading = false;

        _this7.$toastr.error('An unexpected error occurred');
      });
    },
    loadUserInfo: function loadUserInfo() {
      var _this8 = this;

      axios.post('/v1/user_details', {
        id: this.ad.user_id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        if (res.data.status == 'success') {
          _this8.user_info = res.data.user_detail;
        } else {
          _this8.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this8.$toastr.error('An unexpected error occurred');
      });
    },
    loadAdOffers: function loadAdOffers() {
      var _this9 = this;

      var customAxios = axios.create({
        headers: {
          'Content-Type': 'application/json'
        }
      });
      customAxios.post('/v1/get_offer_bid', {
        index: 0,
        bid_type: 0,
        post_id: this.ad.id
      }).then(function (res) {
        if (res.data.status == 'success') {
          _this9.offers = res.data.data;
        }
      })["catch"](function (err) {
        _this9.$toastr.error('An unexpected error occurred');
      });
    },
    loadAdComments: function loadAdComments() {
      var _this10 = this;

      var customAxios = axios.create({
        headers: {
          'Content-Type': 'application/json'
        }
      });
      customAxios.post('/v1/get_offer_bid', {
        index: 0,
        bid_type: this.comment_type,
        post_id: this.ad.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        if (res.data.status == 'success') {
          if (_this10.comment_type == 0) {
            _this10.offers = res.data.data;
          } else {
            _this10.comments = res.data.data;
          }
        }
      })["catch"](function (err) {
        _this10.$toastr.error('An unexpected error occurred');
      });
    },
    follow: function follow() {
      var _this11 = this;

      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        if (!this.follow_loading) {
          this.follow_loading = true;
          axios.post('/v1/follow', {
            user_id: this.$store.getters.isAuthenticated.id,
            following_id: this.ad.user_id
          }, {
            headers: {
              Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
            }
          }).then(function (res) {
            if (res.data.status == 'success') {
              // this.user_info = res.data.user_detail
              _this11.loadAdDetails();

              _this11.follow_loading = false;
            } else {
              _this11.$toastr.error(res.data.message);

              _this11.follow_loading = false;
            }
          })["catch"](function (err) {
            _this11.$toastr.error('An unexpected error occurred');

            _this11.follow_loading = false;
          });
        }
      } else {
        $('#loginModal').modal('show');
      }
    },
    sendComment: function sendComment() {
      var _this12 = this;

      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        if (!this.commenting && this.new_comment != '') {
          this.commenting = true;
          axios.post('/v1/offer_bid', {
            user_id: this.$store.getters.isAuthenticated.id,
            post_id: this.ad.id,
            bid_price: this.new_comment,
            bid_type: this.comment_type,
            timestamp: new Date().getTime()
          }, {
            headers: {
              Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
            }
          }).then(function (res) {
            if (res.data.status == 'success') {
              if (_this12.comment_type == 0) {
                _this12.createFirebaseOffer(_this12.new_comment);

                _this12.loadAdOffers();
              } else {
                _this12.loadAdComments();
              }

              _this12.new_comment = '';
              _this12.commenting = false;

              _this12.$toastr.success("".concat(_this12.comment_type ? 'Comment' : 'Offer', " added"));
            } else {
              _this12.$toastr.error(res.data.message);

              _this12.commenting = false;
            }
          })["catch"](function (err) {
            _this12.$toastr.error('An unexpected error occurred');

            _this12.commenting = false;
          });
        } // else{
        //     Swal.fire({
        //         icon: 'error',
        //         title: 'Comment is required',
        //         showConfirmButton: false
        //     });
        // }

      } else {
        $('#loginModal').modal('show');
      }
    },
    handleReportCase: function handleReportCase(event, reason) {
      if (event.target.checked) {
        this.report_case = reason;
      } else {
        this.report_case = "";
      }
    },
    submitReportPost: function submitReportPost() {
      var _this13 = this;

      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        if (this.report_case && !this.reporting_post) {
          this.reporting_post = true;
          axios.post('/v1/post_report', {
            user_id: this.$store.getters.isAuthenticated.id,
            post_id: this.ad.id,
            reason: this.report_case,
            description: this.report_case_description,
            type: 1
          }, {
            headers: {
              Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
            }
          }).then(function (res) {
            if (res.data.status == 'success') {
              // this.user_info = res.data.user_detail
              _this13.reporting_post = false;
              _this13.report_case = "";
              _this13.report_case_description = "";
              $('.post-report').prop('checked', false);

              _this13.$toastr.success('Post reported successfully');

              $('#reportAdModal').modal('hide');
            } else {
              _this13.$toastr.error(res.data.message);

              _this13.reporting_post = false;
            }
          })["catch"](function (err) {
            _this13.$toastr.error('An unexpected error occurred');

            _this13.reporting_post = false;
          });
        }
      } else {
        $('#loginModal').modal('show');
      }
    },
    deleteComment: function deleteComment(comment) {
      var _this14 = this;

      this.deleting_comment = comment;
      axios.post('/v1/delete_offers', {
        id: comment.id,
        post_id: this.ad.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        if (res.data.status == 'success') {
          if (_this14.comment_type == 1) {
            _this14.comments.splice(_this14.comments.indexOf(comment), 1);
          } else {
            _this14.offers.splice(_this14.offers.indexOf(comment), 1);
          }
        }
      })["catch"](function (err) {
        _this14.$toastr.error('An unexpected error occurred');
      });
    },
    getFirebaseChat: function getFirebaseChat() {
      var _this15 = this;

      firebase.database().ref('messages/ads' + this.ad.id + '_user' + this.ad.user_id + '/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id).on("value", function (snapshot) {
        _this15.currentChat = snapshot.val();
      });
      firebase.database().ref('messages/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id + '/ads' + this.ad.id + '_user' + this.ad.user_id).on("value", function (snapshot) {
        _this15.otherCurrentChat = snapshot.val();
      });
    },
    startChat: function startChat() {
      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        this.chatLoading = true;
        var price = '';

        if (this.ad.price_type != 'price') {
          price = this.ad.price_type;
        } else {
          price = this.ad.price;
        }

        var new_ad = {
          ad_id: this.ad.id.toString(),
          image: this.ad.images ? JSON.parse(this.ad.images)[0].image : '',
          mobileno: this.ad.mobile ? this.ad.mobile : "",
          mobileno_status: this.ad.hide_mobileno,
          price: price.toString(),
          title: this.ad.ad_title
        };
        firebase.database().ref('Ads/ads' + this.ad.id).set(_objectSpread({}, new_ad));

        if (this.ad.user_id == this.$store.getters.isAuthenticated.id) {
          this.$router.push({
            name: 'chat'
          });
        } else {
          var chat_instance = "";

          if (!this.current_chat_instance || !this.other_chat_instance) {
            chat_instance = {
              timestamp: new Date().getTime(),
              "delete": false,
              seen: false
            };
          } else {
            // console.log(`this.current_chat_instance && this.current_chat_instance.delete`, this.current_chat_instance && !this.current_chat_instance.delete)
            // this.checkFirebaseChatInstance(this.current_chat_instance)
            // this.checkFirebaseChatInstance(this.other_chat_instance)
            // if((this.current_chat_instance && this.current_chat_instance.delete) || (this.other_chat_instance && this.other_chat_instance.delete)){
            if (!this.checkFirebaseChatInstance(this.current_chat_instance) || !this.checkFirebaseChatInstance(this.other_chat_instance)) {
              chat_instance = {
                timestamp: new Date().getTime(),
                "delete": false,
                seen: false
              };
            } // else{
            //     chat_instance = {
            //         timestamp : new Date().getTime(),
            //         delete : false,
            //         seen :false,
            //     }
            // }

          }

          var __user = {};
          firebase.database().ref('Users/user' + this.ad.user_id).once('value', function (snapshot) {
            __user.user = snapshot.val();
          });
          __user.ad = new_ad;
          var chat = {
            ad: this.ad,
            user: this.user_info,
            chat_instance: chat_instance,
            __user: __user
          };
          console.log("chat", chat);
          this.$router.push({
            name: 'chat',
            params: {
              newChat: chat
            }
          });
        }
      } else {
        $('#loginModal').modal('show');
      }
    },
    checkFirebaseChatInstance: function checkFirebaseChatInstance(instance) {
      console.log("instance", instance);
      console.log("instance", Object.keys(instance));
      var check = false;

      if (Object.keys(instance).length) {
        check = Object.keys(instance).includes('seen') && Object.keys(instance).includes('delete') && Object.keys(instance).includes('timestamp') && !instance["delete"];
      }

      console.log("check", check);
      return check;
    },
    createFirebaseOffer: function createFirebaseOffer(bid_price) {
      var _this16 = this;

      var timestamp = new Date().getTime();
      var new_ad = {
        ad_id: this.ad.id.toString(),
        images: this.ad.images ? JSON.parse(this.ad.images)[0].image : '',
        mobileno: this.ad.mobile ? this.ad.mobile : "",
        mobileno_status: this.ad.hide_mobileno,
        price: this.ad.price.toString(),
        title: this.ad.ad_title
      };
      firebase.database().ref('Ads/ads' + this.ad.id).set(_objectSpread({}, new_ad));
      var chat_instance = "";

      if (!this.current_chat_instance || !this.other_chat_instance) {
        chat_instance = {
          timestamp: new Date().getTime(),
          seen: false,
          "delete": false
        };
        firebase.database().ref('Chat/user' + this.ad.user_id + '/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id).set(_objectSpread({}, chat_instance));
        firebase.database().ref('Chat/user' + this.$store.getters.isAuthenticated.id + '/ads' + this.ad.id + '_user' + this.ad.user_id).set(_objectSpread({}, chat_instance));
      }

      var message = [];
      message[timestamp] = {
        message: this.$store.getters.getCuerrency + ' ' + bid_price,
        seen: false,
        timestamp: timestamp,
        status: 'pending',
        type: 'offer',
        user: 'user' + this.$store.getters.isAuthenticated.id,
        user_name: this.$store.getters.isAuthenticated.name
      };
      firebase.database().ref('messages/ads' + this.ad.id + '_user' + this.ad.user_id + '/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id).set(_objectSpread(_objectSpread({}, this.currentChat), message));
      firebase.database().ref('messages/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id + '/ads' + this.ad.id + '_user' + this.ad.user_id).set(_objectSpread(_objectSpread({}, this.otherCurrentChat), message));
      var reciever = "";
      console.log('Users/user' + this.ad.user_id);
      firebase.database().ref('Users/user' + this.ad.user_id).once('value', function (snapshot) {
        reciever = snapshot.val();

        if (reciever) {
          reciever.msgcount = (parseInt(reciever.msgcount) + 1).toString();
          firebase.database().ref('Users/user' + _this16.ad.user_id).set(_objectSpread({}, reciever));
        }
      });
      this.getFirebaseChatInstance();
      this.sendPushNotification(message);
      message = '';
    },
    sendPushNotification: function sendPushNotification(message) {
      axios.post('/v1/chat_push', {
        id: this.ad.user_id,
        massage: 'You have a new offer from ' + this.$store.getters.isAuthenticated.name,
        data: ""
      });
    },
    deleteOffer: function deleteOffer(offer) {
      this.deleting_comment = offer;
      console.log(offer);
    },
    defineAdQuality: function defineAdQuality() {
      if (this.ad) {
        if (this.ad.images) {
          var images = JSON.parse(this.ad.images); // console.log(images)

          if (images.length > 5 && images.length < 10) {
            // 75
            this.quality = 75;
          }

          if (images.length >= 10) {
            this.quality = 80; // 80
          }
        }

        if (this.ad.ad_description.length > 200) {
          this.quality += 20;
        }
      }
    },
    packageSelected: function packageSelected(plan) {
      $('#boostPostModal').modal('hide'); // this.$router.push({ name: "payment method", params: {plan}} );

      this.$router.push({
        name: "payment method",
        params: {
          _plan: plan
        }
      }); // console.log(plan , 'sSd')
    },
    initializeSliders: function initializeSliders() {
      // document.addEventListener( 'DOMContentLoaded', function () {
      $(document).ready(function () {
        var secondarySlider = new Splide('#secondary-slider', {
          fixedWidth: 100,
          height: 60,
          gap: 5,
          cover: true,
          isNavigation: true,
          pagination: false,
          drag: false,
          //focus       : 'center',
          breakpoints: {
            '600': {
              fixedWidth: 66,
              height: 40
            }
          }
        }).mount();
        var primarySlider = new Splide('#primary-slider', {
          type: 'slide',
          heightRatio: 0.5,
          pagination: false,
          arrows: true,
          cover: true,
          height: 410,
          breakpoints: {
            '380': {
              height: 350
            }
          } //arrowPath: 'm15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z',

        }); // do not call mount() here.

        primarySlider.sync(secondarySlider).mount();
      });
      $('svg.radial-progress').each(function (index, value) {
        $(this).find($('circle.complete')).removeAttr('style');
      });
    },
    markNotification: function markNotification() {
      axios.post('/v1/notification_status', {
        type: 'single',
        noty_id: this.notification.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {})["catch"](function (err) {});
    }
  },
  data: function data() {
    return {
      ad: '',
      currentChat: '',
      otherCurrentChat: '',
      current_chat_instance: '',
      other_chat_instance: '',
      user_info: '',
      report_case: '',
      quality: 70,
      adsenseKey: 1,
      new_comment: '',
      comment_type: '',
      making_like: false,
      chatLoading: false,
      making_favorite: false,
      sending_comment: false,
      commenting: false,
      report_case_description: '',
      deleting_comment: '',
      topSpace: '',
      loading: false,
      follow_loading: false,
      reporting_post: false,
      show_price: false,
      show_phone: false,
      similar_ads: [],
      offers: [],
      comments: [],
      dummy_images: ['https://www.iihs.org/api/ratings/model-year-images/1664', 'https://static.toiimg.com/thumb/msid-76849381,width-1070,height-580,imgsize-154769,resizemode-75,overlay-toi_sw,pt-32,y_pad-40/photo.jpg', 'https://upload.wikimedia.org/wikipedia/commons/d/d2/2019_Hyundai_Elantra_Limited_%28AD_facelift%29_front_NYIAS_2019.jpg', 'https://i.ytimg.com/vi/iU014EEoqdk/maxresdefault.jpg', 'https://s1.paultan.org/image/2020/03/2020-Hyundai-Elantra-Global-Debut_Exterior-12-1200x628.jpg', 'https://www.automobilemag.com/uploads/sites/11/2020/11/2021-Hyundai-Elantra-N-Line-front-03.jpg?fit=around%7C875:492', 'https://cdn.motor1.com/images/mgl/jqrpR/s1/2021-hyundai-elantra-n-line-exterior-front-quarter.jpg', 'https://cnet1.cbsistatic.com/img/YiOrcbBSVOiWKMYDZ-Yi6SimP40=/1200x675/2020/03/31/c96dfc14-8f2f-4e01-bd74-10f80ce84813/ogi-elantra.jpg']
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/showMarker.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product/showMarker.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-google-maps */ "./node_modules/vue2-google-maps/dist/main.js");
/* harmony import */ var vue2_google_maps__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: {
    google: vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__["gmapApi"]
  },
  data: function data() {
    return {
      center: {
        lat: 10,
        lng: 10
      },
      position: {
        lat: '',
        lng: ''
      },
      markerLocation: {
        lat: '',
        lng: ''
      }
    };
  },
  watch: {
    city_name: function city_name() {
      this.codeAddress();
    },
    country_id: function country_id() {
      this.getCountryMarker();
    }
  },
  mounted: function mounted() {
    if (this.location.lat && this.location.lng) {
      this.markerLocation.lat = parseFloat(this.location.lat);
      this.markerLocation.lng = parseFloat(this.location.lng);
    }
  },
  props: {
    location: {
      type: Object,
      "default": {}
    },
    zoom: {
      type: Number,
      "default": 5
    },
    city_name: {
      type: String
    },
    country_id: {
      type: Number
    }
  },
  methods: {
    reCenterMap: function reCenterMap() {
      var _this = this;

      // self.map.panTo(this.myLatlng);//******* this would shift map on every instantiation with new lat/lng's
      this.$refs.mapRef.$mapPromise.then(function (map) {
        map.panTo({
          lat: _this.markerLocation.lat,
          lng: _this.markerLocation.lng
        });
      });
    },
    setMapMarket: function setMapMarket(address) {
      var _this2 = this;

      console.log("address", address);
      this.markerLocation.lat = address.geometry.location.lat;
      this.markerLocation.lng = address.geometry.location.lng;
      this.$refs.mapRef.$mapPromise.then(function (map) {
        map.panTo({
          lat: _this2.markerLocation.lat,
          lng: _this2.markerLocation.lng
        });

        if (_this2.city_name != 'All Cities') {
          map.zoom = 9;
        }
      }); // map = new google.maps.Map(..., mapOptions);
      // /* Change zoom level to 12  */
      // map.setZoom(12);
      // console.log('setPlace' , event)
      // console.log('setPlace lat' , event.geometry.location.lat())
      // console.log('setPlace lng' , event.geometry.location.lng())
    },
    getAdCountry: function getAdCountry() {
      var _this3 = this;

      var found = '';
      this.$root.countries.map(function (country) {
        if (country.id == _this3.country_id) {
          found = country;
        } // country_id

      });
      return found;
    },
    getCountryMarker: function getCountryMarker() {
      var _this4 = this;

      var country = this.getAdCountry();

      if (this.city_name && this.city_name == 'All Cities') {
        axios.get('https://maps.googleapis.com/maps/api/geocode/json?&components=country:' + country.name + '&key=AIzaSyC62I6UvDILIJa8ERvltfnsJqAHq7h6-TM').then(function (res) {
          console.log('google res', res);

          _this4.setMapMarket(res.data.results[0]); // do something with the first result

        })["catch"](function (err) {
          console.log(err);
        });
      } // this.$gmapApiPromiseLazy({}).then(() => {
      //     var geocoder = new this.google.maps.Geocoder()
      //     geocoder.geocode({'address': country.name }, (result, status) => {    
      //         if(status == 'OK'){
      //             this.setMapMarket(result[0]) // do something with the first result
      //         }
      //     })
      // })

    },
    codeAddress: function codeAddress() {
      var _this5 = this;

      var country = this.getAdCountry();

      if (this.city_name && this.city_name != 'All Cities') {
        // this.codeAddress();
        var country = this.getAdCountry();
        axios.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + this.city_name + '&components=country:' + country.name + '&key=AIzaSyC62I6UvDILIJa8ERvltfnsJqAHq7h6-TM').then(function (res) {
          console.log('codeAddress res', res);

          _this5.setMapMarket(res.data.results[0]); // do something with the first result

        })["catch"](function (err) {
          console.log(err);
        }); // this.$gmapApiPromiseLazy({}).then(() => {
        // var geocoder = new this.google.maps.Geocoder()
        // geocoder.geocode({'address': this.city_name + ', ' + country.name }, (result, status) => {
        //     console.log(`status`, status)
        //     if(status == 'OK'){
        //         console.log(`results`, result)
        //     this.setMapMarket(result[0]) // do something with the first result
        //     }
        // })
        // })
      } // var geocoder = new gmapApi.maps.Geocoder()
      // var address = 'Lahore';
      // geocoder.geocode({
      //     'address': address
      // }, function(results, status) {
      // console.log(`results`, results)
      // console.log(`status`, status)
      // if (status == google.maps.GeocoderStatus.OK) {
      // map.setCenter(results[0].geometry.location);
      // // var marker = new google.maps.Marker({
      // //     map: map,
      // //     position: results[0].geometry.location
      // // });
      // var request = {
      //     location: results[0].geometry.location,
      //     radius: 50000,
      //     name: 'ski',
      //     keyword: 'mountain',
      //     type: ['park']
      // };
      // infowindow = new google.maps.InfoWindow();
      // var service = new google.maps.places.PlacesService(map);
      // service.nearbySearch(request, callback);
      // } else {
      // alert("Geocode was not successful for the following reason: " + status);
      // }
      // });

    },
    // geocodedAddress() {
    //     let geocoder = new google.maps.Geocoder();
    //     console.log(`geocoder`, geocoder)
    // },
    // geocodedAddress() {
    //     var self = this;
    //     let geocoder = new google.maps.Geocoder();
    //     let theLocations = this.locations ;
    //     return Promise.all(_.map(theLocations, addr => { 
    //     console.log(`addr`, addr)
    //     var geocoder = new google.maps.Geocoder();
    //         var locationss = { 
    //             lat : parseFloat(addr.lat),
    //             lng : parseFloat(addr.lng)
    //         } ; 
    //         // var sampleLocation = { lat: 1.39, lng: 103.8 };
    //         return new Promise(function(resolve, reject) {
    //             geocoder.geocode({'location': locationss }, function(results, status){
    //                 if (status === 'OK') {
    //                 if (results[0]) {
    //                     return results[0].formatted_address;
    //                 } else {
    //                     console.log(status);
    //                     window.alert('No results found');
    //                     return null
    //                 }
    //                 }
    //             })
    //         });
    //     })).then(data => {
    //         console.log(data)
    //         this.formatedAddresses = data
    //     })
    // }
    getCoordinates: function getCoordinates() {
      var geocoder = new vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__["gmapApi"].maps.Geocoder();
      geocoder.geocode({
        'address': city_name
      }, function (results, status) {
        if (status == vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__["gmapApi"].maps.GeocoderStatus.OK) {
          newAddress = results[0].geometry.location;
          var latlng = new vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__["gmapApi"].maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng()));
          console.log('latlng', latlng); // gmarkers.push(createMarker(latlng,content[i]));
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#primary-slider .splide__slide img{\r\n  width: auto;\r\n  max-width: 100%;\r\n  /* width: 100%; */\r\n  height: 100%;\r\n  max-height: 100%;\r\n  -o-object-fit: cover;\r\n     object-fit: cover;\n}\n#secondary-slider{\r\n  margin-top: 5px;\n}\n#secondary-slider .splide__slide{\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  opacity: 0.5;\r\n  text-align: center;\n}\n#secondary-slider .splide__slide img{\r\n  width: 100%;\r\n  height: 100%;\r\n  -o-object-fit: contain;\r\n     object-fit: contain;\n}\n.splide__slide, .is-visible{\r\n  transition: all .3s ease-in-out;\r\n  background-color: #222222;\n}\n.is-active{\r\n  opacity: 1 !important;\n}\n.splide.is-active .splide__list{\r\n  text-align: center;\n}\n.splide--nav>.splide__track>.splide__list>.splide__slide.is-active{\r\n  border: none;\r\n  background-blend-mode: normal;\n}\n.splide--nav>.splide__track>.splide__list>.splide__slide{\r\n  border: unset;\n}\nsvg.radial-progress {\r\n  height: auto;\r\n  max-width: 200px;\r\n  padding: 1em;\r\n  transform: rotate(-90deg);\r\n  width: 100%;\n}\nsvg.radial-progress circle {\r\n  fill: rgba(0,0,0,0);\r\n  stroke: #d11d1d;\r\n  stroke-dashoffset: 219.91148575129;\r\n  stroke-width: 10;\n}\nsvg.radial-progress circle.incomplete {\r\n  stroke: #ebeaea;\n}\nsvg.radial-progress circle.complete { stroke-dasharray: 219.91148575129;\n}\nsvg.radial-progress text {\r\n  fill: rgb(0, 0, 0);\r\n  text-anchor: middle;\n}\n.swal2-styled:focus{\r\n  box-shadow: unset;\n}\n.placeholder-full-image img{\r\n  height: auto;\r\n  width: 100%;\r\n  max-height: 100%;\r\n  max-width: 100%;\r\n  -o-object-fit: contain;\r\n     object-fit: contain;\n}\n.placeholder-full-image{\r\n  width: 100%;\r\n  height: 410px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\n}\n.google-adsense-sticky{\r\n  height: auto;\r\n  /* background: #d0ff00; */\r\n  width: 140px;\r\n  transition: .3s all ease-in-out;\r\n  position: fixed;\r\n  right: 10px;\n}\r\n\r\n/* .google-adsense-vertical-banner{\r\n    height: 700px;\r\n} */\n.view-ad-banner-bottom-container{\r\n  /* margin-top: 32px; */\r\n  min-height: 90px;\n}\n.view-ad-banner-bottom{\r\n  max-height: 110px !important;\r\n  height: 110px !important;\r\n  width: 100%;\r\n  max-width: 100%;\n}\n.view-ad-banner-vertical-container{\r\n  /* margin-top: 32px; */\r\n  min-height: 90px;\n}\n.view-ad-banner-vertical{\r\n  max-height: 600px !important;\r\n  height: 600px !important;\r\n  width: 100%;\r\n  max-width: 100%;\n}\n@media screen and ( max-width:850px  ){\n.google-adsense-sticky{\r\n    display: none;\n}\n}\n.view-production-banner-top{\r\n  max-height: 250px !important;\r\n  height: 220px !important;\r\n  width: 100%;\r\n  max-width: 100%;\r\n\r\n  /* width: 100%;\r\n  max-width: 100%;\r\n  height: 120px;\r\n  margin-bottom: 20px; */\n}\r\n\r\n/* @media (min-width: 1200px){\r\n    .view-product .container-xl, .container-lg, .container-md, .container-sm, .container.main-wrapper {\r\n        max-width: 1035px;\r\n    }\r\n} */\r\n\r\n\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./view-product.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("div", { staticClass: "attribute-top" }, [
        _c("div", { staticClass: "bz-text-red text-center" }, [
          _vm.attribute_step > 0
            ? _c("i", {
                staticClass: "fas fa-arrow-left float-left",
                on: { click: _vm.prevAttribute }
              })
            : _vm._e(),
          _vm._v(
            "\r\n            " +
              _vm._s(_vm.attributes[_vm.attribute_step]) +
              " \r\n        "
          )
        ])
      ]),
      _vm._v(" "),
      _vm._l(_vm.product[_vm.attributes[_vm.attribute_step]], function(
        attribute,
        index
      ) {
        return _c(
          "div",
          {
            key: "sub-attr" + index,
            staticClass: "single-item",
            class: {
              selected: _vm.selected_attributes[_vm.attribute_step]
                ? attribute ==
                  _vm.selected_attributes[_vm.attribute_step].seletecvalue
                : false
            },
            on: {
              click: function($event) {
                return _vm.selectAttribute(attribute)
              }
            }
          },
          [
            _c("div", { staticClass: "single-list-item-icon-container" }),
            _vm._v(" "),
            _c("div", { staticClass: "single-list-item-name" }, [
              _vm._v(" " + _vm._s(attribute) + " ")
            ]),
            _vm._v(" "),
            _c("i", { staticClass: "fas fa-angle-right" })
          ]
        )
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=template&id=32c5d004&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product.vue?vue&type=template&id=32c5d004& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "view-product", attrs: { id: "app" } },
    [
      _c("loading", {
        attrs: {
          active: _vm.loading,
          loader: "dots",
          height: 170,
          width: 170,
          color: "#d11d1d",
          "background-color": "rgb(0 0 0)",
          "lock-scroll": true,
          "is-full-page": true
        },
        on: {
          "update:active": function($event) {
            _vm.loading = $event
          }
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "container main-wrapper position-relative" }, [
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            { staticClass: "col-12 p-0" },
            [
              _c("Adsense", {
                key: _vm.adsenseKey + 100,
                staticStyle: {
                  border: "0pt none",
                  display: "inline-block",
                  height: "120px",
                  width: "100%"
                },
                attrs: {
                  "data-ad-client": "ca-pub-2462277409343800",
                  "data-ad-slot": "7741947000",
                  "ins-class": "view-production-banner-top",
                  "is-new-ads-code": "yes",
                  "data-ad-test": "yes"
                }
              })
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 top-user-info-banner" }, [
            _c("div", { staticClass: "user-meta-info bz-white-box " }, [
              _c("div", { staticClass: "user-info" }, [
                _c("div", { staticClass: "user-image" }, [
                  _vm.user_info && _vm.user_info.profile_image
                    ? _c("img", {
                        attrs: {
                          src: _vm._f("serverPath")(_vm.user_info.profile_image)
                        },
                        on: {
                          error: function($event) {
                            $event.target.src = "/bazar/default-user-avatar.png"
                          }
                        }
                      })
                    : _c("img", {
                        attrs: { src: "/bazar/default-user-avatar.png" }
                      })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "product-user-meta-details" }, [
                  _c(
                    "div",
                    { staticClass: "name text-limit" },
                    [
                      _vm.$store.getters.isAuthenticated.id == _vm.ad.user_id
                        ? _c("span", [_vm._v("Welcome")])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id
                        ? _c("span", [_vm._v("Posted By")])
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "router-link",
                        { attrs: { to: "/user/" + _vm.ad.user_id + "/ads" } },
                        [_vm._v(" " + _vm._s(_vm.ad.user_name) + " ")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "total-view" }, [
                    _vm._v(" Views: "),
                    _c("span", [
                      _vm._v(
                        " " +
                          _vm._s(
                            _vm.ad.views_count ? _vm.ad.views_count : "0"
                          ) +
                          " "
                      )
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "right-btn-area" },
                [
                  _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id
                    ? [
                        _c(
                          "a",
                          {
                            staticClass:
                              "bz-sqaure-btn mr-2 full-mobile view-number-btn",
                            attrs: { href: "javascript:;" },
                            on: {
                              click: function($event) {
                                _vm.show_phone = !_vm.show_phone
                              }
                            }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/smartphone.png" }
                            }),
                            _vm._v(" "),
                            _vm.show_phone && _vm.ad.mobile
                              ? _c(
                                  "span",
                                  { staticClass: "view-mobile-number" },
                                  [_vm._v(" " + _vm._s(_vm.ad.mobile) + " ")]
                                )
                              : _c("span", [_vm._v(" Click To View ")])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "bz-sqaure-btn bz-btn-red full-mobile",
                            attrs: { href: "javascript:;" },
                            on: { click: _vm.startChat }
                          },
                          [
                            _c("img", {
                              attrs: { src: "/bazar/chat (2).png" }
                            }),
                            _vm._v("  Chat With User ")
                          ]
                        )
                      ]
                    : _c(
                        "router-link",
                        {
                          staticClass: "bz-sqaure-btn bz-btn-red full-mobile",
                          attrs: { to: "/chat" }
                        },
                        [
                          _c("img", { attrs: { src: "/bazar/chat (2).png " } }),
                          _vm._v("  Go To Chat ")
                        ]
                      )
                ],
                2
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            {
              staticClass:
                "col-md-8 col-xs-12 col-sm-12 product-details-container"
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "bz-white-box product-main-details std-margin-top "
                },
                [
                  _c("div", { staticClass: "product-main-details-top" }, [
                    _c("div", { staticClass: "product-title" }, [
                      _vm.ad.boost && _vm.ad.boost.boost_type == "0"
                        ? _c("img", {
                            staticClass: "boost-image-icon",
                            attrs: { src: "/bazar/icon-1.png" }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.ad.boost && _vm.ad.boost.boost_type == "1"
                        ? _c("img", {
                            staticClass: "boost-image-icon",
                            attrs: { src: "/bazar/icon-2.png" }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", [
                        _vm._v(
                          "\n                " +
                            _vm._s(_vm.ad.ad_title) +
                            "\n              "
                        )
                      ]),
                      _vm._v(" "),
                      _vm.ad.price_type != "price"
                        ? _c(
                            "a",
                            {
                              staticClass: "price-title-mobile",
                              attrs: { href: "#comment-box" }
                            },
                            [_vm._v(_vm._s(_vm.ad.price_type))]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.ad.price_type == "price"
                        ? _c(
                            "a",
                            {
                              staticClass: "price-title-mobile",
                              attrs: { href: "#comment-box" }
                            },
                            [
                              _vm._v(
                                " " +
                                  _vm._s(_vm.$store.getters.getCuerrency) +
                                  " " +
                                  _vm._s(_vm.ad.price)
                              )
                            ]
                          )
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "product-meta-info" }, [
                      _c(
                        "div",
                        { staticClass: "product-published-date text-limit" },
                        [
                          _vm._v("\n                Published on: "),
                          _c("a", { attrs: { href: "#" } }, [
                            _vm._v(
                              " " +
                                _vm._s(
                                  _vm._f("formatedDate")(_vm.ad.created_at)
                                ) +
                                " "
                            )
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "product-category text-limit" },
                        [
                          _vm._v("\n                Category: "),
                          _vm.ad.category
                            ? _c("a", { attrs: { href: "#" } }, [
                                _vm._v(
                                  " " +
                                    _vm._s(
                                      JSON.parse(_vm.ad.category)[0].value
                                    ) +
                                    " "
                                )
                              ])
                            : _vm._e()
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "product-location no-border text-limit"
                        },
                        [
                          _vm._v("\n                Location: "),
                          _c("a", { attrs: { href: "#" } }, [
                            _vm._v(_vm._s(_vm.ad.city))
                          ])
                        ]
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "product-image-gallery" }, [
                    _vm.ad_images
                      ? _c(
                          "div",
                          {
                            staticClass: "splide",
                            attrs: { id: "primary-slider" }
                          },
                          [
                            _c("div", { staticClass: "splide__track" }, [
                              _vm.ad.images
                                ? _c(
                                    "ul",
                                    { staticClass: "splide__list imglist" },
                                    _vm._l(JSON.parse(_vm.ad.images), function(
                                      image,
                                      index
                                    ) {
                                      return _c(
                                        "li",
                                        {
                                          key: index,
                                          staticClass: "splide__slide"
                                        },
                                        [
                                          image && image.image
                                            ? [
                                                _c(
                                                  "a",
                                                  {
                                                    attrs: {
                                                      "data-fancybox": "images",
                                                      href:
                                                        _vm.$root
                                                          .virtualmin_server +
                                                        "/public/images/" +
                                                        image.image
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src:
                                                          _vm.$root
                                                            .virtualmin_server +
                                                          "/public/images/" +
                                                          image.image
                                                      },
                                                      on: {
                                                        error: function(
                                                          $event
                                                        ) {
                                                          $event.target.src =
                                                            "/bazar/no-image-preview.png"
                                                        }
                                                      }
                                                    })
                                                  ]
                                                )
                                              ]
                                            : _vm._e()
                                        ],
                                        2
                                      )
                                    }),
                                    0
                                  )
                                : _vm._e()
                            ])
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.ad_images
                      ? _c(
                          "div",
                          {
                            staticClass: "splide",
                            attrs: { id: "secondary-slider" }
                          },
                          [
                            _c("div", { staticClass: "splide__track" }, [
                              _c(
                                "ul",
                                { staticClass: "splide__list" },
                                _vm._l(JSON.parse(_vm.ad.images), function(
                                  image,
                                  index
                                ) {
                                  return _c(
                                    "li",
                                    {
                                      key: index,
                                      staticClass: "splide__slide"
                                    },
                                    [
                                      image && image.image
                                        ? [
                                            _c("img", {
                                              attrs: {
                                                src:
                                                  _vm.$root.virtualmin_server +
                                                  "/public/images/" +
                                                  image.image
                                              },
                                              on: {
                                                error: function($event) {
                                                  $event.target.src =
                                                    "/bazar/no-image-preview.png"
                                                }
                                              }
                                            })
                                          ]
                                        : _vm._e()
                                    ],
                                    2
                                  )
                                }),
                                0
                              )
                            ])
                          ]
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.ad_images
                      ? _c("div", { staticClass: "placeholder-full-image" }, [
                          _c("img", {
                            attrs: { src: "/bazar/no-image-preview.png" }
                          })
                        ])
                      : _vm._e()
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "bz-white-box p-0 std-margin-top" },
                [
                  _c(
                    "div",
                    { staticClass: "product-share" },
                    [
                      _vm.$store.getters.isAuthenticated.id == _vm.ad.user_id
                        ? [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-4 col-md-4 col-xs-12 share-link share-btn ",
                                on: { click: _vm.deleteAd }
                              },
                              [_vm._m(0)]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-4 col-md-4 col-xs-12 share-link share-btn",
                                on: {
                                  click: function($event) {
                                    return _vm.$router.push(
                                      "/edit/" + _vm.ad.id + "/ad"
                                    )
                                  }
                                }
                              },
                              [_vm._m(1)]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-4 col-md-4 col-xs-12 share-link share-btn ",
                                on: {
                                  click: function($event) {
                                    return _vm.$refs.social_modal.openAdShareModal()
                                  }
                                }
                              },
                              [_vm._m(2)]
                            )
                          ]
                        : [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-3 col-md-3 col-xs-12 wishlist-link share-btn ",
                                on: { click: _vm.likeAd }
                              },
                              [
                                _vm.making_like
                                  ? _c("loader", { staticClass: "bz-text-red" })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("span", [
                                  _c("i", {
                                    staticClass: " fa-thumbs-up",
                                    class: {
                                      fas: _vm.ad.like_stattus == "Yes",
                                      far: _vm.ad.like_stattus != "Yes"
                                    }
                                  }),
                                  _vm._v(
                                    " Like Ad\n\n                          " +
                                      _vm._s(
                                        _vm.ad.like_count > 0
                                          ? "(" + _vm.ad.like_count + ")"
                                          : ""
                                      ) +
                                      "\n\n                          "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "seprator" })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-3 col-md-3 col-xs-12 share-link share-btn ",
                                on: {
                                  click: function($event) {
                                    return _vm.$refs.social_modal.openAdShareModal()
                                  }
                                }
                              },
                              [_vm._m(3)]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-3 col-md-3 col-xs-12 wishlist-link share-btn ",
                                on: { click: _vm.favoriteAd }
                              },
                              [
                                _vm.making_favorite
                                  ? _c("loader", { staticClass: "bz-text-red" })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("span", [
                                  _c("i", {
                                    staticClass: " fa-heart",
                                    class: {
                                      fas: _vm.ad.fav_stattus == "Yes",
                                      far: _vm.ad.fav_stattus != "Yes"
                                    }
                                  }),
                                  _vm._v(
                                    " Favourites\n                          " +
                                      _vm._s(
                                        _vm.ad.favorite_count > 0
                                          ? "(" + _vm.ad.favorite_count + ")"
                                          : ""
                                      ) +
                                      "\n\n                          "
                                  )
                                ])
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-sm-3 col-md-3 col-xs-12 report-link share-btn no-border",
                                on: {
                                  click: function($event) {
                                    return _vm.reportAdModalFunc()
                                  }
                                }
                              },
                              [_vm._m(4)]
                            )
                          ]
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("social-share", {
                    ref: "social_modal",
                    attrs: { ad: _vm.ad }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "bz-white-box p-0 product-description-area std-margin-top"
                },
                [
                  _vm._m(5),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "row product-features" },
                    [
                      _vm.ad_attributes
                        ? [
                            _c(
                              "div",
                              { staticClass: "col-sm-6 col-md-6 col-xs-12" },
                              [
                                _c(
                                  "span",
                                  { staticClass: "description-item-bold" },
                                  [_vm._v(" Ad id ")]
                                ),
                                _vm._v(" : " + _vm._s(_vm.ad.id) + " ")
                              ]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.ad_attributes, function(attr, index) {
                              return _c(
                                "div",
                                {
                                  key: index,
                                  staticClass: "col-sm-6 col-md-6 col-xs-12"
                                },
                                [
                                  _c(
                                    "span",
                                    { staticClass: "description-item-bold" },
                                    [_vm._v(" " + _vm._s(attr.tital) + " ")]
                                  ),
                                  _vm._v(" : " + _vm._s(attr.value) + " ")
                                ]
                              )
                            })
                          ]
                        : _vm._e()
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("div", {
                    staticClass: "product-description-text",
                    domProps: { innerHTML: _vm._s(_vm.ad.ad_description) }
                  }),
                  _vm._v(" "),
                  _vm._m(6),
                  _vm._v(" "),
                  _c("div", { staticClass: "product-location-map" }, [
                    _c(
                      "div",
                      { staticClass: "event-map" },
                      [
                        _c("hr"),
                        _vm._v(" "),
                        _c("show-marker", {
                          attrs: {
                            country_id: _vm.ad.country_id,
                            city_name: _vm.ad.city,
                            location: { lat: 52.3731872, lng: 5.2649616 },
                            zoom: 15
                          }
                        })
                      ],
                      1
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "mt-3 view-ad-banner-bottom-container" },
                [
                  _c("Adsense", {
                    key: _vm.adsenseKey + 10000,
                    staticStyle: {
                      border: "0pt none",
                      display: "inline-block",
                      width: "100%",
                      height: "90px"
                    },
                    attrs: {
                      "data-ad-client": "ca-pub-2462277409343800",
                      "data-ad-slot": "3724162019",
                      "ins-class": "view-ad-banner-bottom",
                      "is-new-ads-code": "yes"
                    }
                  })
                ],
                1
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass:
                "col-md-4 col-xs-12 col-sm-12 std-margin-top product-right-column "
            },
            [
              _c("div", { staticClass: "bz-white-box product-quality" }, [
                _c("a", { attrs: { href: "javascript:;" } }, [
                  _vm.ad && _vm.ad.price_type == "price"
                    ? _c(
                        "div",
                        {
                          staticClass: "ask-for-price-btn",
                          staticStyle: {
                            "font-size": "28px",
                            padding: "9px 10px"
                          }
                        },
                        [
                          _c("img", {
                            staticClass: "straight-speaker",
                            attrs: { src: "/bazar/speake-icon.png" }
                          }),
                          _vm._v(" "),
                          _c("span", [
                            _vm._v(
                              _vm._s(_vm.$store.getters.getCuerrency) +
                                " " +
                                _vm._s(_vm.ad.price) +
                                "  "
                            )
                          ]),
                          _vm._v(" "),
                          _c("img", {
                            staticClass: "rotated-speaker",
                            attrs: { src: "/bazar/speake-icon-mirrored.png" }
                          })
                        ]
                      )
                    : _c("div", { staticClass: "ask-for-price-btn " }, [
                        _c("img", {
                          staticClass: "straight-speaker",
                          attrs: { src: "/bazar/speake-icon.png" }
                        }),
                        _vm._v(" "),
                        _vm.ad.price_type != "price"
                          ? _c("span", [
                              _vm._v(" " + _vm._s(_vm.ad.price_type) + " ")
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.ad.price_type == "price"
                          ? _c("span", [
                              _vm._v(
                                " " +
                                  _vm._s(_vm.$store.getters.getCuerrency) +
                                  " " +
                                  _vm._s(_vm.ad.price) +
                                  " "
                              )
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "rotated-speaker",
                          staticStyle: { "margin-top": "-4px" },
                          attrs: { src: "/bazar/speake-icon-mirrored.png" }
                        })
                      ])
                ]),
                _vm._v(" "),
                _vm.$store.getters.isAuthenticated.id == _vm.ad.user_id
                  ? _c(
                      "div",
                      {
                        staticClass:
                          "bz-white-box-with-header product-quality-stats-container"
                      },
                      [
                        _c("div", { staticClass: "header" }, [
                          _vm._v(
                            "\n              Current Ad Qualtiy\n            "
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "box-body" }, [
                          _c("span", [
                            _vm._v(
                              "\n                              Add more pictures and information to Get more view to make a deal faste\n                          "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "stat-box mt-3" },
                            [
                              _c(
                                "vue-ellipse-progress",
                                {
                                  attrs: {
                                    progress: _vm.quality,
                                    color: "#d11d1d",
                                    colorFill: "transparent",
                                    emptyColor: "#ebeaea",
                                    emptyColorFill: "transparent",
                                    size: 170,
                                    fontSize: "32px",
                                    thickness: 20,
                                    emptyThickness: 20,
                                    mode: "normal",
                                    line: "butt"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                  " +
                                      _vm._s(_vm.quality) +
                                      "%\n                "
                                  )
                                ]
                              )
                            ],
                            1
                          )
                        ])
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id
                  ? _c(
                      "div",
                      {
                        staticClass:
                          "bz-white-box-with-header about-the-user-container"
                      },
                      [
                        _c("div", { staticClass: "header" }, [
                          _vm._v("\n              About the user\n            ")
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "box-body" },
                          [
                            _c("div", { staticClass: "user-profile-details" }, [
                              _c("div", { staticClass: "user-image" }, [
                                _vm.user_info && _vm.user_info.profile_image
                                  ? _c("img", {
                                      attrs: {
                                        src: _vm._f("serverPath")(
                                          _vm.user_info.profile_image
                                        )
                                      },
                                      on: {
                                        error: function($event) {
                                          $event.target.src =
                                            "/bazar/default-user-avatar.png"
                                        }
                                      }
                                    })
                                  : _c("img", {
                                      attrs: {
                                        src: "/bazar/default-user-avatar.png"
                                      }
                                    })
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "user-name-section" },
                                [
                                  _c(
                                    "router-link",
                                    {
                                      staticClass: "user-name",
                                      attrs: {
                                        to: "/user/" + _vm.ad.user_id + "/ads"
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                    " +
                                          _vm._s(_vm.ad.user_name) +
                                          "\n                  "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "membership-details" },
                                    [
                                      _c("i", {
                                        staticClass: "fa fa-calendar-minus-o",
                                        attrs: { "aria-hidden": "true" }
                                      }),
                                      _vm._v(
                                        "\n                    Member since " +
                                          _vm._s(
                                            _vm._f("formatedDateWithDay")(
                                              _vm.ad.user_regiterdate
                                            )
                                          ) +
                                          "\n                  "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "user-social-media-accounts" },
                              [
                                _vm._m(7),
                                _vm._v(" "),
                                _vm.user_info.verify_phone == "true"
                                  ? _c("a", { attrs: { href: "#" } }, [
                                      _c("img", {
                                        staticClass:
                                          "user-social-media-account",
                                        attrs: {
                                          src: "/bazar/view-product-mobile.png"
                                        }
                                      })
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.user_info.verify_facebook == "true"
                                  ? _c("a", { attrs: { href: "#" } }, [
                                      _c("img", {
                                        staticClass:
                                          "user-social-media-account",
                                        attrs: {
                                          src:
                                            "/bazar/view-product.facebook.png"
                                        }
                                      })
                                    ])
                                  : _vm._e(),
                                _vm._v(" "),
                                _vm.user_info.verify_google == "true"
                                  ? _c("a", { attrs: { href: "#" } }, [
                                      _c("img", {
                                        staticClass:
                                          "user-social-media-account",
                                        attrs: {
                                          src:
                                            "/bazar/view-product-googleplus.png"
                                        }
                                      })
                                    ])
                                  : _vm._e()
                              ]
                            ),
                            _vm._v(" "),
                            _vm.user_info
                              ? _c("show-ratings", {
                                  attrs: { user: _vm.user_info }
                                })
                              : _vm._e(),
                            _vm._v(" "),
                            _c("div", { staticClass: "user-stats" }, [
                              _c(
                                "div",
                                { staticClass: "user-stats-box ml-0" },
                                [
                                  _c("div", [
                                    _vm._v(_vm._s(_vm.user_info.ads_count))
                                  ]),
                                  _vm._v(" "),
                                  _c("div", [_vm._v("Ads")])
                                ]
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "user-stats-box" }, [
                                _c("div", [
                                  _vm._v(_vm._s(_vm.user_info.following))
                                ]),
                                _vm._v(" "),
                                _c("div", [_vm._v("Following")])
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "user-stats-box mr-0" },
                                [
                                  _c("div", [
                                    _vm._v(_vm._s(_vm.user_info.followers))
                                  ]),
                                  _vm._v(" "),
                                  _c("div", [_vm._v("Followers")])
                                ]
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "a",
                              {
                                staticClass: "red-button-anc",
                                attrs: { href: "javascript:;" },
                                on: { click: _vm.follow }
                              },
                              [
                                _vm.follow_loading || _vm.loading
                                  ? _c(
                                      "div",
                                      { staticClass: "red-button following" },
                                      [_c("loader")],
                                      1
                                    )
                                  : [
                                      _vm.ad.follow_user == "false"
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "red-button following"
                                            },
                                            [_vm._v("Follow")]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      _vm.ad.follow_user == "true"
                                        ? _c(
                                            "div",
                                            {
                                              staticClass:
                                                "red-button not-following"
                                            },
                                            [_vm._v("Following")]
                                          )
                                        : _vm._e()
                                    ]
                              ],
                              2
                            )
                          ],
                          1
                        )
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id &&
              _vm.ad.price_type == "price"
                ? _c(
                    "div",
                    {
                      staticClass: "bz-white-box std-margin-top",
                      attrs: { id: "comment-box" }
                    },
                    [
                      _c("div", { staticClass: "contact-user-for-ad" }, [
                        _c("div", { staticClass: "heading" }, [
                          _vm._v(
                            "The required price " +
                              _vm._s(_vm.$store.getters.getCuerrency) +
                              " " +
                              _vm._s(_vm.ad.price) +
                              " "
                          )
                        ]),
                        _vm._v(" "),
                        _vm._m(8),
                        _vm._v(" "),
                        _c("div", { staticClass: "comment-box" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.new_comment,
                                expression: "new_comment"
                              }
                            ],
                            staticClass: "comment-input",
                            attrs: { placeholder: "Enter The Amount Of Offer" },
                            domProps: { value: _vm.new_comment },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.new_comment = $event.target.value
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "red-button-anc text-center",
                            attrs: { href: "javascript:;" },
                            on: { click: _vm.sendComment }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "red-button following " },
                              [
                                _vm.commenting
                                  ? _c("loader")
                                  : _c("span", [_vm._v("Place Your Offer")])
                              ],
                              1
                            )
                          ]
                        )
                      ])
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id &&
              _vm.ad.price_type != "price"
                ? _c(
                    "div",
                    {
                      staticClass: "bz-white-box std-margin-top",
                      attrs: { id: "comment-box" }
                    },
                    [
                      _c("div", { staticClass: "contact-user-for-ad" }, [
                        _c("div", { staticClass: "heading" }, [
                          _vm._v("Contact the user about ad")
                        ]),
                        _vm._v(" "),
                        _vm._m(9),
                        _vm._v(" "),
                        _c("div", { staticClass: "comment-box" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.new_comment,
                                expression: "new_comment"
                              }
                            ],
                            staticClass: "comment-input",
                            attrs: { placeholder: "Enter Your Comment" },
                            domProps: { value: _vm.new_comment },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.new_comment = $event.target.value
                              }
                            }
                          })
                        ]),
                        _vm._v(" "),
                        _c(
                          "a",
                          {
                            staticClass: "red-button-anc text-center",
                            attrs: { href: "javascript:;" },
                            on: { click: _vm.sendComment }
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "red-button following " },
                              [
                                _vm.commenting
                                  ? _c("loader")
                                  : _c("span", [_vm._v("Send Your Comment")])
                              ],
                              1
                            )
                          ]
                        )
                      ])
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.$store.getters.isAuthenticated.id == _vm.ad.user_id
                ? _c(
                    "div",
                    {
                      staticClass:
                        "bz-white-box repost-container std-margin-top"
                    },
                    [_vm._m(10)]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.ad.price_type != "price" && _vm.comments.length > 0
                ? _c(
                    "div",
                    { staticClass: "bz-white-box std-margin-top comments-box" },
                    [
                      _c(
                        "a",
                        {
                          attrs: {
                            href: "javascript:;",
                            "data-toggle": "modal",
                            "data-target": "#commentsModal"
                          }
                        },
                        [
                          _c("div", { staticClass: "comment-button" }, [
                            _vm._v(
                              "\n              See All Comments (" +
                                _vm._s(_vm.comments.length) +
                                ")\n            "
                            )
                          ])
                        ]
                      )
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.ad.price_type == "price" && _vm.offers.length > 0
                ? _c(
                    "div",
                    { staticClass: "bz-white-box std-margin-top comments-box" },
                    [
                      _c(
                        "a",
                        {
                          attrs: {
                            href: "javascript:;",
                            "data-toggle": "modal",
                            "data-target": "#offsersModal"
                          }
                        },
                        [
                          _c("div", { staticClass: "comment-button" }, [
                            _vm._v(
                              "\n              See All Offers (" +
                                _vm._s(_vm.offers.length) +
                                ")\n            "
                            )
                          ])
                        ]
                      )
                    ]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "googe-adsense-banner view-ad-banner-vertical-container"
                },
                [
                  _c("Adsense", {
                    key: _vm.adsenseKey + 1000,
                    staticStyle: {
                      border: "0pt none",
                      display: "inline-block",
                      width: "100%",
                      height: "600px"
                    },
                    attrs: {
                      "data-ad-client": "ca-pub-2462277409343800",
                      "data-ad-slot": "7167231937",
                      "ins-class": "view-ad-banner-vertical",
                      "is-new-ads-code": "yes"
                    }
                  })
                ],
                1
              )
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row std-margin-top" }, [
          _c("div", { staticClass: "col-12 p-0 m-0" }, [
            _c("div", { staticClass: "bz-white-box similar-ads-products" }, [
              _vm._m(11),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "product-section pt-2",
                  attrs: { id: "product-section" }
                },
                [
                  _c(
                    "div",
                    { staticClass: "product-list justify-content-center" },
                    _vm._l(_vm.similar_ads, function(ad, index) {
                      return _c("single-product", {
                        key: index,
                        attrs: { ad: ad }
                      })
                    }),
                    1
                  )
                ]
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "reportAdModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(12),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "report-ad-modal" }, [
                    _c("div", { staticClass: "small-text" }, [
                      _vm._v(
                        "\n              Please Select one of the following reasons\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "report-options" }, [
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(13),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-1" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Ad Sold"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-1" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(14),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-2" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Duplicate Ad"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-2" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(15),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-3" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Inappropriate"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-3" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(16),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-4" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Wrong Category Ad"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-4" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(17),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-5" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Website Listing"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-5" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(18),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-6" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Against the rules"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-6" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(19),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass: "custom-control-input post-report",
                                attrs: { type: "checkbox", id: "option-7" },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase($event, "Other")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-7" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option-text" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.report_case_description,
                              expression: "report_case_description"
                            }
                          ],
                          attrs: {
                            placeholder: "Additional info (Optional)",
                            onkeyup: "countChar(this)",
                            maxlength: "120"
                          },
                          domProps: { value: _vm.report_case_description },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.report_case_description = $event.target.value
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "character-length",
                            attrs: { id: "charNum" }
                          },
                          [_vm._v(" 0/120 ")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "submit-btn-area" }, [
                        _c(
                          "button",
                          {
                            staticClass:
                              "bz-sqaure-btn bz-btn-red full-mobile no-bold",
                            attrs: {
                              type: "button",
                              disabled: _vm.reporting_post
                            },
                            on: { click: _vm.submitReportPost }
                          },
                          [
                            _vm.reporting_post
                              ? _c("loader")
                              : _c("span", [_vm._v("SUBMIT")])
                          ],
                          1
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "commentsModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(20),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body p-0" }, [
                  _c(
                    "div",
                    { staticClass: "view-comments" },
                    [
                      _vm._l(_vm.comments, function(comment) {
                        return _c(
                          "div",
                          { key: comment.id, staticClass: "comment" },
                          [
                            _c("div", { staticClass: "comment-image" }, [
                              comment.user_image
                                ? _c("img", {
                                    attrs: {
                                      src: _vm._f("serverPath")(
                                        comment.user_image
                                      )
                                    },
                                    on: {
                                      error: function($event) {
                                        $event.target.src =
                                          "/bazar/default-user-avatar.png"
                                      }
                                    }
                                  })
                                : _c("img", {
                                    attrs: {
                                      src: "/bazar/default-user-avatar.png"
                                    }
                                  })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "comment-details" }, [
                              _c(
                                "div",
                                { staticClass: "user-name" },
                                [
                                  _vm._v(
                                    _vm._s(comment.user_name) +
                                      "\n                  "
                                  ),
                                  _vm.deleting_comment == comment
                                    ? _c("loader", {
                                        staticClass: "bz-text-red float-right"
                                      })
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _vm.$store.getters.isAuthenticated.id ==
                                  comment.user_id
                                    ? _c("i", {
                                        staticClass:
                                          "fas fa-trash float-right mr-3",
                                        on: {
                                          click: function($event) {
                                            return _vm.deleteComment(comment)
                                          }
                                        }
                                      })
                                    : _vm._e()
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "comment-text" }, [
                                _vm._v(" Comment: "),
                                _c("span", [
                                  _vm._v(" " + _vm._s(comment.bid_price) + " ")
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "time-ago" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm._f("formatDifferenceDateTimestamp")(
                                      comment.timestamp
                                    )
                                  ) + " "
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      _vm._v(" "),
                      _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id
                        ? _c(
                            "div",
                            { staticClass: "send-offer" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.new_comment,
                                    expression: "new_comment"
                                  }
                                ],
                                attrs: { placeholder: "Enter Comment" },
                                domProps: { value: _vm.new_comment },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.new_comment = $event.target.value
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _vm.commenting
                                ? _c("simple-loader", {
                                    staticClass: "bz-text-red"
                                  })
                                : _c("img", {
                                    staticClass: "send-icon",
                                    attrs: { src: "/bazar/send-red.png" },
                                    on: { click: _vm.sendComment }
                                  })
                            ],
                            1
                          )
                        : _vm._e()
                    ],
                    2
                  )
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "offsersModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(21),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body p-0" }, [
                  _c(
                    "div",
                    { staticClass: "view-offers" },
                    [
                      _vm._l(_vm.offers, function(offer) {
                        return _c(
                          "div",
                          { key: offer.id, staticClass: "offer" },
                          [
                            _c("div", { staticClass: "offer-image" }, [
                              offer.user_image
                                ? _c("img", {
                                    attrs: {
                                      src: _vm._f("serverPath")(
                                        offer.user_image
                                      )
                                    },
                                    on: {
                                      error: function($event) {
                                        $event.target.src =
                                          "/bazar/default-user-avatar.png"
                                      }
                                    }
                                  })
                                : _c("img", {
                                    attrs: {
                                      src: "/bazar/default-user-avatar.png"
                                    }
                                  })
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "offer-details" }, [
                              _c(
                                "div",
                                { staticClass: "user-name" },
                                [
                                  _vm._v(
                                    _vm._s(offer.user_name) +
                                      "\n                  "
                                  ),
                                  _vm.deleting_comment == offer
                                    ? _c("loader", {
                                        staticClass: "bz-text-red float-right",
                                        attrs: { small: false }
                                      })
                                    : [
                                        _vm.$store.getters.isAuthenticated.id ==
                                        offer.user_id
                                          ? _c("i", {
                                              staticClass:
                                                "fas fa-trash float-right mr-3",
                                              on: {
                                                click: function($event) {
                                                  return _vm.deleteComment(
                                                    offer
                                                  )
                                                }
                                              }
                                            })
                                          : _vm._e()
                                      ]
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "offer-price" }, [
                                _vm._v("Offer: "),
                                _c("span", [
                                  _vm._v(
                                    _vm._s(_vm.$store.getters.getCuerrency) +
                                      _vm._s(offer.bid_price)
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "time-ago" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm._f("formatDifferenceDateTimestamp")(
                                      offer.timestamp
                                    )
                                  ) + " "
                                )
                              ])
                            ])
                          ]
                        )
                      }),
                      _vm._v(" "),
                      _vm.$store.getters.isAuthenticated.id != _vm.ad.user_id
                        ? _c(
                            "div",
                            { staticClass: "send-offer" },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.new_comment,
                                    expression: "new_comment"
                                  }
                                ],
                                attrs: { placeholder: "Send Offer" },
                                domProps: { value: _vm.new_comment },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.new_comment = $event.target.value
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _vm.commenting
                                ? _c("simple-loader", {
                                    staticClass: "bz-text-red"
                                  })
                                : _c("img", {
                                    staticClass: "send-icon",
                                    attrs: { src: "/bazar/send-red.png" },
                                    on: { click: _vm.sendComment }
                                  })
                            ],
                            1
                          )
                        : _vm._e()
                    ],
                    2
                  )
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade bd-example-modal-lg",
          attrs: {
            id: "boostPostModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "modal-dialog modal-lg modal-dialog-centered boost-post-packages-modal",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(22),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "modal-body modal-without-padding" },
                  [
                    _c("boost-post", {
                      attrs: { _ad_id: _vm.ad.id },
                      on: { "package-selected": _vm.packageSelected }
                    })
                  ],
                  1
                )
              ])
            ]
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _c("i", { staticClass: "fa fa-trash-alt" }),
      _vm._v(" Delete ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [_c("i", { staticClass: "fa fa-pen" }), _vm._v(" Edit ")])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _c("i", { staticClass: "fa fa-share-alt" }),
      _vm._v(" Share ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _c("i", { staticClass: "fa fa-share-alt" }),
      _vm._v(" Share ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _c("i", { staticClass: "fa fa-warning" }),
      _vm._v(" Report ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main-heading" }, [
      _c("span", [_vm._v("Description")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main-heading" }, [
      _c("span", [_vm._v("Location")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "#" } }, [
      _c("img", {
        staticClass: "user-social-media-account",
        attrs: { src: "/bazar/view-product-email.png" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center comment-icon-container" }, [
      _c("img", {
        staticClass: "comment-icon",
        attrs: { src: "/bazar/hammer.png" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center comment-icon-container" }, [
      _c("img", {
        staticClass: "comment-icon",
        attrs: { src: "/bazar/comment.png" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-white-box-with-header" }, [
      _c("div", { staticClass: "header" }, [
        _vm._v("\n              Feature or repost your ad\n            ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "box-body" }, [
        _c("div", { staticClass: "more-views-heading" }, [
          _vm._v("\n                Get more views\n              ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "repost-ad-box" }, [
          _c("div", { staticClass: "option option-1" }, [
            _c("img", { attrs: { src: "/bazar/icon-1.png" } })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "option option-2" }, [
            _c("img", { attrs: { src: "/bazar/icon-2.png" } })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "option option-3" }, [
            _c("img", { attrs: { src: "/bazar/icon-3.png" } })
          ])
        ]),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "red-button-anc",
            attrs: {
              href: "javascript:;",
              "data-toggle": "modal",
              "data-target": "#boostPostModal"
            }
          },
          [
            _c("div", { staticClass: "red-button following " }, [
              _vm._v("BUY NOW")
            ])
          ]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main-heading" }, [
      _c("span", [_vm._v("Similar Ads")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [_vm._v("Report Ad")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-1" } },
      [
        _c("label", { attrs: { for: "option-1" } }, [
          _vm._v("\n                    Ad Sold\n                  ")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "report-option-name" }, [
      _c("label", { attrs: { for: "option-2" } }, [
        _vm._v("\n                    Duplicate Ad\n                  ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "report-option-name" }, [
      _c("label", { attrs: { for: "option-3" } }, [
        _vm._v("\n                    Inappropriate\n                  ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "report-option-name" }, [
      _c("label", { attrs: { for: "option-4" } }, [
        _vm._v("\n                    Wrong Category Ad\n                  ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "report-option-name" }, [
      _c("label", { attrs: { for: "option-5" } }, [
        _vm._v("\n                    Website Listing\n                  ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "report-option-name" }, [
      _c("label", { attrs: { for: "option-6" } }, [
        _vm._v("\n                    Against the rules\n                  ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "report-option-name" }, [
      _c("label", { attrs: { for: "option-7" } }, [
        _vm._v("\n                    Other\n                  ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [
        _vm._v("All Comments")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [
        _vm._v("All Offers")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [
        _vm._v("Boost Your Ad")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/showMarker.vue?vue&type=template&id=195581b2&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/products/view-product/showMarker.vue?vue&type=template&id=195581b2& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "GmapMap",
        {
          ref: "mapRef",
          staticStyle: { width: "100%", height: "400px", borderRadius: "5px" },
          attrs: {
            center: _vm.markerLocation,
            zoom: 4,
            "map-type-id": "terrain"
          }
        },
        [
          _vm.markerLocation.lat && _vm.markerLocation.lng
            ? _c("GmapMarker", {
                attrs: {
                  position: {
                    lat: _vm.markerLocation.lat,
                    lng: _vm.markerLocation.lng
                  },
                  clickable: false,
                  draggable: false
                }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/post-ad/product-attributes.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/post-ad/product-attributes.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product-attributes.vue?vue&type=template&id=f7a9dd02& */ "./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&");
/* harmony import */ var _product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-attributes.vue?vue&type=script&lang=js& */ "./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/post-ad/product-attributes.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./product-attributes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./product-attributes.vue?vue&type=template&id=f7a9dd02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/post-ad/product-attributes.vue?vue&type=template&id=f7a9dd02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_product_attributes_vue_vue_type_template_id_f7a9dd02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/products/view-product.vue":
/*!************************************************!*\
  !*** ./resources/js/products/view-product.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_product_vue_vue_type_template_id_32c5d004___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view-product.vue?vue&type=template&id=32c5d004& */ "./resources/js/products/view-product.vue?vue&type=template&id=32c5d004&");
/* harmony import */ var _view_product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view-product.vue?vue&type=script&lang=js& */ "./resources/js/products/view-product.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _view_product_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view-product.vue?vue&type=style&index=0&lang=css& */ "./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _view_product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _view_product_vue_vue_type_template_id_32c5d004___WEBPACK_IMPORTED_MODULE_0__["render"],
  _view_product_vue_vue_type_template_id_32c5d004___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/view-product.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/view-product.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/products/view-product.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./view-product.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************!*\
  !*** ./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./view-product.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/products/view-product.vue?vue&type=template&id=32c5d004&":
/*!*******************************************************************************!*\
  !*** ./resources/js/products/view-product.vue?vue&type=template&id=32c5d004& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_template_id_32c5d004___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./view-product.vue?vue&type=template&id=32c5d004& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product.vue?vue&type=template&id=32c5d004&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_template_id_32c5d004___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_view_product_vue_vue_type_template_id_32c5d004___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/products/view-product/showMarker.vue":
/*!***********************************************************!*\
  !*** ./resources/js/products/view-product/showMarker.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _showMarker_vue_vue_type_template_id_195581b2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./showMarker.vue?vue&type=template&id=195581b2& */ "./resources/js/products/view-product/showMarker.vue?vue&type=template&id=195581b2&");
/* harmony import */ var _showMarker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./showMarker.vue?vue&type=script&lang=js& */ "./resources/js/products/view-product/showMarker.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _showMarker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _showMarker_vue_vue_type_template_id_195581b2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _showMarker_vue_vue_type_template_id_195581b2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/products/view-product/showMarker.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/products/view-product/showMarker.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/products/view-product/showMarker.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showMarker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./showMarker.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/showMarker.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_showMarker_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/products/view-product/showMarker.vue?vue&type=template&id=195581b2&":
/*!******************************************************************************************!*\
  !*** ./resources/js/products/view-product/showMarker.vue?vue&type=template&id=195581b2& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_showMarker_vue_vue_type_template_id_195581b2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./showMarker.vue?vue&type=template&id=195581b2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/products/view-product/showMarker.vue?vue&type=template&id=195581b2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_showMarker_vue_vue_type_template_id_195581b2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_showMarker_vue_vue_type_template_id_195581b2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);