(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/other_user/partials/left-menu.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    user_id: {
      "default": ''
    }
  },
  data: function data() {
    return {
      user_info: '',
      report_case: '',
      report_case_description: '',
      reporting_post: false,
      showList: false,
      loading: false,
      rating_user: false,
      rating: 0,
      review: '',
      review_options: [{
        name: 'Product as described',
        ratings: 1
      }, {
        name: 'Clear pictures ',
        ratings: 1
      }, {
        name: 'Responds quickly ',
        ratings: 1
      }, {
        name: 'Friendly ',
        ratings: 1
      }, {
        name: 'Deal quickly closed ',
        ratings: 1
      }, {
        name: 'Good service ',
        ratings: 1
      }, {
        name: 'Fulfils agreements ',
        ratings: 1
      }, {
        name: 'Fast shipping ',
        ratings: 1
      }, {
        name: 'Meets expectations ',
        ratings: 1
      }, {
        name: 'Professional communication ',
        ratings: 1
      }]
    };
  },
  watch: {// user_id(){
    //     this.loadUserInfo();
    // }
  },
  mounted: function mounted() {
    this.$root.$refs.userLeftMenu = this;
    console.log(this.user_id, 'LEFT MENU');

    if (this.user_id && !this.user_info) {
      console.log('should fetch');
      this.loadUserInfo();
    }

    $(".user-report-option:checkbox").click(function () {
      $(".user-report-option").not(this).prop('checked', false);
      $(this).attr("checked", true);
    });
  },
  methods: {
    reportUserModalFunc: function reportUserModalFunc() {
      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        $('#reportUserModal').modal('show');
      } else {
        $('#loginModal').modal('show');
      }
    },
    rateUserModalFunc: function rateUserModalFunc() {
      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        $('#rateModal').modal('show');
      } else {
        $('#loginModal').modal('show');
      }
    },
    loadUserInfo: function loadUserInfo() {
      var _this = this;

      this.loading = true;
      axios.post('/user_details', {
        id: this.user_id,
        current_user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        console.log("Simple response", res);
      });
      axios.post('/v1/user_details', {
        id: this.user_id,
        current_user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        // console.log(res.data)
        console.log("v1 response", res);
        _this.loading = false;

        if (res.data.status == 'success') {
          _this.user_info = res.data.user_detail;
          _this.review = res.data.user_detail.review;

          if (_this.review) {
            _this.fillRewviewForm();
          }
        } else {
          console.log(res);

          _this.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    handleReportCase: function handleReportCase(event, reason) {
      if (event.target.checked) {
        this.report_case = reason;
      } else {
        this.report_case = "";
      }
    },
    submitReportPost: function submitReportPost() {
      var _this2 = this;

      if (this.report_case && !this.reporting_post) {
        this.reporting_post = true;
        axios.post('/v1/post_report', {
          user_id: this.$store.getters.isAuthenticated.id,
          post_id: this.user_id,
          reason: this.report_case,
          description: this.report_case_description,
          type: '0'
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          console.log(res.data, res.data.status == 'success', res.data.status);

          if (res.data.status == 'success') {
            // this.user_info = res.data.user_detail
            _this2.reporting_post = false;
            _this2.report_case = "";
            _this2.report_case_description = "";
            $('.user-report-option').prop('checked', false);

            _this2.$toastr.success('User reported successfully');

            $('#reportUserModal').modal('hide');
          } else {
            console.log(res);

            _this2.$toastr.error(res.data.message);

            _this2.reporting_post = false;
          }
        })["catch"](function (err) {
          _this2.$toastr.error('An unexpected error occurred');

          _this2.reporting_post = false;
        });
      } // console.log(reason)

    },
    reviewUser: function reviewUser() {
      var _this3 = this;

      this.rating_user = true; // let ratings = [
      //     this.review_options[0].ratings,
      //     this.review_options[1].ratings,
      //     this.review_options[2].ratings,
      //     this.review_options[3].ratings,
      //     this.review_options[4].ratings,
      //     this.review_options[5].ratings,
      //     this.review_options[6].ratings,
      //     this.review_options[7].ratings,
      //     this.review_options[8].ratings,
      //     this.review_options[9].ratings,
      // ]

      var ratings = {
        product_as_described: this.review_options[0].ratings,
        clear_pictures: this.review_options[1].ratings,
        responds_quickly: this.review_options[2].ratings,
        friendly: this.review_options[3].ratings,
        deal_quickly_closed: this.review_options[4].ratings,
        good_service: this.review_options[5].ratings,
        fulfils_agreements: this.review_options[6].ratings,
        fast_shipping: this.review_options[7].ratings,
        meets_expectations: this.review_options[8].ratings,
        professional_communication: this.review_options[9].ratings
      };
      axios.post('/v1/store_review', {
        reviewer_id: this.$store.getters.isAuthenticated.id,
        reviewed_id: this.user_info.id,
        ratings: JSON.stringify(ratings)
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        $('#rateModal').modal('hide');
        _this3.rating_user = false;

        _this3.$refs.show_reviews.getReviews();

        console.log(res);
        _this3.user_info.has_reviewed = true; // this.review_options.map(option => {
        //     option.ratings = 1;
        // });
      })["catch"](function (err) {
        _this3.rating_user = false;
        console.log(err);
      });
    },
    fillRewviewForm: function fillRewviewForm() {
      console.log(this.review.ratings, this.review);
      var review = this.review;
      this.review_options[0].ratings = review.ratings.product_as_described;
      this.review_options[1].ratings = review.ratings.clear_pictures;
      this.review_options[2].ratings = review.ratings.responds_quickly;
      this.review_options[3].ratings = review.ratings.friendly;
      this.review_options[4].ratings = review.ratings.deal_quickly_closed;
      this.review_options[5].ratings = review.ratings.good_service;
      this.review_options[6].ratings = review.ratings.fulfils_agreements;
      this.review_options[7].ratings = review.ratings.fast_shipping;
      this.review_options[8].ratings = review.ratings.meets_expectations;
      this.review_options[9].ratings = review.ratings.professional_communication; //     this.review.ratings.map( rating => {
      //         console.log(rating , 'rating' )
      //     })
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.user-info{\r\n    position: relative;\n}\r\n/* .user-info .other-options .options-box .option{\r\n    padding: 3px 15px;\r\n    width: 150px;\r\n    border-bottom: 1px solid #ececec;\r\n}\r\n.user-info .other-options .options-box .close-btn{\r\n    position: absolute;\r\n    right: 7px;\r\n    top: 0;\r\n    font-size: 12px;\r\n\r\n}\r\n.user-info .other-options .options-box{\r\n    position: absolute;\r\n    padding-top: 15px;\r\n    top: 5px;\r\n    right: 0px;\r\n    border-radius: 5px;\r\n    box-shadow: 0 0 4px 0px #d6d6d6;\r\n}\r\n*/\n.user-info .other-options{\r\n    position: absolute;\r\n    right: 0;\r\n    top: 6px;\r\n    color: #4e6a80;\n}\n@media screen and (max-width:768px) {\n.other-user-profile{\r\n        margin-bottom: 10px;\n}\n.review-user-modal .small-text{\r\n        font-size: 15px;\n}\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./left-menu.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=template&id=6f7d5712&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/other_user/partials/left-menu.vue?vue&type=template&id=6f7d5712& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        { staticClass: "bz-white-box py-0 other-user-profile" },
        [
          _vm.loading
            ? _c("loader", {
                staticClass: "bz-text-red",
                attrs: { small: false }
              })
            : _vm._e(),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "user-info" },
            [
              _c("div", { staticClass: "other-options mobile-only-b" }, [
                !_vm.showList
                  ? _c("i", {
                      staticClass: "fas fa-ellipsis-v",
                      on: {
                        click: function($event) {
                          _vm.showList = true
                        }
                      }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.showList,
                        expression: "showList"
                      }
                    ],
                    staticClass: "view-options"
                  },
                  [
                    _c("div", { staticClass: "option" }, [
                      _c("a", { attrs: { href: "javascript:;" } }, [
                        _c("img", {
                          attrs: {
                            src: "/bazar/dashboard-icons/report.png",
                            "data-toggle": "modal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.reportUserModalFunc()
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "option" }, [
                      _c("a", { attrs: { href: "javascript:;" } }, [
                        _c("img", {
                          attrs: {
                            src: "/bazar/dashboard-icons/rate.png",
                            "data-toggle": "modal"
                          },
                          on: {
                            click: function($event) {
                              return _vm.rateUserModalFunc()
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "option" }, [
                      _c(
                        "a",
                        {
                          attrs: { href: "javascript:;" },
                          on: {
                            click: function($event) {
                              _vm.showList = false
                            }
                          }
                        },
                        [_c("i", { staticClass: "fas fa-times-circle" })]
                      )
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "profile-image" }, [
                _vm.user_info.profile_image
                  ? _c("img", {
                      attrs: {
                        src: _vm._f("serverPath")(_vm.user_info.profile_image)
                      },
                      on: {
                        error: function($event) {
                          $event.target.src =
                            "/bazar/dashboard-icons/userimage.png"
                        }
                      }
                    })
                  : _c("img", {
                      attrs: { src: "/bazar/dashboard-icons/userimage.png" }
                    })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "user-name" }, [
                _vm._v(_vm._s(_vm.user_info.name) + "\r\n                ")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "member-date" }, [
                _c("i", {
                  staticClass: "fa fa-calendar-minus-o",
                  attrs: { "aria-hidden": "true" }
                }),
                _vm._v(
                  "\r\n                Member since " +
                    _vm._s(_vm._f("formatedDate")(_vm.user_info.created_at)) +
                    " "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "user-social-media-accounts" }, [
                _vm._m(0),
                _vm._v(" "),
                _vm.user_info.verify_phone == "true"
                  ? _c("a", { attrs: { href: "#" } }, [
                      _c("img", {
                        staticClass: "user-social-media-account",
                        attrs: { src: "/bazar/view-product-mobile.png" }
                      })
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.user_info.verify_facebook == "true"
                  ? _c("a", { attrs: { href: "#" } }, [
                      _c("img", {
                        staticClass: "user-social-media-account",
                        attrs: { src: "/bazar/view-product.facebook.png" }
                      })
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.user_info.verify_google == "true"
                  ? _c("a", { attrs: { href: "#" } }, [
                      _c("img", {
                        staticClass: "user-social-media-account",
                        attrs: { src: "/bazar/view-product-googleplus.png" }
                      })
                    ])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _vm.user_info
                ? _c("show-ratings", {
                    ref: "show_reviews",
                    attrs: { user: _vm.user_info }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "user-stats" }, [
                _c(
                  "div",
                  { staticClass: "user-stats-box ml-0" },
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/user/" + _vm.user_id + "/ads" } },
                      [
                        _c("div", [_vm._v(_vm._s(_vm.user_info.ads_count))]),
                        _vm._v(" "),
                        _c("div", [_vm._v("Ads")])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "user-stats-box" },
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/user/" + _vm.user_id + "/followings" } },
                      [
                        _c("div", [_vm._v(_vm._s(_vm.user_info.following))]),
                        _vm._v(" "),
                        _c("div", [_vm._v("Following")])
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "user-stats-box no-border" },
                  [
                    _c(
                      "router-link",
                      { attrs: { to: "/user/" + _vm.user_id + "/followers" } },
                      [
                        _c("div", [_vm._v(_vm._s(_vm.user_info.followers))]),
                        _vm._v(" "),
                        _c("div", [_vm._v("Followers")])
                      ]
                    )
                  ],
                  1
                )
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "dashboard-menu" }, [
            _c(
              "div",
              { staticClass: "menu-list" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "menu-item",
                    attrs: { to: "/user/" + _vm.user_id + "/ads" }
                  },
                  [
                    _c("img", {
                      staticClass: "menu-item-icon",
                      attrs: { src: "/bazar/dashboard-icons/marketing.png" }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "menu-name" }, [
                      _vm._v(
                        "\r\n                        My Ads\r\n                        "
                      ),
                      _c("div", { staticClass: "menu-notification-number" }, [
                        _c("div", [_vm._v(_vm._s(_vm.user_info.ads_count))])
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "menu-item",
                    attrs: { to: "/user/" + _vm.user_id + "/followings" }
                  },
                  [
                    _c("img", {
                      staticClass: "menu-item-icon",
                      attrs: { src: "/bazar/dashboard-icons/following.png" }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "menu-name" }, [
                      _vm._v(
                        "\r\n                        Following\r\n                        "
                      ),
                      _c("div", { staticClass: "menu-notification-number" }, [
                        _c("div", [_vm._v(_vm._s(_vm.user_info.following))])
                      ])
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "router-link",
                  {
                    staticClass: "menu-item",
                    attrs: { to: "/user/" + _vm.user_id + "/followers" }
                  },
                  [
                    _c("img", {
                      staticClass: "menu-item-icon",
                      attrs: { src: "/bazar/dashboard-icons/followers.png" }
                    }),
                    _vm._v(" "),
                    _c("div", { staticClass: "menu-name no-border" }, [
                      _vm._v(
                        "\r\n                        Followers\r\n                        "
                      ),
                      _c("div", { staticClass: "menu-notification-number" }, [
                        _c("div", [_vm._v(_vm._s(_vm.user_info.followers))])
                      ])
                    ])
                  ]
                )
              ],
              1
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.user_info && _vm.user_info.id != _vm.$store.getters.isAuthenticated.id
        ? [
            _c(
              "div",
              {
                staticClass: "bz-white-box report-box desktop-only",
                attrs: { "data-toggle": "modal" },
                on: {
                  click: function($event) {
                    return _vm.reportUserModalFunc()
                  }
                }
              },
              [
                _c("img", {
                  attrs: { src: "/bazar/dashboard-icons/warning-sign.png" }
                }),
                _vm._v("\r\n            Report This User\r\n        ")
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "bz-white-box report-box desktop-only",
                attrs: { "data-toggle": "modal" },
                on: {
                  click: function($event) {
                    return _vm.rateUserModalFunc()
                  }
                }
              },
              [
                _c("img", {
                  staticStyle: { width: "23px" },
                  attrs: { src: "/bazar/dashboard-icons/rate.png" }
                }),
                _vm._v("\r\n            Rate This User\r\n        ")
              ]
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "reportUserModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "report-ad-modal report-option" }, [
                    _c("div", { staticClass: "small-text" }, [
                      _vm._v(
                        "\r\n                    Please Select one of the following reasons\r\n                "
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "report-options" }, [
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(2),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-1"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Inauthentic"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-1" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(3),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-2"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Inappropriate Communication"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-2" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(4),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-3"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Violating Ads"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-3" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(5),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-4"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Sold Ads"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-4" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(6),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-5"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Inappropriate Ads"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-5" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(7),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-6"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase(
                                      $event,
                                      "Suspicious Advertiser"
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-6" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option" }, [
                        _vm._m(8),
                        _vm._v(" "),
                        _c("div", { staticClass: "report-item-switch" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "custom-control custom-switch float-right"
                            },
                            [
                              _c("input", {
                                staticClass:
                                  "custom-control-input user-report-option",
                                attrs: {
                                  type: "checkbox",
                                  name: "reason",
                                  id: "option-7"
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.handleReportCase($event, "Other")
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("label", {
                                staticClass: "custom-control-label",
                                attrs: { for: "option-7" }
                              })
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "report-option-text" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.report_case_description,
                              expression: "report_case_description"
                            }
                          ],
                          attrs: {
                            placeholder: "Additional info (Optional)",
                            onkeyup: "countChar(this)",
                            maxlength: "120"
                          },
                          domProps: { value: _vm.report_case_description },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.report_case_description = $event.target.value
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "character-length",
                            attrs: { id: "charNum" }
                          },
                          [_vm._v(" 0/120 ")]
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "submit-btn-area text" }, [
                        _c(
                          "button",
                          {
                            staticClass:
                              "bz-sqaure-btn bz-btn-red full-mobile no-bold",
                            attrs: {
                              type: "button",
                              disabled: _vm.reporting_post
                            },
                            on: { click: _vm.submitReportPost }
                          },
                          [
                            _vm.reporting_post
                              ? _c("loader")
                              : _c("span", [_vm._v(" SUBMIT ")])
                          ],
                          1
                        )
                      ])
                    ])
                  ])
                ])
              ])
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal fade",
          attrs: {
            id: "rateModal",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "exampleModalCenterTitle",
            "aria-hidden": "true"
          }
        },
        [
          _c(
            "div",
            {
              staticClass: "modal-dialog modal-dialog-centered",
              attrs: { role: "document" }
            },
            [
              _c("div", { staticClass: "modal-content" }, [
                _vm._m(9),
                _vm._v(" "),
                _c("div", { staticClass: "modal-body" }, [
                  _c("div", { staticClass: "review-user-modal" }, [
                    _c("div", { staticClass: "small-text" }, [
                      _vm._v(
                        "\r\n                        Please rate from 1 till 5 stars of each experience\r\n                    "
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "review-options" },
                      _vm._l(_vm.review_options, function(option, index) {
                        return _c(
                          "div",
                          { key: index, staticClass: "review-option" },
                          [
                            _c("div", { staticClass: "review-option-name" }, [
                              _c("label", [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(option.name) +
                                    "\r\n                                "
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c("star-rating", {
                              attrs: {
                                "read-only": false,
                                "show-rating": false,
                                "star-size": 20
                              },
                              model: {
                                value: option.ratings,
                                callback: function($$v) {
                                  _vm.$set(option, "ratings", $$v)
                                },
                                expression: "option.ratings"
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "report-option text-center mt-4" },
                      [
                        _c("div", { staticClass: "submit-btn-area" }, [
                          _c(
                            "button",
                            {
                              staticClass:
                                "bz-sqaure-btn bz-btn-red full-mobile no-bold",
                              attrs: {
                                type: "button",
                                disabled: _vm.rating_user
                              },
                              on: { click: _vm.reviewUser }
                            },
                            [
                              _vm.rating_user ? _c("loader") : _vm._e(),
                              _vm._v(" "),
                              _c(
                                "span",
                                { class: { "non-visible": _vm.rating_user } },
                                [_vm._v(" SUBMIT ")]
                              )
                            ],
                            1
                          )
                        ])
                      ]
                    )
                  ])
                ])
              ])
            ]
          )
        ]
      )
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "#" } }, [
      _c("img", {
        staticClass: "user-social-media-account",
        attrs: { src: "/bazar/view-product-email.png" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [
        _vm._v("Report User")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-1" } },
      [
        _c("label", { attrs: { for: "option-1" } }, [
          _vm._v(
            "\r\n                                Inauthentic\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-2" } },
      [
        _c("label", { attrs: { for: "option-2" } }, [
          _vm._v(
            "\r\n                                Inappropriate Communication\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-2" } },
      [
        _c("label", { attrs: { for: "option-3" } }, [
          _vm._v(
            "\r\n                                Violating Ads\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-2" } },
      [
        _c("label", { attrs: { for: "option-4" } }, [
          _vm._v(
            "\r\n                                Sold Ads\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-2" } },
      [
        _c("label", { attrs: { for: "option-5" } }, [
          _vm._v(
            "\r\n                                Inappropriate Ads\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-2" } },
      [
        _c("label", { attrs: { for: "option-6" } }, [
          _vm._v(
            "\r\n                                Suspicious Advertiser\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "report-option-name", attrs: { for: "option-7" } },
      [
        _c("label", { attrs: { for: "option-7" } }, [
          _vm._v(
            "\r\n                                Other\r\n                            "
          )
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [
        _vm._v("Rate This User")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/other_user/partials/left-menu.vue":
/*!*************************************************************!*\
  !*** ./resources/js/user/other_user/partials/left-menu.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _left_menu_vue_vue_type_template_id_6f7d5712___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./left-menu.vue?vue&type=template&id=6f7d5712& */ "./resources/js/user/other_user/partials/left-menu.vue?vue&type=template&id=6f7d5712&");
/* harmony import */ var _left_menu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./left-menu.vue?vue&type=script&lang=js& */ "./resources/js/user/other_user/partials/left-menu.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _left_menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./left-menu.vue?vue&type=style&index=0&lang=css& */ "./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _left_menu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _left_menu_vue_vue_type_template_id_6f7d5712___WEBPACK_IMPORTED_MODULE_0__["render"],
  _left_menu_vue_vue_type_template_id_6f7d5712___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/other_user/partials/left-menu.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/other_user/partials/left-menu.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/user/other_user/partials/left-menu.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./left-menu.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./left-menu.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/user/other_user/partials/left-menu.vue?vue&type=template&id=6f7d5712&":
/*!********************************************************************************************!*\
  !*** ./resources/js/user/other_user/partials/left-menu.vue?vue&type=template&id=6f7d5712& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_template_id_6f7d5712___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./left-menu.vue?vue&type=template&id=6f7d5712& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/other_user/partials/left-menu.vue?vue&type=template&id=6f7d5712&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_template_id_6f7d5712___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_left_menu_vue_vue_type_template_id_6f7d5712___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);