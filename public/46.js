(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[46],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-coins.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/my-coins.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../partials/loader.vue */ "./resources/js/partials/loader.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.loadTransactions();
  },
  data: function data() {
    return {
      loading: false,
      no_more_transactions: false,
      ads: [],
      coins_transactions: [],
      coins: 0,
      index: 0
    };
  },
  methods: {
    loadTransactions: function loadTransactions() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/get_mycoins', {
        type: 0,
        user_id: this.$store.getters.isAuthenticated.id,
        index: this.index // user_id: 3,

      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        console.log(res.data, res.data.status == 'fail');
        _this.loading = false;

        if (res.data.status == 'success') {
          var _this$coins_transacti;

          (_this$coins_transacti = _this.coins_transactions).push.apply(_this$coins_transacti, _toConsumableArray(res.data.data));

          _this.coins = res.data.mycoins;
          _this.index = _this.index + 1;
        } else if (res.data.status == 'fail') {
          _this.no_more_transactions = true;
          console.log(res);
        } else {
          console.log(res);

          _this.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    pageEnd: function pageEnd() {
      var msg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-coins.vue?vue&type=template&id=6f9a7872&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/my-coins.vue?vue&type=template&id=6f9a7872& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid user-dashboard" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-lg-3 col-md-3 col-sm-12 col-xs-12 desktop-only" },
        [_c("left-dashboard")],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-9 col-md-9 col-sm-12 col-xs-12 profile-form-container"
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "my-coins-page" }, [
            _c("div", { staticClass: "section-heading pb-0 ml-3" }, [
              _vm._v("Balance")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "section-heading-small ml-3" }, [
              _vm._v("Today, " + _vm._s(_vm._f("todayDate")(new Date())))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "current-coins ml-3" }, [
              _c("img", { attrs: { src: "/bazar/coins1.png" } }),
              _vm._v(" " + _vm._s(_vm.coins) + " ")
            ]),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "transaction-list-heading" }, [
              _vm._v("\r\n                Transaction List\r\n            ")
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "transaction-list-box" },
              [
                _c(
                  "div",
                  { staticClass: "transaction-list" },
                  _vm._l(_vm.coins_transactions, function(coin) {
                    return _c(
                      "div",
                      { key: coin.id, staticClass: "transaction-list-item" },
                      [
                        _c("div", { staticClass: "transaction-item-icon" }, [
                          coin.type == "redeem" || coin.type == "upgrade"
                            ? _c("img", { attrs: { src: "/bazar/minus.png" } })
                            : _vm._e(),
                          _vm._v(" "),
                          coin.type != "redeem" && coin.type != "upgrade"
                            ? _c("img", { attrs: { src: "/bazar/plus1.png" } })
                            : _vm._e()
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "transaction-item-description" },
                          [_vm._v(_vm._s(coin.tital))]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "transaction-item-time" }, [
                          _vm._v(
                            _vm._s(
                              _vm._f("formatDifferenceDate")(coin.created_at)
                            ) + " "
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "transaction-item-coins",
                            class: {
                              spent:
                                coin.type == "redeem" || coin.type == "upgrade",
                              gained:
                                coin.type != "redeem" || coin.type != "upgrade"
                            }
                          },
                          [_vm._v(_vm._s(coin.coins) + " credit")]
                        )
                      ]
                    )
                  }),
                  0
                ),
                _vm._v(" "),
                _vm.loading
                  ? _c("div", { staticClass: "ads-listing" }, [
                      _c(
                        "div",
                        { staticClass: "ad-list-item" },
                        _vm._l(5, function(index) {
                          return _c(
                            "div",
                            {
                              key: index,
                              staticClass:
                                "ad-list-item-main-container d-block p-2"
                            },
                            [
                              _c("b-skeleton", {
                                attrs: { animation: "wave", width: "100%" }
                              })
                            ],
                            1
                          )
                        }),
                        0
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                !_vm.no_more_transactions
                  ? _c(
                      "Intersect",
                      {
                        on: {
                          enter: _vm.loadTransactions,
                          leave: function($event) {
                            return _vm.pageEnd("Leave")
                          }
                        }
                      },
                      [_c("div")]
                    )
                  : _vm._e()
              ],
              1
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-dashboard-page-header" }, [
      _c("div", { staticClass: "bz-page-header-title text-center" }, [
        _vm._v("\r\n                My Coins\r\n            ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "small-text" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "coins-details-box" }, [
      _c("div", { staticClass: "coins-details-box-heading" }, [
        _vm._v("How to earn more coins ?")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "coin-detail-row" }, [
        _c("div", { staticClass: "coin-type" }, [_vm._v("Types")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-value" }, [_vm._v("Coins")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-credit" }, [_vm._v("Credits")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "coin-detail-row" }, [
        _c("div", { staticClass: "coin-type" }, [_vm._v("Like Ad")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-value" }, [_vm._v("1")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-credit" }, [_vm._v("100")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "coin-detail-row" }, [
        _c("div", { staticClass: "coin-type" }, [_vm._v("Place Ad")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-value" }, [_vm._v("3")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-credit" }, [_vm._v("300")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "coin-detail-row" }, [
        _c("div", { staticClass: "coin-type" }, [_vm._v("Invite Freind")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-value" }, [_vm._v("5")]),
        _vm._v(" "),
        _c("div", { staticClass: "coin-credit" }, [_vm._v("500")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/my-coins.vue":
/*!**************************************************!*\
  !*** ./resources/js/user/dashboard/my-coins.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _my_coins_vue_vue_type_template_id_6f9a7872___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./my-coins.vue?vue&type=template&id=6f9a7872& */ "./resources/js/user/dashboard/my-coins.vue?vue&type=template&id=6f9a7872&");
/* harmony import */ var _my_coins_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./my-coins.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/my-coins.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _my_coins_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _my_coins_vue_vue_type_template_id_6f9a7872___WEBPACK_IMPORTED_MODULE_0__["render"],
  _my_coins_vue_vue_type_template_id_6f9a7872___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/my-coins.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/my-coins.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/user/dashboard/my-coins.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_my_coins_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./my-coins.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-coins.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_my_coins_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/my-coins.vue?vue&type=template&id=6f9a7872&":
/*!*********************************************************************************!*\
  !*** ./resources/js/user/dashboard/my-coins.vue?vue&type=template&id=6f9a7872& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_my_coins_vue_vue_type_template_id_6f9a7872___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./my-coins.vue?vue&type=template&id=6f9a7872& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/my-coins.vue?vue&type=template&id=6f9a7872&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_my_coins_vue_vue_type_template_id_6f9a7872___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_my_coins_vue_vue_type_template_id_6f9a7872___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);