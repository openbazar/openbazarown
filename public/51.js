(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[51],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/profile.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/profile.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../partials/loader.vue */ "./resources/js/partials/loader.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.loadUserInfo();
  },
  data: function data() {
    return {
      user_info: '',
      updating: false,
      password_updating: false,
      verifying_google: false,
      verifying_facebook: false,
      loading: false,
      name: "",
      email: "",
      phone: "",
      reset_password: {
        password: "",
        confirm_password: ""
      },
      user_image: "/bazar/dashboard-icons/userimage.png",
      uploadedImage: ""
    };
  },
  methods: {
    loadUserInfo: function loadUserInfo() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/user_details', {
        id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        _this.loading = false;
        console.log(res.data);

        if (res.data.status == 'success') {
          _this.user_info = res.data.user_detail;
          _this.name = _this.user_info.name;
          _this.email = _this.user_info.email;
          _this.phone = _this.user_info.phone_no;

          if (_this.user_info.profile_image) {
            // console.log(this.$root.data)
            _this.user_image = _this.$root.virtualmin_server + '/public/images/' + _this.user_info.profile_image;
          }
        } else {
          console.log(res);

          _this.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    updatePassword: function updatePassword() {
      var _this2 = this;

      if (this.reset_password.password == this.reset_password.confirm_password) {
        this.password_updating = true;
        axios.post('/v1/reset_password', {
          email: this.email,
          password: this.reset_password.password
        }).then(function (res) {
          if (res.data.status == 'success') {
            _this2.password_updating = false;
            _this2.reset_password.password = '';
            _this2.reset_password.confirm_password = '';
            $('#passwordChangeModal').modal('hide');
          } else {
            console.log(res);
            _this2.password_updating = false;
            _this2.reset_password.password = '';
            _this2.reset_password.confirm_password = '';

            _this2.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          console.log(err);
          _this2.password_updating = false;
          _this2.reset_password.password = '';
          _this2.reset_password.confirm_password = '';

          _this2.$toastr.error('An unexpected error occurred');
        });
      } else {
        this.$toastr.error('Password does not match');
      }
    },
    verifySocialAccount: function verifySocialAccount() {
      var _this3 = this;

      var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      if (type) {
        console.log(type);

        if (type == 'google') {
          this.verifying_google = true;
        } else {
          this.verifying_facebook = true;
        }

        axios.post('/v1/verify_social_account', {
          user_id: this.$store.getters.isAuthenticated.id,
          type: type,
          verify: 'true'
        }).then(function (res) {
          _this3.verifying_google = false;
          _this3.verifying_facebook = false;

          if (res.data.status == 'success') {
            _this3.$root.$refs.leftDashboard.getProfileStats();
          } else {
            console.log(res);
          }
        })["catch"](function (err) {
          console.log(err);
          _this3.verifying_google = false;
          _this3.verifying_facebook = false;

          _this3.$toastr.error('An unexpected error occurred');
        });
      }
    },
    updateProfile: function updateProfile() {
      var _this4 = this;

      this.updating = true;
      var fd = new FormData();
      fd.append('profile_image', this.uploadedImage);
      fd.append('verify_facebook', 'false');
      fd.append('verify_google', 'false');
      fd.append('name', this.name);
      fd.append('id', this.$store.getters.isAuthenticated.id);
      axios.post('/v1/update_profile', fd, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token,
          'Content-Type': 'multipart/form-data'
        } // onUploadProgress: function(progressEvent) {
        //     uploadProgress = parseInt( Math.round((progressEvent.loaded / progressEvent.total) * 100))
        // }.bind(this),

      }).then(function (res) {
        console.log(res.data);

        if (res.data.status == 'success') {
          _this4.user_info = res.data.data;
          _this4.updating = false;

          _this4.$store.commit('profileUpdate', _this4.user_info);
        } else {
          console.log(res);
          _this4.updating = false;

          _this4.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this4.updating = false;

        _this4.$toastr.error('An unexpected error occurred');
      });
    },
    imageSelected: function imageSelected(event) {
      var file = event.target.files[0];
      if (!file) return; // console.log(file)

      var src = URL.createObjectURL(file);
      this.user_image = src;
      this.uploadedImage = file;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/profile.vue?vue&type=template&id=656ffae8&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/profile.vue?vue&type=template&id=656ffae8& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid user-dashboard" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-lg-3 col-md-3 col-sm-12 col-xs-12 desktop-only" },
        [_c("left-dashboard")],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-9 col-md-9 col-sm-12 col-xs-12 profile-form-container"
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "profile-page" },
            [
              _vm.loading
                ? _c("loader", {
                    staticClass: "bz-text-red",
                    attrs: { small: false }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "profile-image" }, [
                _c("img", { attrs: { src: _vm.user_image } }),
                _vm._v(" "),
                _c("input", {
                  ref: "fileInput",
                  staticStyle: { display: "none" },
                  attrs: {
                    accept: "image/*",
                    type: "file",
                    id: "profile-imaeg"
                  },
                  on: { change: _vm.imageSelected }
                }),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "change-photo-text",
                    attrs: { href: "javascript:;" },
                    on: {
                      click: function($event) {
                        return _vm.$refs.fileInput.click()
                      }
                    }
                  },
                  [_vm._v("Change Your Photo")]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "user-form" }, [
                _c(
                  "form",
                  {
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.updateProfile($event)
                      }
                    }
                  },
                  [
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.name,
                            expression: "name"
                          }
                        ],
                        staticClass: "dashboard-input",
                        attrs: { required: "", placeholder: "Name" },
                        domProps: { value: _vm.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.name = $event.target.value
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.email,
                            expression: "email"
                          }
                        ],
                        staticClass: "dashboard-input",
                        attrs: {
                          disabled: "",
                          required: "",
                          placeholder: "Email"
                        },
                        domProps: { value: _vm.email },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.email = $event.target.value
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.phone,
                            expression: "phone"
                          }
                        ],
                        staticClass: "dashboard-input",
                        attrs: { required: "", placeholder: "Phone" },
                        domProps: { value: _vm.phone },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.phone = $event.target.value
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c(
                        "button",
                        {
                          staticClass: "dashboard-white-btn",
                          attrs: { type: "submit", disabled: _vm.updating }
                        },
                        [
                          _vm.updating
                            ? _c("loader")
                            : _c("span", [_vm._v("Save")])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "dashboard-white-btn mt-3",
                          attrs: {
                            type: "button",
                            "data-toggle": "modal",
                            "data-target": "#passwordChangeModal"
                          }
                        },
                        [_vm._v("Change Your Password")]
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "build-trust" }, [
                  _c("div", { staticClass: "build-trust-text" }, [
                    _vm._v("Build Trust")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c(
                      "button",
                      { staticClass: "dashboard-white-btn google-option" },
                      [
                        _vm.verifying_google
                          ? _c("loader", { staticClass: "bz-text-red" })
                          : _vm._e(),
                        _vm._v(" "),
                        _c("img", { attrs: { src: "/bazar/google.png" } }),
                        _vm._v(
                          "\r\n                            Google\r\n\r\n                            "
                        ),
                        _c(
                          "div",
                          {
                            staticClass:
                              "custom-control custom-switch float-right"
                          },
                          [
                            _c("input", {
                              staticClass: "custom-control-input",
                              attrs: { type: "checkbox", id: "google-switch" },
                              domProps: {
                                checked: _vm.user_info.verify_google == "true"
                              },
                              on: {
                                change: function($event) {
                                  return _vm.verifySocialAccount("google")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", {
                              staticClass: "custom-control-label",
                              attrs: { for: "google-switch" }
                            })
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-group" }, [
                    _c(
                      "button",
                      { staticClass: "dashboard-white-btn facebook-option" },
                      [
                        _vm.verifying_facebook
                          ? _c("loader", { staticClass: "bz-text-red" })
                          : _vm._e(),
                        _vm._v(" "),
                        _c("img", { attrs: { src: "/bazar/facebook.png" } }),
                        _vm._v(
                          "\r\n                            Facebook\r\n\r\n                            "
                        ),
                        _c(
                          "div",
                          {
                            staticClass:
                              "custom-control custom-switch float-right"
                          },
                          [
                            _c("input", {
                              staticClass: "custom-control-input",
                              attrs: {
                                type: "checkbox",
                                id: "facebook-switch"
                              },
                              domProps: {
                                checked: _vm.user_info.verify_facebook == "true"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.verifySocialAccount("fb")
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", {
                              staticClass: "custom-control-label",
                              attrs: { for: "facebook-switch" }
                            })
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "modal fade",
                  attrs: {
                    id: "passwordChangeModal",
                    tabindex: "-1",
                    role: "dialog",
                    "aria-labelledby": "passwordChangeModalTitle",
                    "aria-hidden": "true"
                  }
                },
                [
                  _c(
                    "div",
                    {
                      staticClass: "modal-dialog modal-dialog-centered",
                      attrs: { role: "document" }
                    },
                    [
                      _c("div", { staticClass: "modal-content" }, [
                        _vm._m(1),
                        _vm._v(" "),
                        _c(
                          "form",
                          {
                            on: {
                              submit: function($event) {
                                $event.preventDefault()
                                return _vm.updatePassword($event)
                              }
                            }
                          },
                          [
                            _c("div", { staticClass: "modal-body" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.reset_password.password,
                                      expression: "reset_password.password"
                                    }
                                  ],
                                  staticClass: "dashboard-input",
                                  attrs: {
                                    type: "password",
                                    required: "",
                                    placeholder: "Type Password"
                                  },
                                  domProps: {
                                    value: _vm.reset_password.password
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.reset_password,
                                        "password",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "form-group" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value:
                                        _vm.reset_password.confirm_password,
                                      expression:
                                        "reset_password.confirm_password"
                                    }
                                  ],
                                  staticClass: "dashboard-input",
                                  attrs: {
                                    type: "password",
                                    required: "",
                                    placeholder: "Re-Type Password"
                                  },
                                  domProps: {
                                    value: _vm.reset_password.confirm_password
                                  },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.reset_password,
                                        "confirm_password",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "modal-footer" }, [
                              _c(
                                "button",
                                {
                                  staticClass:
                                    "bz-sqaure-btn bz-btn-red full-mobile update-password-btn",
                                  attrs: { type: "submit" }
                                },
                                [
                                  _vm.password_updating
                                    ? _c("loader")
                                    : _c("span", [_vm._v("Update")])
                                ],
                                1
                              )
                            ])
                          ]
                        )
                      ])
                    ]
                  )
                ]
              )
            ],
            1
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-dashboard-page-header" }, [
      _c("div", { staticClass: "bz-page-header-title text-center" }, [
        _vm._v("\r\n                    Edit Profile\r\n                ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "small-text" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "modal-header" }, [
      _c(
        "h5",
        {
          staticClass: "modal-title",
          attrs: { id: "exampleModalCenterTitle" }
        },
        [_vm._v("Change Password")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "close",
          attrs: {
            type: "button",
            "data-dismiss": "modal",
            "aria-label": "Close"
          }
        },
        [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/profile.vue":
/*!*************************************************!*\
  !*** ./resources/js/user/dashboard/profile.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _profile_vue_vue_type_template_id_656ffae8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.vue?vue&type=template&id=656ffae8& */ "./resources/js/user/dashboard/profile.vue?vue&type=template&id=656ffae8&");
/* harmony import */ var _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/profile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _profile_vue_vue_type_template_id_656ffae8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _profile_vue_vue_type_template_id_656ffae8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/profile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/profile.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/user/dashboard/profile.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./profile.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/profile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/profile.vue?vue&type=template&id=656ffae8&":
/*!********************************************************************************!*\
  !*** ./resources/js/user/dashboard/profile.vue?vue&type=template&id=656ffae8& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_656ffae8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./profile.vue?vue&type=template&id=656ffae8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/profile.vue?vue&type=template&id=656ffae8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_656ffae8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_656ffae8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);