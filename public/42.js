(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[42],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/blocked-users.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/blocked-users.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../partials/loader.vue */ "./resources/js/partials/loader.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.loadBlockedUsers();
    firebase.app();
    var database = firebase.database();
  },
  data: function data() {
    return {
      loading: false,
      following_process: "",
      unblocking: "",
      blocked_list: []
    };
  },
  methods: {
    loadBlockedUsers: function loadBlockedUsers() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/blocked_list', {
        // user_id: 1,
        // index: 5,
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        console.log(res.data);

        if (res.data.status == 'true') {
          _this.blocked_list = res.data.data;
        }

        _this.loading = false;
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    },
    unblockUser: function unblockUser(user) {
      var _this2 = this;

      if (!this.unblocking) {
        var _block = [];
        _block['blockeduser' + user.id] = false;
        firebase.database().ref('Block/user' + user.id + '/user' + this.$store.getters.isAuthenticated.id).set(_objectSpread({}, _block)); // _block['blockeduser' + this.$store.getters.isAuthenticated.id] = false;
        // firebase.database().ref('Block/user' + this.$store.getters.isAuthenticated.id + '/user' + user.id).set({
        //     ..._block
        // })

        this.unblocking = user;
        axios.post('/v1/block_user', {
          block_id: user.id,
          type: 'unblock',
          user_id: this.$store.getters.isAuthenticated.id
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          // console.log(res.data)
          _this2.unblocking = '';

          if (res.data.status == 'true') {
            _this2.blocked_list.splice(_this2.blocked_list.indexOf(user), 1);
          }
        })["catch"](function (err) {
          _this2.unblocking = '';

          _this2.$toastr.error('An unexpected error occurred');
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/blocked-users.vue?vue&type=template&id=474ccbe6&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/blocked-users.vue?vue&type=template&id=474ccbe6& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid user-dashboard" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-lg-3 col-md-3 col-sm-12 col-xs-12 desktop-only" },
        [_c("left-dashboard")],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-9 col-md-9 col-sm-12 col-xs-12 profile-form-container"
        },
        [
          _c("div", { staticClass: "dashboard" }, [
            _c("div", { staticClass: "bz-dashboard-page-header" }, [
              _c("div", { staticClass: "bz-page-header-title text-center" }, [
                _vm._v(
                  "\r\n                    Blocked Users (" +
                    _vm._s(_vm.blocked_list.length) +
                    ")\r\n                "
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "small-text" })
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "follow-list" },
              [
                _vm.loading
                  ? _c(
                      "div",
                      { staticClass: "loading-skeliton" },
                      _vm._l(8, function(index) {
                        return _c(
                          "div",
                          {
                            key: index,
                            staticClass: "follow-list-item d-block"
                          },
                          [
                            _c("b-skeleton", {
                              attrs: { animation: "wave", width: "85%" }
                            }),
                            _vm._v(" "),
                            _c("b-skeleton", {
                              attrs: { animation: "wave", width: "55%" }
                            }),
                            _vm._v(" "),
                            _c("b-skeleton", {
                              attrs: { animation: "wave", width: "70%" }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm._l(_vm.blocked_list, function(user) {
                  return _c(
                    "div",
                    { key: user.id, staticClass: "follow-list-item" },
                    [
                      _c("div", { staticClass: "follow-list-item-image-box" }, [
                        user && user.profile_image
                          ? _c("img", {
                              attrs: {
                                src: _vm._f("serverPath")(user.profile_image)
                              }
                            })
                          : _c("img", {
                              attrs: {
                                src: "/bazar/dashboard-icons/userimage.png"
                              }
                            })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "follow-list-item-user-info" }, [
                        _c("div", { staticClass: "user-name" }, [
                          _vm._v(_vm._s(user.name))
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "user-posts" }, [
                          _vm._v(_vm._s(user.ads_count) + " Posts")
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "follow-list-item-btns" }, [
                        _c(
                          "a",
                          {
                            staticClass: "following",
                            attrs: { href: "javascript:;" },
                            on: {
                              click: function($event) {
                                return _vm.unblockUser(user)
                              }
                            }
                          },
                          [
                            _vm.unblocking == user
                              ? _c("loader")
                              : _c("span", [_vm._v("Unblock")])
                          ],
                          1
                        )
                      ])
                    ]
                  )
                }),
                _vm._v(" "),
                !_vm.loading && _vm.blocked_list.length == 0
                  ? _c("no-data-box", {
                      attrs: {
                        message: "No Blocked Users",
                        icon: "/bazar/dashboard-icons/blocked-users.png"
                      }
                    })
                  : _vm._e()
              ],
              2
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/blocked-users.vue":
/*!*******************************************************!*\
  !*** ./resources/js/user/dashboard/blocked-users.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocked_users_vue_vue_type_template_id_474ccbe6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blocked-users.vue?vue&type=template&id=474ccbe6& */ "./resources/js/user/dashboard/blocked-users.vue?vue&type=template&id=474ccbe6&");
/* harmony import */ var _blocked_users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blocked-users.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/blocked-users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _blocked_users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _blocked_users_vue_vue_type_template_id_474ccbe6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _blocked_users_vue_vue_type_template_id_474ccbe6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/blocked-users.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/blocked-users.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/user/dashboard/blocked-users.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_blocked_users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./blocked-users.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/blocked-users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_blocked_users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/blocked-users.vue?vue&type=template&id=474ccbe6&":
/*!**************************************************************************************!*\
  !*** ./resources/js/user/dashboard/blocked-users.vue?vue&type=template&id=474ccbe6& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blocked_users_vue_vue_type_template_id_474ccbe6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./blocked-users.vue?vue&type=template&id=474ccbe6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/blocked-users.vue?vue&type=template&id=474ccbe6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blocked_users_vue_vue_type_template_id_474ccbe6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_blocked_users_vue_vue_type_template_id_474ccbe6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);