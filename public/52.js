(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[52],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/wallet.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/wallet.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../partials/loader.vue */ "./resources/js/partials/loader.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    loader: _partials_loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.loadCoins();
  },
  data: function data() {
    return {
      loading: false,
      no_more_transactions: false,
      redeeming_coins: false,
      ads: [],
      coins_transactions: [],
      coins: 0,
      index: 0,
      redeem_coins: ''
    };
  },
  methods: {
    loadCoins: function loadCoins() {
      var _this = this;

      if (!this.loading) {
        this.loading = true; // axios.post('/v1/get_mycoins', {

        axios.post('/v1/get_mycoins', {
          type: 1,
          index: this.index,
          user_id: this.$store.getters.isAuthenticated.id
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          // console.log(res.data)
          _this.loading = false;
          _this.index = _this.index + 1;

          if (res.data.status == 'success') {
            var _this$coins_transacti;

            (_this$coins_transacti = _this.coins_transactions).push.apply(_this$coins_transacti, _toConsumableArray(res.data.data));

            _this.coins = res.data.mycoins;
          } else if (res.data.status == "fail") {
            _this.no_more_transactions = true;
          } else {
            console.log(res); // this.loading = false

            _this.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this.loading = false;

          _this.$toastr.error('An unexpected error occurred');
        }); // axios.post('/v1/get_total_credits', {
        //     user_id: this.$store.getters.isAuthenticated.id,
        // },{
        //     headers:{
        //         Authorization: "Bearer "+ this.$store.getters.isAuthenticated.token
        //     }
        // }).then (res => {
        // console.log(res.data)
        // }).catch( err => {
        //     this.loading = false
        //     this.$toastr.error('An unexpected error occurred');
        // })
      }
    },
    loadMoreAds: function loadMoreAds() {
      this.loadCoins();
    },
    pageEnd: function pageEnd(msg) {
      console.log(msg);
    },
    refetchAllTransactions: function refetchAllTransactions() {
      this.index = 0;
      this.coins_transactions = [];
      this.loadCoins();
    },
    redeemCoins: function redeemCoins() {
      var _this2 = this;

      if (this.redeem_coins) {
        this.redeeming_coins = true;
        this.index = this.index + 1;
        axios.post('/v1/redeem_coin', {
          coin: this.redeem_coins,
          user_id: this.$store.getters.isAuthenticated.id
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          // console.log(res.data)
          _this2.redeeming_coins = false;

          if (res.data.status == 'success') {
            $('#redeemModal').modal('hide'); // this.ads.push(...res.data.data)
            // this.loadCoins();

            _this2.redeem_coins = '';

            _this2.refetchAllTransactions();
          } else {
            console.log(res);

            _this2.$toastr.error(res.data.message);
          }
        })["catch"](function (err) {
          _this2.redeeming_coins = false;

          _this2.$toastr.error('An unexpected error occurred');
        });
      }
    },
    packageSelected: function packageSelected(plan) {
      $('#buyCreditsModal').modal('hide');
      this.$router.push({
        name: "payment method",
        params: {
          _plan: plan
        }
      }); // console.log(plan , 'sSd')
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/wallet.vue?vue&type=template&id=d419b3ec&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/wallet.vue?vue&type=template&id=d419b3ec& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid user-dashboard" }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-lg-3 col-md-3 col-sm-12 col-xs-12 desktop-only" },
        [_c("left-dashboard")],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "col-lg-9 col-md-9 col-sm-12 col-xs-12 profile-form-container"
        },
        [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "my-wallet-page", attrs: { id: "app" } }, [
            _c("div", { staticClass: "section-heading pb-0 ml-3" }, [
              _vm._v("Balance")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "section-heading-small ml-3" }, [
              _vm._v(_vm._s(_vm._f("todayDate")("Today," + new Date())))
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "current-coins ml-3" }, [
              _vm._v(" " + _vm._s(_vm.coins) + " ")
            ]),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "transaction-list-heading" }, [
              _vm._v(
                "\r\n                    Transaction List\r\n                "
              )
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "wallet-transaction-list-box" },
              [
                _vm._l(_vm.coins_transactions, function(coin) {
                  return _c(
                    "div",
                    { key: coin.id, staticClass: "wallet-transaction-list" },
                    [
                      _c(
                        "div",
                        { staticClass: "wallet-transaction-list-item" },
                        [
                          _c(
                            "div",
                            { staticClass: "wallet-transaction-item-icon" },
                            [
                              coin.type == "redeem" || coin.type == "upgrade"
                                ? _c("img", {
                                    attrs: { src: "/bazar/minus.png" }
                                  })
                                : _vm._e(),
                              _vm._v(" "),
                              coin.type != "redeem" && coin.type != "upgrade"
                                ? _c("img", {
                                    attrs: { src: "/bazar/plus1.png" }
                                  })
                                : _vm._e()
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "wallet-transaction-item-description"
                            },
                            [_vm._v(_vm._s(coin.tital))]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "wallet-transaction-item-time" },
                            [
                              _vm._v(
                                _vm._s(
                                  _vm._f("formatDifferenceDate")(
                                    coin.created_at
                                  )
                                ) + " "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "wallet-transaction-item-coins",
                              class: {
                                spent:
                                  coin.type == "redeem" ||
                                  coin.type == "upgrade",
                                gained:
                                  coin.type != "redeem" ||
                                  coin.type != "upgrade"
                              }
                            },
                            [_vm._v(_vm._s(coin.coins) + " credit")]
                          )
                        ]
                      )
                    ]
                  )
                }),
                _vm._v(" "),
                !_vm.loading && _vm.coins_transactions.length == 0
                  ? _c("no-data-box", {
                      attrs: {
                        height: "264px",
                        message: "No Transactions Found",
                        icon: "/bazar/dashboard-icons/wallet.png"
                      }
                    })
                  : _vm._e(),
                _vm._v(" "),
                _vm.loading
                  ? _c("div", { staticClass: "ads-listing" }, [
                      _c(
                        "div",
                        { staticClass: "ad-list-item" },
                        _vm._l(5, function(index) {
                          return _c(
                            "div",
                            {
                              key: index,
                              staticClass:
                                "ad-list-item-main-container d-block p-2"
                            },
                            [
                              _c("b-skeleton", {
                                attrs: { animation: "wave", width: "100%" }
                              })
                            ],
                            1
                          )
                        }),
                        0
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                !_vm.no_more_transactions
                  ? _c(
                      "Intersect",
                      {
                        on: {
                          enter: _vm.loadMoreAds,
                          leave: function($event) {
                            return _vm.pageEnd("Leave")
                          }
                        }
                      },
                      [_c("div")]
                    )
                  : _vm._e()
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "modal fade",
                attrs: {
                  id: "redeemModal",
                  tabindex: "-1",
                  role: "dialog",
                  "aria-labelledby": "exampleModalCenterTitle",
                  "aria-hidden": "true"
                }
              },
              [
                _c(
                  "div",
                  {
                    staticClass: "modal-dialog modal-dialog-centered",
                    attrs: { role: "document" }
                  },
                  [
                    _c("div", { staticClass: "modal-content" }, [
                      _c("div", { staticClass: "modal-body" }, [
                        _c("div", { staticClass: "redeem-modal" }, [
                          _vm._m(2),
                          _vm._v(" "),
                          _c("div", { staticClass: "redeem-modal-input" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.redeem_coins,
                                  expression: "redeem_coins"
                                }
                              ],
                              staticClass: "dashboard-input",
                              attrs: {
                                placeholder: "Enter The Amount Of Coins"
                              },
                              domProps: { value: _vm.redeem_coins },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.redeem_coins = $event.target.value
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "redeem-modal-small-text" },
                            [
                              _vm._v(
                                "\r\n                                By Clicking on Redeem your Coins will be on your wallet as Credits\r\n                            "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass: "redeem-modal-button",
                              attrs: {
                                disabled: _vm.redeeming_coins,
                                type: "button"
                              },
                              on: { click: _vm.redeemCoins }
                            },
                            [
                              _vm.redeeming_coins
                                ? _c("loader")
                                : _c("span", [_vm._v("Redeem")])
                            ],
                            1
                          )
                        ])
                      ])
                    ])
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "modal fade bd-example-modal-lg",
                attrs: {
                  id: "buyCreditsModal",
                  tabindex: "-1",
                  role: "dialog",
                  "aria-labelledby": "exampleModalCenterTitle",
                  "aria-hidden": "true"
                }
              },
              [
                _c(
                  "div",
                  {
                    staticClass:
                      "modal-dialog modal-lg modal-dialog-centered upgrade-credit-packages-modal",
                    attrs: { role: "document" }
                  },
                  [
                    _c("div", { staticClass: "modal-content" }, [
                      _vm._m(3),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "modal-body p-0" },
                        [
                          _c("credit-package", {
                            on: { "package-selected": _vm.packageSelected }
                          })
                        ],
                        1
                      )
                    ])
                  ]
                )
              ]
            )
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-dashboard-page-header" }, [
      _c("div", { staticClass: "bz-page-header-title text-center" }, [
        _vm._v("\r\n                My Wallet\r\n            ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "small-text" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row coins-details-row" }, [
      _c("div", { staticClass: "col-6" }, [
        _c("div", { staticClass: "wallet-coins-details-box" }, [
          _c("img", { attrs: { src: "/bazar/atm-card.png" } }),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "big-text curser-pointer",
              attrs: {
                "data-toggle": "modal",
                "data-target": "#buyCreditsModal"
              }
            },
            [
              _vm._v(
                "\r\n                                    \r\n                                    Buy Credits\r\n                                "
              )
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-6" }, [
        _c("div", { staticClass: "wallet-coins-details-box" }, [
          _c("img", { attrs: { src: "/bazar/redeem.png" } }),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "big-text curser-pointer",
              attrs: { "data-toggle": "modal", "data-target": "#redeemModal" }
            },
            [
              _vm._v(
                "\r\n                                    \r\n                                    Redeem\r\n                                "
              )
            ]
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "redeem-modal-title" }, [
      _c("img", { attrs: { src: "/bazar/redeem.png" } }),
      _vm._v(
        "\r\n                                Exchange your Coins\r\n                            "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "bz-modal-header" }, [
      _c("div", { staticClass: "bz-modal-header-title" }, [
        _vm._v("Buy Credits Packages")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/wallet.vue":
/*!************************************************!*\
  !*** ./resources/js/user/dashboard/wallet.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wallet_vue_vue_type_template_id_d419b3ec___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./wallet.vue?vue&type=template&id=d419b3ec& */ "./resources/js/user/dashboard/wallet.vue?vue&type=template&id=d419b3ec&");
/* harmony import */ var _wallet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./wallet.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/wallet.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _wallet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _wallet_vue_vue_type_template_id_d419b3ec___WEBPACK_IMPORTED_MODULE_0__["render"],
  _wallet_vue_vue_type_template_id_d419b3ec___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/wallet.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/wallet.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/user/dashboard/wallet.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_wallet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./wallet.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/wallet.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_wallet_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/wallet.vue?vue&type=template&id=d419b3ec&":
/*!*******************************************************************************!*\
  !*** ./resources/js/user/dashboard/wallet.vue?vue&type=template&id=d419b3ec& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_wallet_vue_vue_type_template_id_d419b3ec___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./wallet.vue?vue&type=template&id=d419b3ec& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/wallet.vue?vue&type=template&id=d419b3ec&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_wallet_vue_vue_type_template_id_d419b3ec___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_wallet_vue_vue_type_template_id_d419b3ec___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);