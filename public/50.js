(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[50],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/singleAd.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/partials/singleAd.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    _ad: {
      type: Object,
      "default": ''
    }
  },
  computed: {},
  data: function data() {
    return {
      fav_loading: false,
      show_number: false,
      ad: '',
      user_info: '',
      current_chat_instance: "",
      other_chat_instance: ""
    };
  },
  mounted: function mounted() {
    this.ad = this._ad;

    if (this._ad.favorite == '1') {
      this.ad.fav_stattus = "Yes";
    }

    this.ad.show_number = false;

    if (this.$root.$refs.userLeftMenu) {
      this.user_info = this.$root.$refs.userLeftMenu.user_info;
    }

    this.getFirebaseChatInstance(); // console.log(this.ad.id)
  },
  methods: {
    favoriteAdModalFunc: function favoriteAdModalFunc() {
      var store = window.store;

      if (store.getters.isAuthenticated || store.getters.isAuthenticated.token) {
        this.favoriteAd();
      } else {
        $('#loginModal').modal('show');
      }
    },
    favoriteAd: function favoriteAd() {
      var _this = this;

      if (!this.fav_loading) {
        this.fav_loading = true;
        axios.post('/v1/ad_view', {
          post_id: this.ad.id,
          user_id: this.$store.getters.isAuthenticated.id,
          type: 'favorite'
        }, {
          headers: {
            Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
          }
        }).then(function (res) {
          // console.log(res.data)
          _this.fav_loading = false;

          if (res.data.data.favorite == '1') {
            _this.ad.fav_stattus = "Yes";

            _this.$emit('favorite-added', _this.ad);
          } else {
            _this.ad.fav_stattus = "No";

            _this.$emit('favorite-removed', _this.ad);
          }
        })["catch"](function (err) {
          _this.fav_loading = false;

          _this.$toastr.error('An unexpected error occurred');
        });
      }
    },
    startChat: function startChat() {
      var price = '';

      if (this.ad.price_type != 'price') {
        price = this.ad.price_type;
      } else {
        price = this.ad.price;
      }

      var new_ad = {
        ad_id: this.ad.id.toString(),
        image: this.ad.images ? JSON.parse(this.ad.images)[0].image : '',
        mobileno: this.ad.mobile ? this.ad.mobile : "",
        mobileno_status: this.ad.hide_mobileno,
        price: price.toString(),
        title: this.ad.ad_title
      };
      firebase.database().ref('Ads/ads' + this.ad.id).set(_objectSpread({}, new_ad));
      console.log("this.current_chat_instance , !this.other_chat_instance", this.current_chat_instance, this.other_chat_instance);

      if (this.ad.user_id == this.$store.getters.isAuthenticated.id) {
        this.$router.push({
          name: 'chat'
        });
      } else {
        var chat_instance = "";

        if (!this.current_chat_instance || !this.other_chat_instance) {
          chat_instance = {
            timestamp: new Date().getTime(),
            "delete": false,
            seen: false
          };
        } else {
          if (!this.checkFirebaseChatInstance(this.current_chat_instance) || !this.checkFirebaseChatInstance(this.other_chat_instance)) {
            chat_instance = {
              timestamp: new Date().getTime(),
              "delete": false,
              seen: false
            };
          }
        }

        var __user = {};
        firebase.database().ref('Users/user' + this.ad.user_id).once('value', function (snapshot) {
          __user.user = snapshot.val();
        });
        __user.ad = new_ad;
        var chat = {
          ad: this.ad,
          user: this.user_info,
          chat_instance: chat_instance,
          __user: __user
        };
        console.log("chat", chat);
        this.$router.push({
          name: 'chat',
          params: {
            newChat: chat
          }
        });
      }
    },
    checkFirebaseChatInstance: function checkFirebaseChatInstance(instance) {
      var check = false;

      if (Object.keys(instance).length) {
        check = Object.keys(instance).includes('seen') && Object.keys(instance).includes('delete') && Object.keys(instance).includes('timestamp') && !instance["delete"];
      }

      return check;
    },
    getFirebaseChatInstance: function getFirebaseChatInstance() {
      var _this2 = this;

      firebase.database().ref('Chat/user' + this.ad.user_id + '/ads' + this.ad.id + '_user' + this.$store.getters.isAuthenticated.id).once('value', function (snapshot) {
        _this2.current_chat_instance = snapshot.val();
      });
      firebase.database().ref('Chat/user' + this.$store.getters.isAuthenticated.id + '/ads' + this.ad.id + '_user' + this.ad.user_id).once('value', function (snapshot) {
        _this2.other_chat_instance = snapshot.val();
      });
    } // startChat(){
    //     let new_ad = {
    //         ad_id: this.ad.id,
    //         images: this.ad.images ? JSON.parse(this.ad.images)[0].image : '',
    //         mobileno: this.ad.mobile,
    //         mobileno_status: this.ad.hide_mobileno,
    //         price: this.ad.price,
    //         title: this.ad.ad_title,
    //     }
    //     firebase.database().ref('Ads/ads' + this.ad.id).set({
    //         ...new_ad
    //     })
    //     if(this.ad.user_id == this.$store.getters.isAuthenticated.id){
    //         this.$router.push( { name: 'chat'})
    //     }else{
    //         let chat_instance = {
    //             timestamp : new Date().getTime(),
    //             seen :false,
    //         }
    //         let __user = {};
    //         firebase.database().ref('Users/user' + this.ad.user_id).once( 'value' , (snapshot) =>  {
    //             __user.user = snapshot.val();
    //         });
    //         __user.ad = new_ad
    //         let chat = {
    //             ad : this.ad,
    //             user: {
    //                 id: this.ad.user_id ,
    //                 images: this.ad.user_image ,
    //                 name: this.ad.user_name ,
    //             },
    //             __user,
    //         }
    //         console.log(chat)
    //         this.$router.push( { name: 'chat'  , params: {newChat: chat }})
    //     }
    // },

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/singleAd.vue?vue&type=template&id=3b859de7&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/partials/singleAd.vue?vue&type=template&id=3b859de7& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.ad
    ? _c("div", { staticClass: "ad-list-item" }, [
        _c("div", { staticClass: "ad-list-item-main-container" }, [
          _c(
            "div",
            { staticClass: "ad-item-image" },
            [
              _c(
                "router-link",
                {
                  staticClass: "ad-title text-limit",
                  attrs: { to: "/product/" + _vm.ad.id + "/view" }
                },
                [
                  JSON.parse(_vm.ad.images) && JSON.parse(_vm.ad.images).length
                    ? _c("img", {
                        attrs: {
                          src:
                            _vm.$root.virtualmin_server +
                            "/public/images/" +
                            JSON.parse(_vm.ad.images)[0].image
                        }
                      })
                    : _c("img", {
                        attrs: { src: "/bazar/no-image-preview.png" }
                      }),
                  _vm._v(" "),
                  _c("div", { staticClass: "info-icons" }, [
                    _c("div", { staticClass: "camera-icon single-icon" }, [
                      _c("i", { staticClass: "fas fa-camera" }),
                      _vm._v(
                        " " +
                          _vm._s(
                            JSON.parse(_vm.ad.images) &&
                              JSON.parse(_vm.ad.images).length
                              ? JSON.parse(_vm.ad.images).length
                              : 0
                          ) +
                          "  "
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "view-icon single-icon" }, [
                      _c("i", { staticClass: "fas fa-eye" }),
                      _vm._v(" " + _vm._s(_vm.ad.views_count) + " ")
                    ])
                  ])
                ]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "ad-info" },
            [
              _c(
                "router-link",
                {
                  staticClass: "ad-title text-limit",
                  attrs: { to: "/product/" + _vm.ad.id + "/view" }
                },
                [
                  _vm._v(
                    "\r\n                " +
                      _vm._s(_vm.ad.ad_title) +
                      "\r\n            "
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "ad-meta-info" }, [
                _c("div", { staticClass: "ad-date" }, [
                  _c("i", { staticClass: "fas fa-calendar" }),
                  _vm._v(
                    " " +
                      _vm._s(
                        _vm._f("formatDifferenceDate")(_vm.ad.created_at)
                      ) +
                      "\r\n                "
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "ad-location" }, [
                  _c("i", { staticClass: "fa fa-map-marker-alt " }),
                  _vm._v(" " + _vm._s(_vm.ad.city) + "\r\n                ")
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "ad-bottom-row" }, [
                _c(
                  "div",
                  {
                    staticClass: "ad-price-type cursor-pointer",
                    on: {
                      click: function($event) {
                        return _vm.$root.gotoPage(
                          "/product/" + _vm.ad.id + "/view"
                        )
                      }
                    }
                  },
                  [
                    _vm.ad.price_type == "price"
                      ? _c("span", [
                          _vm._v(
                            " " +
                              _vm._s(_vm.$store.getters.getCuerrency) +
                              _vm._s(_vm.ad.price) +
                              " "
                          )
                        ])
                      : _c("span", { staticClass: "text-capitalize" }, [
                          _vm._v(_vm._s(_vm.ad.price_type))
                        ])
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "ad-actions desktop-only-f" }, [
                  _c(
                    "a",
                    {
                      staticClass: "call-btn ad-action-btn",
                      attrs: { href: "javascript:;" },
                      on: {
                        click: function($event) {
                          _vm.show_number = !_vm.show_number
                        }
                      }
                    },
                    [
                      _c("i", { staticClass: "far fa-phone-alt " }),
                      _vm._v(" "),
                      _vm.show_number
                        ? _c("span", [
                            _vm._v(" " + _vm._s(_vm.ad.mobile) + " ")
                          ])
                        : _c("span", [_vm._v("View Number")])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass: "chat-btn ad-action-btn",
                      attrs: { href: "javascript:;" },
                      on: { click: _vm.startChat }
                    },
                    [
                      _c("i", { staticClass: "far fa-comment-alt-lines" }),
                      _vm._v(
                        "\r\n                        Chat\r\n                    "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      staticClass: "favorite-btn ad-action-btn no-border",
                      attrs: { href: "javascript:;" },
                      on: { click: _vm.favoriteAdModalFunc }
                    },
                    [
                      _vm.fav_loading
                        ? _c("loader", { staticClass: "bz-text-red" })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("i", {
                        staticClass: "fa-heart",
                        class: {
                          fas: _vm.ad.fav_stattus == "Yes",
                          far: _vm.ad.fav_stattus != "Yes"
                        }
                      }),
                      _vm._v(
                        "\r\n                        Favorite\r\n                    "
                      )
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "ad-actions mobile-only-f" },
          [
            _c(
              "a",
              {
                staticClass: "call-btn ad-action-btn",
                attrs: { href: "javascript:;" },
                on: {
                  click: function($event) {
                    _vm.show_number = !_vm.show_number
                  }
                }
              },
              [
                _c("i", { staticClass: "far fa-phone-alt" }),
                _vm._v(" "),
                _vm.show_number
                  ? _c("span", [_vm._v(" " + _vm._s(_vm.ad.mobile) + " ")])
                  : _c("span", [_vm._v("View Number")])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              { staticClass: "chat-btn ad-action-btn", attrs: { to: "/chat" } },
              [
                _c("i", { staticClass: "far fa-comment-alt-lines" }),
                _vm._v("\r\n            Chat\r\n        ")
              ]
            ),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "favorite-btn ad-action-btn no-border",
                attrs: { href: "javascript:;" },
                on: { click: _vm.favoriteAdModalFunc }
              },
              [
                _vm.fav_loading
                  ? _c("loader", { staticClass: "bz-text-red" })
                  : _vm._e(),
                _vm._v(" "),
                _c("i", {
                  staticClass: "fa-heart",
                  class: {
                    fas: _vm.ad.fav_stattus == "Yes",
                    far: _vm.ad.fav_stattus != "Yes"
                  }
                }),
                _vm._v("\r\n            Favorite\r\n        ")
              ],
              1
            )
          ],
          1
        )
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/partials/singleAd.vue":
/*!***********************************************************!*\
  !*** ./resources/js/user/dashboard/partials/singleAd.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _singleAd_vue_vue_type_template_id_3b859de7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleAd.vue?vue&type=template&id=3b859de7& */ "./resources/js/user/dashboard/partials/singleAd.vue?vue&type=template&id=3b859de7&");
/* harmony import */ var _singleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./singleAd.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/partials/singleAd.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _singleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _singleAd_vue_vue_type_template_id_3b859de7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _singleAd_vue_vue_type_template_id_3b859de7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/partials/singleAd.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/partials/singleAd.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/singleAd.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_singleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./singleAd.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/singleAd.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_singleAd_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/partials/singleAd.vue?vue&type=template&id=3b859de7&":
/*!******************************************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/singleAd.vue?vue&type=template&id=3b859de7& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_singleAd_vue_vue_type_template_id_3b859de7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./singleAd.vue?vue&type=template&id=3b859de7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/singleAd.vue?vue&type=template&id=3b859de7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_singleAd_vue_vue_type_template_id_3b859de7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_singleAd_vue_vue_type_template_id_3b859de7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);