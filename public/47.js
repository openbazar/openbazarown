(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[47],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    console.log('Left Dashboard Created');
    this.$root.$refs.leftDashboard = this;
    this.getProfileStats(); // if(!this.$store.getters.getUserStats){
    //     this.getProfileStats();
    // }
  },
  data: function data() {
    return {
      user_info: '',
      loading: false,
      rating: 3
    };
  },
  methods: {
    getProfileStats: function getProfileStats() {
      var _this = this;

      this.loading = true;
      axios.post('/v1/myprofile', {
        user_id: this.$store.getters.isAuthenticated.id
      }, {
        headers: {
          Authorization: "Bearer " + this.$store.getters.isAuthenticated.token
        }
      }).then(function (res) {
        console.log(res.data);
        _this.loading = false;

        if (res.data.status == 'success') {
          _this.user_info = res.data.data;

          _this.$store.commit('statsUpdate', res.data.data);
        } else {
          console.log(res);

          _this.$toastr.error(res.data.message);
        }
      })["catch"](function (err) {
        _this.loading = false;

        _this.$toastr.error('An unexpected error occurred');
      });
    } // loadUserInfo(){
    //     axios.post('/v1/user_details', {
    //         id: this.$store.getters.isAuthenticated.id,
    //     },{
    //         headers:{
    //             Authorization: "Bearer "+ this.$store.getters.isAuthenticated.token
    //         }
    //     }).then (res => {
    //     console.log(res.data)
    //     if(res.data.status ==  'success'){
    //         this.user_info = res.data.user_detail
    //     }else{
    //         console.log(res)
    //         this.$toastr.error( res.data.message );
    //     }
    //     }).catch( err => {
    //         this.$toastr.error('An unexpected error occurred');
    //     })
    // },

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=template&id=06daf72a&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=template&id=06daf72a& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "bz-white-box py-0" },
    [
      _vm.loading
        ? _c("loader", { staticClass: "bz-text-red", attrs: { small: false } })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "user-info" },
        [
          _c("div", { staticClass: "profile-image" }, [
            _c("div", { staticClass: "small-text" }, [_vm._v(" My Profile ")]),
            _vm._v(" "),
            _vm.$store.getters.isAuthenticated &&
            _vm.$store.getters.isAuthenticated.profile_image
              ? _c("img", {
                  attrs: {
                    src:
                      _vm.$root.virtualmin_server +
                      "/public/images/" +
                      _vm.$store.getters.isAuthenticated.profile_image
                  }
                })
              : _c("img", {
                  attrs: { src: "/bazar/dashboard-icons/userimage.png" }
                })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "user-name" }, [
            _vm._v(_vm._s(_vm.$store.getters.isAuthenticated.name))
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "member-date" }, [
            _c("i", {
              staticClass: "fa fa-calendar-minus-o",
              attrs: { "aria-hidden": "true" }
            }),
            _vm._v(
              "\n                        Member since " +
                _vm._s(
                  _vm._f("formatedDateWithDay")(
                    _vm.$store.getters.isAuthenticated.created_at
                  )
                ) +
                " "
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "user-social-media-accounts" }, [
            _vm.$store.getters.isAuthenticated.email
              ? _c("a", { attrs: { href: "#", title: _vm.user_info.email } }, [
                  _c("img", {
                    staticClass: "user-social-media-account",
                    attrs: { src: "/bazar/view-product-email.png" }
                  })
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.$store.getters.getUserStats.verify_phone
              ? _c(
                  "a",
                  { attrs: { href: "#", title: _vm.user_info.phone_no } },
                  [
                    _c("img", {
                      staticClass: "user-social-media-account",
                      attrs: { src: "/bazar/view-product-mobile.png" }
                    })
                  ]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.$store.getters.getUserStats.verify_facebook == "true"
              ? _c(
                  "a",
                  {
                    attrs: { href: "#", title: _vm.user_info.facebook_details }
                  },
                  [
                    _c("img", {
                      staticClass: "user-social-media-account",
                      attrs: { src: "/bazar/view-product.facebook.png" }
                    })
                  ]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.$store.getters.getUserStats.verify_google == "true"
              ? _c(
                  "a",
                  { attrs: { href: "#", title: _vm.user_info.google_details } },
                  [
                    _c("img", {
                      staticClass: "user-social-media-account",
                      attrs: { src: "/bazar/view-product-googleplus.png" }
                    })
                  ]
                )
              : _vm._e()
          ]),
          _vm._v(" "),
          _vm.$store.getters.isAuthenticated
            ? _c("show-ratings", {
                attrs: { user: _vm.$store.getters.isAuthenticated }
              })
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "user-stats" }, [
            _c(
              "div",
              { staticClass: "user-stats-box ml-0" },
              [
                _c("router-link", { attrs: { to: "/dashboard/my-ads" } }, [
                  _c("div", [
                    _vm._v(_vm._s(_vm.$store.getters.getUserStats.user_adcount))
                  ]),
                  _vm._v(" "),
                  _c("div", [_vm._v("My Ads")])
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "user-stats-box" },
              [
                _c("router-link", { attrs: { to: "/dashboard/followings" } }, [
                  _c("div", [
                    _vm._v(_vm._s(_vm.$store.getters.getUserStats.following))
                  ]),
                  _vm._v(" "),
                  _c("div", [_vm._v("Following")])
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "user-stats-box no-border" },
              [
                _c("router-link", { attrs: { to: "/dashboard/followers" } }, [
                  _c("div", [
                    _vm._v(_vm._s(_vm.$store.getters.getUserStats.followers))
                  ]),
                  _vm._v(" "),
                  _c("div", [_vm._v("Followers")])
                ])
              ],
              1
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "dashboard-menu" }, [
        _c(
          "div",
          { staticClass: "menu-list" },
          [
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/my-profile" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/man.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                Edit Profile\n                            "
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/my-wallet" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/wallet.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                My Wallet\n                            "
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/my-coins" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/coins.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                My Coins\n                            "
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/my-account" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/king.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                Account\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(_vm.$store.getters.getUserStats.plan) +
                        "\n                                "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/recent-views" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/eye.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                Recently Viewed\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(_vm.$store.getters.getUserStats.view_count) +
                        "\n                                "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/favorite-ads" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/heart.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                Favorite Ads\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(_vm.$store.getters.getUserStats.fav_count) +
                        "\n                                "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              { staticClass: "menu-item", attrs: { to: "/dashboard/my-ads" } },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/marketing.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                My Ads\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(_vm.$store.getters.getUserStats.user_adcount) +
                        "\n                                "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/my-offer-ads" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/cash.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                My Offers Ads\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(_vm.$store.getters.getUserStats.bidcount) +
                        "\n                                "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/following-ads" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/advertising.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                Followings Ads\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(
                          _vm.$store.getters.getUserStats.following_adcount
                        ) +
                        "\n                                "
                    )
                  ])
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item",
                attrs: { to: "/dashboard/blocked-users" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/blocked-users.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name" }, [
                  _vm._v(
                    "\n                                Blocked Users\n                                "
                  ),
                  _c("div", { staticClass: "menu-notification-number" })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "menu-item ",
                attrs: { to: "/dashboard/invite-freinds" }
              },
              [
                _c("img", {
                  staticClass: "menu-item-icon",
                  attrs: { src: "/bazar/dashboard-icons/users.png" }
                }),
                _vm._v(" "),
                _c("div", { staticClass: "menu-name no-border" }, [
                  _vm._v(
                    "\n                                Invite your freinds\n                            "
                  )
                ])
              ]
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/user/dashboard/partials/left-dashboard.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/left-dashboard.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _left_dashboard_vue_vue_type_template_id_06daf72a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./left-dashboard.vue?vue&type=template&id=06daf72a& */ "./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=template&id=06daf72a&");
/* harmony import */ var _left_dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./left-dashboard.vue?vue&type=script&lang=js& */ "./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _left_dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _left_dashboard_vue_vue_type_template_id_06daf72a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _left_dashboard_vue_vue_type_template_id_06daf72a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/user/dashboard/partials/left-dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_left_dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./left-dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_left_dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=template&id=06daf72a&":
/*!************************************************************************************************!*\
  !*** ./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=template&id=06daf72a& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_left_dashboard_vue_vue_type_template_id_06daf72a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./left-dashboard.vue?vue&type=template&id=06daf72a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/user/dashboard/partials/left-dashboard.vue?vue&type=template&id=06daf72a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_left_dashboard_vue_vue_type_template_id_06daf72a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_left_dashboard_vue_vue_type_template_id_06daf72a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);