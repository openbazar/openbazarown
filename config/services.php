<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '2276072989-kg3pge0p4ogvq5pkmpfn27ikv7398u9c.apps.googleusercontent.com',
        'client_secret' => 'qpkq_9NIkQdHDIO73Li8mpB_',
        'redirect' => '/login-google/callback',
    ],

    'facebook' => [
        // CV Builder
        // 'client_id' => '207242407362508',
        // 'client_secret' => 'a7fbc0375f460ff367fb725ea29c102d',

        // // Openbazar Personal
        // 'client_id' => '267506645165771',
        // 'client_secret' => 'fff13cd39bb48d473f24a5632eb24a16',

        // Openbazar Official
        'client_id' => '518804081949521',
        'client_secret' => '70b4d1831b0769846fe6a5e63ca91bef',
        'redirect' => '/login-facebook/callback',
    ],

];
