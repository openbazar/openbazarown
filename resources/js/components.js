window.Vue = require('vue');


Vue.component('example-component', () =>
    import ('./components/ExampleComponent.vue'));
Vue.component('slider-component', () =>
    import ('./components/vue-slider.vue'));
Vue.component('auth-components', () =>
    import ('./auth/auth.vue'));
Vue.component('login-model', () =>
    import ('./components/models/login-model'));

Vue.component('login-component', () =>
    import ('./auth/login.vue'));
Vue.component('singup-component', () =>
    import ('./auth/signup.vue'));

// categories-section
Vue.component('categories-section', () =>
    import ('./components/post-ad/CategoriesSection.vue'));



// Vue.component('audio-player', () =>
//     import ('./components/audio.vue'));

// Modals
Vue.component('boost-post', () =>
    import ('./components/view-products/boost-post.vue'));
Vue.component('upgrade-packages', () =>
    import ('./components/my-account/upgrade-packages.vue'));
Vue.component('credit-package', () =>
    import ('./components/my-wallet/buy-credit-new.vue'));
// Vue.component('credit-package', () => import('./components/my-wallet/buy-credit.vue'));




Vue.component('single-product', () =>
    import ('./products/singleProduct.vue'));
Vue.component('loading-single-product', () =>
    import ('./products/loadingSingleProduct.vue'));


Vue.component('sidebar-filters-new', () =>
    import ('./products/sidebar-filters-new.vue'));
Vue.component('modal-filters', () =>
    import ('./products/modal-filters.vue'));

Vue.component('ad-post-gallery-section', () =>
    import ('./components/post-ad/gallery-section.vue'));
Vue.component('product-attributes', () =>
    import ('./components/post-ad/product-attributes.vue'));



Vue.component('product-gallery', () =>
    import ('./products/partials/view-product-gallery.vue'));






// dashboard
Vue.component('single-ad', () =>
    import ('./user/dashboard/partials/singleAd.vue'));
Vue.component('loading-single-ad', () =>
    import ('./user/dashboard/partials/loadingSingleAd.vue'));
Vue.component('mobile-menu', () =>
    import ('./partials/mobile-menu.vue'));
Vue.component('left-menu', () =>
    import ('./user/other_user/partials/left-menu.vue'));
Vue.component('no-data-box', () =>
    import ('./user/dashboard/partials/no-data-box.vue'));
Vue.component('bz-footer', () =>
    import ('./partials/footer.vue'));








// loader
Vue.component('loader', () =>
    import ('./partials/loader.vue'));
Vue.component('simple-loader', () =>
    import ('./partials/simple-loader.vue'));








// dashboard
Vue.component('left-dashboard', () =>
    import ('./user/dashboard/partials/left-dashboard.vue'));
Vue.component('show-ratings', () =>
    import ('./user/dashboard/partials/show-ratings.vue'));

// navbar
Vue.component('navbar', () =>
    import ('./partials/navbar.vue'));


Vue.component('main-wrapper', () =>
    import ('./main-wrapper.vue'));
