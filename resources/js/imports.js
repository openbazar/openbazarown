window.Vue = require('vue');


import VueSocialSharing from 'vue-social-sharing'
Vue.use(VueSocialSharing);

// import Vuex from 'vuex'

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);



import { longClickDirective } from 'vue-long-click'

const longClickInstance = longClickDirective({delay: 1000, interval: 0 })
Vue.directive('longclick', longClickInstance)
// Vue.use('vue-audio' , VueAudio);




// import { slider, slideritem } from 'vue-concise-slider'


// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
window.Swal = Swal;
// import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/src/sweetalert2.scss'

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    // timerProgressBar: true,
    // didOpen: (toast) => {
    //   toast.addEventListener('mouseenter', Swal.stopTimer)
    //   toast.addEventListener('mouseleave', Swal.resumeTimer)
    // }
})
window.Toast = Toast;


import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
import 'vue-select/dist/vue-select.css';


import { BSpinner, BSkeletonWrapper, BSkeleton, BCard, BSkeletonImg, BOverlay, BProgress } from 'bootstrap-vue'
Vue.component('BSpinner', BSpinner)
Vue.component('BSkeleton', BSkeleton)
Vue.component('BCard', BCard)
Vue.component('BSkeletonWrapper', BSkeletonWrapper)
Vue.component('BSkeletonImg', BSkeletonImg)
Vue.component('b-overlay', BOverlay)
Vue.component('b-progress', BProgress)


import VueQuillEditor from 'vue-quill-editor'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

// var toolbarOptions = ['bold', 'italic', 'underline', 'strike'];
var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'], // toggled buttons
    ['blockquote', 'code-block'],
    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [{ 'align': [] }],
];

Vue.use(VueQuillEditor, {
    modules: {
        toolbar: toolbarOptions
    }
})


import Intersect from 'vue-intersect'
Vue.component('Intersect', Intersect)

// import Multiselect from 'vue-multiselect'
// // register globally
// Vue.component('multiselect', Multiselect)



import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin

Vue.use(Loading, {
    // props
    color: '#d11d1d'
})


import VueEllipseProgress from 'vue-ellipse-progress';

Vue.use(VueEllipseProgress);


// import VueSimpleRangeSlider from 'vue-simple-range-slider';
// import 'vue-simple-range-slider/dist/vueSimpleRangeSlider.css'


// import VueSplide from '@splidejs/vue-splide';

// Vue.use( VueSplide );


// import '@splidejs/splide/dist/css/themes/splide-default.min.css';
// or
// import '@splidejs/splide/dist/css/themes/splide-sea-green.min.css';
// // or
// import '@splidejs/splide/dist/css/themes/splide-skyblue.min.css';
// import 'vue-range-component/dist/vue-range-slider.css'
// import VueRangeSlider from 'vue-range-component'
// Vue.component(VueRangeSlider)

// import VueSlider from 'vue-slider-component'
// import 'vue-slider-component/theme/default.css'

// Vue.component('VueSlider', VueSlider)



import StarRating from 'vue-star-rating'
Vue.component('star-rating', StarRating);


// import { Swiper, SwiperSlide } from 'swiper/vue';
// Vue.component(Swiper)
// Vue.component(SwiperSlide)

// import 'swiper/swiper.scss';


// import VueChatScroll from 'vue-chat-scroll'
// Vue.use(VueChatScroll)

import * as VueGoogleMaps from 'vue2-google-maps'
// import {gmapApi } from 'vue2-google-maps'
// window.gmapApi = gmapApi
// import GmapCluster from 'vue2-google-maps/dist/components/cluster' // replace src with dist if you have Babel issues

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyC62I6UvDILIJa8ERvltfnsJqAHq7h6-TM',
        // key: 'AIzaSyDaDKp-80cWcDxFGRVeXl_I9xkICK1O7O8',
        // key: 'AIzaSyDgwGG8qXO_mA60W94j_SyFYA87ktrrF7Y',
        // key: 'AIzaSyC4ZAcJ4piMycXN_RluyRcQlvEh0nhNC_0',
        // key: 'AIzaSyDY917S7EjVumokKe0VIDkz_Rp7fOKAXpQ',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
})


import VueClipboard from 'vue-clipboard2'
VueClipboard.config.autoSetContainer = true // add this line
Vue.use(VueClipboard)
