
export default {
    state: {
        authUser: "",
        country: "",
        stats: "",
        conversionRate: 0,
        paymentPlan: '',
        currentUrl: '',
        notification_count: 0,
        view: 'card',
    },
    mutations: {
        setCountry(state , payload) {
            state.country = payload;
        },
        removeCountry(state) {
            state.country = '';
        },
        decrementNotifcation(state) {
            state.notification_count = state.notification_count - 1;
        },
        login(state , payload) {
            state.authUser = payload;
            //   console.log(state , payload , 'Store')
        },
        profileUpdate(state , payload) {
            // console.log(payload)
            state.authUser.name = payload.name;
            state.authUser.profile_image = payload.profile_image;
            //   state.authUser.token = '';
        },
        logout(state) {
            state.authUser = '';
            state.country = '';
            state.stats = '';
            state.notification_count = 0;

            //   state.authUser =  "";
            //   state.country =  "";
            //   state.stats =  "";
            //   state.conversionRate =  1;
            //   state.paymentPlan =  '';
            //   state.notification_count = 0 ;

            //   console.log(state , payload , 'Store')
        },

        statsUpdate(state , payload) {
            state.stats = payload;
            //   console.log(state , payload , 'Store')
        },

        setConversionRate(state , payload) {
            state.conversionRate = payload._value;
            //   console.log(state , payload , 'Store')
        },
        setPaymentPlan(state , payload) {
            state.paymentPlan = payload;
            //   console.log(state , payload , 'Store')
        },
        setNotificationCount(state , payload) {
            state.notification_count = payload;
        },
        setCurrentUrl(state , payload) {
            state.currentUrl = payload;
        },
        setViewOption(state , payload) {
            state.view = payload;
        },
        resetState(state){
            state.authUser =  "";
            state.country =  "";
            state.stats =  "";
            state.conversionRate =  1;
            state.paymentPlan =  '';
            state.notification_count = 0 ;

        },
    },
    getters: {
        getViewOption(state) {
            return state.view;
        },
        isAuthenticated(state){
            if(state.authUser){
                return state.authUser;
            }else{
                return false
            }
        },
        currentCountry(state){
            if(state.country){
                return state.country;
            }else{
                return false
            }
        },
        getUserStats(state){
            if(state.stats){
                return state.stats;
            }else{
                return false
            }
        },
        getCuerrency(state){
            if(state.country && state.country.id == 4){
                return '₹';
            }else{
                return '€'
            }
        },
        getActualURL(state){
            if(state.currentUrl){
                return state.currentUrl
            }
        },
        conversionRate(state){

            if(state.country && state.country.id == 4){
                return state.conversionRate;
            }else{
                return 1
            }
        },
        currencyConversion(state , euros){

            if(state.country && state.country.id == 4){
                return state.conversionRate * euros;
            }else{
                return 1 * euros;
            }
        },
        getPaymentPlan(state){
            return state.paymentPlan;
        },
        getCuerrencyCode(state){
            if(state.country && state.country.id == 4){
                return 'INR'
            }else{
                return 'EUR';
            }
        },
        getNotificationCount(state) {
            return state.notification_count;
        },

        getAllCities(state){

            let all_cities = [
                {"id":"562","city_name":"All Cities\n","country_id":"1"},
                {"id":"563","city_name":"All Cities\n","country_id":"2"},
                {"id":"564","city_name":"All Cities\n","country_id":"3"},
                {"id":"565","city_name":"All Cities\n","country_id":"4"},
                {"id":"0","city_name":"All Cities","country_id":"5"},
                {"id":"566","city_name":"All Cities\n","country_id":"6"},
                {"id":"567","city_name":"All Cities\n","country_id":"7"},

                {"id":"1","city_name":"Alkmaar","country_id":"5"},
                {"id":"2","city_name":"Almelo","country_id":"5"},
                {"id":"3","city_name":"Almere","country_id":"5"},
                {"id":"4","city_name":"Alphen aan den Rijn","country_id":"5"},
                {"id":"5","city_name":"Amersfoort","country_id":"5"},
                {"id":"6","city_name":"Amstelveen","country_id":"5"},
                {"id":"7","city_name":"Amsterdam","country_id":"5"},
                {"id":"8","city_name":"Apeldoorn","country_id":"5"},
                {"id":"9","city_name":"Appingedam","country_id":"5"},
                {"id":"10","city_name":"Arnemuiden","country_id":"5"},
                {"id":"11","city_name":"Arnhem","country_id":"5"},
                {"id":"12","city_name":"Assen","country_id":"5"},
                {"id":"13","city_name":"Baarn","country_id":"5"},
                {"id":"14","city_name":"Bergen op Zoom","country_id":"5"},
                {"id":"15","city_name":"Biddinghuizen","country_id":"5"},
                {"id":"16","city_name":"Blokzijl","country_id":"5"},
                {"id":"17","city_name":"Bolsward","country_id":"5"},
                {"id":"18","city_name":"Boxtel","country_id":"5"},
                {"id":"19","city_name":"Breda","country_id":"5"},
                {"id":"20","city_name":"Bredevoort","country_id":"5"},
                {"id":"21","city_name":"Bunschoten","country_id":"5"},
                {"id":"22","city_name":"Buren","country_id":"5"},
                {"id":"23","city_name":"Coevorden","country_id":"5"},
                {"id":"24","city_name":"Culemborg","country_id":"5"},
                {"id":"25","city_name":"Delft","country_id":"5"},
                {"id":"26","city_name":"Delfzijl","country_id":"5"},
                {"id":"27","city_name":"Den Haag","country_id":"5"},
                {"id":"28","city_name":"Den Helder","country_id":"5"},
                {"id":"29","city_name":"Deventer","country_id":"5"},
                {"id":"30","city_name":"Dieren","country_id":"5"},
                {"id":"31","city_name":"Doetinchem","country_id":"5"},
                {"id":"32","city_name":"Dokkum","country_id":"5"},
                {"id":"33","city_name":"Dordrecht","country_id":"5"},
                {"id":"34","city_name":"Echt","country_id":"5"},
                {"id":"35","city_name":"Edam","country_id":"5"},
                {"id":"36","city_name":"Ede","country_id":"5"},
                {"id":"37","city_name":"Eemnes","country_id":"5"},
                {"id":"38","city_name":"Eindhoven","country_id":"5"},
                {"id":"39","city_name":"Emmeloord","country_id":"5"},
                {"id":"40","city_name":"Emmen","country_id":"5"},
                {"id":"41","city_name":"Enkhuizen","country_id":"5"},
                {"id":"42","city_name":"Enschede","country_id":"5"},
                {"id":"43","city_name":"Enspijk","country_id":"5"},
                {"id":"44","city_name":"Franeker","country_id":"5"},
                {"id":"45","city_name":"Geertruidenberg","country_id":"5"},
                {"id":"46","city_name":"Geldrop","country_id":"5"},
                {"id":"47","city_name":"Geleen","country_id":"5"},
                {"id":"48","city_name":"Gendt","country_id":"5"},
                {"id":"49","city_name":"Genemuiden","country_id":"5"},
                {"id":"50","city_name":"Gennep","country_id":"5"},
                {"id":"51","city_name":"Goes","country_id":"5"},
                {"id":"52","city_name":"Gorinchem","country_id":"5"},
                {"id":"53","city_name":"Gouda","country_id":"5"},
                {"id":"54","city_name":"Grave","country_id":"5"},
                {"id":"55","city_name":"Groenlo","country_id":"5"},
                {"id":"56","city_name":"Groningen","country_id":"5"},
                {"id":"57","city_name":"Haarlem","country_id":"5"},
                {"id":"58","city_name":"Hagestein","country_id":"5"},
                {"id":"59","city_name":"Harderwijk","country_id":"5"},
                {"id":"60","city_name":"Harlingen","country_id":"5"},
                {"id":"61","city_name":"Hasselt","country_id":"5"},
                {"id":"62","city_name":"Hattem","country_id":"5"},
                {"id":"63","city_name":"Heerhugowaard","country_id":"5"},
                {"id":"64","city_name":"Heerlen","country_id":"5"},
                {"id":"65","city_name":"Helmond","country_id":"5"},
                {"id":"66","city_name":"Hengelo","country_id":"5"},
                {"id":"67","city_name":"Heukelum","country_id":"5"},
                {"id":"68","city_name":"Heusden","country_id":"5"},
                {"id":"69","city_name":"Hilversum","country_id":"5"},
                {"id":"70","city_name":"Hindeloopen","country_id":"5"},
                {"id":"71","city_name":"Hoofddorp","country_id":"5"},
                {"id":"72","city_name":"Hoogeveen","country_id":"5"},
                {"id":"73","city_name":"Hoogezand-Sappemeer","country_id":"5"},
                {"id":"74","city_name":"Hoorn","country_id":"5"},
                {"id":"75","city_name":"Houten","country_id":"5"},
                {"id":"76","city_name":"Huissen","country_id":"5"},
                {"id":"77","city_name":"Hulst","country_id":"5"},
                {"id":"78","city_name":"IJlst","country_id":"5"},
                {"id":"79","city_name":"IJsselstein","country_id":"5"},
                {"id":"80","city_name":"Kampen","country_id":"5"},
                {"id":"81","city_name":"Kerkrade","country_id":"5"},
                {"id":"82","city_name":"Kessel","country_id":"5"},
                {"id":"83","city_name":"Klundert","country_id":"5"},
                {"id":"84","city_name":"Landgraaf","country_id":"5"},
                {"id":"85","city_name":"Laren","country_id":"5"},
                {"id":"86","city_name":"Leerdam","country_id":"5"},
                {"id":"87","city_name":"Leeuwarden","country_id":"5"},
                {"id":"88","city_name":"Leiden","country_id":"5"},
                {"id":"89","city_name":"Lelystad","country_id":"5"},
                {"id":"90","city_name":"Maastricht","country_id":"5"},
                {"id":"91","city_name":"Medemblik","country_id":"5"},
                {"id":"92","city_name":"Meppel","country_id":"5"},
                {"id":"93","city_name":"Middelburg","country_id":"5"},
                {"id":"94","city_name":"Monnickendam","country_id":"5"},
                {"id":"95","city_name":"Montfoort","country_id":"5"},
                {"id":"96","city_name":"Montfort","country_id":"5"},
                {"id":"97","city_name":"Muiden","country_id":"5"},
                {"id":"98","city_name":"Naarden","country_id":"5"},
                {"id":"99","city_name":"Nieuwegein","country_id":"5"},
                {"id":"100","city_name":"Nieuwstadt","country_id":"5"},
                {"id":"101","city_name":"Nijkerk","country_id":"5"},
                {"id":"102","city_name":"Nijmegen","country_id":"5"},
                {"id":"103","city_name":"Oldenzaal","country_id":"5"},
                {"id":"104","city_name":"Oosterhout","country_id":"5"},
                {"id":"105","city_name":"Oss","country_id":"5"},
                {"id":"106","city_name":"Oudewater","country_id":"5"},
                {"id":"107","city_name":"Purmerend","country_id":"5"},
                {"id":"108","city_name":"Ravenstein","country_id":"5"},
                {"id":"109","city_name":"Rhenen","country_id":"5"},
                {"id":"110","city_name":"Roermond","country_id":"5"},
                {"id":"111","city_name":"Roosendaal","country_id":"5"},
                {"id":"112","city_name":"Rotterdam","country_id":"5"},
                {"id":"113","city_name":"Schagen","country_id":"5"},
                {"id":"114","city_name":"Schin op Geul","country_id":"5"},
                {"id":"115","city_name":"s-Hertogenbosch (Den Bosch)","country_id":"5"},
                {"id":"116","city_name":"Sint-Oedenrode","country_id":"5"},
                {"id":"117","city_name":"Sittard","country_id":"5"},
                {"id":"118","city_name":"Sloten","country_id":"5"},
                {"id":"119","city_name":"Sluis","country_id":"5"},
                {"id":"120","city_name":"Sneek","country_id":"5"},
                {"id":"121","city_name":"Spijkenisse","country_id":"5"},
                {"id":"122","city_name":"Stadskanaal","country_id":"5"},
                {"id":"123","city_name":"Stavoren","country_id":"5"},
                {"id":"124","city_name":"Steenwijk","country_id":"5"},
                {"id":"125","city_name":"Stein","country_id":"5"},
                {"id":"126","city_name":"Susteren","country_id":"5"},
                {"id":"127","city_name":"Terneuzen","country_id":"5"},
                {"id":"128","city_name":"Thorn","country_id":"5"},
                {"id":"129","city_name":"Tiel","country_id":"5"},
                {"id":"130","city_name":"Tilburg","country_id":"5"},
                {"id":"131","city_name":"Utrecht","country_id":"5"},
                {"id":"132","city_name":"Valkenburg","country_id":"5"},
                {"id":"133","city_name":"Valkenswaard","country_id":"5"},
                {"id":"134","city_name":"Veendam","country_id":"5"},
                {"id":"135","city_name":"Veenendaal","country_id":"5"},
                {"id":"136","city_name":"Veere","country_id":"5"},
                {"id":"137","city_name":"Veldhoven","country_id":"5"},
                {"id":"138","city_name":"Velsen","country_id":"5"},
                {"id":"139","city_name":"Venlo","country_id":"5"},
                {"id":"140","city_name":"Vianen","country_id":"5"},
                {"id":"141","city_name":"Vlissingen","country_id":"5"},
                {"id":"142","city_name":"Volendam","country_id":"5"},
                {"id":"143","city_name":"Vollenhove","country_id":"5"},
                {"id":"144","city_name":"Waalwijk","country_id":"5"},
                {"id":"145","city_name":"Wageningen","country_id":"5"},
                {"id":"146","city_name":"Weert","country_id":"5"},
                {"id":"147","city_name":"Weesp","country_id":"5"},
                {"id":"148","city_name":"Wijchen","country_id":"5"},
                {"id":"149","city_name":"Wijk bij Duurstede","country_id":"5"},
                {"id":"150","city_name":"Willemstad","country_id":"5"},
                {"id":"151","city_name":"Winschoten","country_id":"5"},
                {"id":"152","city_name":"Winterswijk","country_id":"5"},
                {"id":"153","city_name":"Woerden","country_id":"5"},
                {"id":"154","city_name":"Workum","country_id":"5"},
                {"id":"155","city_name":"Woudrichem","country_id":"5"},
                {"id":"156","city_name":"Zaanstad","country_id":"5"},
                {"id":"157","city_name":"Zaltbommel","country_id":"5"},
                {"id":"158","city_name":"Zeist","country_id":"5"},
                {"id":"159","city_name":"Zevenaar","country_id":"5"},
                {"id":"160","city_name":"Zierikzee","country_id":"5"},
                {"id":"161","city_name":"Zoetermeer","country_id":"5"},
                {"id":"162","city_name":"Zutphen","country_id":"5"},
                {"id":"163","city_name":"Zwolle","country_id":"5"},
                {"id":"164","city_name":"(Geneva) Annemasse","country_id":"2"},
                {"id":"165","city_name":"Aix-en-Provence","country_id":"2"},
                {"id":"166","city_name":"Amiens","country_id":"2"},
                {"id":"167","city_name":"Angers","country_id":"2"},
                {"id":"168","city_name":"Angoulême","country_id":"2"},
                {"id":"169","city_name":"Annecy","country_id":"2"},
                {"id":"170","city_name":"Annecy","country_id":"2"},
                {"id":"171","city_name":"Avignon","country_id":"2"},
                {"id":"172","city_name":"Bayonne","country_id":"2"},
                {"id":"173","city_name":"Besançon","country_id":"2"},
                {"id":"174","city_name":"Béthune","country_id":"2"},
                {"id":"175","city_name":"Bordeaux","country_id":"2"},
                {"id":"176","city_name":"Brest","country_id":"2"},
                {"id":"177","city_name":"Caen","country_id":"2"},
                {"id":"178","city_name":"Chambéry","country_id":"2"},
                {"id":"179","city_name":"Clermont-Ferrand","country_id":"2"},
                {"id":"180","city_name":"Dijon","country_id":"2"},
                {"id":"181","city_name":"Douai–Lens","country_id":"2"},
                {"id":"182","city_name":"Dunkerque","country_id":"2"},
                {"id":"183","city_name":"Grenoble","country_id":"2"},
                {"id":"184","city_name":"La Rochelle","country_id":"2"},
                {"id":"185","city_name":"Le Havre","country_id":"2"},
                {"id":"186","city_name":"Le Mans","country_id":"2"},
                {"id":"187","city_name":"Lille–Roubaix Tourcoing","country_id":"2"},
                {"id":"188","city_name":"Limoges","country_id":"2"},
                {"id":"189","city_name":"Lorient","country_id":"2"},
                {"id":"190","city_name":"Lyon","country_id":"2"},
                {"id":"191","city_name":"Marseille","country_id":"2"},
                {"id":"192","city_name":"Maubeuge","country_id":"2"},
                {"id":"193","city_name":"Metz","country_id":"2"},
                {"id":"194","city_name":"Montbéliard","country_id":"2"},
                {"id":"195","city_name":"Montpellier","country_id":"2"},
                {"id":"196","city_name":"Mulhouse","country_id":"2"},
                {"id":"197","city_name":"Nancy","country_id":"2"},
                {"id":"198","city_name":"Nantes","country_id":"2"},
                {"id":"199","city_name":"Nice","country_id":"2"},
                {"id":"200","city_name":"Nîmes","country_id":"2"},
                {"id":"201","city_name":"Orléans","country_id":"2"},
                {"id":"202","city_name":"Paris","country_id":"2"},
                {"id":"203","city_name":"Pau","country_id":"2"},
                {"id":"204","city_name":"Perpignan","country_id":"2"},
                {"id":"205","city_name":"Poitiers","country_id":"2"},
                {"id":"206","city_name":"Reims","country_id":"2"},
                {"id":"207","city_name":"Rennes","country_id":"2"},
                {"id":"208","city_name":"Rouen","country_id":"2"},
                {"id":"209","city_name":"Saint Etienne","country_id":"2"},
                {"id":"210","city_name":"Saint-Nazaire","country_id":"2"},
                {"id":"212","city_name":"Strasbourg","country_id":"2"},
                {"id":"213","city_name":"Thionville","country_id":"2"},
                {"id":"214","city_name":"Toulon","country_id":"2"},
                {"id":"215","city_name":"Toulouse","country_id":"2"},
                {"id":"216","city_name":"Tours","country_id":"2"},
                {"id":"217","city_name":"Troyes","country_id":"2"},
                {"id":"218","city_name":"Valence","country_id":"2"},
                {"id":"219","city_name":"Valenciennes","country_id":"2"},
                {"id":"220","city_name":"Aalst, Belgium","country_id":"1"},
                {"id":"221","city_name":"Anderlecht","country_id":"1"},
                {"id":"222","city_name":"Antwerp","country_id":"1"},
                {"id":"223","city_name":"Beringen, Belgium","country_id":"1"},
                {"id":"224","city_name":"Beveren","country_id":"1"},
                {"id":"225","city_name":"Bilzen","country_id":"1"},
                {"id":"226","city_name":"Binche","country_id":"1"},
                {"id":"227","city_name":"Braine-l'Alleud","country_id":"1"},
                {"id":"228","city_name":"Brasschaat","country_id":"1"},
                {"id":"229","city_name":"Bruges","country_id":"1"},
                {"id":"230","city_name":"Brussels","country_id":"1"},
                {"id":"231","city_name":"Charleroi","country_id":"1"},
                {"id":"232","city_name":"Châtelet, Belgium","country_id":"1"},
                {"id":"233","city_name":"Dendermonde","country_id":"1"},
                {"id":"234","city_name":"Dilbeek","country_id":"1"},
                {"id":"235","city_name":"Etterbeek","country_id":"1"},
                {"id":"236","city_name":"Evere","country_id":"1"},
                {"id":"237","city_name":"Evergem","country_id":"1"},
                {"id":"238","city_name":"Forest, Belgium","country_id":"1"},
                {"id":"239","city_name":"Geel","country_id":"1"},
                {"id":"240","city_name":"Genk","country_id":"1"},
                {"id":"241","city_name":"Geraardsbergen","country_id":"1"},
                {"id":"242","city_name":"Ghent","country_id":"1"},
                {"id":"243","city_name":"Grimbergen","country_id":"1"},
                {"id":"244","city_name":"Halle, Belgium","country_id":"1"},
                {"id":"245","city_name":"Hasselt","country_id":"1"},
                {"id":"246","city_name":"Heist-op-den-Berg","country_id":"1"},
                {"id":"247","city_name":"Herstal","country_id":"1"},
                {"id":"248","city_name":"Heusden-Zolder","country_id":"1"},
                {"id":"249","city_name":"Houthalen-Helchteren","country_id":"1"},
                {"id":"250","city_name":"Ixelles","country_id":"1"},
                {"id":"251","city_name":"Jette","country_id":"1"},
                {"id":"252","city_name":"Knokke-Heist","country_id":"1"},
                {"id":"253","city_name":"Kortrijk","country_id":"1"},
                {"id":"254","city_name":"La Louvière","country_id":"1"},
                {"id":"255","city_name":"Leuven","country_id":"1"},
                {"id":"256","city_name":"Liège","country_id":"1"},
                {"id":"257","city_name":"Lier, Belgium","country_id":"1"},
                {"id":"258","city_name":"Lokeren","country_id":"1"},
                {"id":"259","city_name":"Lommel","country_id":"1"},
                {"id":"260","city_name":"Maasmechelen","country_id":"1"},
                {"id":"261","city_name":"Mechelen","country_id":"1"},
                {"id":"262","city_name":"Menen","country_id":"1"},
                {"id":"263","city_name":"Mol, Belgium","country_id":"1"},
                {"id":"264","city_name":"Mons","country_id":"1"},
                {"id":"265","city_name":"Mouscron","country_id":"1"},
                {"id":"266","city_name":"Namur (city)","country_id":"1"},
                {"id":"267","city_name":"Ninove","country_id":"1"},
                {"id":"268","city_name":"Ostend","country_id":"1"},
                {"id":"269","city_name":"Ottignies-Louvain-la-Neuve","country_id":"1"},
                {"id":"270","city_name":"Roeselare","country_id":"1"},
                {"id":"271","city_name":"Saint-Gilles, Belgium","country_id":"1"},
                {"id":"272","city_name":"Schaerbeek","country_id":"1"},
                {"id":"273","city_name":"Schoten","country_id":"1"},
                {"id":"274","city_name":"Seraing","country_id":"1"},
                {"id":"275","city_name":"Sint-Jans-Molenbeek","country_id":"1"},
                {"id":"276","city_name":"Sint-Niklaas","country_id":"1"},
                {"id":"277","city_name":"Sint-Pieters-Leeuw","country_id":"1"},
                {"id":"278","city_name":"Sint-Truiden","country_id":"1"},
                {"id":"279","city_name":"Tienen","country_id":"1"},
                {"id":"280","city_name":"Tournai","country_id":"1"},
                {"id":"281","city_name":"Turnhout","country_id":"1"},
                {"id":"282","city_name":"Uccle","country_id":"1"},
                {"id":"283","city_name":"Verviers","country_id":"1"},
                {"id":"284","city_name":"Vilvoorde","country_id":"1"},
                {"id":"285","city_name":"Waregem","country_id":"1"},
                {"id":"286","city_name":"Wavre","country_id":"1"},
                {"id":"287","city_name":"Wevelgem","country_id":"1"},
                {"id":"288","city_name":"Woluwe-Saint-Lambert","country_id":"1"},
                {"id":"289","city_name":"Woluwe-Saint-Pierre","country_id":"1"},
                {"id":"290","city_name":"Ypres","country_id":"1"},
                {"id":"291","city_name":"Aachen","country_id":"3"},
                {"id":"292","city_name":"Augsburg","country_id":"3"},
                {"id":"293","city_name":"Bergisch Gladbach","country_id":"3"},
                {"id":"294","city_name":"Berlin","country_id":"3"},
                {"id":"295","city_name":"Bielefeld","country_id":"3"},
                {"id":"296","city_name":"Bochum","country_id":"3"},
                {"id":"297","city_name":"Bonn","country_id":"3"},
                {"id":"298","city_name":"Bottrop","country_id":"3"},
                {"id":"299","city_name":"Braunschweig","country_id":"3"},
                {"id":"300","city_name":"Bremen","country_id":"3"},
                {"id":"301","city_name":"Bremerhaven","country_id":"3"},
                {"id":"302","city_name":"Chemnitz","country_id":"3"},
                {"id":"303","city_name":"Cologne (Köln)","country_id":"3"},
                {"id":"304","city_name":"Darmstadt","country_id":"3"},
                {"id":"305","city_name":"Dortmund","country_id":"3"},
                {"id":"306","city_name":"Dresden","country_id":"3"},
                {"id":"307","city_name":"Duisburg","country_id":"3"},
                {"id":"308","city_name":"Düsseldorf","country_id":"3"},
                {"id":"309","city_name":"Erfurt","country_id":"3"},
                {"id":"310","city_name":"Essen","country_id":"3"},
                {"id":"311","city_name":"Frankfurt am Main","country_id":"3"},
                {"id":"312","city_name":"Freiburg im Breisgau","country_id":"3"},
                {"id":"313","city_name":"Gelsenkirchen","country_id":"3"},
                {"id":"314","city_name":"Hagen","country_id":"3"},
                {"id":"315","city_name":"Halle (Saale)","country_id":"3"},
                {"id":"316","city_name":"Hamburg","country_id":"3"},
                {"id":"317","city_name":"Hamm","country_id":"3"},
                {"id":"318","city_name":"Hanover","country_id":"3"},
                {"id":"319","city_name":"Heidelberg","country_id":"3"},
                {"id":"320","city_name":"Herne","country_id":"3"},
                {"id":"321","city_name":"Ingolstadt","country_id":"3"},
                {"id":"322","city_name":"Karlsruhe","country_id":"3"},
                {"id":"323","city_name":"Kassel","country_id":"3"},
                {"id":"324","city_name":" Kiel","country_id":"3"},
                {"id":"325","city_name":"Koblenz","country_id":"3"},
                {"id":"326","city_name":"Krefeld","country_id":"3"},
                {"id":"327","city_name":"Leipzig","country_id":"3"},
                {"id":"328","city_name":"Leverkusen","country_id":"3"},
                {"id":"329","city_name":"Lübeck","country_id":"3"},
                {"id":"330","city_name":"Mainz","country_id":"3"},
                {"id":"331","city_name":"Mannheim","country_id":"3"},
                {"id":"332","city_name":"Mönchengladbach","country_id":"3"},
                {"id":"333","city_name":"Mülheim an der Ruhr","country_id":"3"},
                {"id":"334","city_name":"Munich","country_id":"3"},
                {"id":"335","city_name":"Münster","country_id":"3"},
                {"id":"336","city_name":"Nuremberg","country_id":"3"},
                {"id":"337","city_name":"Oberhausen","country_id":"3"},
                {"id":"338","city_name":"Offenbach am Main","country_id":"3"},
                {"id":"339","city_name":"Osnabrück","country_id":"3"},
                {"id":"340","city_name":"Paderborn","country_id":"3"},
                {"id":"341","city_name":"Potsdam","country_id":"3"},
                {"id":"342","city_name":"Regensburg","country_id":"3"},
                {"id":"343","city_name":"Rostock","country_id":"3"},
                {"id":"344","city_name":"Solingen","country_id":"3"},
                {"id":"345","city_name":"Stuttgart","country_id":"3"},
                {"id":"346","city_name":"Wiesbaden","country_id":"3"},
                {"id":"347","city_name":"Wolfsburg","country_id":"3"},
                {"id":"348","city_name":"Wuppertal","country_id":"3"},
                {"id":"349","city_name":"Erlangen","country_id":"3"},
                {"id":"350","city_name":"Fürth","country_id":"3"},
                {"id":"351","city_name":"Göttingen","country_id":"3"},
                {"id":"352","city_name":"Heilbronn","country_id":"3"},
                {"id":"353","city_name":"Hildesheim","country_id":"3"},
                {"id":"354","city_name":"Jena","country_id":"3"},
                {"id":"355","city_name":"Ludwigshafen am Rhein","country_id":"3"},
                {"id":"356","city_name":"Magdeburg","country_id":"3"},
                {"id":"357","city_name":"Moers","country_id":"3"},
                {"id":"358","city_name":"Neuss","country_id":"3"},
                {"id":"359","city_name":"Oldenburg","country_id":"3"},
                {"id":"360","city_name":"Pforzheim","country_id":"3"},
                {"id":"361","city_name":"Recklinghausen","country_id":"3"},
                {"id":"362","city_name":"Remscheid","country_id":"3"},
                {"id":"363","city_name":"Reutlingen","country_id":"3"},
                {"id":"364","city_name":"Saarbrücken","country_id":"3"},
                {"id":"365","city_name":"Salzgitter","country_id":"3"},
                {"id":"366","city_name":"Siegen","country_id":"3"},
                {"id":"367","city_name":"Trier","country_id":"3"},
                {"id":"368","city_name":"Ulm","country_id":"3"},
                {"id":"369","city_name":"Würzburg","country_id":"3"},
                {"id":"370","city_name":"Agra","country_id":"4"},
                {"id":"371","city_name":"Ahmedabad ","country_id":"4"},
                {"id":"372","city_name":"Ajmer","country_id":"4"},
                {"id":"373","city_name":"Aligarh","country_id":"4"},
                {"id":"374","city_name":"Allahabad","country_id":"4"},
                {"id":"375","city_name":"Ambattur","country_id":"4"},
                {"id":"376","city_name":"Amravati","country_id":"4"},
                {"id":"377","city_name":"Amritsar","country_id":"4"},
                {"id":"378","city_name":"Asansol","country_id":"4"},
                {"id":"379","city_name":"Aurangabad","country_id":"4"},
                {"id":"380","city_name":"Bangalore ","country_id":"4"},
                {"id":"381","city_name":"Bareilly","country_id":"4"},
                {"id":"382","city_name":"Belgaum","country_id":"4"},
                {"id":"383","city_name":"Bhavnagar","country_id":"4"},
                {"id":"384","city_name":"Bhilai Nagar","country_id":"4"},
                {"id":"385","city_name":"Bhiwandi","country_id":"4"},
                {"id":"386","city_name":"Bhopal","country_id":"4"},
                {"id":"387","city_name":"Bhubaneswar","country_id":"4"},
                {"id":"388","city_name":"Bikaner","country_id":"4"},
                {"id":"389","city_name":"Chandigarh","country_id":"4"},
                {"id":"390","city_name":"Chennai ","country_id":"4"},
                {"id":"391","city_name":"Coimbatore","country_id":"4"},
                {"id":"392","city_name":"Cuttack","country_id":"4"},
                {"id":"393","city_name":"Dehradun","country_id":"4"},
                {"id":"394","city_name":"Delhi","country_id":"4"},
                {"id":"395","city_name":"Dhanbad","country_id":"4"},
                {"id":"396","city_name":"Durgapur","country_id":"4"},
                {"id":"397","city_name":"Faridabad","country_id":"4"},
                {"id":"398","city_name":"Firozabad","country_id":"4"},
                {"id":"399","city_name":"Gaya","country_id":"4"},
                {"id":"400","city_name":"Ghaziabad","country_id":"4"},
                {"id":"401","city_name":"Gorakhpur","country_id":"4"},
                {"id":"402","city_name":"Gulbarga","country_id":"4"},
                {"id":"403","city_name":"Guntur","country_id":"4"},
                {"id":"404","city_name":"Gurgaon","country_id":"4"},
                {"id":"405","city_name":"Guwahati","country_id":"4"},
                {"id":"406","city_name":"Gwalior","country_id":"4"},
                {"id":"407","city_name":"Haora","country_id":"4"},
                {"id":"408","city_name":"Hubli and Dharwad","country_id":"4"},
                {"id":"409","city_name":"Hyderabad ","country_id":"4"},
                {"id":"410","city_name":"Indore","country_id":"4"},
                {"id":"411","city_name":"Jabalpur","country_id":"4"},
                {"id":"412","city_name":"Jaipur","country_id":"4"},
                {"id":"413","city_name":"Jalandhar","country_id":"4"},
                {"id":"414","city_name":"Jalgaon","country_id":"4"},
                {"id":"415","city_name":"Jammu","country_id":"4"},
                {"id":"416","city_name":"Jamnagar","country_id":"4"},
                {"id":"417","city_name":"Jamshedpur","country_id":"4"},
                {"id":"418","city_name":"Jhansi","country_id":"4"},
                {"id":"419","city_name":"Jodhpur","country_id":"4"},
                {"id":"420","city_name":"Kalyan & Dombivali","country_id":"4"},
                {"id":"421","city_name":"Kanpur","country_id":"4"},
                {"id":"422","city_name":"Kochi","country_id":"4"},
                {"id":"423","city_name":"Kolapur","country_id":"4"},
                {"id":"424","city_name":"Kolkata","country_id":"4"},
                {"id":"425","city_name":"Kota","country_id":"4"},
                {"id":"426","city_name":"Loni","country_id":"4"},
                {"id":"427","city_name":"Lucknow","country_id":"4"},
                {"id":"428","city_name":"Ludhiana","country_id":"4"},
                {"id":"429","city_name":"Madurai","country_id":"4"},
                {"id":"430","city_name":"Maheshtala","country_id":"4"},
                {"id":"431","city_name":"Malegoan","country_id":"4"},
                {"id":"432","city_name":"Mangalore","country_id":"4"},
                {"id":"433","city_name":"Meerut","country_id":"4"},
                {"id":"434","city_name":"Mira and Bhayander","country_id":"4"},
                {"id":"435","city_name":"Moradabad","country_id":"4"},
                {"id":"436","city_name":"Mumbai ","country_id":"4"},
                {"id":"437","city_name":"Mysore","country_id":"4"},
                {"id":"438","city_name":"Nagpur","country_id":"4"},
                {"id":"439","city_name":"Nanded Waghala","country_id":"4"},
                {"id":"440","city_name":"Nashik","country_id":"4"},
                {"id":"441","city_name":"Navi Mumbai","country_id":"4"},
                {"id":"442","city_name":"Nellore","country_id":"4"},
                {"id":"443","city_name":"Noida","country_id":"4"},
                {"id":"444","city_name":"Patna","country_id":"4"},
                {"id":"445","city_name":"Pimpri & Chinchwad","country_id":"4"},
                {"id":"446","city_name":"Pune","country_id":"4"},
                {"id":"447","city_name":"Raipur","country_id":"4"},
                {"id":"448","city_name":"Rajkot","country_id":"4"},
                {"id":"449","city_name":"Ranchi","country_id":"4"},
                {"id":"450","city_name":"Saharanpur","country_id":"4"},
                {"id":"451","city_name":"Salem","country_id":"4"},
                {"id":"452","city_name":"Sangli Miraj Kupwad","country_id":"4"},
                {"id":"453","city_name":"Siliguri","country_id":"4"},
                {"id":"454","city_name":"Solapur","country_id":"4"},
                {"id":"455","city_name":"Srinagar","country_id":"4"},
                {"id":"456","city_name":"Surat","country_id":"4"},
                {"id":"457","city_name":"Thane","country_id":"4"},
                {"id":"458","city_name":"Thiruvananthapuram","country_id":"4"},
                {"id":"459","city_name":"Tiruchirappalli","country_id":"4"},
                {"id":"460","city_name":"Tirunelveli","country_id":"4"},
                {"id":"461","city_name":"Udaipur","country_id":"4"},
                {"id":"462","city_name":"Ujjain","country_id":"4"},
                {"id":"463","city_name":"Ulhasnagar","country_id":"4"},
                {"id":"464","city_name":"Vadodara","country_id":"4"},
                {"id":"465","city_name":"Varanasi","country_id":"4"},
                {"id":"466","city_name":"Vasai Virar","country_id":"4"},
                {"id":"467","city_name":"Vijayawada","country_id":"4"},
                {"id":"468","city_name":"Visakhapatnam","country_id":"4"},
                {"id":"469","city_name":"Warangal","country_id":"4"},
                {"id":"470","city_name":"Bialystok","country_id":"6"},
                {"id":"471","city_name":"Bielsko-Biala","country_id":"6"},
                {"id":"472","city_name":"Bydgoszcz","country_id":"6"},
                {"id":"473","city_name":"Czestochowa","country_id":"6"},
                {"id":"474","city_name":"Elblag","country_id":"6"},
                {"id":"475","city_name":"Gdansk","country_id":"6"},
                {"id":"476","city_name":"Gdynia","country_id":"6"},
                {"id":"477","city_name":"Gliwice","country_id":"6"},
                {"id":"478","city_name":"Gorzow Wielkopolski","country_id":"6"},
                {"id":"479","city_name":"Grudziadz","country_id":"6"},
                {"id":"480","city_name":"Jastrzebie-Zdroj","country_id":"6"},
                {"id":"481","city_name":"Kalisz","country_id":"6"},
                {"id":"482","city_name":"Katowice","country_id":"6"},
                {"id":"483","city_name":"Kielce","country_id":"6"},
                {"id":"484","city_name":"Koszalin","country_id":"6"},
                {"id":"485","city_name":"Krakow","country_id":"6"},
                {"id":"486","city_name":"Legnica","country_id":"6"},
                {"id":"487","city_name":"Lodz","country_id":"6"},
                {"id":"488","city_name":"Lublin","country_id":"6"},
                {"id":"489","city_name":"Olsztyn","country_id":"6"},
                {"id":"490","city_name":"Opole","country_id":"6"},
                {"id":"491","city_name":"Plock","country_id":"6"},
                {"id":"492","city_name":"Poznan","country_id":"6"},
                {"id":"493","city_name":"Radom","country_id":"6"},
                {"id":"494","city_name":"Rybnik","country_id":"6"},
                {"id":"495","city_name":"Rzeszow","country_id":"6"},
                {"id":"496","city_name":"Slupsk","country_id":"6"},
                {"id":"497","city_name":"Sosnowiec","country_id":"6"},
                {"id":"498","city_name":"Szczecin","country_id":"6"},
                {"id":"499","city_name":"Tarnow","country_id":"6"},
                {"id":"500","city_name":"Torun","country_id":"6"},
                {"id":"501","city_name":"Walbrzych","country_id":"6"},
                {"id":"502","city_name":"Warszawa","country_id":"6"},
                {"id":"503","city_name":"Wloclawek","country_id":"6"},
                {"id":"504","city_name":"Wroclaw","country_id":"6"},
                {"id":"505","city_name":"Zielona Gora","country_id":"6"},
                {"id":"506","city_name":"A Coruña–Oleiros–Arteixo","country_id":"7"},
                {"id":"507","city_name":"Albacete","country_id":"7"},
                {"id":"508","city_name":"Algeciras–La Línea","country_id":"7"},
                {"id":"509","city_name":"Alicante–Elche(–Elda)","country_id":"7"},
                {"id":"510","city_name":"Almería","country_id":"7"},
                {"id":"511","city_name":"Alzira–Xàtiva","country_id":"7"},
                {"id":"512","city_name":"Badajoz","country_id":"7"},
                {"id":"513","city_name":"Barcelona","country_id":"7"},
                {"id":"514","city_name":"Benidorm","country_id":"7"},
                {"id":"515","city_name":"Bilbao","country_id":"7"},
                {"id":"516","city_name":"Blanes ","country_id":"7"},
                {"id":"517","city_name":"Burgos","country_id":"7"},
                {"id":"518","city_name":"Cádiz","country_id":"7"},
                {"id":"519","city_name":"Cartagena","country_id":"7"},
                {"id":"520","city_name":"Castellón de la Plana","country_id":"7"},
                {"id":"521","city_name":"Córdoba","country_id":"7"},
                {"id":"522","city_name":"Ferrol–Narón","country_id":"7"},
                {"id":"523","city_name":"Gandia(–Oliva)","country_id":"7"},
                {"id":"524","city_name":"Girona–Salt","country_id":"7"},
                {"id":"525","city_name":"Granada","country_id":"7"},
                {"id":"526","city_name":"Guadalajara","country_id":"7"},
                {"id":"527","city_name":"Huelva","country_id":"7"},
                {"id":"528","city_name":"Jaén–Martos","country_id":"7"},
                {"id":"529","city_name":"Jerez de la Frontera","country_id":"7"},
                {"id":"530","city_name":"La Orotava","country_id":"7"},
                {"id":"531","city_name":"Las Palmas","country_id":"7"},
                {"id":"532","city_name":"León","country_id":"7"},
                {"id":"533","city_name":"Lleida","country_id":"7"},
                {"id":"534","city_name":"Logroño","country_id":"7"},
                {"id":"535","city_name":"Lugo","country_id":"7"},
                {"id":"536","city_name":"Madrid","country_id":"7"},
                {"id":"537","city_name":"Málaga","country_id":"7"},
                {"id":"538","city_name":"Manresa","country_id":"7"},
                {"id":"539","city_name":"Marbella","country_id":"7"},
                {"id":"540","city_name":"Murcia–Orihuela","country_id":"7"},
                {"id":"541","city_name":"Ourense","country_id":"7"},
                {"id":"542","city_name":"Oviedo–Gijón–Avilés","country_id":"7"},
                {"id":"543","city_name":"Palencia","country_id":"7"},
                {"id":"544","city_name":"Palma","country_id":"7"},
                {"id":"545","city_name":"Pamplona","country_id":"7"},
                {"id":"546","city_name":"Pontevedra–Marín","country_id":"7"},
                {"id":"547","city_name":"Salamanca","country_id":"7"},
                {"id":"548","city_name":"San Sebastián","country_id":"7"},
                {"id":"549","city_name":"Santa Cruz de Tenerife–La Laguna","country_id":"7"},
                {"id":"550","city_name":"Santander–Torrelavega","country_id":"7"},
                {"id":"551","city_name":"Santiago de Compostela","country_id":"7"},
                {"id":"552","city_name":"Seville","country_id":"7"},
                {"id":"553","city_name":"Tarragona(–Reus)","country_id":"7"},
                {"id":"554","city_name":"Toledo","country_id":"7"},
                {"id":"555","city_name":"Torrelavega","country_id":"7"},
                {"id":"556","city_name":"Valencia ","country_id":"7"},
                {"id":"557","city_name":"Valladolid","country_id":"7"},
                {"id":"558","city_name":"Vic–Manlleu","country_id":"7"},
                {"id":"559","city_name":"Vigo","country_id":"7"},
                {"id":"560","city_name":"Vitoria-Gasteiz","country_id":"7"},
                {"id":"561","city_name":"Zaragoza","country_id":"7"},
            ]
            return all_cities;
        }
    },

          // getProfileStats(){
      //   console.log(this.getters)
      //   axios.post('/v1/myprofile', {
      //       user_id: this.getters.isAuthenticated.id,
      //   },{
      //       headers:{
      //           Authorization: "Bearer "+ this.getters.isAuthenticated.token
      //       }
      //   }).then (res => {

      //     console.log(res)

      //   }).catch( err => {
      //       // this.$toastr.error('An unexpected error occurred');
      //   })
      // },


  // searchQuery(state , payload) {

      //   if(payload.search){
      //     state.search.search = payload.search;
      //   }

      //   if(payload.category){
      //     state.search.category = payload.category;
      //   }

      //   if(payload.location){
      //     state.search.location = payload.location;
      //   }


      //   //   console.log(state , payload , 'Store')
      // },



  }
