<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Open Bazar</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <link rel="icon" href="{{asset('bazar/white-favicon-round.png')}}" type="image/png">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jplayer/2.9.2/skin/blue.monday/css/jplayer.blue.monday.css" integrity="sha512-hy+p5g5kWOE+l5AHCBmTo1gq1OetErrJK8u6TT2WQYUVmINNwzu9pu7oyG2f6S+qRJnbGcHu/QXpC2FV4sAvSg==" crossorigin="anonymous" />




    {{-- <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script> --}}


    {{-- <script src="https://unpkg.com/nprogress"></script> --}}

    <style>
        .open_text h1 , h2, h3 , h4, h5 , h6{
            font-weight: 700;
        }
        .modal-content{
            margin-top: 40px;
        }
        .modal-remove-icon{
            position: absolute;
            top: 5px;
            left: 13px;
            color: #a6a6a6c2;
            font-size: 20px;
            cursor: pointer;
            z-index: 30;
        }
        .user-location{
            position: relative;
        }
        .country-icon{
            position: relative;
            border-radius: 200px;
        }
        .country-icon.white-bordered{
            border: 2px solid #fafafa;
        }
        .country-icon.red-bordered{
            border: 2px solid #fafafa;
        }
        .bz-country-dropdown.vertical{
            display: flex !important;
        }
        .bz-country-dropdown{
            position: absolute;
            background-color: #ffffff;
            top: 45px;
            left: 30px;
            width: max-content;
            border-radius: 3px;
            z-index: 5;
            flex-wrap: wrap;
            max-width: 230px;
            justify-content: center;
            align-items: center;
            border: 1px solid #dfdfdf;
            display: flex;
        }
        .bz-country-dropdown .bz-dropdown-option{
            color: black;
            padding: 6px 10px;

            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            font-size: 13px;
            color: #4a687f;
        }
        .bz-country-dropdown .bz-dropdown-option:hover{
            background-color: #e9e9e9;
        }
        .bz-country-dropdown a{
            color: black;
            border-right: none;
            text-decoration: none;
            width: 75px;
        }
        .bz-country-dropdown .bz-dropdown-option img{
            width: 38px !important;
            margin-bottom: 5px;
        }
        .user-location .icon-green{
            color: #4e6a80;
        }
        .user-location .icon-white{
            color: white;
        }
        .flag-dropdown-icon{
            font-size: 13px;
            position: absolute;
            top: 13px;
            right: -18px;
        }

        .menu-dropdown img{
            width: 15px !important;
            margin-right: 2px;
            margin-top: -3px;
        }
        .custom-control-label::before{
            background-color: #dadbdd;
            border: #dadbdd solid 1px;
        }
        .custom-control-label::after{
            background-color: white !important;
        }
        .custom-control-input:focus::checked ~ .custom-control-label::before{
            border: none !important;
        }
        .custom-control-input:focus::before{
            border: none !important;
        }
        .custom-control-input:focus:not(:checked) ~ .custom-control-label::before{
            border: none !important;
        }
        .custom-control-input:focus ~ .custom-control-label::before{
            box-shadow: none !important;
        }

        .grey-footer{
            background-color: #ededed;
            margin-top: -2px;
        }

        @media screen and (max-width:768px){
            .grey-footer{
                background-color: #fafafa;
                margin-top: 0;
            }
        }


    </style>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" />

    {{--  <link rel="stylesheet" href="http://mediaelementjs.com/js/mejs-2.11.2/mediaelementplayer.min.css" />
    <link rel="stylesheet" href="http://mediaelementjs.com/js/mejs-2.11.2/mejs-skins.css" />  --}}

    {{--  <script src="{{asset('assets/OneSignal-Web-SDK-HTTPS-Integration-Files/OneSignal-Web-SDK-HTTPS-Integration-Files/OneSignalSDKUpdaterWorker.js')}}"></script>
    <script src="{{asset('assets/OneSignal-Web-SDK-HTTPS-Integration-Files/OneSignal-Web-SDK-HTTPS-Integration-Files/OneSignalSDKWorker.js')}}"></script>  --}}

    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    @yield('styles')

</head>
<body class="bazar-body">




        <div id="vue">
            <span class='mainWindow d-none'>Main Window</span>
            <span class='childWindow d-none'>Child Window</span>

        <div  class="full-wrapper" >
        {{--  @dump(session('user'));  --}}
         {{-- @dd(session('paymentResponse'))  --}}
        <main-wrapper
            @if(session('user')) :social_user="{{ json_encode(session('user'))}}" @endif
            @if(session('referred')) :referred="{{ json_encode(session('referred'))}}" @endif
            @if(session('error')) error_message="{{session('error')}}" @endif
            @if(session('paymentResponse')) :payment_response="{{ json_encode(session('paymentResponse'))}}" @endif

            ></main-wrapper>



        {{--  <div class="bazar-footer">
            <div class="bazar-footer-content">
                <div class="download-box">
                    <div class="app-download-text">
                        Download the app
                    </div>
                    <div class="qoute">
                        Whatever you’ve got in mind, we’ve got inside. OpenBazar App is what you’d want to have on your mobile phone.
                    </div>
                    <div class="download-button">
                        <a href="https://play.google.com/store/apps/details?id=com.open_bazar" target="_blank" >
                            <img class="google-download" src="{{asset('bazar/google-download-icon.png')}}" />

                        </a>

                        <a href="https://apps.apple.com/nl/app/open-bazar/id1486285030?l=en" target="_blank" >
                            <img class="apple-download" src="{{asset('bazar/apple-download-icon.png')}}" />
                        </a>
                    </div>
                </div>
                <div class="follow-us">
                    Follow Us
                </div>
                <div class="social-icons">
                    <div class="social-icon"><a href="https://www.facebook.com/OpenBAZAR4u/"><i class="fab fa-facebook-f"></i></a>  </div>
                    <div class="social-icon"><a href="https://mobile.twitter.com/BazarOpen"><i class="fab fa-twitter"></i></a>  </div>
                    <div class="social-icon"><a href="https://www.youtube.com/c/OpenBazar"><i class="fab fa-youtube"></i></a>  </div>
                    <div class="social-icon"><a href="https://www.instagram.com/openbazar4u/?hl=nl"><i class="fab fa-instagram"></i></a>  </div>
                </div>

                <div class="footer-menu">
                    <router-link to="/terms-of-use" > Terms of use </router-link>
                    <router-link to="/privacy-policy"> Privacy Policy </router-link>
                    <router-link to="/contact-us"> Contact us </router-link>
                    <router-link to="/help"> Help </router-link>
                </div>
                <div class="all-rights">
                    © 2019 OpenBazar.nl Developed And Managed By MobiSparx
                </div>

            </div>


        </div>  --}}

    </div>
    <!-- wrapper end -->
</div>




{{-- <div id="divadsensedisplaynone" style="display:none;">
    <!-- put here all adsense code -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle"
        style="display:block"
        data-ad-client="pub-6933751435930860"
        data-ad-slot="AdMob"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
</div> --}}



    {{--  <script src="https://cdn.onesignal.com/sdks/OneSignalSDKWorker.js"></script>  --}}
    <script>
        // TODO: Replace the following with your app's Firebase project configuration
        // For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
        {{--  var firebaseConfig = {
            apiKey: "AIzaSyCWaM6cWBXsKQqrwmUCMFfOcNbjRS6hAso",
            authDomain: "openbazar-366a2.firebaseapp.com",
            projectId: "openbazar-366a2",
            storageBucket: "openbazar-366a2.appspot.com",
            messagingSenderId: "546979912823",
            appId: "1:546979912823:web:59072c985542e93624fa72",
            measurementId: "G-T44KD0VMS5"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        const remoteConfig = firebase.remoteConfig();
        remoteConfig.settings.minimumFetchIntervalMillis = 1;
        remoteConfig.defaultConfig = {
            "eurotoinr": "INR"
          };
        const val = remoteConfig.getValue("eurotoinr");
        //remoteConfig.fetch()
        remoteConfig.fetchAndActivate()
        .then( () => {

            remoteConfig.activate()
            console.log(  val , remoteConfig , remoteConfig._storageCache.activeConfig , 'Firebase')
            // ...
        })
        .catch((err) => {
            console.log(err)
            // ...
        });  --}}

      </script>



{{--  <!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-analytics.js"></script>

    <script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCWaM6cWBXsKQqrwmUCMFfOcNbjRS6hAso",
    authDomain: "openbazar-366a2.firebaseapp.com",
    projectId: "openbazar-366a2",
    storageBucket: "openbazar-366a2.appspot.com",
    messagingSenderId: "546979912823",
    appId: "1:546979912823:web:59072c985542e93624fa72",
    measurementId: "G-T44KD0VMS5"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>  --}}

{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/mediaelement/4.2.16/mediaelement.min.js" integrity="sha512-+b9/1TlD066WAdb06Kad2E1EY+rX1RHQ54KCMa70c1YcCDJ/Wfr1MqqOddjBAwvZ/2igGiBg+YADB3ZHZllb9g==" crossorigin="anonymous"></script>  --}}



<!-- Insert these scripts at the bottom of the HTML, but before you usee any Firebase services -->

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to usee -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-remote-config.js"></script>

<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-database.js"></script>

{{--  <script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-remote-config.js"></script>  --}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


{{--  <script src="https://www.paypal.com/sdk/js?currency=EUR&client-id=Ad0zJFMDmM0aeXLaKB2qiRekjkmPABTpVNWWuVqgOv2MD2EqduxiDAg80bVx1Sq_6URw4YJvAKHooGI3&disable-funding=credit,card"></script>  --}}
{{--  <script src="https://www.paypalobjects.com/api/checkout.js"></script>  --}}
{{--  <script src="http://mediaelementjs.com/js/mejs-2.11.2/mediaelement-and-player.min.js"></script>  --}}

{{--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>   --}}


<script src="https://www.youtube.com/iframe_api"></script>

{{-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-6933751435930860" crossorigin="anonymous"></script> --}}


{{-- <script async src="https://pagead2.googlesyndication.com/pagead/adsbygoogle.js?client=ca-pub-6933751435930860" ></script> --}}

<script src="{{asset('js/app.js')}}"></script>

<script src="{{asset('js/window_count.js')}}"></script>


{{-- <script src="{{asset('js/vendor.js')}}"></script>
<script src="{{asset('js/manifest.js')}}"></script> --}}
{{--  <script>
    $(document).ready(function() {
        $('.audio-player').mediaelementplayer({
            alwaysShowControls: true,
            features: ['playpause','volume','progress'],
            audioVolume: 'horizontal',
            audioWidth: 400,
            audioHeight: 120
        });
    });

</script>  --}}



<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>



<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>

@if(session()->has('referred'))
<script>
    toastr.success('Please signup to claim your coins');
</script>
@endif

@php
    session()->forget('referred');
@endphp
<script>


    function countChar(val) {
        var len = val.value.length;
        if (len >= 120) {
            val.value = val.value.substring(0, 120);
        } else {
            $('#charNum').text( len + '/120');
        }
    };


    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })






    function openAuthModal(){
        $('#authModal').modal('show');
    }


    function openLoginModal(){

        $('#loginModal').modal('show');
        $('#registerModal').modal('hide');

        checkIfModalOpen();
    }


    function openRegisterModal(){

        $('#loginModal').modal('hide');
        $('#registerModal').modal('show');

        checkIfModalOpen();

    }


    function closeModal(){
        $('.modal').modal('hide');
    }

    function checkIfModalOpen(){
        $(document).on('hidden.bs.modal', function (event) {
            if ($('.modal:visible').length) {
              $('body').addClass('modal-open');
            }
        });
    }







      $(window).scroll(function(){
        $('svg.radial-progress').each(function( index, value ) {
          // If svg.radial-progress is approximately 25% vertically into the window when scrolling from the top or the bottom
          if (
              $(window).scrollTop() > $(this).offset().top - ($(window).height() * 0.75) &&
              $(window).scrollTop() < $(this).offset().top + $(this).height() - ($(window).height() * 0.25)
          ) {
              // Get percentage of progress
              percent = $(value).data('percentage');
              // Get radius of the svg's circle.complete
              radius = $(this).find($('circle.complete')).attr('r');
              // Get circumference (2πr)
              circumference = 2 * Math.PI * radius;
              // Get stroke-dashoffset value based on the percentage of the circumference
              strokeDashOffset = circumference - ((percent * circumference) / 100);
              // Transition progress for 1.25 seconds
              $(this).find($('circle.complete')).animate({'stroke-dashoffset': strokeDashOffset}, 1250);
          }
        });
      }).trigger('scroll');


    console.log(localStorage.getItem('WINDOW_VALIDATION') , 'MAIN')

</script>



<script src="{{asset('/assets/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('/assets/js/jquery.flexslider-min.js')}}"></script>



@yield('scripts')

</body>
</html>



